﻿namespace MManager.RegistrationModels
{
    public partial class RegistrationLogs
    {
        public long Id { get; set; }
        public string VoterId { get; set; }
        public string Date { get; set; }
        public string Reason { get; set; }
        public string PsCode { get; set; }
    }
}
