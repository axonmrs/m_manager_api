﻿namespace MManager.RegistrationModels
{
    public partial class Districts
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long RegionId { get; set; }
    }
}
