﻿namespace MManager.RegistrationModels
{
    public partial class RegisteredVoters
    {
        public long Id { get; set; }
        public string VoterId { get; set; }
        public string PsName { get; set; }
        public string PsCode { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string DateOfBirth { get; set; }
        public long? EstimatedAge { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string RegisteredBy { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public long ResidentialDistrictId { get; set; }
        public long IdTypeId { get; set; }
        public string IdNumber { get; set; }
        public string IdExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public long? HomeTownDistrictId { get; set; }
        public string IsVisuallyImpaired { get; set; }
        public string IsDisabled { get; set; }
        public byte[] Photo { get; set; }
        public byte[] FaceTemplate { get; set; }
        public byte[] LeftThumb { get; set; }
        public byte[] LeftIndex { get; set; }
        public byte[] LeftMiddle { get; set; }
        public byte[] LeftRing { get; set; }
        public byte[] LeftPinky { get; set; }
        public byte[] RightThumb { get; set; }
        public byte[] RightIndex { get; set; }
        public byte[] RightMiddle { get; set; }
        public byte[] RightRing { get; set; }
        public byte[] RightPinky { get; set; }
        public string CreatedAt { get; set; }
        public string BackedUp { get; set; }
        public string BackedUpOn { get; set; }
        public string Guarantor1VoterId { get; set; }
        public string Guarantor2VoterId { get; set; }
        public string Guarantor1Name { get; set; }
        public string Guarantor2Name { get; set; }
        public string IsEstimatedAge { get; set; }
        public long? CardPrintCount { get; set; }
        public string Transmitted { get; set; }
        public string TransmittedOn { get; set; }
        public byte[] CompositeTemplate { get; set; }
        public string HearingImpaired { get; set; }
        public string Leper { get; set; }
        public string MissingFingersL { get; set; }
        public string MissingFingersR { get; set; }
        public string AmputatedHandsL { get; set; }
        public string AmputatedHandsR { get; set; }
        public byte[] LeftThumbTpl { get; set; }
        public byte[] LeftIndexTpl { get; set; }
        public byte[] LeftMiddleTpl { get; set; }
        public byte[] LeftRingTpl { get; set; }
        public byte[] LeftPinkyTpl { get; set; }
        public byte[] RightThumbTpl { get; set; }
        public byte[] RightIndexTpl { get; set; }
        public byte[] RightMiddleTpl { get; set; }
        public byte[] RightRingTpl { get; set; }
        public byte[] RightPinkyTpl { get; set; }
        public string ChallengeReasons { get; set; }
        public byte[] PhotoJ2k { get; set; }
        public string IsUpdated { get; set; }
        public string UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
    }
}
