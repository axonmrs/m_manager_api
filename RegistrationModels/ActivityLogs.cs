﻿namespace MManager.RegistrationModels
{
    public partial class ActivityLogs
    {
        public long Id { get; set; }
        public string Date { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
    }
}
