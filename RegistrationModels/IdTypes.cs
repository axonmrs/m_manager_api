﻿namespace MManager.RegistrationModels
{
    public partial class IdTypes
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
