﻿namespace MManager.RegistrationModels
{
    public partial class AppSettings
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
