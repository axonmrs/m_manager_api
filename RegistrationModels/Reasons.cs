﻿namespace MManager.RegistrationModels
{
    public partial class Reasons
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
