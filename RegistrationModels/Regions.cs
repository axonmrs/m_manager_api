﻿namespace MManager.RegistrationModels
{
    public partial class Regions
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
