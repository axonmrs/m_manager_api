﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace MManager.RegistrationModels
{
    public partial class registration_dbContext : DbContext
    {
        public registration_dbContext()
        {
        }

        public registration_dbContext(DbContextOptions<registration_dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ActivityLogs> ActivityLogs { get; set; }
        public virtual DbSet<AppSettings> AppSettings { get; set; }
        public virtual DbSet<Backups> Backups { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<Districts> Districts { get; set; }
        public virtual DbSet<ElectoralAreas> ElectoralAreas { get; set; }
        public virtual DbSet<IdTypes> IdTypes { get; set; }
        public virtual DbSet<PollingStations> PollingStations { get; set; }
        public virtual DbSet<Reasons> Reasons { get; set; }
        public virtual DbSet<Regions> Regions { get; set; }
        public virtual DbSet<RegisteredVoters> RegisteredVoters { get; set; }
        public virtual DbSet<RegistrationLogs> RegistrationLogs { get; set; }
        public virtual DbSet<ReportLogs> ReportLogs { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.Options.Extensions.OfType<RelationalOptionsExtension>().Any(ext => !string.IsNullOrEmpty(ext.ConnectionString) || ext.Connection != null))
                optionsBuilder.UseSqlite("DataSource=./Documents/Templates/registration_db.db;");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ActivityLogs>(entity =>
            {
                entity.ToTable("activity_logs");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivityType)
                    .IsRequired()
                    .HasColumnName("activity_type");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by");

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasColumnName("date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<AppSettings>(entity =>
            {
                entity.ToTable("app_settings");

                entity.HasIndex(e => e.Name)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasColumnType("NVARCHAR");
            });

            modelBuilder.Entity<Backups>(entity =>
            {
                entity.ToTable("backups");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.BackedUpBy)
                    .IsRequired()
                    .HasColumnName("backed_up_by");

                entity.Property(e => e.BackupDate)
                    .IsRequired()
                    .HasColumnName("backup_date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.NumberOfRecords).HasColumnName("number_of_records");
            });

            modelBuilder.Entity<Devices>(entity =>
            {
                entity.ToTable("devices");

                entity.HasIndex(e => e.IdentificationCode)
                    .IsUnique();

                entity.HasIndex(e => e.Reference)
                    .IsUnique();

                entity.HasIndex(e => e.Sequence)
                    .IsUnique();

                entity.HasIndex(e => e.SerialNumber)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivationCode)
                    .IsRequired()
                    .HasColumnName("activation_code")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.ActivationCodeHash)
                    .IsRequired()
                    .HasColumnName("activation_code_hash")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.IdentificationCode)
                    .IsRequired()
                    .HasColumnName("identification_code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("region")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Sequence)
                    .IsRequired()
                    .HasColumnName("sequence")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.SerialNumber)
                    .IsRequired()
                    .HasColumnName("serial_number")
                    .HasColumnType("NVARCHAR2(128)");
            });

            modelBuilder.Entity<Districts>(entity =>
            {
                entity.ToTable("districts");

                entity.HasIndex(e => e.Name)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("LARGEINT");
            });

            modelBuilder.Entity<ElectoralAreas>(entity =>
            {
                entity.ToTable("electoral_areas");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Constituency)
                    .IsRequired()
                    .HasColumnName("constituency")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("region")
                    .HasColumnType("NVARCHAR(128)");
            });

            modelBuilder.Entity<IdTypes>(entity =>
            {
                entity.ToTable("id_types");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");
            });

            modelBuilder.Entity<PollingStations>(entity =>
            {
                entity.ToTable("polling_stations");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Constituency)
                    .IsRequired()
                    .HasColumnName("constituency")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.ElectoralArea)
                    .IsRequired()
                    .HasColumnName("electoral_area")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("region")
                    .HasColumnType("NVARCHAR(128)");
            });

            modelBuilder.Entity<Reasons>(entity =>
            {
                entity.ToTable("reasons");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.Name)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Regions>(entity =>
            {
                entity.ToTable("regions");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");
            });

            modelBuilder.Entity<RegisteredVoters>(entity =>
            {
                entity.ToTable("registered_voters");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.VoterId)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AmputatedHandsL)
                    .IsRequired()
                    .HasColumnName("amputatedHandsL")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.AmputatedHandsR)
                    .IsRequired()
                    .HasColumnName("amputatedHandsR")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.BackedUp)
                    .IsRequired()
                    .HasColumnName("backed_up")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.BackedUpOn)
                    .HasColumnName("backed_up_on")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.CardPrintCount)
                    .HasColumnName("card_print_count")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ChallengeReasons).HasColumnName("challengeReasons");

                entity.Property(e => e.CompositeTemplate).HasColumnName("compositeTemplate");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnName("created_at")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("DATE");

                entity.Property(e => e.EstimatedAge)
                    .HasColumnName("estimated_age")
                    .HasColumnType("INT")
                    .HasDefaultValueSql("18");

                entity.Property(e => e.FaceTemplate).HasColumnName("face_template");

                entity.Property(e => e.FatherName)
                    .HasColumnName("father_name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Guarantor1Name).HasColumnName("guarantor1_name");

                entity.Property(e => e.Guarantor1VoterId).HasColumnName("guarantor1_voter_id");

                entity.Property(e => e.Guarantor2Name).HasColumnName("guarantor2_name");

                entity.Property(e => e.Guarantor2VoterId).HasColumnName("guarantor2_voter_id");

                entity.Property(e => e.HearingImpaired)
                    .IsRequired()
                    .HasColumnName("hearingImpaired")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.HomeTown)
                    .HasColumnName("home_town")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.HomeTownAddress)
                    .HasColumnName("home_town_address")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.HomeTownDistrictId)
                    .HasColumnName("home_town_district_id")
                    .HasColumnType("LARGEINT");

                entity.Property(e => e.IdExpiry)
                    .HasColumnName("id_expiry")
                    .HasColumnType("DATE");

                entity.Property(e => e.IdNumber)
                    .IsRequired()
                    .HasColumnName("id_number")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.IdTypeId)
                    .HasColumnName("id_type_id")
                    .HasColumnType("LARGEINT");

                entity.Property(e => e.IsDisabled)
                    .HasColumnName("is_disabled")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.IsEstimatedAge)
                    .HasColumnName("is_estimated_age")
                    .HasColumnType("BOOL");

                entity.Property(e => e.IsUpdated)
                    .HasColumnName("is_updated")
                    .HasColumnType("BOOL");

                entity.Property(e => e.IsVisuallyImpaired)
                    .HasColumnName("is_visually_impaired")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.LeftIndex).HasColumnName("left_index");

                entity.Property(e => e.LeftIndexTpl).HasColumnName("left_index_tpl");

                entity.Property(e => e.LeftMiddle).HasColumnName("left_middle");

                entity.Property(e => e.LeftMiddleTpl).HasColumnName("left_middle_tpl");

                entity.Property(e => e.LeftPinky).HasColumnName("left_pinky");

                entity.Property(e => e.LeftPinkyTpl).HasColumnName("left_pinky_tpl");

                entity.Property(e => e.LeftRing).HasColumnName("left_ring");

                entity.Property(e => e.LeftRingTpl).HasColumnName("left_ring_tpl");

                entity.Property(e => e.LeftThumb).HasColumnName("left_thumb");

                entity.Property(e => e.LeftThumbTpl).HasColumnName("left_thumb_tpl");

                entity.Property(e => e.Leper)
                    .IsRequired()
                    .HasColumnName("leper")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.MissingFingersL)
                    .IsRequired()
                    .HasColumnName("missingFingersL")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.MissingFingersR)
                    .IsRequired()
                    .HasColumnName("missingFingersR")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.MotherName)
                    .HasColumnName("mother_name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.OtherNames)
                    .HasColumnName("other_names")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Photo).HasColumnName("photo");

                entity.Property(e => e.PhotoJ2k).HasColumnName("photo_j2k");

                entity.Property(e => e.PsCode)
                    .IsRequired()
                    .HasColumnName("ps_code")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.PsName)
                    .IsRequired()
                    .HasColumnName("ps_name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.RegisteredBy)
                    .IsRequired()
                    .HasColumnName("registered_by")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.ResidentialAddress)
                    .HasColumnName("residential_address")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.ResidentialDistrictId)
                    .HasColumnName("residential_district_id")
                    .HasColumnType("LARGEINT");

                entity.Property(e => e.ResidentialTown)
                    .HasColumnName("residential_town")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.RightIndex).HasColumnName("right_index");

                entity.Property(e => e.RightIndexTpl).HasColumnName("right_index_tpl");

                entity.Property(e => e.RightMiddle).HasColumnName("right_middle");

                entity.Property(e => e.RightMiddleTpl).HasColumnName("right_middle_tpl");

                entity.Property(e => e.RightPinky).HasColumnName("right_pinky");

                entity.Property(e => e.RightPinkyTpl).HasColumnName("right_pinky_tpl");

                entity.Property(e => e.RightRing).HasColumnName("right_ring");

                entity.Property(e => e.RightRingTpl).HasColumnName("right_ring_tpl");

                entity.Property(e => e.RightThumb).HasColumnName("right_thumb");

                entity.Property(e => e.RightThumbTpl).HasColumnName("right_thumb_tpl");

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasColumnName("surname")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Transmitted)
                    .IsRequired()
                    .HasColumnName("transmitted")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.TransmittedOn)
                    .HasColumnName("transmitted_on")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("DateTime");

                entity.Property(e => e.UpdatedBy).HasColumnName("updated_by");

                entity.Property(e => e.VoterId)
                    .IsRequired()
                    .HasColumnName("voter_id")
                    .HasColumnType("NVARCHAR(64)");
            });

            modelBuilder.Entity<RegistrationLogs>(entity =>
            {
                entity.ToTable("registration_logs");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasColumnName("date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.PsCode)
                    .IsRequired()
                    .HasColumnName("ps_code");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason");

                entity.Property(e => e.VoterId)
                    .IsRequired()
                    .HasColumnName("voter_id");
            });

            modelBuilder.Entity<ReportLogs>(entity =>
            {
                entity.ToTable("report_logs");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasColumnName("date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasColumnName("generated_by");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CompositeTemplate).HasColumnName("compositeTemplate");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("is_active")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR(256)");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnName("password_hash")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Picture).HasColumnName("picture");

                entity.Property(e => e.Selected)
                    .IsRequired()
                    .HasColumnName("selected")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("NVARCHAR(64)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("NVARCHAR(64)");
            });
        }
    }
}
