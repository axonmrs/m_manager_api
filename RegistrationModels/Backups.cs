﻿namespace MManager.RegistrationModels
{
    public partial class Backups
    {
        public long Id { get; set; }
        public string BackupDate { get; set; }
        public string BackedUpBy { get; set; }
        public long NumberOfRecords { get; set; }
    }
}
