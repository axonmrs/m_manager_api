﻿namespace MManager.RegistrationModels
{
    public partial class ReportLogs
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string GeneratedBy { get; set; }
    }
}
