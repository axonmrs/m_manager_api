﻿namespace MManager.RegistrationModels
{
    public partial class Users
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Type { get; set; }
        public string PasswordHash { get; set; }
        public long Id { get; set; }
        public string IsActive { get; set; }
        public string Selected { get; set; }
        public byte[] CompositeTemplate { get; set; }
        public byte[] Picture { get; set; }
        public string DateCreated { get; set; }
    }
}
