﻿namespace MManager
{
    public static class SystemConfig
    {
        public static Setting Setting { get; set; }
    }

    public class Setting
    {
        public string MrsLogsConnectionString { get; set; }
        public bool GenerateProvisionalList { get; set; }
        public bool GenerateFinalList { get; set; }
    }
}
