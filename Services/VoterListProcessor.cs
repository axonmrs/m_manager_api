﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.RegistrationModels;
using MManager.Repositories;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MManager.Services
{
    public class VoterListProcessor
    {
        public readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _context;
        private Logger logger = LogManager.GetCurrentClassLogger();
        public VoterListProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }
        public async Task GenerateProvisionalList()
        {
            if (!SystemConfig.Setting.GenerateProvisionalList) return;
            try
            {
                await new ReportsRepository(_context).GenerateProvisionalVoterList().ConfigureAwait(false);
            }
            catch (Exception) { }
        }

        public async Task GenerateFinalList()
        {
            if (!SystemConfig.Setting.GenerateFinalList) return;
            try
            {


            }
            catch (Exception) { }
        }

        public async Task CompleteDuplicates()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var dups = logsContext.DuplicateVoters.Where(x => x.Status == DuplicateVoterRecordStatus.Resolved)
                    .Take(500).ToList();
                foreach (var dup in dups)
                {
                    var tempReg = logsContext.TemporalVoterRegister
                        .FirstOrDefault(x => x.Id == dup.TemporalVoterRegisterId);
                    if (tempReg == null) continue;
                    
                    if (dup.Decision.ToLower().Contains("keep last"))
                    {
                        //Do nothing
                    }
                    else if (dup.Decision.ToLower().Contains("remove all"))
                    {
                        //sync the main and add a dup reference
                        var mainDupId = SaveTempInMain(dup.TemporalVoterRegister, context, logsContext);
                        if (mainDupId != 0)
                        {
                            var hits = logsContext.DuplicateVoterHits.Where(x => x.DuplicateVoterId == dup.Id).ToList();
                            foreach (var hit in hits)
                            {
                                var eligibleVoter = context.VoterRegister
                                    .FirstOrDefault(x => x.VoterId == hit.HitVoterId);
                                if (eligibleVoter != null)
                                {
                                    eligibleVoter.HasDuplicate = true;
                                    context.DuplicateVoterReferences.Add(new DuplicateVoterReference
                                    {
                                        Status = "Active",
                                        DuplicateVoterRegisterId = mainDupId,
                                        HitVoterRegisterId = eligibleVoter.Id
                                    });
                                }                                
                            }
                        }
                    }
                    else if (dup.Decision.ToLower().Contains("keep all") || dup.Decision.ToLower().Contains("keep both"))
                    {
                        logsContext.EligibleVoters.Add(new EligibleVoter
                        {
                            VoterId = dup.VoterId,
                            VoterReference = dup.VoterReference,
                            Status = EligibleVoterRecordStatus.Pending
                        });
                    }
                    tempReg.SyncedStatus = "Synced";
                    tempReg.Status = TemporalVoterRegisterStatus.DuplicateResolved;
                    dup.Status = DuplicateVoterRecordStatus.Completed;
                    logsContext.SaveChanges();
                    context.SaveChanges();
                }
            }
            catch (Exception e) 
            {
                var msg = e.Message;
            }
        }

        public int SaveTempInMain(TemporalVoterRegister temp, ApplicationDbContext mrsContext, mrs_logsContext logsContext)
        {
            logger.Info($"SaveTempInMain Start Run at - {DateTime.UtcNow:MM/dd/yyyy HH:mm:ss}\n\n");

            var existingVoter = mrsContext.VoterRegister
                                .FirstOrDefault(x => x.VoterId == temp.VoterId);
            if (existingVoter != null)
            {
                logger.Info($"EXISTING VOTERID - {temp.VoterId}. Please investigate");
                return 0;
            }

            logger.Info($"before bio");
            var bios = logsContext.TemporalVoterRegisterBio
                    .Where(x => x.TemporalVoterRegisterId == temp.Id)
                    .Select(x => new { x.Id, x.TemporalVoterRegisterId, x.Photo, x.CompositeTemplate })
                    .ToList();
            var biosx = logsContext.TemporalVoterRegisterBioNew
            .Where(x => x.TemporalVoterRegisterId == temp.Id)
            .Select(x => new { x.Id, x.TemporalVoterRegisterId, x.Photo, x.CompositeTemplate })
            .ToList();
            bios.AddRange(biosx);
            if (!bios.Any())
            {
                logger.Info($"NO BIO IN TEMP VOTERID - {temp.VoterId}. Please investigate");
                return 0;
            }

            logger.Info($"after bio - Bio Count: {bios.Count}");
            var station = mrsContext.Stations
                .FirstOrDefault(x => x.Code == temp.PollingStationCode);
            int? idTypeId = 0;
            if (int.TryParse(temp.IdentificationTypeId, out var idType))
            {
                idTypeId = idType > 0 ? mrsContext.IdTypes.FirstOrDefault(x => x.Id == idType)?.Id : null;
            }
            var resDistrict = new District();
            if (temp.ResidentialDistrictId != null)
            {
                resDistrict = mrsContext.Districts
                .FirstOrDefault(x => x.Id == int.Parse(temp.ResidentialDistrictId));
            }
            else
            {
                resDistrict = mrsContext.Districts
                .FirstOrDefault();
            }
            var homeDistrict = new District();
            if (temp.IdentificationTypeId != null)
            {
                homeDistrict = mrsContext.Districts
                .FirstOrDefault(x => x.Id == int.Parse(temp.HomeTownDistrictId));
            }
            else
            {
                homeDistrict = mrsContext.Districts
                .FirstOrDefault();
            }

            var bio = bios.FirstOrDefault(x => x.TemporalVoterRegisterId == temp.Id);
            if (bio == null)
            {
                logger.Info($"NO BIO IN TEMP VOTERID - {temp.VoterId}. Please investigate");
                return 0;
            }

            var voter = new VoterRegister
            {
                IdentificationTypeCode = temp.IdentificationTypeCode,
                ResidentialDistrictCode = temp.ResidentialDistrictCode,
                CreatedAt = temp.CreatedAt,
                SyncedAt = DateTime.UtcNow,
                Leper = temp.Leper,
                AmputatedHandsRight = temp.AmputatedHandsRight,
                HearingImpaired = temp.HearingImpaired,
                Reference = temp.Reference,
                VoterId = temp.VoterId,
                RegisteredBy = temp.RegisteredBy,
                PollingStationName = temp.PollingStationName,
                PollingStationCode = temp.PollingStationCode,
                PollingStationId = station.Id,
                Surname = temp.Surname,
                OtherNames = temp.OtherNames,
                PhoneNumber = temp.PhoneNumber,
                DateOfBirth = temp.DateOfBirth,
                EstimatedAge = temp.EstimatedAge,
                Sex = temp.Sex == MrsLogsModels.Sex.Male ? Models.Sex.Male : Models.Sex.Female,
                ResidentialAddress = temp.ResidentialAddress,
                ResidentialTown = temp.ResidentialTown,
                ResidentialDistrictId = resDistrict.Id,
                IdentificationTypeId = idTypeId > 0 ? idTypeId : null,
                IdentificationNumber = temp.IdentificationNumber,
                IdentificationExpiry = temp.IdentificationExpiry,
                FatherName = temp.FatherName,
                MotherName = temp.MotherName,
                HomeTownAddress = temp.HomeTownAddress,
                HomeTown = temp.HomeTown,
                HomeTownDistrictId = homeDistrict.Id,
                IsVisuallyImpaired = temp.IsVisuallyImpaired,
                IsDisabled = temp.IsDisabled,
                HomeTownDistrictCode = temp.HomeTownDistrictCode,
                AmputatedHandsLeft = temp.AmputatedHandsLeft,
                GuarantorOneVoterId = temp.GuarantorOneVoterId,
                GuarantorTwoVoterId = temp.GuarantorTwoVoterId,
                GuarantorOneName = temp.GuarantorOneName,
                GuarantorTwoName = temp.GuarantorTwoName,
                IsAgeEstimated = temp.IsAgeEstimated,
                CardPrintCount = temp.CardPrintCount,
                MissingFingersRight = temp.MissingFingersRight,
                MissingFingersLeft = temp.MissingFingersLeft,
                Challenges = temp.Challenges,
                HasDuplicate = true,
                ReceivedAt = temp.ReceivedAt,
                Reviewed = false,
                Source = temp.Source
            };
            mrsContext.VoterRegister.Add(voter);
            var voterBio = new VoterRegisterTemplates
            {
                VoterRegister = voter,
                Photo = bio.Photo,
                CompositeTemplate = bio.CompositeTemplate
            };
            mrsContext.VoterRegisterTemplates.Add(voterBio);
            mrsContext.SaveChanges();
            logger.Info($"SaveTempInMain End");
            return voter.Id;
        }
    }
}
