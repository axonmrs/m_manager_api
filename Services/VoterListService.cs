﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace MManager.Services
{
    [DisallowConcurrentExecution]
    public class VoterListService : IJob
    {
        private readonly VoterListProcessor _voterListProcessor;
        public VoterListService(IServiceProvider serviceProvider)
        {
            _voterListProcessor = new VoterListProcessor(serviceProvider);
        }
        public async Task Execute(IJobExecutionContext context)
        {
            Parallel.Invoke(
                () => _voterListProcessor.CompleteDuplicates().ConfigureAwait(false));
                //() => _voterListProcessor.GenerateProvisionalList().ConfigureAwait(false),
                //() => _voterListProcessor.GenerateFinalList().ConfigureAwait(false));
        }
    }
}
