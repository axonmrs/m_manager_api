﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace MManager.Services
{
    [DisallowConcurrentExecution]
    public class StatisticsProcessService : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        public StatisticsProcessService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            await new StatisticsProcessor(_serviceProvider).Compute();
        }
    }
}
