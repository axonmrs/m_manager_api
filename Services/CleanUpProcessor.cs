﻿using Castle.Components.DictionaryAdapter;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.RegistrationModels;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.Services
{
    public class CleanUpProcessor
    {
        public readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _context;
        public CleanUpProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }
        public async Task ClearTempFolder()
        {
            try
            {
                var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
                if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);                
                tempFolder = tempFolder.Replace("wwwroot\\wwwroot", "wwwroot");
                if (!Directory.Exists(tempFolder)) return;
                string[] files = Directory.GetFiles(tempFolder);
                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastAccessTime < DateTime.Now.AddHours(-24))
                        fi.Delete();
                }

            }
            catch (Exception) { }
        }

        public async Task DataSetUp()
        {
            try
            {
                var date = DateTime.Now;
                var user = "System";
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "final_setup.csv");
                var logsRepo = new LogsRepository(_context);

                var data = new List<SetUpFileRow>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');
                        if (cols.Length != 10)
                            user = user;

                        data.Add(new SetUpFileRow
                        {
                            RegName = cols[0].Trim().ToUpper(),
                            RegCode = cols[1].Trim().ToUpper(),
                            DistName = cols[2].Trim().ToUpper(),
                            DistCode = cols[3].Trim().ToUpper(),
                            ConstName = cols[4].Trim().ToUpper(),
                            ConstCode = cols[5].Trim().ToUpper(),
                            EAName = cols[6].Trim().ToUpper(),
                            EACode = cols[7].Trim().ToUpper(),
                            PSName = cols[8].Trim().ToUpper(),
                            PSCode = cols[9].Trim().ToUpper()
                        });
                    }
                }
                var regions = data.Select(x => new { x.RegName, x.RegCode }).OrderBy(x => x.RegCode).Distinct().ToList();
                var districts = data.Select(x => new { x.DistName, x.DistCode, x.RegCode }).OrderBy(x => x.DistCode).Distinct().ToList();
                var consts = data.Select(x => new { x.ConstName, x.ConstCode, x.DistCode }).OrderBy(x => x.ConstCode).Distinct().ToList();
                var eas = data.Select(x => new { x.EAName, x.EACode, x.ConstCode }).OrderBy(x => x.EACode).Distinct().ToList();
                var stations = data.Select(x => new { x.PSName, x.PSCode, x.EACode }).OrderBy(x => x.PSCode).Distinct().ToList();

                logsRepo.AddLog(_context,SystemLogType.DataSetup, "System", "SETUP", 
                    $"Data setup for {data.Count()} records.", 
                    JsonConvert.SerializeObject(data.Count()));

                //regions
                var lastReg = _context.Regions.Where(x => x.Id > 0).OrderByDescending(x => x.Id).FirstOrDefault();
                var lastRegId = lastReg != null ? lastReg.Id : 1;
                foreach (var r in regions)
                {
                    var ext = _context.Regions.FirstOrDefault(x => x.Code.ToLower() == r.RegCode.ToLower());
                    if (ext == null)
                    {
                        lastRegId++;
                        _context.Regions.Add(new Region
                        {
                            Id = lastRegId,
                            Code = r.RegCode,
                            Name = r.RegName,
                            CreatedAt = date,
                            ModifiedAt = date,
                            CreatedBy = user,
                            ModifiedBy = user,
                            OrderIndex = 1,
                            Notes = r.RegName
                        });
                    }
                    else
                    {
                        ext.Name = r.RegName;
                        ext.Notes = r.RegName;
                    }
                }
                _context.SaveChanges();
                logsRepo.AddLog(_context, SystemLogType.DataSetup, "System", "SETUP",
                    $"Successful Data setup for {regions.Count()} regions.",
                    JsonConvert.SerializeObject(regions.Count()));

                //districts
                var lastDist = _context.Districts.Where(x => x.Id > 0).OrderByDescending(x => x.Id).FirstOrDefault();
                var lastDistId = lastDist != null ? lastDist.Id : 1;
                foreach (var r in districts)
                {
                    var reg = _context.Regions.First(x => x.Code.ToLower() == r.RegCode.ToLower());
                    var ext = _context.Districts.FirstOrDefault(x => x.Code.ToLower() == r.DistCode.ToLower());
                    if (ext == null)
                    {
                        lastDistId++;
                        _context.Districts.Add(new District
                        {
                            Id = lastDistId,
                            Code = r.DistCode,
                            Name = r.DistName,
                            CreatedAt = date,
                            ModifiedAt = date,
                            CreatedBy = user,
                            ModifiedBy = user,
                            OrderIndex = 1,
                            Notes = r.DistName,
                            RegionId = reg.Id
                        });
                    }
                    else
                    {
                        ext.Name = r.DistName;
                        ext.Notes = r.DistName;
                        ext.RegionId = reg.Id;
                    }
                }
                _context.SaveChanges();
                logsRepo.AddLog(_context, SystemLogType.DataSetup, "System", "SETUP",
                    $"Successful Data setup for {districts.Count()} districts.",
                    JsonConvert.SerializeObject(districts.Count()));

                //constituencies
                var lastConst = _context.Constituencies.Where(x => x.Id > 0).OrderByDescending(x => x.Id).FirstOrDefault();
                var lastConstId = lastConst != null ? lastConst.Id : 1;
                foreach (var r in consts)
                {
                    var reg = _context.Districts.First(x => x.Code.ToLower() == r.DistCode.ToLower());
                    var ext = _context.Constituencies.FirstOrDefault(x => x.Code.ToLower() == r.ConstCode.ToLower());
                    if (ext == null)
                    {
                        lastConstId++;
                        _context.Constituencies.Add(new Constituency
                        {
                            Id = lastConstId,
                            Code = r.ConstCode,
                            Name = r.ConstName,
                            CreatedAt = date,
                            ModifiedAt = date,
                            CreatedBy = user,
                            ModifiedBy = user,
                            OrderIndex = 1,
                            Notes = r.ConstName,
                            DistrictId = reg.Id
                        });
                    }
                    else
                    {
                        ext.Name = r.ConstName;
                        ext.Notes = r.ConstName;
                        ext.DistrictId = reg.Id;
                    }
                }
                _context.SaveChanges();
                logsRepo.AddLog(_context, SystemLogType.DataSetup, "System", "SETUP",
                    $"Successful Data setup for {consts.Count()} constituencies.",
                    JsonConvert.SerializeObject(consts.Count()));

                //electoral areas
                var lastEa = _context.ElectoralAreas.Where(x => x.Id > 0).OrderByDescending(x => x.Id).FirstOrDefault();
                var lastEaId = lastEa != null ? lastEa.Id : 1;
                foreach (var r in eas)
                {
                    var reg = _context.Constituencies.First(x => x.Code.ToLower() == r.ConstCode.ToLower());
                    var ext = _context.ElectoralAreas.FirstOrDefault(x => x.Code.ToLower() == r.EACode.ToLower());
                    if (ext == null)
                    {
                        lastEaId++;
                        _context.ElectoralAreas.Add(new ElectoralArea
                        {
                            Id = lastEaId,
                            Code = r.EACode,
                            Name = r.EAName,
                            CreatedAt = date,
                            ModifiedAt = date,
                            CreatedBy = user,
                            ModifiedBy = user,
                            OrderIndex = 1,
                            Notes = r.EAName,
                            ConstituencyId = reg.Id
                        });
                    }
                    else
                    {
                        ext.Name = r.EAName;
                        ext.Notes = r.EAName;
                        ext.ConstituencyId = reg.Id;
                    }
                }
                _context.SaveChanges();
                logsRepo.AddLog(_context, SystemLogType.DataSetup, "System", "SETUP",
                    $"Successful Data setup for {eas.Count()} electoral areas.",
                    JsonConvert.SerializeObject(eas.Count()));

                //polling stations
                var lastPs = _context.Stations.Where(x => x.Id > 0).OrderByDescending(x => x.Id).FirstOrDefault();
                var lastPsId = lastPs != null ? lastPs.Id : 1;
                foreach (var r in stations)
                {
                    var reg = _context.ElectoralAreas.First(x => x.Code.ToLower() == r.EACode.ToLower());
                    var ext = _context.Stations.FirstOrDefault(x => x.Code.ToLower() == r.PSCode.ToLower());
                    if (ext == null)
                    {
                        lastPsId++;
                        _context.Stations.Add(new Station
                        {
                            Id = lastPsId,
                            Code = r.PSCode,
                            Name = r.PSName,
                            CreatedAt = date,
                            ModifiedAt = date,
                            CreatedBy = user,
                            ModifiedBy = user,
                            OrderIndex = 1,
                            Notes = r.PSName,
                            ElectoralAreaId = reg.Id
                        });
                    }
                    else
                    {
                        ext.Name = r.PSName;
                        ext.Notes = r.PSName;
                        ext.ElectoralAreaId = reg.Id;
                    }
                }
                _context.SaveChanges();
                logsRepo.AddLog(_context, SystemLogType.DataSetup, "System", "SETUP",
                    $"Successful Data setup for {stations.Count()} polling stations.",
                    JsonConvert.SerializeObject(stations.Count()));

            }
            catch (Exception ex) 
            {
                var msg = ex.Message;
            }
        }

        public async Task AllRegSetupData()
        {
            var dt = DateTime.UtcNow.ToFileTime();           

            var regions = _context.Regions.Where(x => x.Id > 0).ToList();
            var districts = _context.Districts.Where(x => x.Id > 0).ToList();
            var idTypes = _context.IdTypes.Where(x => x.Id > 0).ToList();
            var reasons = _context.ChallengeReasons.Where(x => x.Id > 0).ToList();

            var districtIds = districts.Select(x => x.Id).ToList();

            foreach (var dId in districtIds)
            {
                var district = districts.FirstOrDefault(x => x.Id == dId);
                if (district == null) continue;
                var region = regions.FirstOrDefault(x => x.Id == district.RegionId);
                if (region == null) continue;

                #region file location things and copy sqlite db into folder
                var regFolder = district.Code + "_" + district.Name.Replace(" ", "") + "_Reg_Setup_Data";
                var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder);
                var zipFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder + ".zip");
                var mainDbPath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "registration_db.db");

                if (System.IO.File.Exists(zipFolder)) continue;
                if (Directory.Exists(tempFolder)) Directory.Delete(tempFolder, true);
                Directory.CreateDirectory(tempFolder);
                var newDbFileName = "registration_db.rdb";
                var newDbPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, newDbFileName);
                System.IO.File.Copy(mainDbPath, newDbPath, true);
                #endregion

                //todo:check that the user has the right privilege
                var stations = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == district.Id).Include(x => x.ElectoralArea.Constituency.District.Region).ToList();
                if (!stations.Any()) continue;
                var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == district.Id && x.DistrictConfirmed).ToList();
                if (!devices.Any()) continue;
                var eas = _context.ElectoralAreas.Where(x => x.Constituency.DistrictId == district.Id).Include(x => x.Constituency.District.Region).ToList();
                if (!eas.Any()) continue;

                

                //update the sqlite db with values
                var optionsBuilder = new DbContextOptionsBuilder<registration_dbContext>();
                optionsBuilder.UseSqlite($"DataSource=./wwwroot/TempFiles/{regFolder}/{newDbFileName};");
                var regSqliteContext = new registration_dbContext(optionsBuilder.Options);

                //check and update stations
                var existingstations = regSqliteContext.PollingStations.Where(x => x.Id > 0).ToList();
                regSqliteContext.PollingStations.RemoveRange(existingstations);
                regSqliteContext.SaveChanges();
                foreach (var rec in stations)
                {
                    regSqliteContext.PollingStations.Add(new RegistrationModels.PollingStations
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        ElectoralArea = $"{rec.ElectoralArea?.Code}-{rec.ElectoralArea?.Name}",
                        Constituency = $"{rec.ElectoralArea?.Constituency?.Code}-{rec.ElectoralArea?.Constituency?.Name}",
                        District = $"{rec.ElectoralArea?.Constituency?.District?.Code}-{rec.ElectoralArea?.Constituency?.District?.Name}",
                        Region = $"{rec.ElectoralArea?.Constituency?.District?.Region?.Code}-{rec.ElectoralArea?.Constituency?.District?.Region?.Name}"
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Stations Saved.",
                        JsonConvert.SerializeObject(stations.Count));

                //check and update electoral areas
                var existingeas = regSqliteContext.ElectoralAreas.Where(x => x.Id > 0).ToList();
                regSqliteContext.ElectoralAreas.RemoveRange(existingeas);
                regSqliteContext.SaveChanges();
                foreach (var rec in eas)
                {
                    regSqliteContext.ElectoralAreas.Add(new RegistrationModels.ElectoralAreas
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        Constituency = rec.Constituency?.Code,
                        District = rec.Constituency?.District?.Code,
                        Region = rec.Constituency?.District?.Region?.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Electoral Areas Saved.",
                        JsonConvert.SerializeObject(eas.Count));

                //check and update devices
                var secHelp = new SecurityHelpers();
                var existingdevices = regSqliteContext.Devices.Where(x => x.Id > 0).ToList();
                regSqliteContext.Devices.RemoveRange(existingdevices);
                regSqliteContext.SaveChanges();
                foreach (var rec in devices)
                {
                    regSqliteContext.Devices.Add(new RegistrationModels.Devices
                    {
                        Id = rec.Id,
                        SerialNumber = rec.SerialNumber,
                        IdentificationCode = rec.IdentificationCode,
                        Sequence = rec.Sequence,
                        ActivationCode = rec.ActivationCode,
                        ActivationCodeHash = secHelp.HashString(rec.ActivationCode, "12345"),
                        District = "",
                        Region = "",
                        Reference = rec.Reference
                    });
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Devices Saved.",
                        JsonConvert.SerializeObject(devices.Count()));

                //check and update idtypes
                var existingIds = regSqliteContext.IdTypes.Where(x => x.Id > 0).ToList();
                regSqliteContext.IdTypes.RemoveRange(existingIds);
                regSqliteContext.SaveChanges();
                foreach (var rec in idTypes)
                {
                    regSqliteContext.IdTypes.Add(new IdTypes
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"ID Types Saved.",
                        JsonConvert.SerializeObject(idTypes.Count));

                //check and update regions
                var existingRegs = regSqliteContext.Regions.Where(x => x.Id > 0).ToList();
                regSqliteContext.Regions.RemoveRange(existingRegs);
                regSqliteContext.SaveChanges();
                foreach (var rec in regions)
                {
                    regSqliteContext.Regions.Add(new Regions
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Regions Saved.",
                        JsonConvert.SerializeObject(regions.Count));

                //check and update districts
                var existingDist = regSqliteContext.Districts.Where(x => x.Id > 0).ToList();
                regSqliteContext.Districts.RemoveRange(existingDist);
                foreach (var rec in districts)
                {
                    regSqliteContext.Districts.Add(new Districts
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        RegionId = rec.RegionId
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Districts Saved.",
                        JsonConvert.SerializeObject(districts.Count));

                //check and update reasons
                var existingReas = regSqliteContext.Reasons.Where(x => x.Id > 0).ToList();
                regSqliteContext.Reasons.RemoveRange(existingReas);
                foreach (var rec in reasons)
                {
                    regSqliteContext.Reasons.Add(new Reasons
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                //_context.SaveChanges();
                regSqliteContext.SaveChanges();
                new LogsRepository(_context).AddLog(_context, SystemLogType.Download, "System", "Registrations Setup Download",
                        $"Challenge Reasons Saved.",
                        JsonConvert.SerializeObject(reasons.Count));

                #region App Settings
                var appsettings = regSqliteContext.AppSettings.ToList();
                foreach (var set in appsettings)
                {
                    switch (set.Name)
                    {
                        case "district":
                            set.Value = district.Code;
                            break;
                        case "region":
                            set.Value = region.Code;
                            break;
                        case "no_of_stations":
                            set.Value = stations.Count().ToString();
                            break;
                        case "no_of_devices":
                            set.Value = devices.Count().ToString();
                            break;
                        case "date_generated":
                            set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            break;
                        case "copied":
                            set.Value = "false";
                            break;
                        case "backed_up":
                            set.Value = "false";
                            break;
                        case "last_backup_date":
                            set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            break;
                    }
                }
                regSqliteContext.SaveChanges();
                #endregion

                //secure the db file
                using (var stream = System.IO.File.Open(newDbPath, FileMode.Open))
                {
                    var bytes = new byte[125];
                    var cnt = stream.Read(bytes, 0, 125);
                    if (cnt == 125)
                    {
                        var bytesRev = bytes.Reverse().ToArray();
                        stream.Position = 0;
                        stream.Write(bytesRev, 0, 125);
                    }
                }

                //zip the folder and return it 
                ZipFile.CreateFromDirectory(tempFolder, zipFolder, CompressionLevel.Optimal, true);
                Directory.Delete(tempFolder, true);

                new LogsRepository(_context).AddLog(SystemLogType.Download, "System", "RegistrationData", "Download Registration Setup Data", JsonConvert.SerializeObject(zipFolder));
                
            }

        }

        public async Task CleanDuplicates()
        {
            try
            {
                using var logsContext = new mrs_logsContext();
                var duplicates = logsContext.DuplicateVoters.Where(x => x.Id > 0)
                    .Include(x => x.Hits).ToList();
                var ZeroHits = duplicates.Where(x => x.Hits.Count == 0).ToList();
                var batch = 500;
                var i = 0;
                while (true)
                {
                    var rcs = ZeroHits.Skip(i * batch).Take(batch).ToList();
                    foreach (var dup in rcs)
                    {
                        dup.Status = DuplicateVoterRecordStatus.Completed;
                        dup.ResolvedAt = DateTime.UtcNow;
                        dup.ResolvedBy = "System";
                    }
                    logsContext.SaveChanges();
                    i++;
                    if (rcs.Count < batch)
                        break;
                }
                logsContext.SaveChanges();
            }
            catch (Exception e) 
            { }
        }

        public async Task FixSex()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var batch = 50000;
                var i = 0;
                while (true)
                {
                    var correctMales = logsContext.TemporalVoterRegister
                    .Where(x => x.Sex == MrsLogsModels.Sex.Male)
                    .Select(x => new { x.VoterId, x.Reference }).Skip(i * batch).Take(batch).ToList();
                    var voterIds = correctMales.Select(x => x.VoterId).ToList();
                    var refs = correctMales.Select(x => x.Reference).ToList();

                    var wrongMales = context.VoterRegister
                        .Where(x => x.Sex == Models.Sex.Female && voterIds.Contains(x.VoterId) && refs.Contains(x.Reference))
                        .ToList();
                    if (wrongMales.Any())
                    {
                        wrongMales.ForEach(x => x.Sex = Models.Sex.Male);
                        context.SaveChanges();
                    }
                    i++;
                    if (correctMales.Count < batch)
                        break;
                }
            }
            catch (Exception e)
            { }
        }

        public async Task AddToExceptionListU18()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "under18.csv");

                var voterIds = new List<string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');

                        voterIds.Add(cols[0].Trim());
                    }
                }
                var data = context.VoterRegister
                        .Where(x => voterIds.Contains(x.VoterId))
                        .Select(x => new { x.Id, x.VoterId, x.Reference, x.PollingStationCode })
                        .ToList();
                var cnt = 1;
                var dt = DateTime.Now;
                foreach(var rec in data)
                {
                    var existing = logsContext.ExceptionList
                        .FirstOrDefault(x => x.VoterId == rec.VoterId && x.CaseNumber.Contains("U18"));
                    if (existing == null)
                    {
                        logsContext.ExceptionList.Add(new ExceptionList
                        {
                            IsActive = true,
                            Reason = "System Run:: Under 18 years Applicant",
                            PollingStationCode = rec.PollingStationCode,
                            VoterId = rec.VoterId,
                            VoterReference = rec.Reference,
                            CaseNumber = $"{dt.Year}-U18-{cnt:0000}"
                        });
                        cnt++;
                    }
                    
                }
                logsContext.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task AddToExceptionListGuarantors()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "guarantors.csv");

                var voterIds = new List<string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');

                        voterIds.Add(cols[0].Trim());
                    }
                }
                var data = context.VoterRegister
                        .Where(x => voterIds.Contains(x.VoterId))
                        .Select(x => new { x.Id, x.VoterId, x.Reference, x.PollingStationCode })
                        .ToList();
                var cnt = 1;
                var dt = DateTime.Now;
                foreach (var rec in data)
                {
                    var existing = logsContext.ExceptionList
                        .FirstOrDefault(x => x.VoterId == rec.VoterId && x.CaseNumber.Contains("DefGua"));
                    if (existing == null)
                    {
                        logsContext.ExceptionList.Add(new ExceptionList
                        {
                            IsActive = true,
                            Reason = "System Run:: Defaulting Guarantors",
                            PollingStationCode = rec.PollingStationCode,
                            VoterId = rec.VoterId,
                            VoterReference = rec.Reference,
                            CaseNumber = $"{dt.Year}-DefGua-{cnt:0000}"
                        });
                        cnt++;
                    }

                }
                logsContext.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task RemoveExceptions()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var data = logsContext.ExceptionList
                        .Where(x => x.IsActive)
                        .Take(100)
                        .ToList();
                var voterIds = data.Select(x => x.VoterId).ToList();
                var voters = context.VoterRegister
                        .Where(x => voterIds.Contains(x.VoterId))
                        .ToList();
                var ids = voters.Select(x => x.Id).ToList();
                var bios = context.VoterRegisterTemplates
                        .Where(x => ids.Contains(x.VoterRegisterId))
                        .ToList();

                foreach (var eligibleVoter in voters)
                {
                    var exception = data.First(x => x.VoterId == eligibleVoter.VoterId);
                    var voterbio = bios.FirstOrDefault(x => x.VoterRegisterId == eligibleVoter.Id);
                    if (voterbio != null)
                    {
                        var excVoter = new ExceptionListApplicant
                        {
                            //Id = eligibleVoter.Id,
                            IdentificationTypeCode = eligibleVoter.IdentificationTypeCode,
                            ResidentialDistrictCode = eligibleVoter.ResidentialDistrictCode,
                            CreatedAt = eligibleVoter.CreatedAt,
                            SyncedAt = DateTime.UtcNow,
                            Leper = eligibleVoter.Leper,
                            AmputatedHandsRight = eligibleVoter.AmputatedHandsRight,
                            HearingImpaired = eligibleVoter.HearingImpaired,
                            Reference = eligibleVoter.Reference,
                            VoterId = eligibleVoter.VoterId,
                            RegisteredBy = eligibleVoter.RegisteredBy,
                            PollingStationName = eligibleVoter.PollingStationName,
                            PollingStationCode = eligibleVoter.PollingStationCode,
                            PollingStationId = eligibleVoter.PollingStationId,
                            Surname = eligibleVoter.Surname,
                            OtherNames = eligibleVoter.OtherNames,
                            PhoneNumber = eligibleVoter.PhoneNumber,
                            DateOfBirth = eligibleVoter.DateOfBirth,
                            EstimatedAge = eligibleVoter.EstimatedAge,
                            Sex = eligibleVoter.Sex,
                            ResidentialAddress = eligibleVoter.ResidentialAddress,
                            ResidentialTown = eligibleVoter.ResidentialTown,
                            ResidentialDistrictId = eligibleVoter.ResidentialDistrictId,
                            IdentificationTypeId = eligibleVoter.IdentificationTypeId,
                            IdentificationNumber = eligibleVoter.IdentificationNumber,
                            IdentificationExpiry = eligibleVoter.IdentificationExpiry,
                            FatherName = eligibleVoter.FatherName,
                            MotherName = eligibleVoter.MotherName,
                            HomeTownAddress = eligibleVoter.HomeTownAddress,
                            HomeTown = eligibleVoter.HomeTown,
                            HomeTownDistrictId = eligibleVoter.HomeTownDistrictId,
                            IsVisuallyImpaired = eligibleVoter.IsVisuallyImpaired,
                            IsDisabled = eligibleVoter.IsDisabled,
                            HomeTownDistrictCode = eligibleVoter.HomeTownDistrictCode,
                            AmputatedHandsLeft = eligibleVoter.AmputatedHandsLeft,
                            GuarantorOneVoterId = eligibleVoter.GuarantorOneVoterId,
                            GuarantorTwoVoterId = eligibleVoter.GuarantorTwoVoterId,
                            GuarantorOneName = eligibleVoter.GuarantorOneName,
                            GuarantorTwoName = eligibleVoter.GuarantorTwoName,
                            IsAgeEstimated = eligibleVoter.IsAgeEstimated,
                            CardPrintCount = eligibleVoter.CardPrintCount,
                            MissingFingersRight = eligibleVoter.MissingFingersRight,
                            MissingFingersLeft = eligibleVoter.MissingFingersLeft,
                            Challenges = eligibleVoter.Challenges,
                            HasDuplicate = true,
                            ReceivedAt = eligibleVoter.ReceivedAt,
                            Reviewed = eligibleVoter.Reviewed,
                            Source = eligibleVoter.Source
                        };
                        logsContext.ExceptionListApplicants.Add(excVoter);
                        var multiRegBio = new ExceptionListApplicantBio
                        {
                            ExceptionListApplicant = excVoter,
                            Photo = voterbio.Photo,
                            CompositeTemplate = voterbio.CompositeTemplate
                        };
                        logsContext.ExceptionListApplicantsBios.Add(multiRegBio);
                        context.VoterRegister.Remove(eligibleVoter);
                        context.VoterRegisterTemplates.Remove(voterbio);
                        exception.IsActive = false;
                    }
                }
                logsContext.SaveChanges();
                context.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task AddToExceptionListTogolese()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "Togoloese.csv");

                var voterIds = new List<string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');

                        voterIds.Add(cols[0].Trim());
                    }
                }
                var data = context.VoterRegister
                        .Where(x => voterIds.Contains(x.VoterId))
                        .Select(x => new { x.Id, x.VoterId, x.Reference, x.PollingStationCode })
                        .ToList();
                var cnt = 1;
                var dt = DateTime.Now;
                foreach (var rec in data)
                {
                    var existing = logsContext.ExceptionList
                        .FirstOrDefault(x => x.VoterId == rec.VoterId && x.CaseNumber.Contains("U18"));
                    if (existing == null)
                    {
                        logsContext.ExceptionList.Add(new ExceptionList
                        {
                            IsActive = true,
                            Reason = "System Run:: Suspected Togolese",
                            PollingStationCode = rec.PollingStationCode,
                            VoterId = rec.VoterId,
                            VoterReference = rec.Reference,
                            CaseNumber = $"{dt.Year}-TOGO-{cnt:0000}"
                        });
                        cnt++;
                    }

                }
                logsContext.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task AddChallenges()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "north_not_on_list.csv");

                var list = new List<NewChallengeList>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');
                        if (string.IsNullOrEmpty(cols[0])) continue;
                        list.Add(new NewChallengeList
                        {
                            VoterId = cols[0].Trim(),
                            Reason = cols[1].Trim(),
                            Verdict = cols[2].Trim()
                        });
                    }
                }
                var voterIds = list.Select(x => x.VoterId).ToList();
                var temps = logsContext.TemporalVoterRegister.Where(x => voterIds.Contains(x.VoterId)).ToList();
                var voters = _context.VoterRegister.Where(x => voterIds.Contains(x.VoterId)).ToList();
                var dt = DateTime.Now;
                var cnt = 1;
                foreach(var t in temps)
                {
                    try
                    {
                        
                        var caseNumber = $"{dt.Year}-{t.PollingStationCode}-LT-{cnt:0000}";
                        var chll = list.First(x => x.VoterId == t.VoterId);
                        var newChallenge = new ChallengedVoter
                        {
                            Challenges = chll.Reason,
                            PollingStationCode = t.PollingStationCode,
                            Status = ChallengedVoterStatus.Resolved,
                            VoterId = t.VoterId,
                            VoterReference = t.Reference,
                            CaseNumber = caseNumber,
                            Decision = chll.Verdict,
                            ResolvedAt = DateTime.UtcNow,
                            Officials = "Authorization from EC",
                            ResolvedBy = "System"
                        };
                        logsContext.ChallengedVoters.Add(newChallenge);
                        if (newChallenge.Decision.ToLower().Contains("remove"))
                        {
                            var newException = new ExceptionList
                            {
                                CaseNumber = newChallenge.CaseNumber,
                                PollingStationCode = t.PollingStationCode,
                                VoterId = newChallenge.VoterId,
                                VoterReference = newChallenge.VoterReference,
                                Reason = newChallenge.Decision
                            };
                            logsContext.ExceptionList.Add(newException);
                            var voter = voters.FirstOrDefault(x => x.VoterId == t.VoterId);
                            if(voter != null)
                            {
                                voter.HasDuplicate = true;
                            }
                        }

                        cnt++;
                    }
                    catch(Exception ex)
                    {
                        var msg = ex.Message;
                    }
                    
                }
                logsContext.SaveChanges();
                _context.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public class NewChallengeList
        {
            public string VoterId { get; set; }
            public string Reason { get; set; }
            public string Verdict { get; set; }
        }

        public async Task FixWrongChallenges()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "north_incorrect_decisions.csv");

                var list = new List<NewChallengeList>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');
                        if (string.IsNullOrEmpty(cols[0])) continue;
                        list.Add(new NewChallengeList
                        {
                            VoterId = cols[0].Trim(),
                            Verdict = cols[1].Trim()
                        });
                    }
                }
                var voterIds = list.Select(x => x.VoterId).ToList();
                var challenges = logsContext.ChallengedVoters.Where(x => voterIds.Contains(x.VoterId)).ToList();
                var voters = _context.VoterRegister.Where(x => voterIds.Contains(x.VoterId)).ToList();
                var exceptions = logsContext.ExceptionList.Where(x => voterIds.Contains(x.VoterId)).ToList();
                var dt = DateTime.Now;
                foreach (var t in challenges)
                {
                    try
                    { //Remove from voter list
                        //Keep on voter list
                        var verdict = list.FirstOrDefault(x => x.VoterId == t.VoterId);
                        if (verdict != null)
                        {
                            var challenge = challenges.FirstOrDefault(x => x.VoterId == t.VoterId);
                            if (challenge != null)
                            {
                                if(challenge.Decision == null)
                                {
                                    if (verdict.Verdict.ToLower().Contains("keep"))
                                    {
                                        var voter = voters.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (voter != null) voter.HasDuplicate = false;
                                        var excep = exceptions.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (excep != null) logsContext.ExceptionList.Remove(excep);
                                        challenge.Decision = "Keep on voter list";
                                        challenge.Status = ChallengedVoterStatus.Resolved;
                                        challenge.Confirmed = true;


                                    }
                                    else
                                    {
                                        var voter = voters.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (voter != null) voter.HasDuplicate = true;
                                        challenge.Decision = "Remove from voter list";
                                        var newException = new ExceptionList
                                        {
                                            CaseNumber = $"{dt.Year}-{t.PollingStationCode}-{t.VoterId}",
                                            PollingStationCode = t.PollingStationCode,
                                            VoterId = t.VoterId,
                                            VoterReference = t.VoterReference,
                                            Reason = challenge.Decision
                                        };
                                        logsContext.ExceptionList.Add(newException);
                                        challenge.Status = ChallengedVoterStatus.Resolved;
                                        challenge.Confirmed = true;
                                    }
                                }
                                else
                                {
                                    if (verdict.Verdict.ToLower().Contains("keep") && challenge.Decision.ToLower().Contains("remove"))
                                    {
                                        var voter = voters.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (voter != null) voter.HasDuplicate = false;
                                        var excep = exceptions.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (excep != null) logsContext.ExceptionList.Remove(excep);
                                        challenge.Decision = "Keep on voter list";

                                    }
                                    else if (verdict.Verdict.ToLower().Contains("remove") && challenge.Decision.ToLower().Contains("keep"))
                                    {
                                        var voter = voters.FirstOrDefault(x => x.VoterId == t.VoterId);
                                        if (voter != null) voter.HasDuplicate = true;
                                        challenge.Decision = "Remove from voter list";
                                        var newException = new ExceptionList
                                        {
                                            CaseNumber = $"{dt.Year}-{t.PollingStationCode}-{t.VoterId}",
                                            PollingStationCode = t.PollingStationCode,
                                            VoterId = t.VoterId,
                                            VoterReference = t.VoterReference,
                                            Reason = challenge.Decision
                                        };
                                        logsContext.ExceptionList.Add(newException);
                                    }
                                }


                                
                            }
                        }                        
                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                    }

                }
                logsContext.SaveChanges();
                _context.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task FixEligible()
        {
            try
            {
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "vids.csv");

                var list = new List<string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');
                        if (string.IsNullOrEmpty(cols[0])) continue;
                        list.Add(cols[0].Trim());
                    }
                }

                var data = logsContext.EligibleVoters.Where(x => list.Contains(x.VoterId)).ToList();
                data = data.DistinctBy(x => x.VoterId).ToList();

                var exstIds = data.Select(x => x.VoterId).ToList();
                var notFound = list.Except(exstIds).ToList();

                data.ForEach(x => x.Status = EligibleVoterRecordStatus.Verified);
                var flname = Path.Combine(Directory.GetCurrentDirectory(), "notfound.txt");
                var recs = JsonConvert.SerializeObject(notFound);
                File.WriteAllText(flname, recs);

                logsContext.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task FixNotFound()
        {
            try
            {
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "notfound.txt");

                var data = File.ReadAllText(filePath);
                var list = JsonConvert.DeserializeObject(data);
                logsContext.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task FixSyncedDuplicatesxx()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();
                var duplicates = logsContext.DuplicateVoters
                    .Where(x => x.Decision.ToLower().Contains("remove") || x.Status == DuplicateVoterRecordStatus.Pending)
                    .Include(x => x.TemporalVoterRegister)
                    .ToList();
                var vids = duplicates.Select(x => x.VoterId).ToList();
                var refs = duplicates.Select(x => x.VoterReference).ToList();
                var voters = context.VoterRegister.Where(x => vids.Contains(x.VoterId) && refs.Contains(x.Reference))
                    .ToList();
                var ids = voters.Select(x => x.Id).ToList();
                var batch = 20;
                var i = 0;
                while (true)
                {
                    var vs = voters.Skip(i * batch).Take(batch).ToList();
                    context.VoterRegister.RemoveRange(vs);
                    context.SaveChanges();
                    i++;
                    if (vs.Count < batch)
                        break;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task AddToExceptionListBurkinabe()
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Burkinabe2.csv");

                var voterIds = new List<string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');

                        voterIds.Add(cols[0].Trim());
                    }
                }
                var data = context.VoterRegister
                        .Where(x => voterIds.Contains(x.VoterId))
                        .ToList();
                var cnt = 20;
                var dt = DateTime.Now;
                foreach (var rec in data)
                {
                    rec.HasDuplicate = true;
                    var existing = logsContext.ExceptionList
                        .FirstOrDefault(x => x.VoterId == rec.VoterId && x.CaseNumber.Contains("BURKINA"));
                    if (existing == null)
                    {
                        logsContext.ExceptionList.Add(new ExceptionList
                        {
                            IsActive = true,
                            Reason = "System Run:: Suspected Burkinabe",
                            PollingStationCode = rec.PollingStationCode,
                            VoterId = rec.VoterId,
                            VoterReference = rec.Reference,
                            CaseNumber = $"{dt.Year}-BURKINA-{cnt:0000}"
                        });
                        cnt++;
                    }

                }
                logsContext.SaveChanges();
                context.SaveChanges();
            }
            catch (Exception e)
            { }
        }

        public async Task CheckSequences()
        {
            try
            {
                using var logsContext = new mrs_logsContext();

                var dt = new DateTime(2020, 09, 29);
                var sequences = _context.RegistrationDevices
                    .Where(x => x.Sequence != null && x.AssignedToDistrictOn > dt && x.AssignedToDistrict)
                    .Select(x=> new { Sequence = int.Parse(x.Sequence), x.IdentificationCode, x.SerialNumber})
                    .ToList();
                var data = _context.VoterRegister
                        .Select(x => x.VoterId)
                        .ToList();
                //var datax = logsContext.TemporalVoterRegister
                //    .Where(x => !data.Contains(x.VoterId))
                //        .Select(x => x.VoterId)
                //        .ToList();
                //data.AddRange(datax);
                var lgSeq = sequences.OrderByDescending(s => s.Sequence).First().Sequence;
                var list = new Dictionary<string, string>();
                if(lgSeq%10 == 0)lgSeq = lgSeq + 5;
                foreach (var seq in sequences)
                {

                    var sq = seq.Sequence + 5;
                    var dataCount = data.Count(x => int.Parse(x.Substring(0, 5)) == sq);
                    if (dataCount == 0)
                    {
                        list.Add(seq.IdentificationCode, sq.ToString());
                        continue;
                    }
                    else
                    {
                        while (true)
                        {
                            var dataCountx = data.Count(x => int.Parse(x.Substring(0, 5)) == lgSeq);
                            if(dataCountx != 0)
                            {
                                lgSeq = lgSeq + 10;
                            }
                            else
                            {
                                list.Add(seq.IdentificationCode, lgSeq.ToString());
                                lgSeq = lgSeq + 10;
                                break;
                            }
                        }
                    }
                }
                string CSV = "DeviceIdentification,NewSequence\n";
                foreach (var l in list)
                {
                    CSV += $"{l.Key},{l.Value}\n";
                }
                File.WriteAllText("NewDevSeq.csv", CSV);
            }
            catch (Exception e)
            { }
        }

        public async Task UpdateNewSequences()
        {
            try
            {
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "NewDevSeq.csv");

                var devices = new Dictionary<string, string>();
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var cols = line.Split(',');

                        devices.Add(cols[0].Trim(), cols[1].Trim());
                    }
                }
                var Ids = devices.Select(x => x.Key).ToList();

                var data = _context.RegistrationDevices
                        .Where(x => Ids.Contains(x.IdentificationCode))
                        .ToList();
                foreach (var rec in devices)
                {
                    var dev = data.FirstOrDefault(x => x.IdentificationCode == rec.Key);
                    if (dev != null)
                    {
                        dev.Sequence = rec.Value;
                    }

                }
                _context.SaveChanges();
            }
            catch (Exception e)
            { }
        }
    }
}
