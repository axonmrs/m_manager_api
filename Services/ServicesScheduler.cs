﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace MManager.Services
{
    public class ServicesScheduler
    {
        public static async Task StartAsync(IServiceProvider serviceProvider)
        {
            try
            {
                // Grab the Scheduler instance from the Factory
                NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
                StdSchedulerFactory factory = new StdSchedulerFactory(props);
                IScheduler scheduler = await factory.GetScheduler();
                scheduler.JobFactory = new PortalJobFactory(serviceProvider);
                await scheduler.Start();

                var cleanUpService = JobBuilder.Create<CleanUpProcessService>()
                    .WithIdentity("net_core_starter_job_1", "net_core_starter_group_1")
                    .Build();
                var cleanUpTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_1", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(10)
                            //.WithIntervalInHours(24)
                            .RepeatForever())
                        .Build();
                await scheduler.ScheduleJob(cleanUpService, cleanUpTrigger);

                var statsService = JobBuilder.Create<StatisticsProcessService>()
                    .WithIdentity("net_core_starter_job_2", "net_core_starter_group_1")
                    .Build();
                var statsTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_2", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInMinutes(10)
                            .RepeatForever())
                        .Build();
                //await scheduler.ScheduleJob(statsService, statsTrigger);

                var voterListService = JobBuilder.Create<VoterListService>()
                    .WithIdentity("net_core_starter_job_3", "net_core_starter_group_1")
                    .Build();
                var voterListTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_3", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInMinutes(10)
                            .RepeatForever())
                        .Build();
                //await scheduler.ScheduleJob(voterListService, voterListTrigger);
            }
            catch (SchedulerException ex)
            {

            }
        }

    }
}
