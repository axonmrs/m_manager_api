﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Models;
using MManager.MrsLogsModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MManager.Services
{
    public class StatisticsProcessor
    {
        private readonly IServiceProvider _serviceProvider;
        public StatisticsProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task Compute()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            using var logsContext = new mrs_logsContext();
            try
            {
                var dashStats = new DashboardStatistics();
                var statsFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles","stats.json");



            }
            catch (Exception) { }
        }
    }

    public class DashboardStatistics
    {
        public Statistics Stats { get; set; } = new Statistics();
        public List<Duplicate> Duplicates { get; set; } = new List<Duplicate>();
        public SexStat Sex { get; set; } = new SexStat();
        public DistrictStat DistrictStat { get; set; } = new DistrictStat();
        public List<DistrictStatVm> RegionStats { get; set; } = new List<DistrictStatVm>();
        public List<AgeGroupStat> AgeGroupStats { get; set; } = new List<AgeGroupStat>();
    }

    public class Statistics
    {
        public int Processed { get; set; } = 0;
        public int Registrations { get; set; } = 0;
        public int Duplicates { get; set; } = 0;
        public int WithIssues { get; set; } = 0;
        public int Pending { get; set; } = 0;
        public int Adjudications { get; set; } = 0;
    }

    public class SexStat
    {
        public int Male { get; set; } = 0;
        public int Female { get; set; } = 0;
    }

    public class DistrictStat
    {
        public List<string> List { get; set; } = new List<string>();
        public List<DistrictStatVm> Data { get; set; } = new List<DistrictStatVm>();
    }
}
