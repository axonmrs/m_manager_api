﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace MManager.Services
{
    [DisallowConcurrentExecution]
    public class CleanUpProcessService : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly CleanUpProcessor _cleanUpProcessor;
        public CleanUpProcessService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _cleanUpProcessor = new CleanUpProcessor(_serviceProvider);
        }
        public async Task Execute(IJobExecutionContext context)
        {
            //Parallel.Invoke(
            ////    //() => _cleanUpProcessor.AllRegSetupData().ConfigureAwait(false),
            ////() => _cleanUpProcessor.AddToExceptionListBurkinabe().ConfigureAwait(false));
            ////() => _cleanUpProcessor.CleanDuplicates().ConfigureAwait(false),
            //    () => _cleanUpProcessor.UpdateNewSequences().ConfigureAwait(false));
        }
    }
}
