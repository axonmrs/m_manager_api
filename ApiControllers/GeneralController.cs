﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GeneralController : ControllerBase
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _context;
        public GeneralController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        //[HttpPost]
        //[Route("saveendofdaystats")]
        //public async Task<ActionResult> SaveEodStats(EndOfDayReportStatisticsVM obj)
        //{
        //    try
        //    {
        //        var uId = User.FindFirst("Id")?.Value;
        //        var user = _context.Users.FirstOrDefault(x => x.Id == uId);
        //        if (user.Type != UserType.District) throw new Exception("Not permitted");
        //        if (obj == null) throw new Exception("Invalid POST data");
        //        var db = new mrs_logsContext();
        //        var exstn = db.Endofdayreportstatistics.FirstOrDefault(x => x.PollingStationCode == obj.PollingStationCode && x.Date == obj.Date && x.LastRegistrationDate == obj.LastRegistrationDate);
        //        if (exstn != null) throw new Exception("This report has already been uploaded");
        //        db.Endofdayreportstatistics.Add(new Endofdayreportstatistics
        //        {
        //            Device = obj.Device??"",
        //            PollingStationCode = obj.PollingStationCode,
        //            Reference = StringGenerators.GenerateRandomString(64),
        //            Date = obj.Date,
        //            Status = 1,
        //            CreatedAt = DateTime.UtcNow,
        //            CreatedBy = user.UserName,
        //            LastRegistrationDate = obj.LastRegistrationDate,
        //            NoOfFemales = obj.NoOfFemales,
        //            NoOfMales = obj.NoOfMales,
        //            PollingStationName = obj.PollingStationName,
        //            PrintedBy = obj.PrintedBy,
        //            RegistrationOfficer = obj.RegistrationOfficer,
        //            TotalNumberOfRegistrations = obj.TotalNumberOfRegistrations
        //        });
        //        new LogsRepository(_context).AddLog(SystemLogType.EndOfDayReport, user.UserName, "EndOfDay", "End Of Day Statistics Report", JsonConvert.SerializeObject(obj));
        //        await db.SaveChangesAsync(); ;

        //        return Ok(new { Message = "Saved Successfully" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

        [HttpPost]
        [Route("queryendofdayreports")]
        public ActionResult QueryEodReports(QueryEndOfDayReportsFilter filter)
        {
            try
            {
                var db = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = filter.BuildQuery(db.EndOfDayReportStatistics.Where(x => x.Id > 0));

                if (user.Type != UserType.System)
                {
                    var psCodes = new List<string>();
                    if (user.Type == UserType.Regional)
                    {
                        psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Code).ToList();
                    }
                    raw = raw.Where(x => psCodes.Contains(x.PollingStationCode));
                }

                var total = raw.Count();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x=> x.Date).Take(filter.Pager.Size * filter.Pager.Page);
                var data = raw.ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.Device,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.RegistrationOfficer,
                        x.Date,
                        x.LastRegistrationDate,
                        x.TotalNumberOfRegistrations,
                        x.NoOfFemales,
                        x.NoOfMales,
                        x.PrintedBy,
                        x.CreatedBy,
                        x.CreatedAt,
                        x.Status
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getendofdayreport")]
        public ActionResult Get(int id)
        {
            try
            {
                var db = new mrs_logsContext();
                var res = db.EndOfDayReportStatistics.Where(x => x.Id == id).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.Device,
                    x.PollingStationCode,
                    x.PollingStationName,
                    x.RegistrationOfficer,
                    x.Date,
                    x.LastRegistrationDate,
                    x.TotalNumberOfRegistrations,
                    x.NoOfFemales,
                    x.NoOfMales,
                    x.PrintedBy,
                    x.CreatedBy,
                    x.CreatedAt,
                    x.Status,
                    x.StatusNotes
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        //[HttpGet]
        //[AllowAnonymous]
        //[Route("updatesettings")]
        //public async Task<ActionResult> UpdateRegDistToStations(string passcode)
        //{
        //    try
        //    {
        //        if (passcode.ToLower() != "kenkuts") throw new Exception("Wrong Passcode");
        //        var date = DateTime.Now;
        //        var user = "System";
        //        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "setup.csv");

        //        var data = new List<SetUpFileRow>();
        //        using (var reader = new StreamReader(filePath))
        //        {
        //            while (!reader.EndOfStream)
        //            {
        //                var line = reader.ReadLine();
        //                var cols = line.Split(',');
        //                if (cols.Length != 10)
        //                    passcode = passcode;

        //                data.Add(new SetUpFileRow
        //                {
        //                    RegName = cols[0].Trim().ToUpper(),
        //                    RegCode = cols[1].Trim().ToUpper(),
        //                    DistName = cols[2].Trim().ToUpper(),
        //                    DistCode = cols[3].Trim().ToUpper(),
        //                    ConstName = cols[4].Trim().ToUpper(),
        //                    ConstCode = cols[5].Trim().ToUpper(),
        //                    EAName = cols[6].Trim().ToUpper(),
        //                    EACode = cols[7].Trim().ToUpper(),
        //                    PSName = cols[8].Trim().ToUpper(),
        //                    PSCode = cols[9].Trim().ToUpper()
        //                });
        //            }
        //        }
        //        var regions = data.Select(x => new { x.RegName, x.RegCode }).OrderBy(x => x.RegCode).Distinct().ToList();
        //        var districts = data.Select(x => new { x.DistName, x.DistCode, x.RegCode }).OrderBy(x => x.DistCode).Distinct().ToList();
        //        var consts = data.Select(x => new { x.ConstName, x.ConstCode, x.DistCode }).OrderBy(x => x.ConstCode).Distinct().ToList();
        //        var eas = data.Select(x => new { x.EAName, x.EACode, x.ConstCode }).OrderBy(x => x.EACode).Distinct().ToList();
        //        var stations = data.Select(x => new { x.PSName, x.PSCode, x.EACode }).OrderBy(x => x.PSCode).Distinct().ToList();

        //        //regions
        //        foreach(var r in regions)
        //        {
        //            var ext = _context.Regions.FirstOrDefault(x => x.Code.ToLower() == r.RegCode.ToLower());
        //            if(ext == null)
        //            {
        //                _context.Regions.Add(new Region
        //                {
        //                    Code = r.RegCode,
        //                    Name = r.RegName,
        //                    CreatedAt = date,
        //                    ModifiedAt = date,
        //                    CreatedBy = user,
        //                    ModifiedBy = user,
        //                    OrderIndex = 1,
        //                    Notes = r.RegName
        //                });
        //            }
        //            else
        //            {
        //                ext.Name = r.RegName;
        //            }
        //        }
        //        _context.SaveChanges();


        //        return Ok(new { Message = "Setup data updated" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}


        //[HttpGet]
        //[AllowAnonymous]
        //[Route("confirmallregionalregistrationdevices")]
        //public async Task<ActionResult> ConfirmAllRegionalRegistrationDevices(string passcode)
        //{
        //    try
        //    {
        //        if (passcode.ToLower() != "manamana") throw new Exception("Wrong Passcode");
        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Regional Device Confirmation",
        //            JsonConvert.SerializeObject(""));

        //        //get all unconfirmed devices
        //        var devices = _context.RegistrationDevices.Where(x => x.AssignedToRegion && !x.RegionConfirmed).ToList();
        //        foreach (var dev in devices)
        //        {
        //            dev.RegionConfirmed = true;
        //            dev.RegionConfirmedBy = "System";
        //            dev.RegionConfirmedOn = DateTime.Now;
        //        }
        //        _context.SaveChanges();

        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk Regional Device Confirmation Successful",
        //            JsonConvert.SerializeObject(""));
        //        _context.SaveChanges();
        //        return Created("ConfirmDevices", new { Message = "Bulk Regional Device Confirmation Successful" });
        //    }
        //    catch (Exception ex)
        //    {
        //        new LogsRepository(_context)
        //           .AddLog(SystemLogType.Insert, "System",
        //           "User", "Bulk Regional Device Confirmation failed",
        //           JsonConvert.SerializeObject(ex));
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}


        //[HttpGet]
        //[AllowAnonymous]
        //[Route("confirmalldistrictregistrationdevices")]
        //public async Task<ActionResult> ConfirmAllDistrictRegistrationDevices(string passcode)
        //{
        //    try
        //    {
        //        if (passcode.ToLower() != "manamana") throw new Exception("Wrong Passcode");
        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "District Device Confirmation",
        //            JsonConvert.SerializeObject(""));

        //        //get all unconfirmed devices
        //        var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && !x.DistrictConfirmed).ToList();
        //        foreach (var dev in devices)
        //        {
        //            dev.DistrictConfirmed = true;
        //            dev.DistrictConfirmedBy = "System";
        //            dev.DistrictConfirmedOn = DateTime.Now;
        //        }
        //        _context.SaveChanges();

        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk District Device Confirmation Successful",
        //            JsonConvert.SerializeObject(""));
        //        _context.SaveChanges();
        //        return Created("ConfirmDevices", new { Message = "Bulk District Device Confirmation Successful" });
        //    }
        //    catch (Exception ex)
        //    {
        //        new LogsRepository(_context)
        //           .AddLog(SystemLogType.Insert, "System",
        //           "User", "Bulk District Device Confirmation failed",
        //           JsonConvert.SerializeObject(ex));
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //[Route("admindeviceassignment")]
        //public async Task<ActionResult> AdminDeviceAssignment()
        //{
        //    try
        //    {
        //        var distDevices = new List<DistrictDevicesRow>();
        //        var devicesFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "devices.csv");
        //        var assignedDevicesFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "assigneddevices.csv");

        //        var devices = new List<DevicesRow>();
        //        using (var reader = new StreamReader(devicesFilePath))
        //        {
        //            while (!reader.EndOfStream)
        //            {
        //                var line = reader.ReadLine();
        //                var cols = line.Split(',');
        //                if (cols.Length != 2)
        //                    throw new Exception("Devices Issues");

        //                devices.Add(new DevicesRow
        //                {
        //                    IdentificationCode = cols[0].Trim().ToUpper(),
        //                    SerialNumber = cols[1].Trim().ToUpper()
        //                });
        //            }
        //        }

        //        var assignedDevices = new List<AssignedDevicesRow>();
        //        using (var reader = new StreamReader(assignedDevicesFilePath))
        //        {
        //            while (!reader.EndOfStream)
        //            {
        //                var line = reader.ReadLine();
        //                var cols = line.Split(',');
        //                if (cols.Length != 5)
        //                    throw new Exception("Assigned Devices Issues");

        //                assignedDevices.Add(new AssignedDevicesRow
        //                {
        //                    District = cols[0].Trim().ToUpper(),
        //                    Start = (cols[1].Trim().ToUpper()).PadLeft(4,'0'),
        //                    End = (cols[2].Trim().ToUpper()).PadLeft(4, '0'),
        //                    Skip = (cols[3].Trim().ToUpper()).PadLeft(4, '0'),
        //                    Add = (cols[4].Trim().ToUpper()).PadLeft(4, '0')
        //                });
        //            }
        //        }

        //         var districtNames = assignedDevices.Select(x => x.District).ToList();
        //        var districts = _context.Districts.Where(x => districtNames.Contains(x.Name.ToUpper())).ToList();
        //        var districtXNames = districts.Select(x => x.Name.ToUpper()).ToList();
        //        var missing = districtNames.Except(districtXNames).ToList();
        //        if(missing.Count() > 0)
        //        {
        //            TextWriter tw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "IncorrectDistricts.txt"));

        //            foreach (var s in missing)
        //                tw.WriteLine(s);

        //            tw.Close();
        //        }
        //        assignedDevices = assignedDevices.Where(x => !missing.Contains(x.District)).ToList();

        //        foreach(var x in assignedDevices)
        //        {
        //            var dist = districts.FirstOrDefault(d => d.Name == x.District);
        //            if (dist == null) throw new Exception($"Could Not find District- {dist.Name}");
        //            var end = int.Parse(x.End);
        //            for(var i = int.Parse(x.Start); i <= end; i++)
        //            {
        //                x.List.Add(i.ToString().PadLeft(4, '0'));
        //            }

        //            if (x.Add != "0000") x.List.Add(x.Add);
        //            if(x.Skip != "0000") x.List.Remove(x.Skip);

        //            var xx = x.List;

        //            foreach(var y in x.List)
        //            {
        //                var dev = devices.FirstOrDefault(x => x.IdentificationCode == $"GHN{y}");
        //                if(dev == null) throw new Exception($"Could Not find device- {dev}");
        //                distDevices.Add(new DistrictDevicesRow
        //                {
        //                    DistrictCode = dist.Code,
        //                    IdentificationCode = dev.IdentificationCode,
        //                    SerialNumber = dev.SerialNumber
        //                });
        //            }
        //        }

        //        var allIds = devices.Select(x => x.IdentificationCode);
        //        var assignedDevIds = distDevices.Select(x => x.IdentificationCode);
        //        var unassignedDevices = allIds.Except(assignedDevIds);
        //        if (unassignedDevices.Count() > 0)
        //        {
        //            TextWriter tw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "UnAssignedDevices.txt"));

        //            foreach (var s in unassignedDevices)
        //                tw.WriteLine(s);

        //            tw.Close();
        //        }

        //        if (distDevices.Count() > 0)
        //        {
        //            TextWriter tw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "AssignedDistrictDevices.csv"));

        //            foreach (var s in distDevices)
        //                tw.WriteLine($"{s.SerialNumber},{s.IdentificationCode},{s.DistrictCode}");
        //            tw.Close();
        //        }
        //        return Created("DeviceAssignment", new { Message = "Bulk District Device Assignment Successful" });
        //    }
        //    catch (Exception ex)
        //    {
        //        new LogsRepository(_context)
        //           .AddLog(SystemLogType.Insert, "System",
        //           "User", "Bulk District Device Assignment failed",
        //           JsonConvert.SerializeObject(ex));
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}


        [HttpPost]
        [AllowAnonymous]
        [Route("generateprovisionalvoterlist")]
        public async Task<ActionResult> GenerateProvisionalVoterList(GenerateVoterListReportFilter filter)
        {
            try
            {
                if (filter == null) return BadRequest("Please provide the election date and the secret code");
                new ReportsRepository(_context).GenerateProvisionalVoterListReport(filter).ConfigureAwait(false);
                return Ok(new { Message = "Initiated Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("regeneratecards")]
        [AllowAnonymous]
        public async Task<ActionResult> RegenerateCards()
        {
            try
            {
                using var logsDb = new mrs_logsContext();
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                var newIds = logsDb.FixedDuplicates.Where(x => x.Id > 0).Select(x => x.TemporalVoterRegisterId).ToList();
                
                var bios = logsDb.TemporalVoterRegisterBioNew
                    .Where(x => newIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                var biosx = logsDb.TemporalVoterRegisterBio
                    .Where(x => newIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                bios.AddRange(biosx);
                var records = logsDb.TemporalVoterRegister.Where(x => newIds.Contains(x.Id)).ToList()
                    .Select(x => new
                {
                    x.Id,
                    x.Surname,
                    x.OtherNames,
                    x.PollingStationCode,
                    x.Sex,
                    x.DateOfBirth,
                    x.CreatedAt,
                    x.VoterId,
                        Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.Id == p.TemporalVoterRegisterId)?.Photo
                    }).ToList();


                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
