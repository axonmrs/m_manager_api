﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class OldVoterRegisterController : BaseController<OldVoterRegister>
    {
        public OldVoterRegisterController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Post(OldVoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(OldVoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Delete(long id)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(long id)
        {
            try
            {
                var res = _context.OldVoterRegister.Where(x => x.Id == id).ToList().Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.VoterNumber,
                    x.RegisterationDate,
                    x.RegisteredBy,
                    x.PollingStationCode,
                    x.ElectoralAreaCode,
                    x.ConstituencyCode,
                    x.DistrictCode,
                    x.RegionCode,
                    x.Surname,
                    x.OtherNames,
                    x.PhoneNumber,
                    x.DateOfBirth,
                    x.Age,
                    Sex = x.Sex.ToString(),
                    x.ResidentialAddress,
                    x.ResidentialTown,
                    x.ResidentialTownDistrict,
                    x.ResidentialTownRegion,
                    x.IdentificationType,
                    x.IdentificationNumber,
                    x.FatherName,
                    x.MotherName,
                    x.HomeTown,
                    x.HomeTownAddress,
                    x.HomeTownDistrict,
                    x.HomeTownRegion,
                    x.IsDisabled,
                    x.IsVisuallyImpaired,
                    x.Photo,
                    x.Comments
                    //x.CreatedAt,
                    //x.CreatedBy,
                    //x.ModifiedAt,
                    //x.ModifiedBy
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(OldVoterRegisterFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter);
                
                if (user.Type == UserType.Regional)
                {
                    var psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    raw = raw.Where(x => psCodes.Contains(x.PollingStationCode));
                }
                else if (user.Type == UserType.District)
                {
                    var psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Code).ToList();
                    raw = raw.Where(x => psCodes.Contains(x.PollingStationCode));
                }

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterNumber,
                        x.RegisterationDate,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.ElectoralAreaCode,
                        x.ConstituencyCode,
                        x.DistrictCode,
                        x.RegionCode,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.Age,
                        Sex = x.Sex.ToString(),
                        x.IdentificationType,
                        x.IdentificationNumber,
                        x.FatherName,
                        x.MotherName,
                        x.IsDisabled,
                        x.IsVisuallyImpaired,
                        x.Photo,
                        x.Comments
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var file = ExcelHelpers.GenerateRegionsUploadTemplate();
                return Ok(new { data = file });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadRegions(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Old Voter Register", "Uploaded Old Voter Register", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
