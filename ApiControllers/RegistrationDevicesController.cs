﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class RegistrationDevicesController : BaseController<RegistrationDevice>
    {
        public RegistrationDevicesController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Post(RegistrationDevice model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            if (user == null) throw new Exception("User not authorized");
            try
            {
                
                var lastDevice = _context.RegistrationDevices.OrderByDescending(x => x.Sequence).FirstOrDefault();
                if(lastDevice == null)
                {
                    model.Sequence = "1000";
                }
                else
                {
                    var seq = int.Parse(lastDevice.Sequence);
                    if (seq > 9999) model.Sequence = (seq + 10).ToString();
                    else model.Sequence = (seq + 1).ToString();
                }
                var existing = _context.RegistrationDevices.FirstOrDefault(x => x.IdentificationCode == model.IdentificationCode || x.SerialNumber == model.SerialNumber);
                if (existing != null)
                {
                    if (existing.IdentificationCode == model.IdentificationCode)
                        throw new Exception($"There is already a device with this Identification Code: {model.IdentificationCode}");
                    else
                        throw new Exception($"There is already a device with this Serial Number: {model.SerialNumber}");
                }
                model.ActivationCode = StringGenerators.GenerateRandomNumber(8);
                model.AdminCode = StringGenerators.GenerateRandomNumber(6);
                model.IsActive = true;
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                _context.RegistrationDevices.Add(model);
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Add Device", JsonConvert.SerializeObject(model));
                return Created($"CreateDevice", new { Message = "Saved Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                       .AddLog(SystemLogType.Device, user.UserName,
                       "Devices", "Add Device Failure",
                       JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> Create(RegistrationDeviceVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            if (user == null) throw new Exception("User not authorized");
            try
            {
                
                var model = new RegistrationDevice();
                model.SerialNumber = obj.SerialNumber.ToUpper().Trim();
                model.IdentificationCode = obj.IdentificationCode.ToUpper().Trim();
                model.Sequence = null;
                var existing = _context.RegistrationDevices.FirstOrDefault(x => x.IdentificationCode == model.IdentificationCode || x.SerialNumber == model.SerialNumber);
                if (existing != null)
                {
                    if (existing.IdentificationCode == model.IdentificationCode)
                        throw new Exception($"There is already a device with this Identification Code: {model.IdentificationCode}");
                    else
                        throw new Exception($"There is already a device with this Serial Number: {model.SerialNumber}");
                }
                model.ActivationCode = StringGenerators.GenerateRandomNumber(8);
                model.AdminCode = StringGenerators.GenerateRandomNumber(6);
                model.IsActive = true;
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                _context.RegistrationDevices.Add(model);
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Add Device", JsonConvert.SerializeObject(model));
                return Created($"CreateDevice", new { Message = "Saved Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                       .AddLog(SystemLogType.Device, user.UserName,
                       "Devices", "Add Device Failure",
                       JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("update")]
        public async Task<ActionResult> Update(RegistrationDeviceUpdateVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            if (user == null) throw new Exception("User not authorized");
            try
            {
                
                var device = _context.RegistrationDevices.FirstOrDefault(x => x.Id == obj.Id);
                if (device == null) throw new Exception($"Please check the selected device");

                device.SerialNumber = obj.SerialNumber.ToUpper().Trim();
                device.IdentificationCode = obj.IdentificationCode.ToUpper().Trim();
                device.CreatedAt = DateTime.UtcNow;
                device.ModifiedAt = DateTime.UtcNow;
                device.CreatedBy = user.UserName;
                device.ModifiedBy = user.UserName;
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Update Device", JsonConvert.SerializeObject(obj));
                return Created($"UpdateDevice", new { Message = "Saved Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                       .AddLog(SystemLogType.Device, user.UserName,
                       "Devices", "Update Device Failure",
                       JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(RegistrationDevice model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            if (user == null) throw new Exception("User not authorized");
            try
            {
                
                var device = _context.RegistrationDevices.FirstOrDefault(x => x.Id == model.Id);
                if (device == null) throw new Exception("Please check the selected registration device");
                device.ModifiedAt = DateTime.UtcNow;
                device.ModifiedBy = user.UserName;
                device.SerialNumber = model.SerialNumber.ToUpper().Trim();
                device.IdentificationCode = model.IdentificationCode.ToUpper().Trim();
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Update Device", JsonConvert.SerializeObject(model));
                return Created($"UpdateDevice", new { Message = "Updated Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                       .AddLog(SystemLogType.Device, user.UserName,
                       "Devices", "Update Device Failure",
                       JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPut]
        [Route("updatesequence")]
        public async Task<ActionResult> UpdateSequence(UpdateDeviceSequenceVM model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            if (user == null) throw new Exception("User not authorized");
            try
            {
                
                var dev = _context.RegistrationDevices.FirstOrDefault(x => x.Sequence == model.Sequence);
                if (dev != null) throw new Exception($"There is already a device with this sequence: {model.Sequence}.");
                
                var device = _context.RegistrationDevices.FirstOrDefault(x => x.Id == model.Id);
                if (device == null) throw new Exception("Please check the selected registration device");
                device.ModifiedAt = DateTime.UtcNow;
                device.ModifiedBy = user.UserName;
                device.Sequence = model.Sequence;
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Update Device Sequence", JsonConvert.SerializeObject(model));
                return Created($"UpdateDevice", new { Message = "Updated Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                       .AddLog(SystemLogType.Device, user.UserName,
                       "Devices", "Update Device Sequence Failure",
                       JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.RegistrationDevices.Where(x => x.Id > 0 && !x.IsDeleted);
                var data = raw.ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    //x.AdminCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.AssignedToDistrict,
                    x.AssignedToDistrictBy,
                    x.AssignedToDistrictOn,
                    x.AssignedToRegion,
                    x.AssignedToRegionBy,
                    x.AssignedToRegionOn,
                    x.DistrictConfirmed,
                    x.DistrictConfirmedBy,
                    x.DistrictConfirmedOn,
                    x.RegionConfirmed,
                    x.RegionConfirmedOn,
                    x.RegionConfirmedBy,
                    District = x.District?.Name,
                    Region = x.Region?.Name,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Get Devices", JsonConvert.SerializeObject(res));
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.RegistrationDevices.Where(x => x.Id == id).ToList().Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    //x.AdminCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.AssignedToDistrict,
                    x.AssignedToDistrictBy,
                    x.AssignedToDistrictOn,
                    x.AssignedToRegion,
                    x.AssignedToRegionBy,
                    x.AssignedToRegionOn,
                    x.DistrictConfirmed,
                    x.DistrictConfirmedBy,
                    x.DistrictConfirmedOn,
                    x.RegionConfirmed,
                    x.RegionConfirmedOn,
                    x.RegionConfirmedBy,
                    District = x.District?.Name,
                    Region = x.Region?.Name,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).FirstOrDefault();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Get Device", JsonConvert.SerializeObject(res));
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(RegistrationDevicesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _repository.Query(filter);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.IdentificationCode).ThenBy(x => x.SerialNumber).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.ActivationCode,
                        //x.AdminCode,
                        x.SerialNumber,
                        x.IdentificationCode,
                        x.Sequence,
                        x.Reference,
                        x.AssignedToDistrict,
                        x.AssignedToDistrictBy,
                        x.AssignedToDistrictOn,
                        x.AssignedToRegion,
                        x.AssignedToRegionBy,
                        x.AssignedToRegionOn,
                        x.DistrictConfirmed,
                        x.DistrictConfirmedBy,
                        x.DistrictConfirmedOn,
                        x.RegionConfirmed,
                        x.RegionConfirmedOn,
                        x.RegionConfirmedBy,
                        District = x.District?.Name,
                        Region = x.Region?.Name,
                        x.IsActive,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Query Devices", JsonConvert.SerializeObject(data));
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }


        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var ef = new ExcelFile();
                var sheet1 = ef.Worksheets.Add("Main");
                sheet1.Cells[0, 0].Value = "SERIAL_NUMBER";
                sheet1.Cells[0, 1].Value = "IDENTIFICATION_CODE";
                sheet1.Cells[0, 2].Value = "DISTRICT_CODE";

                FileContentResult robj;
                using (MemoryStream stream = new MemoryStream())
                {
                    ef.Save(stream, SaveOptions.XlsxDefault);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"DevicesUploadTemplate_{DateTime.Now.ToUniversalTime().ToFileTime()}.xlsx");
                    robj = bytesdata;
                }
                return Ok(robj);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadDevices(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Devices", "Uploaded Devices File", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("querydistrictdevices")]
        public async Task<ActionResult> QueryDistrictDevices(DistrictRegistrationDevicesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _repository.Query(filter).Where(x=> x.DistrictId > 0 && x.AssignedToDistrict && x.RegionConfirmed);

                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.District.RegionId == user.RegionId);
                }
                else if(user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.DistrictId == user.DistrictId);
                }

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.IdentificationCode).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.ActivationCode,
                        x.SerialNumber,
                        x.IdentificationCode,
                        x.Sequence,
                        x.Reference,
                        x.AssignedToDistrict,
                        x.AssignedToDistrictBy,
                        x.AssignedToDistrictOn,
                        x.IsActive,
                        x.DistrictId,
                        District = x.District?.Name,
                        Region = x.District?.Region?.Name,
                        x.DistrictConfirmed,
                        x.DistrictConfirmedBy,
                        x.DistrictConfirmedOn,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Query District Devices", JsonConvert.SerializeObject(new { filter, data }));
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryregiondevices")]
        public async Task<ActionResult> QueryRegionDevices(RegionRegistrationDevicesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _repository.Query(filter).Where(x => x.RegionId > 0 && x.AssignedToRegion)
                    .Include(x=> x.District.Region)
                    .Where(x=> x.Id > 0);
                var dat = raw.ToList();

                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.District.RegionId == user.RegionId);
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.IdentificationCode).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.ActivationCode,
                        x.SerialNumber,
                        x.IdentificationCode,
                        x.Sequence,
                        x.Reference,
                        x.AssignedToDistrict,
                        x.AssignedToDistrictBy,
                        x.AssignedToDistrictOn,
                        x.IsActive,
                        x.DistrictId,
                        District = x.District?.Name,
                        Region = x.Region?.Name,
                        x.RegionConfirmed,
                        x.RegionConfirmedBy,
                        x.RegionConfirmedOn,
                        x.DistrictConfirmed,
                        x.DistrictConfirmedBy,
                        x.DistrictConfirmedOn,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Query Regional Devices", JsonConvert.SerializeObject(new { filter, data }));
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdatedistrictdevices")]
        public ActionResult AddOrUpdateDistrictDevices(DistrictDevicesVM data)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.System && user.Type != UserType.Regional) throw new Exception("Not permitted");               

                var district = _context.Districts.FirstOrDefault(x => x.Id == data.DistrictId);
                if (district == null) throw new Exception("Invalid district");
                var deviceIds = data.DeviceIds;
                var devices = _context.RegistrationDevices.Where(x => deviceIds.Contains(x.Id)).ToList();

                var seq = 1000;
                var lastDevice = _context.RegistrationDevices.Where(x => x.Sequence != null).ToList().OrderByDescending(x => int.Parse(x.Sequence)).FirstOrDefault();
                if (lastDevice != null)
                {
                    seq = int.Parse(lastDevice.Sequence);
                }
                foreach (var d in devices)
                {
                    if (seq > 9999) seq = seq + 10;
                    else seq = seq + 10;
                    if (d.AssignedToDistrict) throw new Exception($"The device with Id Code '{d.IdentificationCode}' has already been assigned to a district. Please unassign the device before assigning it to another district.");
                    d.Sequence = seq.ToString();
                    d.AssignedToDistrict = true;
                    d.AssignedToDistrictBy = user.UserName;
                    d.AssignedToDistrictOn = DateTime.UtcNow;
                    d.DistrictId = district.Id;
                    d.AssignedToRegion = true;
                    d.AssignedToRegionBy = user.UserName;
                    d.AssignedToRegionOn = DateTime.UtcNow;
                    d.RegionId = district.RegionId;
                }
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "District Device Assignment", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("adminupload")]
        public ActionResult AdminUpload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.AdminUploadDevices(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Devices", "Uploaded Reg Devices File By Admin", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Admin Reg Device Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdateregionaldevices")]
        public ActionResult AddOrUpdateRegionDevices(RegionDevicesVM data)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.System) throw new Exception("You are not permitted to add regional devices");

                var region = _context.Regions.FirstOrDefault(x => x.Id == data.RegionId);
                if (region == null) throw new Exception("Invalid region");
                var deviceIds = data.DeviceIds;
                var devices = _context.RegistrationDevices.Where(x => deviceIds.Contains(x.Id)).ToList();

                foreach (var d in devices)
                {
                    if (d.AssignedToRegion) throw new Exception($"The device with Id Code '{d.IdentificationCode}' has already been assigned to a region. Please unassign the device before assigning it to another region.");
                    d.AssignedToRegion = true;
                    d.AssignedToRegionBy = user.UserName;
                    d.AssignedToRegionOn = DateTime.UtcNow;
                    d.RegionId = region.Id;
                    d.AssignedToDistrict = false;
                    d.AssignedToDistrictBy = null;
                    d.AssignedToDistrictOn = null;
                    d.DistrictId = null;
                }
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Regional Device Assignment", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryunassigneddevices")]
        public async Task<ActionResult> QueryUnassigned(UnassignedRegistrationDevicesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = filter.BuildQuery(_context.UnassignedRegistrationDevices.Include(x=> x.Device));

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.Device.IdentificationCode).ThenBy(x => x.Device.SerialNumber).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Device.SerialNumber,
                        x.Device.IdentificationCode,
                        x.Device.Reference,
                        x.Device.IsActive,
                        x.Reason,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Query Unassigned Devices", JsonConvert.SerializeObject(data));
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("unassigndevices")]
        public ActionResult UnassignDevices(UnassignDevicesVM model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.System) throw new Exception("You are not permitted to unassign devices");

                if (string.IsNullOrEmpty(model.Reason)) throw new Exception("Please provide reasons why these device(s) are being unassigned.");

                var deviceIds = model.DeviceIds;
                var devices = _context.RegistrationDevices.Where(x => deviceIds.Contains(x.Id)).ToList();

                foreach(var device in devices)
                {
                    var deviceJson = JsonConvert.SerializeObject(new RegistrationDevice { 
                        Id = device.Id,
                        RegionId = device.RegionId,
                        DistrictId = device.DistrictId,
                        SerialNumber = device.SerialNumber,
                        IdentificationCode = device.IdentificationCode,
                        ActivationCode = device.ActivationCode,
                        Sequence = device.Sequence,
                        AssignedToDistrict  = device.AssignedToDistrict,
                        AssignedToDistrictBy = device.AssignedToDistrictBy,
                        AssignedToDistrictOn = device.AssignedToDistrictOn,
                        AssignedToRegion = device.AssignedToRegion,
                        AssignedToRegionBy = device.AssignedToRegionBy,
                        AssignedToRegionOn = device.AssignedToRegionOn,
                        RegionConfirmed = device.RegionConfirmed,
                        RegionConfirmedBy = device.RegionConfirmedBy,
                        RegionConfirmedOn = device.RegionConfirmedOn,
                        DistrictConfirmed = device.DistrictConfirmed,
                        DistrictConfirmedBy = device.DistrictConfirmedBy,
                        DistrictConfirmedOn = device.DistrictConfirmedOn,
                        CreatedAt = device.CreatedAt,
                        CreatedBy = device.CreatedBy,
                        ModifiedAt = device.ModifiedAt,
                        ModifiedBy = device.ModifiedBy,
                        Reference = device.Reference,
                        IsActive = device.IsActive,
                        Locked = device.Locked,
                        IsDeleted = device.IsDeleted
                    });
                    device.AssignedToRegion = false;
                    device.AssignedToRegionBy = null;
                    device.AssignedToRegionOn = null;
                    device.RegionId = null;
                    device.AssignedToDistrict = false;
                    device.AssignedToDistrictBy = null;
                    device.AssignedToDistrictOn = null;
                    device.DistrictId = null;
                    device.RegionConfirmed = false;
                    device.RegionConfirmedBy = null;
                    device.RegionConfirmedOn = null;
                    device.DistrictConfirmed = false;
                    device.DistrictConfirmedBy = null;
                    device.DistrictConfirmedOn = null;
                    device.Sequence = null;

                    _context.UnassignedRegistrationDevices.Add(new UnassignedRegistrationDevice
                    {
                        DeviceId = device.Id,
                        Reason = model.Reason,
                        DeviceData = deviceJson,
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName
                    });
                   // _context.SaveChanges();
                }

                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", $"Unassign Device by {user.UserName}", JsonConvert.SerializeObject(deviceIds));
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getunassigneddistrictdevices")]
        public ActionResult GetUnassignedDistrictDevices()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.RegistrationDevices.Where(x => x.DistrictId == null && !x.AssignedToDistrict).OrderBy(x => x.IdentificationCode).ToList();
                if (user.Type == UserType.Regional)
                {
                    res = res.Where(x => x.RegionId == user.RegionId).ToList();
                }
                
                var data = res.Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.AssignedToDistrict,
                    x.AssignedToDistrictBy,
                    x.AssignedToDistrictOn,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Unassigned District Devices", JsonConvert.SerializeObject(data));
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getunassignedregionaldevices")]
        public ActionResult GetUnassignedRegionalDevices()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.RegistrationDevices.Where(x => x.RegionId == null && !x.AssignedToRegion).OrderBy(x => x.IdentificationCode).ToList().Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.AssignedToRegion,
                    x.AssignedToRegionBy,
                    x.AssignedToRegionOn,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Unassigned Regional Devices", JsonConvert.SerializeObject(res));
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getassigneddevices")]
        public ActionResult GetAssignedDevices()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.RegistrationDevices.Where(x => x.AssignedToDistrict || x.AssignedToRegion).OrderBy(x => x.IdentificationCode).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Assigned Devices", JsonConvert.SerializeObject(data));
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regionaldeviceconfirmation")]
        public ActionResult RegionalDeviceConfirmation(DeviceConfirmationVM data)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.Regional) throw new Exception("Not permitted");
                var region = _context.Regions.FirstOrDefault(x => x.Id == user.RegionId);
                if (region == null) throw new Exception("Region Error: Please contact the system administrator");

                var deviceIds = new List<int>();
                var codes = new List<string>();
                var devices = new List<RegistrationDevice>();
                if (data.DeviceIds != null)
                {
                    deviceIds = data.DeviceIds;
                    devices = _context.RegistrationDevices.Where(x => deviceIds.Contains(x.Id) && x.RegionId == region.Id).Include(x => x.District.Region).Distinct().ToList();
                }
                else
                {
                    codes = data.IdentificationCodes;
                    devices = _context.RegistrationDevices.Where(x => codes.Contains(x.IdentificationCode) && x.RegionId == region.Id).Include(x => x.District.Region).Distinct().ToList();
                }
                foreach (var d in devices)
                {
                    d.RegionConfirmed = true;
                    d.RegionConfirmedBy = user.UserName;
                    d.RegionConfirmedOn = DateTime.UtcNow;
                }
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Regional Device Confirmation", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districtdeviceconfirmation")]
        public ActionResult DistrictDeviceConfirmation(DeviceConfirmationVM data)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.District) throw new Exception("Not permitted");
                var district = _context.Districts.FirstOrDefault(x => x.Id == user.DistrictId);
                if (district == null) throw new Exception("District Error: Please contact the system administrator");

                var deviceIds = new List<int>();
                var codes = new List<string>();
                var devices = new List<RegistrationDevice>();
                if (data.DeviceIds != null)
                {
                    deviceIds = data.DeviceIds;
                    devices = _context.RegistrationDevices.Where(x => deviceIds.Contains(x.Id) && x.DistrictId == district.Id).Include(x => x.District.Region).Distinct().ToList();
                }
                else
                {
                    codes = data.IdentificationCodes;
                    devices = _context.RegistrationDevices.Where(x => codes.Contains(x.IdentificationCode) && x.DistrictId == district.Id).Include(x => x.District.Region).Distinct().ToList();
                }
                foreach (var d in devices)
                {
                    d.DistrictConfirmed = true;
                    d.DistrictConfirmedBy = user.UserName;
                    d.DistrictConfirmedOn = DateTime.UtcNow;
                }
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "District Device Confirmation", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getunconfirmedregionaldevices")]
        public async Task<ActionResult> GetUnconfirmedRegionalDevices()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.Regional) throw new Exception("Not permitted");
                var raw = _context.RegistrationDevices.Where(x => !x.RegionConfirmed && x.RegionId == user.RegionId)
                    .Include(x=> x.District.Region).ToList();
                var data = raw.ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Get Unconfirmed Regional Devices", JsonConvert.SerializeObject(res));
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getunconfirmeddistrictdevices")]
        public async Task<ActionResult> GetUnconfirmedDistrictDevices()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.District) throw new Exception("Not permitted");
                var raw = _context.RegistrationDevices.Where(x => x.RegionConfirmed && !x.DistrictConfirmed && x.DistrictId == user.DistrictId)
                    .Include(x => x.District.Region).ToList();
                var data = raw.ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.ActivationCode,
                    x.SerialNumber,
                    x.IdentificationCode,
                    x.Sequence,
                    x.Reference,
                    x.IsActive,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Get Unconfirmed District Devices", JsonConvert.SerializeObject(res));
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getdistrictdevices")]
        public ActionResult GetDistrictDevices(long districtId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == districtId).OrderBy(x => x.IdentificationCode).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.SerialNumber,
                    x.IdentificationCode
                }).ToList();
                new LogsRepository(_context).AddLog(SystemLogType.Device, user.UserName, "Devices", "Assigned Devices", JsonConvert.SerializeObject(data));
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
