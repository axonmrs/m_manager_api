﻿using MManager.Models;

namespace MManager.ApiControllers
{
    public class ExternalSystemsController : BaseController<ExternalSystem>
    {
        public ExternalSystemsController(ApplicationDbContext context) : base(context)
        {
        }
    }
}
