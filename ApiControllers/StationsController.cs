﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class StationsController : BaseController<Station>
    {
        public StationsController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                
                var raw = _context.Stations.Where(x=> x.Id > 0);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if(user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var data = raw.Include(x => x.ElectoralArea.Constituency.District.Region).OrderBy(x => x.OrderIndex).ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    x.ElectoralAreaId,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Stations.Where(x => x.Id == id);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var res = raw.Include(x => x.ElectoralArea.Constituency.District.Region).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    x.ElectoralAreaId,
                    Region = x.ElectoralArea?.Constituency?.District?.Region?.Name,
                    District = x.ElectoralArea?.Constituency?.District?.Name,
                    Constituency = x.ElectoralArea?.Constituency?.Name,
                    ElectoralArea = x.ElectoralArea?.Name
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getall")]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                //var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var data = _context.Stations.Where(x => x.Id > 0).OrderBy(x => x.OrderIndex).ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    Name = x.Code + " - " + x.Name,
                    x.Code,
                    x.ElectoralAreaId,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(StationsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                
                var raw = _repository.Query(filter);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.Include(x => x.ElectoralArea.Constituency.District.Region)
                    .OrderBy(x => x.OrderIndex).ThenBy(x => x.ElectoralArea.Constituency.District.Region.Code).ThenBy(x => x.ElectoralArea.Constituency.District.Code).ThenBy(x => x.ElectoralArea.Constituency.Code).ThenBy(x => x.ElectoralArea.Code).ThenBy(x => x.Code).ThenBy(x => x.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.OrderIndex,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.ElectoralAreaId,
                        Region = x.ElectoralArea?.Constituency?.District?.Region?.Name,
                        District = x.ElectoralArea?.Constituency?.District?.Name,
                        Constituency = x.ElectoralArea?.Constituency?.Name,
                        ElectoralArea = x.ElectoralArea?.Name
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var filename = $"PollingStationsUploadTemplate_{DateTime.Now.ToUniversalTime().ToFileTime()}.xlsx";
                var ef = new ExcelFile();
                var sheet1 = ef.Worksheets.Add("Main");
                sheet1.Cells[0, 0].Value = "CODE";
                sheet1.Cells[0, 1].Value = "NAME";
                sheet1.Cells[0, 2].Value = "ELECTORAL_AREA_CODE";
                sheet1.Cells[0, 3].Value = "CONSTITUENCY_CODE";
                sheet1.Cells[0, 4].Value = "DISTRICT_CODE";
                sheet1.Cells[0, 5].Value = "REGION_CODE";

                FileContentResult robj;
                using (MemoryStream stream = new MemoryStream())
                {
                    ef.Save(stream, SaveOptions.XlsxDefault);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                    robj = bytesdata;
                }
                return Ok(robj);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadStations(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Stations", "Uploaded Stations", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbyelectoralarea")]
        public ActionResult GetByElectoralArea(long electoralAreaId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Stations.Where(x => x.ElectoralAreaId == electoralAreaId);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var res = raw.Include(x => x.ElectoralArea).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    ElectoralArea = x.ElectoralArea?.Name,
                    Constituency = x.ElectoralArea?.Constituency?.Name,
                    District = x.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.ElectoralArea?.Constituency?.District?.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbydistrict")]
        public ActionResult GetByDistrict(long districtId)
        {
            try
            {
                var res = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == districtId).Include(x => x.ElectoralArea.Constituency.District.Region).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    ElectoralArea = x.ElectoralArea?.Name,
                    Constituency = x.ElectoralArea?.Constituency?.Name,
                    District = x.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.ElectoralArea?.Constituency?.District?.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getallbyelectoralarea")]
        public ActionResult GetAllByElectoralArea(long electoralAreaId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Stations.Where(x => x.ElectoralAreaId == electoralAreaId);
                var res = raw.Include(x => x.ElectoralArea).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    ElectoralArea = x.ElectoralArea?.Name,
                    Constituency = x.ElectoralArea?.Constituency?.Name,
                    District = x.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.ElectoralArea?.Constituency?.District?.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
