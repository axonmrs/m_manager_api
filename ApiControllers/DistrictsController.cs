﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class DistrictsController : BaseController<District>
    {
        public DistrictsController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Districts.Where(x => x.Id > 0);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Id == user.DistrictId);
                }
                var res = raw.Include(x=> x.Region).Include(x => x.Constituencies).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Region = x.Region?.Name,
                    Constituencies = x.Constituencies.Count()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var res = _context.Districts.Where(x => x.Id == id).Include(x => x.Region).Include(x => x.Constituencies).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Region = x.Region?.Name,
                    Constituencies = x.Constituencies.Select(q => new { q.Id, q.Name }).ToList()
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getall")]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                //var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.Districts.Where(x => x.Id > 0)
                    .OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                        Name = x.Code + " - " + x.Name,
                        x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    //Region = x.Region?.Name,
                    //Constituencies = x.Constituencies.Count()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(DistrictsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = filter.BuildQuery(_context.Districts.Include(x => x.Region));
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Id == user.DistrictId);
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No District Found" });
                var data = raw.OrderBy(x => x.Code).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.OrderIndex,
                        x.RegionId,
                        Region = x.Region?.Name,
                        //Constituencies = x.Constituencies.Count()
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbyregion")]
        public ActionResult GetByRegion(long regionId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Districts.Where(x => x.RegionId == regionId);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Id == user.DistrictId);
                }
                var res = raw.Include(x => x.Region).Include(x => x.Constituencies).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Region = x.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getallbyregion")]
        public ActionResult GetAllByRegion(long regionId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Districts.Where(x => x.RegionId == regionId);
                var res = raw.Include(x => x.Region).Include(x => x.Constituencies).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Region = x.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var filename = $"DistrictsUploadTemplate_{DateTime.Now.ToUniversalTime().ToFileTime()}.xlsx";
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "TempFiles", filename);
                var ef = new ExcelFile();
                var sheet1 = ef.Worksheets.Add("Main");
                sheet1.Cells[0, 0].Value = "CODE";
                sheet1.Cells[0, 1].Value = "NAME";
                sheet1.Cells[0, 2].Value = "REGION_CODE";

                FileContentResult robj;
                using (MemoryStream stream = new MemoryStream())
                {
                    ef.Save(stream, SaveOptions.XlsxDefault);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                    robj = bytesdata;
                }
                return Ok(robj);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadDistricts(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Districts", "Uploaded Districts", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
