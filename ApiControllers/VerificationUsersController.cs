﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VerificationUsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration configuration;

        public VerificationUsersController(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            this.configuration = configuration;
        }

        [HttpPost]
        [Route("createuser")]
        public ActionResult CreateUser(VerificationUserModel obj)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                //todo: Check if the station selected belongs to the user making the post request
                var model = new VerificationUser();
                model.Name = obj.Name;
                model.Email = obj.Email;
                model.PhoneNumber = obj.PhoneNumber;
                model.UserName = obj.UserName;
                model.BioTemplate = obj.BioTemplate;
                model.Locked = false;
                model.PasswordHash = new SecurityHelpers(configuration).ComputeSha256Hash(obj.UserName + obj.Password);
                model.CreatedAt = DateTime.Now;
                model.ModifiedAt = DateTime.Now;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;

                _context.VerificationUsers.Add(model);
                _context.SaveChanges();

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("updateuser")]
        public ActionResult UpdateUser(VerificationUser model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                //todo: Check if the station selected belongs to the user making the post request

                var rec = _context.VerificationUsers.FirstOrDefault(x => x.Id == model.Id);
                if (rec == null) return NotFound("Updating user not found. Please update an existing user");

                rec.Name = model.Name;
                rec.ModifiedAt = DateTime.Now.ToUniversalTime();
                rec.ModifiedBy = user.UserName;
                rec.PhoneNumber = model.PhoneNumber;
                rec.Email = model.Email;
                rec.Photo = model.Photo;
                rec.BioTemplate = model.BioTemplate;
                rec.Address = model.Address;

                _context.SaveChanges();

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetUsers")]
        public ActionResult GetUsers()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.VerificationUsers.Where(x => x.Id > 0);

                var res = raw.ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.Photo,
                    x.Address,
                }).OrderBy(x => x.UserName).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("QueryUsers")]
        public ActionResult QueryUsers(VerificationUserFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.VerificationUsers).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.Photo,
                    x.Address
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpDelete]
        [Route("DeleteUser")]
        public ActionResult DeleteUser(int id)
        {
            try
            {
                var res = _context.VerificationUsers.FirstOrDefault(x => x.Id == id);
                if (res == null)
                    throw new Exception("No User Found");
                _context.VerificationUsers.Remove(res);
                _context.SaveChanges();
                return Ok(new { Message = "User Deleted Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetUser")]
        public ActionResult GetUser(int id)
        {
            try
            {
                var res = _context.VerificationUsers.Where(x => x.Id == id).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.Photo,
                    x.Address,
                    x.BioTemplate
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("resetpassword")]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var pass = new SecurityHelpers(configuration).ComputeSha256Hash(model.UserName + model.Password);
                var us = _context.VerificationUsers.FirstOrDefault(x => x.UserName == model.UserName && !x.Hidden && !x.IsDeleted);
                if (us == null) return NotFound("Unknown Username");
                us.PasswordHash = pass;
                us.ModifiedAt = DateTime.Now;
                us.ModifiedBy = user.UserName;
                _context.SaveChanges();
                return Ok(new { Message = "Password Reset Successful" });
            }
            catch (Exception e)
            {
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }
    }
}
