﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransfersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TransfersController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        [HttpPost]
        [Route("requestOld")]
        public ActionResult CreateRequestOld(TransferRequestVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName, 
                    "New Transfer Request", "New Transfer Request Started", 
                    JsonConvert.SerializeObject(obj));

                var voter = _context.VoterRegister.FirstOrDefault(x => x.Id == obj.VoterRecordId);
                if (voter == null) throw new Exception("Please check the selected voter record");
                if(voter.PollingStationId == obj.NewPollingStationId)
                    throw new Exception("This request was not successful because the voter is already registered to the selected polling station.");

                var existing = _context.TransferRequests.FirstOrDefault(x => x.VoterRecordId == obj.VoterRecordId && (x.Status == TransferRequestStatus.Pending || x.Status == TransferRequestStatus.Approved || x.Status == TransferRequestStatus.Reviewed));
                if (existing != null) throw new Exception($"This request cannot be saved because there is a transfer record in the system with status '{existing.Status.ToString()}'");


                //todo: Check if the station selected belongs to the user making the post request
                var model = new TransferRequest
                {
                    VoterRecordId = obj.VoterRecordId,
                    NewPollingStationId = obj.NewPollingStationId,
                    Reason = obj.Reason,
                    Status = TransferRequestStatus.Pending,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName
                };

                _context.TransferRequests.Add(model);
                _context.SaveChanges();

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Transfer Request", "New Transfer Request Completed",
                    JsonConvert.SerializeObject(model));
                return Created("TransferRequest", new { model.Id, Message = "Request created Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Transfer Request", "New Transfer Request Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("request")]
        public ActionResult CreateRequest(TransferRequestVM obj)
        {
            using var logsContext = new mrs_logsContext();
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanCreateTransferRequest"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Transfer Request", "New Transfer Request Started",
                    JsonConvert.SerializeObject(obj));

                var voter = _context.VoterRegister.FirstOrDefault(x => x.Id == obj.VoterRecordId);
                if (voter == null) throw new Exception("Please check the selected voter record");
                if (voter.PollingStationId == obj.NewPollingStationId)
                    throw new Exception("This request was not successful because the voter is already registered to the selected polling station.");

                var existing = _context.TransferRequests.FirstOrDefault(x => x.VoterRecordId == obj.VoterRecordId && (x.Status == TransferRequestStatus.Pending || x.Status == TransferRequestStatus.Approved || x.Status == TransferRequestStatus.Reviewed));
                if (existing != null) throw new Exception($"This request cannot be saved because there is a transfer record in the system with status '{existing.Status.ToString()}'");


                //todo: Check if the station selected belongs to the user making the post request
                var rec = _context.VoterRegister.First(x => x.Id == obj.VoterRecordId);
                var newPS = _context.Stations.First(x => x.Id == obj.NewPollingStationId);
                var model = new TransferRequest
                {
                    VoterRecordId = obj.VoterRecordId,
                    NewPollingStationId = obj.NewPollingStationId,
                    Reason = obj.Reason,
                    Status = TransferRequestStatus.Confirmed,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName,
                    VettingNotes = $"Direct Confirmation By System; Old PS Code::{rec.PollingStationCode}; Old PS Id::{rec.PollingStationId}",
                    VettedOn = DateTime.UtcNow,
                    VettedBy = user.UserName
                };
                
                rec.PollingStationId = newPS.Id;
                rec.PollingStationName = newPS.Name;
                rec.PollingStationCode = newPS.Code;

                _context.TransferRequests.Add(model);                

                var tempRecs = logsContext.TemporalVoterRegister.Where(x => x.VoterId == rec.VoterId).ToList();
                if (tempRecs.Count > 1)
                    tempRecs = tempRecs
                        .Where(x => x.Surname == rec.Surname
                        && x.OtherNames == rec.OtherNames
                        && x.RegisteredBy == rec.RegisteredBy
                        && x.CreatedAt == rec.CreatedAt).ToList();
                foreach(var tempRec in tempRecs)
                {
                    tempRec.PollingStationCode = newPS.Code;
                    tempRec.PollingStationName = newPS.Name;
                }
                logsContext.SaveChanges();

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Transfer Request", "New Transfer Request Completed",
                    JsonConvert.SerializeObject(model.Id));
                return Created("TransferRequest", new { model.Id, Message = "Request created Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Transfer Request", "New Transfer Request Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getpendingrequests")]
        public ActionResult GetPendingRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Get Pending Transfer Requests", "Get All Pending Transfer Requests",
                    JsonConvert.SerializeObject(""));

                var raw = _context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Pending);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x=> x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Querypending")]
        public ActionResult QueryPendingTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Pending Transfer Requests", "Pending Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x=> x.Status == TransferRequestStatus.Pending));
                if (user.Type == UserType.Regional)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Pending Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.ToList().Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getreviewedrequests")]
        public ActionResult GetReviewedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Get Reviewed Transfer Requests", "Get All Reviewed Transfer Requests",
                    JsonConvert.SerializeObject(""));

                var raw = _context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Reviewed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryreviewed")]
        public ActionResult QueryReviewedTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Reviewed Transfer Requests", "Reviewed Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Reviewed));
                if (user.Type == UserType.Regional)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Reviewed Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.ToList().Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getapprovedrequests")]
        public ActionResult GetapprovedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Get Approved Transfer Requests", "Get All Approved Transfer Requests",
                    JsonConvert.SerializeObject(""));

                var raw = _context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Approved);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryapproved")]
        public ActionResult QueryApprovedTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Approved Transfer Requests", "Aapproved Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Approved)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Approved Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Queryrejected")]
        public ActionResult QueryRejectedTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Rejected Transfer Requests", "Rejected Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Rejected));
                if (user.Type == UserType.Regional)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Reviewed Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.ToList().Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getconfirmedrequests")]
        public ActionResult GetConfirmedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Confirmed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryconfirmed")]
        public ActionResult QueryConfirmedTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Confirmed Transfer Requests", "Confirmed Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Confirmed)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Confirmed Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getreversedrequests")]
        public ActionResult GetReversedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Get Reversed Transfer Requests", "Get all Reversed Transfer Requests",
                    JsonConvert.SerializeObject(""));

                var raw = _context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Reversed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryreversed")]
        public ActionResult QueryReversedTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Reversed Transfer Requests", "Reversed Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Reversed)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Reversed Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getrequest")]
        public ActionResult GetRequest(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Get Transfer Request", "Transfer Request Get with id",
                    JsonConvert.SerializeObject(id));

                var raw = _context.TransferRequests.Where(x => x.Id == id);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.NewPollingStationId) || psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList(); 
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).OrderBy(x => x.CreatedAt
                ).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Query")]
        public ActionResult QueryTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Transfer Requests", "Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests);
                if (user.Type == UserType.Regional)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    res = res.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList(); 
                var data = res.ToList().Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId,
                    NewPollingStation = x.NewPollingStation?.Name,
                    NewElectoralArea = x.NewPollingStation?.ElectoralArea?.Name,
                    NewConstituency = x.NewPollingStation?.ElectoralArea?.Constituency?.Name,
                    NewDistrict = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    NewRegion = x.NewPollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Cancel")]
        public ActionResult Cancel(TransferRequestVetVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanRejectTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Cancellation", "Transfer Request Cancellation Started",
                    JsonConvert.SerializeObject(obj));

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if(res.Status != TransferRequestStatus.Pending)
                    throw new Exception("This request is not pending and thus cannot be cancelled.");

                res.VettingNotes = $"{res.VettingNotes}\nCancellation Notes: {obj.Notes}";
                res.Status = TransferRequestStatus.Cancelled;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Cancellation", "Transfer Request Cancellation Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Cancelled Successfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Cancellation", "Transfer Request Cancellation Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("reject")]
        public ActionResult Reject(TransferRequestVetVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanRejectTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Rejection", "Transfer Request Rejection Started",
                    JsonConvert.SerializeObject(obj));

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != TransferRequestStatus.Reviewed && res.Status != TransferRequestStatus.Approved)
                    throw new Exception("This request has not been reviewed or approved and thus cannot be rejected.");

                res.VettingNotes = $"{res.VettingNotes}\nRejection Notes: {obj.Notes}";
                res.Status = TransferRequestStatus.Rejected;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Rejection", "Transfer Request Rejection Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Rejected Successfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Rejection", "Transfer Request Rejection Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("review")]
        public ActionResult Review(TransferRequestVetVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanReviewTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Review", "Transfer Request Review Started",
                    JsonConvert.SerializeObject(obj));

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != TransferRequestStatus.Pending)
                    throw new Exception("This request is not pending and thus cannot be reviewed.");

                res.VettingNotes = $"{res.VettingNotes}\nReview Notes: {obj.Notes}";
                res.Status = TransferRequestStatus.Reviewed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Review", "Transfer Request Review Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Reviewed." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Transfer Request Review", "Transfer Request Review Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("approve")]
        public ActionResult Approve(TransferRequestVetVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanApproveTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Approve Transfer Request", "Transfer Request Approval Started",
                    JsonConvert.SerializeObject(obj));

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != TransferRequestStatus.Reviewed)
                    throw new Exception("This request has not been reviewd and thus cannot be approved.");

                res.VettingNotes = $"{res.VettingNotes}\nApproval Notes: {obj.Notes}";
                res.Status = TransferRequestStatus.Approved;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Approve Transfer Request", "Transfer Request Approval Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Approved Successfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Approve Transfer Request", "Transfer Request Approval Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("confirm")]
        public ActionResult Confirmed(TransferRequestVetVM obj)
        {
            using var logsContext = new mrs_logsContext();
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanConfirmTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Confirm Transfer Request", "Transfer Request Confirmation Started",
                    JsonConvert.SerializeObject(obj)); 

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != TransferRequestStatus.Approved)
                    throw new Exception("This request has not been approved and thus cannot be confirmed.");

                var rec = _context.VoterRegister.First(x => x.Id == res.VoterRecord.Id);
                var newPS = _context.Stations.First(x => x.Id == res.NewPollingStationId);

                res.VettingNotes = res.VettingNotes = $"{res.VettingNotes}\nConfirmation Notes: {obj.Notes}; Old PS Code::{rec.PollingStationCode}; Old PS Id::{rec.PollingStationId}";
                res.Status = TransferRequestStatus.Confirmed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;

                
                rec.PollingStationId = newPS.Id;
                rec.PollingStationName = newPS.Name;
                rec.PollingStationCode = newPS.Code;

                var tempRecs = logsContext.TemporalVoterRegister.Where(x => x.VoterId == rec.VoterId).ToList();
                if (tempRecs.Count > 1)
                    tempRecs = tempRecs
                        .Where(x => x.Surname == rec.Surname
                        && x.OtherNames == rec.OtherNames
                        && x.RegisteredBy == rec.RegisteredBy
                        && x.CreatedAt == rec.CreatedAt).ToList();
                foreach (var tempRec in tempRecs)
                {
                    tempRec.PollingStationCode = newPS.Code;
                    tempRec.PollingStationName = newPS.Name;
                }
                logsContext.SaveChanges();
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Confirm Transfer Request", "Transfer Request Confirmation Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Confirmed Successfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Confirm Transfer Request", "Transfer Request Confirmation Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("reverseconfirmed")]
        public ActionResult ReverseConfirmed(TransferRequestVetVM obj)
        {
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            //var claims = user.Claims;
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanReverseConfirmedTransfer"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Reverse Transfer Request Confirmation", "Transfer Request Confirmation Reversal Started",
                    JsonConvert.SerializeObject(obj));

                var res = _context.TransferRequests.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != TransferRequestStatus.Confirmed)
                    throw new Exception("This transfer has not been confirmed and thus cannot be reversed.");

                res.VettingNotes = res.VettingNotes = $"{res.VettingNotes}\nReversal Notes: {obj.Notes}"; ;
                res.Status = TransferRequestStatus.Reversed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;

                var rec = _context.VoterRegister.First(x => x.Id == res.VoterRecord.Id);
                var newPS = _context.Stations.First(x => x.Id == res.NewPollingStationId);
                rec.PollingStationId = newPS.Id;
                rec.PollingStationName = newPS.Name;
                rec.PollingStationCode = newPS.Code;

                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Reverse Transfer Request Confirmation", "Transfer Request Confirmation Reversal Completed",
                    JsonConvert.SerializeObject(obj));
                return Ok(new { Message = "Reversal Successful." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Reverse Transfer Request Confirmation", "Transfer Request Confirmation Reversal Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("specialrequest")]
        public ActionResult CreateSpecialRequest(SpecialTransferRequestVM obj)
        {
            using var logsContext = new mrs_logsContext();
            var uId = User.FindFirst("Id")?.Value;
            var user = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(user, "CanCreateSpecialTransferRequest"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Special Transfer Request", "New Special Transfer Request Started",
                    JsonConvert.SerializeObject(obj));

                var voter = _context.VoterRegister.FirstOrDefault(x => x.Id == obj.VoterRecordId);
                if (voter == null) throw new Exception("Please check the selected voter record");
                if (voter.PollingStationId == obj.SpecialPollingStationId)
                    throw new Exception("This request was not successful because the voter is already registered to the selected polling station.");

                var existing = _context.TransferRequests.FirstOrDefault(x => x.VoterRecordId == obj.VoterRecordId && (x.Status == TransferRequestStatus.Pending || x.Status == TransferRequestStatus.Approved || x.Status == TransferRequestStatus.Reviewed));
                if (existing != null) throw new Exception($"This request cannot be saved because there is a transfer record in the system with status '{existing.Status.ToString()}'");


                //todo: Check if the station selected belongs to the user making the post request
                var rec = _context.VoterRegister.First(x => x.Id == obj.VoterRecordId);
                var newPS = _context.Stations.First(x => x.Id == obj.SpecialPollingStationId);
                var model = new TransferRequest
                {
                    VoterRecordId = obj.VoterRecordId,
                    NewPollingStationId = newPS.Id,
                    OldPollingStationId = rec.PollingStationId,
                    Reason = "SPECIAL TRANSFER",
                    IsSpecial = true,
                    Status = TransferRequestStatus.Confirmed,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName,
                    VettingNotes = $"Direct Confirmation By System; Old PS Code::{rec.PollingStationCode}; Old PS Id::{rec.PollingStationId}",
                    VettedOn = DateTime.UtcNow,
                    VettedBy = user.UserName
                };

                rec.PollingStationId = newPS.Id;
                rec.PollingStationName = newPS.Name;
                rec.PollingStationCode = newPS.Code;

                _context.TransferRequests.Add(model);

                var tempRecs = logsContext.TemporalVoterRegister.Where(x => x.VoterId == rec.VoterId).ToList();
                if (tempRecs.Count > 1)
                    tempRecs = tempRecs
                        .Where(x => x.Surname == rec.Surname
                        && x.OtherNames == rec.OtherNames
                        && x.RegisteredBy == rec.RegisteredBy
                        && x.DateOfBirth == rec.DateOfBirth).ToList();
                foreach (var tempRec in tempRecs)
                {
                    tempRec.PollingStationCode = newPS.Code;
                    tempRec.PollingStationName = newPS.Name;
                }
                logsContext.SaveChanges();

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Special Transfer Request", "New Special Transfer Request Completed",
                    JsonConvert.SerializeObject(model.Id));
                return Created("SpecialTransferRequest", new { model.Id, Message = "Request created Successfully and Confirmed" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "New Special Transfer Request", "New Special Transfer Request Failure",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Queryconfirmedspecialtransfers")]
        public ActionResult QueryConfirmedSpecialTransfers(TransfersFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Transfers, user.UserName,
                    "Query Confirmed Special Transfer Requests", "Confirmed Special Transfer Requests Query",
                    JsonConvert.SerializeObject(filter));

                var res = filter.BuildQuery(_context.TransferRequests.Where(x => x.Status == TransferRequestStatus.Confirmed && x.IsSpecial)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Confirmed Special Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    x.OldPollingStationId,
                    OldPollingStation = x.OldPollingStation?.Name,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.VoterRecordId == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.NewPollingStationId
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
