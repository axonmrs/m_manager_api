﻿using Microsoft.AspNetCore.Mvc;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    public class IdTypesController : BaseController<IdType>
    {
        public IdTypesController(ApplicationDbContext context) : base(context)
        {
        }

        [HttpPost]
        [Route("query")]
        public ActionResult Query(IdTypesFilter filter)
        {
            try
            {
                var raw = _repository.Query(filter);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Id Type Found" });
                var data = raw.OrderBy(x => x.Code).ThenBy(x=> x.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
