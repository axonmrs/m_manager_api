﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class VotingStationsController : BaseController<VotingStation>
    {
        public VotingStationsController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.VotingStations.Where(x => x.Id > 0);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var data = raw.Include(x => x.Station.ElectoralArea.Constituency.District.Region).ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    x.StationId,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.FromLetter,
                    x.ToLetter,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VotingStations.Where(x => x.Id == id);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }
                var res = raw.Include(x => x.Station.ElectoralArea.Constituency.District.Region).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.FromLetter,
                    x.ToLetter,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.StationId,
                    Region = x.Station.ElectoralArea.Constituency.District.Region.Name,
                    District = x.Station.ElectoralArea.Constituency.District.Name,
                    Constituency = x.Station.ElectoralArea.Constituency.Name,
                    ElectoralArea = x.Station.ElectoralArea.Name,
                    Station = x.Station.Name
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(VotingStationsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _repository.Query(filter);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Station.ElectoralArea.Constituency.DistrictId == user.DistrictId);
                }

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.Include(x => x.Station.ElectoralArea.Constituency.District.Region)
                    .OrderBy(x => x.Station.ElectoralArea.Constituency.District.Region.Code)
                    .ThenBy(x => x.Station.ElectoralArea.Constituency.District.Code)
                    .ThenBy(x => x.Station.ElectoralArea.Constituency.Code)
                    .ThenBy(x => x.Station.ElectoralArea.Code)
                    .ThenBy(x => x.Code)
                    .ThenBy(x => x.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.FromLetter,
                        x.ToLetter,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.StationId,
                        Region = x.Station?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        District = x.Station?.ElectoralArea?.Constituency?.District?.Name,
                        Constituency = x.Station?.ElectoralArea?.Constituency?.Name,
                        ElectoralArea = x.Station?.ElectoralArea?.Name,
                        Station = x.Station?.Name
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        //[HttpGet]
        //[Route("downloaduploadtemplate")]
        //public ActionResult DownloadUploadTemplate()
        //{
        //    try
        //    {
        //        var file = ExcelHelpers.GeneratePollingStationsUploadTemplate();
        //        return Ok(new { data = file });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}
    }
}
