﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.Repositories;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DuplicatesController : ControllerBase
    {
        public readonly IServiceProvider _serviceProvider;
        public DuplicatesController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        [HttpGet]
        [Route("gethits")]
        public async Task<ActionResult> GetHits(int id)
        {
            try
            {
                using var logsContext = new mrs_logsContext();

                var hits = logsContext.DuplicateVoterHits
                    .Where(x => x.DuplicateVoterId == id)
                    .Include(x => x.HitTemporalVoterRegister)
                    .ToList();
                var tempIds = hits.Select(x => x.HitTemporalVoterRegisterId).ToList();
                var bios = logsContext.TemporalVoterRegisterBioNew
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                var biosx = logsContext.TemporalVoterRegisterBio
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                bios.AddRange(biosx);
                var res = hits
                    .Select(x => new
                    {
                        x.Id,
                        x.HitTemporalVoterRegister.VoterId,
                        x.HitTemporalVoterRegister.Surname,
                        x.HitTemporalVoterRegister.OtherNames,
                        x.HitTemporalVoterRegister.PhoneNumber,
                        x.HitTemporalVoterRegister.DateOfBirth,
                        Sex = x.HitTemporalVoterRegister.Sex.ToString(),
                        x.HitTemporalVoterRegister.EstimatedAge,
                        x.HitTemporalVoterRegister.PollingStationCode,
                        x.HitTemporalVoterRegister.PollingStationName,
                        x.HitTemporalVoterRegister.CreatedAt,
                        x.HitTemporalVoterRegisterId,
                        x.FingerPrintsScore,
                        x.FaceScore,
                        x.FusedMatchingScore,
                        Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.HitTemporalVoterRegisterId == p.TemporalVoterRegisterId)?.Photo,
                    }).ToList();
                
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(DuplicatesFilter filter)
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = filter.BuildQuery(logsContext.DuplicateVoters.Where(x=> x.Status == DuplicateVoterRecordStatus.Pending));
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    raw = raw.Where(x => psCodes.Contains(x.TemporalVoterRegister.PollingStationCode));
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.TemporalVoterRegister.Surname)
                    .ThenBy(x => x.TemporalVoterRegister.OtherNames)
                    .ToList()
                    .Where(x=> x.Hits.Count > 0)
                    .Select(x => new
                    {
                        x.Id,
                        x.TemporalVoterRegister.VoterId,
                        x.TemporalVoterRegister.Surname,
                        x.TemporalVoterRegister.OtherNames,
                        x.TemporalVoterRegister.PhoneNumber,
                        Sex = x.TemporalVoterRegister.Sex.ToString(),
                        x.TemporalVoterRegister.EstimatedAge,
                        x.TemporalVoterRegister.PollingStationCode,
                        Status = x.Status.ToString(),
                        Hits = x.Hits.Count
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryresolved")]
        public async Task<ActionResult> QueryResolved(DuplicatesFilter filter)
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsContext = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = filter.BuildQuery(logsContext.DuplicateVoters.Where(x => x.Status != DuplicateVoterRecordStatus.Pending));
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    raw = raw.Where(x => psCodes.Contains(x.TemporalVoterRegister.PollingStationCode));
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.TemporalVoterRegister.Surname)
                    .ThenBy(x => x.TemporalVoterRegister.OtherNames)
                    .ToList().Select(x => new
                    {
                        x.Id,
                        x.TemporalVoterRegister.VoterId,
                        x.TemporalVoterRegister.Surname,
                        x.TemporalVoterRegister.OtherNames,
                        x.TemporalVoterRegister.PhoneNumber,
                        Sex = x.TemporalVoterRegister.Sex.ToString(),
                        x.TemporalVoterRegister.EstimatedAge,
                        x.TemporalVoterRegister.PollingStationCode,
                        Status = x.Status.ToString(),
                        x.Decision,
                        Hits = x.Hits.Count()
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("get")]
        public async Task<ActionResult> GetOne(int id)
        {
            try
            {
                using var logsContext = new mrs_logsContext();

                var raw = logsContext.DuplicateVoters.Where(x => x.Id == id)
                    .Include(x => x.TemporalVoterRegister).Include(x => x.Hits).ToList();
                var hits = logsContext.DuplicateVoterHits
                    .Where(x => x.DuplicateVoterId == id)
                    .Include(x => x.HitTemporalVoterRegister)
                    .ToList();
                var tempIds = raw.Select(x => x.TemporalVoterRegisterId).ToList();
                var hitsTempIds = hits.Select(x => x.HitTemporalVoterRegisterId).ToList();
                tempIds.AddRange(hitsTempIds);
                var bios = logsContext.TemporalVoterRegisterBioNew
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                var biosx = logsContext.TemporalVoterRegisterBio
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                bios.AddRange(biosx);
                var res = hits
                    .Select(x => new
                    {
                        x.Id,
                        x.HitTemporalVoterRegister.VoterId,
                        x.HitTemporalVoterRegister.Surname,
                        x.HitTemporalVoterRegister.OtherNames,
                        x.HitTemporalVoterRegister.PhoneNumber,
                        x.HitTemporalVoterRegister.DateOfBirth,
                        Sex = x.HitTemporalVoterRegister.Sex.ToString(),
                        x.HitTemporalVoterRegister.EstimatedAge,
                        x.HitTemporalVoterRegister.PollingStationCode,
                        x.HitTemporalVoterRegister.PollingStationName,
                        x.HitTemporalVoterRegister.CreatedAt,
                        x.HitTemporalVoterRegisterId,
                        x.HitTemporalVoterRegister.DeviceId,
                        x.HitTemporalVoterRegister.Source,
                        x.FingerPrintsScore,
                        x.FaceScore,
                        x.FusedMatchingScore,
                        Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.HitTemporalVoterRegisterId == p.TemporalVoterRegisterId)?.Photo,
                    }).ToList();

                var data = raw.Select(x => new
                {
                    x.Id,
                    x.Decision,
                    x.Notes,
                    x.Officials,
                    x.TemporalVoterRegister.VoterId,
                    x.TemporalVoterRegister.Surname,
                    x.TemporalVoterRegister.OtherNames,
                    x.TemporalVoterRegister.PhoneNumber,
                    Sex = x.TemporalVoterRegister.Sex.ToString(),
                    x.TemporalVoterRegister.EstimatedAge,
                    x.TemporalVoterRegister.PollingStationCode,
                    x.TemporalVoterRegister.PollingStationName,
                    x.TemporalVoterRegister.CreatedAt,
                    x.TemporalVoterRegister.DeviceId,
                    x.TemporalVoterRegister.Source,
                    Status = x.Status.ToString(),
                    Hits = res,
                    Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.TemporalVoterRegisterId == p.TemporalVoterRegisterId)?.Photo,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applydecision")]
        public ActionResult ApplyDecision(ResolveDuplicateVoterVM data)
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsDb = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var duplicate = logsDb.DuplicateVoters.FirstOrDefault(x => x.Id == data.Id);
                if (duplicate == null) return BadRequest("Please Check the Id provided.");
                duplicate.ResolvedAt = DateTime.Now;
                duplicate.ResolvedBy = user.UserName;
                duplicate.Notes = $"Resolution Notes: {data.Notes}";
                duplicate.Officials = data.Officials;
                duplicate.Decision = data.Decision;
                duplicate.Confirmed = data.Confirmed;
                duplicate.Status = DuplicateVoterRecordStatus.Resolved;

                logsDb.SaveChanges();
                return Ok(new { Data = new List<object>(), Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getrecord")]
        public async Task<ActionResult> GetRecord(string voterId)
        {
            try
            {
                //check that the voterid length is 10
                if (voterId.Length != 10)
                    return BadRequest("Please check the length of the voter id provided");
                using var logsContext = new mrs_logsContext();

                var dup = logsContext.DuplicateVoters
                    .FirstOrDefault(x => x.VoterId.Trim() == voterId.Trim());
                if(dup == null)
                {
                    var hit = logsContext.DuplicateVoterHits
                        .Where(x => x.HitVoterId.Trim() == voterId.Trim())
                        .Include(x=> x.DuplicateVoter).FirstOrDefault();
                    if(hit == null)
                        return BadRequest($"The applicant with this voter ID {voterId} is not on the multiple list");
                    dup = hit.DuplicateVoter;
                }
                if(dup.Status == DuplicateVoterRecordStatus.Pending)
                    return BadRequest($"The duplicate record with this voter ID {voterId} is pending. " +
                        $"\nYou can only update a resolved duplicate record.");

                var raw = logsContext.DuplicateVoters.Where(x => x.Id == dup.Id)
                    .Include(x => x.TemporalVoterRegister).Include(x => x.Hits).ToList();
                var hits = logsContext.DuplicateVoterHits
                    .Where(x => x.DuplicateVoterId == dup.Id)
                    .Include(x => x.HitTemporalVoterRegister)
                    .ToList();

                var tempIds = raw.Select(x => x.TemporalVoterRegisterId).ToList();
                var hitsTempIds = hits.Select(x => x.HitTemporalVoterRegisterId).ToList();
                tempIds.AddRange(hitsTempIds);
                var bios = logsContext.TemporalVoterRegisterBioNew
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                var biosx = logsContext.TemporalVoterRegisterBio
                    .Where(x => tempIds.Contains(x.TemporalVoterRegisterId))
                .Select(x => new { x.TemporalVoterRegisterId, x.Photo })
                    .ToList();
                bios.AddRange(biosx);
                var vIds = raw.Select(x => x.VoterId).ToList();
                vIds.AddRange(hits.Select(x => x.HitVoterId).ToList());
                var res = hits
                    .Select(x => new
                    {
                        x.Id,
                        x.HitTemporalVoterRegister.VoterId,
                        x.HitTemporalVoterRegister.Surname,
                        x.HitTemporalVoterRegister.OtherNames,
                        x.HitTemporalVoterRegister.PhoneNumber,
                        x.HitTemporalVoterRegister.DateOfBirth,
                        Sex = x.HitTemporalVoterRegister.Sex.ToString(),
                        x.HitTemporalVoterRegister.EstimatedAge,
                        x.HitTemporalVoterRegister.PollingStationCode,
                        x.HitTemporalVoterRegister.PollingStationName,
                        x.HitTemporalVoterRegister.CreatedAt,
                        x.HitTemporalVoterRegisterId,
                        x.HitTemporalVoterRegister.DeviceId,
                        x.HitTemporalVoterRegister.Source,
                        x.FingerPrintsScore,
                        x.FaceScore,
                        x.FusedMatchingScore,
                        Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.HitTemporalVoterRegisterId == p.TemporalVoterRegisterId)?.Photo,
                    }).ToList();

                var data = raw.Select(x => new
                {
                    x.Id,
                    x.Decision,
                    x.Notes,
                    x.Officials,
                    x.TemporalVoterRegister.VoterId,
                    x.TemporalVoterRegister.Surname,
                    x.TemporalVoterRegister.OtherNames,
                    x.TemporalVoterRegister.PhoneNumber,
                    Sex = x.TemporalVoterRegister.Sex.ToString(),
                    x.TemporalVoterRegister.EstimatedAge,
                    x.TemporalVoterRegister.PollingStationCode,
                    x.TemporalVoterRegister.PollingStationName,
                    x.TemporalVoterRegister.CreatedAt,
                    x.TemporalVoterRegister.DeviceId,
                    x.TemporalVoterRegister.Source,
                    Status = x.Status.ToString(),
                    Hits = res,
                    VoterIds = vIds,
                    Photo = "data:image/jpeg;base64," + bios.FirstOrDefault(p => x.TemporalVoterRegisterId == p.TemporalVoterRegisterId)?.Photo,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("updatedecision")]
        public ActionResult UpdateDecision(UpdateDuplicateDecisionVM data)
        {
            try
            {
                using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                using var logsDb = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var voterId = new MySqlParameter("@vId", data.VoterId);
                var notes = new MySqlParameter("@userNotes", data.Notes);

                var res = logsDb.Database
                    .ExecuteSqlCommand("call UpdateDuplicateVoterDecision(@vId , @userNotes)", voterId, notes);


                //var duplicate = logsDb.DuplicateVoters.FirstOrDefault(x => x.Id == data.Id);
                //if (duplicate == null) return BadRequest("Please Check the Id provided.");
                //duplicate.Notes = $"{duplicate.Notes}\nUpdate Notes: {data.Notes}\nUpdated By: {user.UserName}";
                //duplicate.Decision = $"Keep One => VoterId:{data.VoterId}";

                ////remove all others from the voter register
                //var vIds = duplicate.Hits.Select(x => x.HitVoterId).ToList();
                //vIds.Add(duplicate.VoterId);
                //vIds = vIds.Where(x => x != data.VoterId).ToList();
                //var recs = context.VoterRegister.Where(x => vIds.Contains(x.VoterId)).ToList();
                //recs.ForEach(x => x.HasDuplicate = true);

                ////Manupilate the data
                //var main = context.VoterRegister.FirstOrDefault(x => x.VoterId.Trim() == data.VoterId);
                //if (main != null)
                //    main.HasDuplicate = false;

                //var dupRef = context.DuplicateVoterReferences
                //    .Where(x => x.DuplicateVoterRegister.VoterId == duplicate.VoterId)
                //    .Include(x => x.DuplicateVoterRegister)
                //    .ToList();
                //dupRef.ForEach(x => x.Status = "Reversed");

                //context.SaveChanges();
                logsDb.SaveChanges();
                return Ok(new { Data = new List<object>(), Message = "Decision Has Been Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }
}
