﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class RegionsController : BaseController<Region>
    {
        public RegionsController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Regions.Where(x=> x.Id > 0);
                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        raw = raw.Where(x => x.Id == user.RegionId);
                    }
                    else if (user.Type == UserType.District)
                    {
                        raw = raw.Where(x => x.Id == user.District.RegionId);
                    }
                }
                
                var res = raw.Include(x => x.Districts).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code+" - "+x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Districts = x.Districts.Count()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getall")]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                //var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.Regions.Where(x => x.Id > 0);
                var res = raw.Include(x => x.Districts).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    Name = x.Code + " - " + x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Districts = x.Districts.Count()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var res = _context.Regions.Where(x=> x.Id == id).Include(x => x.Districts).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Districts = x.Districts.Select(q => new { q.Id, q.Name }).ToList()
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(RegionsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter);
                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        raw = raw.Where(x => x.Id == user.RegionId);
                    }
                    else if (user.Type == UserType.District)
                    {
                        raw = raw.Where(x => x.Id == user.District.RegionId);
                    }
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Region Found" });
                var data = raw.OrderBy(x => x.OrderIndex).ThenBy(x => x.Code).ThenBy(x=> x.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.OrderIndex,
                        Districts = x.Districts.Count()
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var filename = $"RegionsUploadTemplate_{DateTime.Now.ToUniversalTime().ToFileTime()}.xlsx";
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "TempFiles", filename);
                var ef = new ExcelFile();
                var sheet1 = ef.Worksheets.Add("Main");
                sheet1.Cells[0, 0].Value = "CODE";
                sheet1.Cells[0, 1].Value = "NAME";

                FileContentResult robj;
                using (MemoryStream stream = new MemoryStream())
                {
                    ef.Save(stream, SaveOptions.XlsxDefault);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                    robj = bytesdata;
                }
                return Ok(robj);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadRegions(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Regions", "Uploaded Regions", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
