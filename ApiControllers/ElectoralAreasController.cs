﻿using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class ElectoralAreasController : BaseController<ElectoralArea>
    {
        public ElectoralAreasController(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.ElectoralAreas.Where(x => x.Id > 0);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Constituency.DistrictId == user.DistrictId);
                }
                var res = raw.Include(x => x.Constituency.District.Region).Include(x => x.Stations).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var res = _context.ElectoralAreas.Where(x => x.Id == id)
                    .Include(x => x.Constituency.District.Region)
                    .Include(x => x.Stations).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Region = x.Constituency?.District?.Region?.Name,
                    District = x.Constituency?.District?.Name,
                    Constituency = x.Constituency?.Name,
                    Stations = x.Stations?.Select(q => new { q.Id, q.Name }).ToList()
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getall")]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                //var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = _context.ElectoralAreas.Where(x => x.Id > 0)
                    .OrderBy(x => x.OrderIndex)
                    .ToList().Select(x => new
                {
                    x.Id,
                        Name = x.Code + " - " + x.Name,
                        x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.OrderIndex,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(ElectoralAreasFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Constituency.DistrictId == user.DistrictId);
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var data = raw.OrderBy(x => x.OrderIndex).ThenBy(x => x.Constituency.District.Region.Code).ThenBy(x => x.Constituency.District.Code).ThenBy(x => x.Constituency.Code).ThenBy(x => x.Code).ThenBy(x => x.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Code,
                        Label = x.Code + " - " + x.Name,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.ConstituencyId,
                        x.OrderIndex,
                        Region = x.Constituency?.District?.Region?.Name,
                        District = x.Constituency?.District?.Name,
                        Constituency = x.Constituency?.Name,
                        Stations = x.Stations?.Count()
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloaduploadtemplate")]
        public ActionResult DownloadUploadTemplate()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var filename = $"ElectoralAreasUploadTemplate_{DateTime.Now.ToUniversalTime().ToFileTime()}.xlsx";
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "TempFiles", filename);
                var ef = new ExcelFile();
                var sheet1 = ef.Worksheets.Add("Main");
                sheet1.Cells[0, 0].Value = "CODE";
                sheet1.Cells[0, 1].Value = "NAME";
                sheet1.Cells[0, 2].Value = "CONSTITUENCY_CODE";
                sheet1.Cells[0, 3].Value = "DISTRICT_CODE";
                sheet1.Cells[0, 4].Value = "REGION_CODE";

                FileContentResult robj;
                using (MemoryStream stream = new MemoryStream())
                {
                    ef.Save(stream, SaveOptions.XlsxDefault);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                    robj = bytesdata;
                }
                return Ok(robj);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(UploadFile data)
        {
            try
            {
                var userName = User.FindFirst("UserName")?.Value;
                ExcelHelpers.UploadElectoralAreas(userName, data, _context);
                new LogsRepository(_context).AddLog(SystemLogType.Upload, userName, "Electoral Areas", "Uploaded Electoral Areas", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Upload Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbyconstituency")]
        public ActionResult GetByConstituency(long constituencyId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.ElectoralAreas.Where(x => x.ConstituencyId == constituencyId);
                if (user.Type == UserType.Regional)
                {
                    raw = raw.Where(x => x.Constituency.District.RegionId == user.RegionId);
                }
                else if (user.Type == UserType.District)
                {
                    raw = raw.Where(x => x.Constituency.DistrictId == user.DistrictId);
                }
                var res = raw.Include(x => x.Constituency).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Constituency = x.Constituency?.Name,
                    District = x.Constituency?.District?.Name,
                    Region = x.Constituency?.District?.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getallbyconstituency")]
        public ActionResult GetAllByConstituency(long constituencyId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.ElectoralAreas.Where(x => x.ConstituencyId == constituencyId);
                var res = raw.Include(x => x.Constituency).OrderBy(x => x.OrderIndex).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    Label = x.Code + " - " + x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.OrderIndex,
                    Constituency = x.Constituency?.Name,
                    District = x.Constituency?.District?.Name,
                    Region = x.Constituency?.District?.Region?.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
