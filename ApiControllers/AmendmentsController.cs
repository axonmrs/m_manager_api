﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AmendmentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public AmendmentsController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        [HttpPost]
        [Route("request")]
        public ActionResult CreateRequest(AmendmentVM obj)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var voter = _context.VoterRegister.FirstOrDefault(x => x.Id == obj.VoterRecordId);
                if (voter == null) throw new Exception("Please check the selected voter record");

                //todo: Check if the station selected belongs to the user making the post request
                var model = new Amendment
                {
                    VoterRecordId = obj.VoterRecordId,
                    Data = obj.Data,
                    Reason = obj.Reason,
                    Status = AmendmentStatus.Pending,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName
                };

                _context.Amendments.Add(model);
                _context.SaveChanges();

                return Created("AmendmentRequest", new { model.Id, Message = "Amendment Request created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getpendingrequests")]
        public ActionResult GetPendingRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Status == AmendmentStatus.Pending);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Querypending")]
        public ActionResult QueryPendingAmendments(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments.Where(x => x.Status == AmendmentStatus.Pending)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Pending Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getreviewedrequests")]
        public ActionResult GetReviewedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Status == AmendmentStatus.Reviewed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryreviewed")]
        public ActionResult QueryReviewed(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments.Where(x => x.Status == AmendmentStatus.Reviewed)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Reviewed Amendment Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getapprovedrequests")]
        public ActionResult GetapprovedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Status == AmendmentStatus.Approved);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryapproved")]
        public ActionResult QueryApprovedTransfers(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments.Where(x => x.Status == AmendmentStatus.Approved)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Approved Amendments Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getconfirmedrequests")]
        public ActionResult GetConfirmedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Status == AmendmentStatus.Confirmed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryconfirmed")]
        public ActionResult QueryConfirmedTransfers(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments.Where(x => x.Status == AmendmentStatus.Confirmed)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Confirmed Amendments Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getreversedrequests")]
        public ActionResult GetReversedRequests()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Status == AmendmentStatus.Reversed);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("Queryreversed")]
        public ActionResult QueryReversedTransfers(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments.Where(x => x.Status == AmendmentStatus.Reversed)).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Reversed Amendments Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getrequest")]
        public ActionResult GetRequest(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var raw = _context.Amendments.Where(x => x.Id == id);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => psIds.Contains(x.VoterRecord.PollingStationId.Value));
                }

                var res = raw.Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                var ids = raw.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).OrderBy(x => x.CreatedAt
                ).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Query")]
        public ActionResult QueryAmendments(AmendmentsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = filter.BuildQuery(_context.Amendments).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No Transfer Requests Found" });
                var ids = res.Select(x => x.VoterRecord.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reason,
                    Status = x.Status.ToString(),
                    x.VoterRecord.Reference,
                    x.VoterRecord.VoterId,
                    x.VoterRecord.PollingStationCode,
                    x.VoterRecord.PollingStationName,
                    x.VoterRecord.PollingStationId,
                    ElectoralArea = x.VoterRecord.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.VoterRecord.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.VoterRecord.Surname,
                    x.VoterRecord.OtherNames,
                    x.VoterRecord.PhoneNumber,
                    x.VoterRecord.DateOfBirth,
                    x.VoterRecord.EstimatedAge,
                    x.VoterRecord.IsAgeEstimated,
                    Sex = x.VoterRecord.Sex.ToString(),
                    x.VoterRecord.IsDisabled,
                    x.VoterRecord.IsVisuallyImpaired,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.Data
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("Cancel")]
        public ActionResult Cancel(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Pending)
                    throw new Exception("This request is not pending and thus cannot be cancelled.");

                res.VettingNotes = $"{res.VettingNotes}\nCancellation Notes: {obj.Notes}";
                res.Status = AmendmentStatus.Cancelled;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                return Ok(new { Message = "Cancelled Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("reject")]
        public ActionResult Reject(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Reviewed)
                    throw new Exception("This request has not been reviewed and thus cannot be rejected.");

                res.VettingNotes = $"{res.VettingNotes}\nRejection Notes: {obj.Notes}";
                res.Status = AmendmentStatus.Rejected;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                return Ok(new { Message = "Rejected Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("review")]
        public ActionResult Review(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Pending)
                    throw new Exception("This request is not pending and thus cannot be reviewed.");

                res.VettingNotes = $"{res.VettingNotes}\nReview Notes: {obj.Notes}";
                res.Status = AmendmentStatus.Reviewed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                return Ok(new { Message = "Reviewed." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("approve")]
        public ActionResult Approve(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Reviewed)
                    throw new Exception("This request has not been reviewd and thus cannot be approved.");

                res.VettingNotes = $"{res.VettingNotes}\nApproval Notes: {obj.Notes}";
                res.Status = AmendmentStatus.Approved;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;
                _context.SaveChanges();
                return Ok(new { Message = "Approved Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("confirm")]
        public ActionResult Confirmed(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Approved)
                    throw new Exception("This request has not been approved and thus cannot be confirmed.");

                res.VettingNotes = res.VettingNotes = $"{res.VettingNotes}\nConfirmation Notes: {obj.Notes}"; ;
                res.Status = AmendmentStatus.Confirmed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;

                var rec = _context.VoterRegister.First(x => x.Id == res.VoterRecord.Id);
                //effect the amendment
                var oldData = "";
                if(res.Type == AmendmentType.ChangeOfName)
                {
                    oldData = JsonConvert.SerializeObject(new ChangeOfNameVM
                    {
                        Surname = rec.Surname,
                        OtherNames = rec.OtherNames
                    });
                    var dta = JsonConvert.DeserializeObject<ChangeOfNameVM>(res.Data);
                    if (!string.IsNullOrEmpty(dta.Surname)) rec.Surname = dta.Surname;
                    if (!string.IsNullOrEmpty(dta.OtherNames)) rec.OtherNames = dta.OtherNames;
                }
                else if(res.Type == AmendmentType.ChangeOfContactInformation)
                {
                    oldData = JsonConvert.SerializeObject(new ChangeOfContactInformationVM
                    {
                        PhoneNumber = rec.PhoneNumber
                    });
                    var dta = JsonConvert.DeserializeObject<ChangeOfContactInformationVM>(res.Data);
                    if(!string.IsNullOrEmpty( dta.PhoneNumber)) rec.PhoneNumber = dta.PhoneNumber;
                }
                else if (res.Type == AmendmentType.ChangeOfAddress)
                {
                    oldData = JsonConvert.SerializeObject(new ChangeOfAddressVM
                    {
                        ResidentialAddress = rec.ResidentialAddress,
                        ResidentialTown = rec.ResidentialTown,
                        ResidentialDistrictId = rec.ResidentialDistrictId,
                        HomeTownAddress = rec.HomeTownAddress,
                        HomeTown = rec.HomeTown,
                        HomeTownDistrictId = rec.HomeTownDistrictId
                    });
                    var dta = JsonConvert.DeserializeObject<ChangeOfAddressVM>(res.Data);
                    if (!string.IsNullOrEmpty(dta.ResidentialAddress)) rec.ResidentialAddress = dta.ResidentialAddress;
                    if (!string.IsNullOrEmpty(dta.ResidentialTown)) rec.ResidentialTown = dta.ResidentialTown;
                    if (dta.ResidentialDistrictId > 0) rec.ResidentialDistrictId = dta.ResidentialDistrictId;
                    if (!string.IsNullOrEmpty(dta.HomeTownAddress)) rec.HomeTownAddress = dta.HomeTownAddress;
                    if (!string.IsNullOrEmpty(dta.HomeTown)) rec.HomeTown = dta.HomeTown;
                    if (dta.HomeTownDistrictId > 0) rec.HomeTownDistrictId = dta.HomeTownDistrictId;
                }
                else if (res.Type == AmendmentType.ChangeOfIdentification)
                {
                    oldData = JsonConvert.SerializeObject(new ChangeOfIdentificationVM
                    {
                        IdentificationNumber = rec.IdentificationNumber,
                        IdentificationExpiry = rec.IdentificationExpiry,
                        IdentificationTypeId = rec.IdentificationTypeId
                    });
                    var dta = JsonConvert.DeserializeObject<ChangeOfIdentificationVM>(res.Data);
                    if (!string.IsNullOrEmpty(dta.IdentificationNumber)) rec.IdentificationNumber = dta.IdentificationNumber;
                    if (dta.IdentificationExpiry.HasValue) rec.IdentificationExpiry = dta.IdentificationExpiry.Value;
                    if (dta.IdentificationTypeId > 0) rec.IdentificationTypeId = dta.IdentificationTypeId;
                }
                res.OldData = oldData;
                _context.SaveChanges();
                return Ok(new { Message = "Confirmed Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("reverseconfirmed")]
        public ActionResult ReverseConfirmed(AmendmentVetVM obj)
        {
            try
            {
                //todo: log this request
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var claims = user.Claims;

                var res = _context.Amendments.FirstOrDefault(x => x.Id == obj.Id);
                if (res == null)
                    throw new Exception("No Request Found");
                if (res.Status != AmendmentStatus.Confirmed)
                    throw new Exception("This amendment has not been confirmed and thus cannot be reversed.");

                res.VettingNotes = res.VettingNotes = $"{res.VettingNotes}\nReversal Notes: {obj.Notes}"; ;
                res.Status = AmendmentStatus.Reversed;
                res.VettedOn = DateTime.UtcNow;
                res.VettedBy = user.UserName;

                var rec = _context.VoterRegister.First(x => x.Id == res.VoterRecord.Id);
                //reverse the amendment
                var oldData = "";
                if (res.Type == AmendmentType.ChangeOfName)
                {
                    var dta = JsonConvert.DeserializeObject<ChangeOfNameVM>(res.OldData);
                    if (!string.IsNullOrEmpty(dta.Surname)) rec.Surname = dta.Surname;
                    if (!string.IsNullOrEmpty(dta.OtherNames)) rec.OtherNames = dta.OtherNames;
                }
                else if (res.Type == AmendmentType.ChangeOfContactInformation)
                {
                    var dta = JsonConvert.DeserializeObject<ChangeOfContactInformationVM>(res.OldData);
                    if (!string.IsNullOrEmpty(dta.PhoneNumber)) rec.PhoneNumber = dta.PhoneNumber;
                }
                else if (res.Type == AmendmentType.ChangeOfAddress)
                {
                    var dta = JsonConvert.DeserializeObject<ChangeOfAddressVM>(res.OldData);
                    if (!string.IsNullOrEmpty(dta.ResidentialAddress)) rec.ResidentialAddress = dta.ResidentialAddress;
                    if (!string.IsNullOrEmpty(dta.ResidentialTown)) rec.ResidentialTown = dta.ResidentialTown;
                    if (dta.ResidentialDistrictId > 0) rec.ResidentialDistrictId = dta.ResidentialDistrictId;
                    if (!string.IsNullOrEmpty(dta.HomeTownAddress)) rec.HomeTownAddress = dta.HomeTownAddress;
                    if (!string.IsNullOrEmpty(dta.HomeTown)) rec.HomeTown = dta.HomeTown;
                    if (dta.HomeTownDistrictId > 0) rec.HomeTownDistrictId = dta.HomeTownDistrictId;
                }
                else if (res.Type == AmendmentType.ChangeOfIdentification)
                {
                    var dta = JsonConvert.DeserializeObject<ChangeOfIdentificationVM>(res.OldData);
                    if (!string.IsNullOrEmpty(dta.IdentificationNumber)) rec.IdentificationNumber = dta.IdentificationNumber;
                    if (dta.IdentificationExpiry.HasValue) rec.IdentificationExpiry = dta.IdentificationExpiry.Value;
                    if (dta.IdentificationTypeId > 0) rec.IdentificationTypeId = dta.IdentificationTypeId;
                }
                res.OldData = oldData;

                _context.SaveChanges();
                return Ok(new { Message = "Reversal Successful." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }



    }
}
