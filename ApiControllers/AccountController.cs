﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountController : ControllerBase
    {
        //private readonly UserManager<User> userManager;
        //private readonly RoleManager<Role> roleManager;
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<User> signInManager;
        private readonly IConfiguration configuration;
        private readonly IServiceProvider _serviceProvider;

        public AccountController(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            //userManager = serviceProvider.GetService<UserManager<User>>();
            //roleManager = serviceProvider.GetService<RoleManager<Role>>();
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            signInManager = serviceProvider.GetService<SignInManager<User>>();
            this.configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<ActionResult> Login(LoginModel model)
        {
            try
            {
                using var userManager = _serviceProvider.GetService<UserManager<User>>();

                var user = await userManager.FindByNameAsync(model.Username);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Login, user.UserName,
                    "User", "User Login Attempt",
                    JsonConvert.SerializeObject(model.Username));

                if (user.Type == UserType.District) throw new Exception("You are not allowed to login on this system");

                if (user != null)
                {
                    var result = await signInManager.CheckPasswordSignInAsync(user, model.Password, model.RememberMe);

                    if (result.Succeeded)
                    {
                        user = _context.Users.Where(x => x.Id == user.Id).Include(x => x.UserRoles).Include(x => x.Claims).First();
                        var role = _context.Roles.Where(x => x.Id == user.UserRoles.First().RoleId).Include(x => x.RoleClaims).First();
                        var roleClaims = role.RoleClaims.Select(x => x.ClaimValue).Distinct().ToList();

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var nowUtc = DateTime.Now.ToUniversalTime();
                        var expires = nowUtc.AddDays(90).ToUniversalTime();

                        var claims = new List<Claim> { new Claim("Id", user.Id), new Claim("UserName", user.UserName) };
                        var token = new JwtSecurityToken(
                        configuration["Tokens:Issuer"],
                        configuration["Tokens:Audience"],
                        claims,
                        expires: expires,
                        signingCredentials: creds);
                        var tokenResponse = new JwtSecurityTokenHandler().WriteToken(token);
                        var location = "National";
                        if (user.Type == UserType.Regional) location = user.Region?.Name;
                        else if (user.Type == UserType.District) location = user.District?.Name;

                        var data = new
                        {
                            user.Id,
                            Username = user.UserName,
                            user.Name,
                            user.Email,
                            user.PhoneNumber,
                            Role = role.Name,
                            Claims = roleClaims,
                            Token = tokenResponse,
                            Location = location,
                            Type = user.Type.ToString(),
                            user.RegionId,
                            user.DistrictId
                        };
                        new LogsRepository(_context)
                    .AddLog(SystemLogType.Login, user.UserName,
                    "User", "Login Successful",
                    JsonConvert.SerializeObject(model.Username));
                        return Ok(new
                        {
                            data,
                            message = "Login Successful"
                        });
                    }

                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Login, user.UserName,
                    "User", "Invalid Username or Password",
                    JsonConvert.SerializeObject(model.Username));
                    return BadRequest("Invalid Username or Password");
                }

                return BadRequest("Invalid Username or Password");
            }
            catch (Exception e)
            {
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("portallogin")]
        public async Task<ActionResult> PortalLogin(LoginModel model)
        {
            try
            {
                using var userManager = _serviceProvider.GetService<UserManager<User>>();
                var user = await userManager.FindByNameAsync(model.Username);

                if (user != null)
                {
                    var result = await signInManager.CheckPasswordSignInAsync(user, model.Password, model.RememberMe);

                    if (result.Succeeded)
                    {
                        user = _context.Users.Where(x => x.Id == user.Id).Include(x => x.UserRoles).Include(x => x.Claims).First();
                        var role = _context.Roles.Where(x => x.Id == user.UserRoles.First().RoleId).Include(x => x.RoleClaims).First();
                        var roleClaims = role.RoleClaims.Select(x => x.ClaimValue).Distinct().ToList();

                        if(!(roleClaims.Contains(Privileges.IsDistrictUser) || roleClaims.Contains(Privileges.IsRegionalUser))) return BadRequest("You are not permitted to login to this portal");

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var nowUtc = DateTime.Now.ToUniversalTime();
                        var expires = nowUtc.AddDays(90).ToUniversalTime();

                        var claims = new List<Claim> { new Claim("Id", user.Id), new Claim("UserName", user.UserName) };
                        var token = new JwtSecurityToken(
                        configuration["Tokens:Issuer"],
                        configuration["Tokens:Audience"],
                        claims,
                        expires: expires,
                        signingCredentials: creds);
                        var tokenResponse = new JwtSecurityTokenHandler().WriteToken(token);

                        var data = new
                        {
                            user.Id,
                            Username = user.UserName,
                            user.Name,
                            user.Email,
                            user.PhoneNumber,
                            Role = role.Name,
                            Claims = roleClaims,
                            Token = tokenResponse
                        };

                        return Ok(new
                        {
                            data,
                            message = "Login Successful"
                        });
                    }

                    return BadRequest("Invalid Username or Password");
                }

                return BadRequest("Invalid Username or Password");
            }
            catch (Exception e)
            {
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<ActionResult> Logout()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Logout, user.UserName,
                    "User", "Logout Attempt",
                    JsonConvert.SerializeObject(""));
                await signInManager.SignOutAsync();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Logout, user.UserName,
                    "User", "User Logged Out",
                    JsonConvert.SerializeObject(""));
                return Ok(new { Message = "User Logged Out" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("createuser")]
        public async Task<ActionResult> CreateUser(User model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                using var userManager = _serviceProvider.GetService<UserManager<User>>();

                if (!StringGenerators.CheckPrivilege(usr, "CanCreateSystemUsers"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "User Creation Attempt",
                    JsonConvert.SerializeObject(model.UserName));
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Type = UserType.System;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                    //var rslt = userManager.AddToRoleAsync(user, model.Role);
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "User creation failed",
                    JsonConvert.SerializeObject(model.UserName));
                    return BadRequest(WebHelpers.ProcessException(result));
                }                    

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "User has been created Successfully",
                    JsonConvert.SerializeObject(model.UserName));
                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "User creation failed",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("updateuser")]
        public ActionResult UpdateUser(User model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanUpdateSystemUsers"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "User Update Attempt",
                    JsonConvert.SerializeObject(model));

                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.Now.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                user.RegionId = model.RegionId;
                user.DistrictId = model.DistrictId;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "User has been updated successfully",
                    JsonConvert.SerializeObject(model));

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "User update failed",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetUsers")]
        public ActionResult GetUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.System).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("QueryUsers")]
        public ActionResult QueryUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Include(x => x.UserRoles).ToList();
                res = res.Where(x => x.Type == UserType.System && !x.Name.ToLower().Contains("super")).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpDelete]
        [Route("DeleteUser")]
        public ActionResult DeleteUser(string id)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "User", "User Delete Attempt",
                    JsonConvert.SerializeObject(id));
                new UserRepository(_context).Delete(id);
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "User", "User Deleted Successfully.",
                    JsonConvert.SerializeObject(id));
                return Ok(new { Message = "User Deleted Successfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "User", "User Delete Failed.",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetUser")]
        public ActionResult GetUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.Id == id).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.DistrictId,
                    District = x.District?.Name,
                    x.RegionId,
                    Region = x.Region?.Name,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        #region Regional User
        [HttpPost]
        [Route("createregionaluser")]
        public async Task<ActionResult> CreateRegionalUser(User model)
        {
            using var userManager = _serviceProvider.GetService<UserManager<User>>();
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanCreateRegionalUsers"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "Regional User Creation Attempt",
                    JsonConvert.SerializeObject(model.UserName));
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Type = UserType.Regional;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "Regional User creation failed",
                    JsonConvert.SerializeObject(model.UserName));
                    return BadRequest(WebHelpers.ProcessException(result));
                }
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "Regional User has been created Successfully",
                    JsonConvert.SerializeObject(model.UserName));
                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                   .AddLog(SystemLogType.Insert, usr.UserName,
                   "User", "Regional User creation failed",
                   JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRegionalUsers")]
        public ActionResult GetRegionalUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.Regional && !x.Name.ToLower().Contains("super")).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.RegionId,
                    Region = x.Region?.Name,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("QueryRegionalUsers")]
        public ActionResult QueryRegionalUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Include(x => x.UserRoles).ToList();
                res = res.Where(x => x.Type == UserType.Regional && !x.Name.ToLower().Contains("super")).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.RegionId,
                    Region = x.Region?.Name,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        #endregion

        #region District User
        [HttpPost]
        [Route("createdistrictuser")]
        public async Task<ActionResult> CreateDistrictUser(User model)
        {
            using var userManager = _serviceProvider.GetService<UserManager<User>>();
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanCreateDistrictUsers"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "District User Creation Attempt",
                    JsonConvert.SerializeObject(model.UserName));
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Type = UserType.District;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "District User creation failed",
                    JsonConvert.SerializeObject(model.UserName));
                    return BadRequest(WebHelpers.ProcessException(result));
                }
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "User", "District User has been created Successfully",
                    JsonConvert.SerializeObject(model.UserName));
                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                   .AddLog(SystemLogType.Insert, usr.UserName,
                   "User", "District User creation failed",
                   JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetDistrictUsers")]
        public ActionResult GetDistrictUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.District && !x.Name.ToLower().Contains("super")).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.DistrictId,
                    x.District?.RegionId,
                    District = x.District?.Name,
                    Region = x.District?.Region?.Name,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("QueryDistrictUsers")]
        public ActionResult QueryDistrictUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Include(x => x.UserRoles).ToList();
                res = res.Where(x => x.Type == UserType.District && !x.Name.ToLower().Contains("super")).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    x.DistrictId,
                    x.District?.RegionId,
                    District = x.District?.Name,
                    Region = x.District?.Region?.Name,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        #endregion

        [HttpGet]
        [Route("GetClaims")]
        [AllowAnonymous]
        public ActionResult GetClaims()
        {
            try
            {
                var data = _context.RoleClaims.Select(x => x.ClaimValue).Distinct().ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRoles")]
        [AllowAnonymous]
        public ActionResult GetRoles()
        {
            try
            {
                var data = _context.Roles.Where(x => !x.IsDeleted && !x.Name.ToLower().Contains("super")).Include(x => x.RoleClaims).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.NormalizedName,
                    Type = x.Type.ToString(),
                    Claims = x.RoleClaims.Select(c => c.ClaimValue).ToList()
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRole/{id}")]
        public ActionResult GetRole(string id)
        {
            try
            {
                var data = _context.Roles.Where(x => x.Id == id).Include(x => x.RoleClaims).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.NormalizedName,
                    Type = x.Type.ToString(),
                    Claims = x.RoleClaims.Select(c => c.ClaimValue).ToList()
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRolesbytype")]
        public ActionResult GetRolesByType(UserType type)
        {
            try
            {
                var data = _context.Roles.Where(x => x.Type == type && !x.Name.ToLower().Contains("super")).Include(x => x.RoleClaims).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.NormalizedName,
                    Type = x.Type.ToString(),
                    Claims = x.RoleClaims.Select(c => c.ClaimValue).ToList()
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("CreateRole")]
        public ActionResult CreateRole(Role model)
        {
            using var roleManager = _serviceProvider.GetService<RoleManager<Role>>();
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanCreateRoles"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(_context, SystemLogType.Insert, usr.UserName,
                    "Role", "Role Creation Attempt",
                    JsonConvert.SerializeObject(model));

                model.NormalizedName = model.Name;
                var claims = model.Claims;
                var existingRole = _context.Roles.FirstOrDefault(x => x.Name == model.Name);
                if (existingRole == null)
                {
                    var res = roleManager.CreateAsync(model);
                    if (res.Result.Succeeded)
                    {
                        //var role = _context.Roles.First(x => x.Name == model.Name);
                        foreach (var c in claims)
                        {
                            roleManager.AddClaimAsync(model,
                        new Claim(GenericProperties.Privilege, c)).Wait();
                        }
                    }
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(_context, SystemLogType.Insert, usr.UserName,
                    "Role", "Role Creation Failed",
                    JsonConvert.SerializeObject(model.Id));
                    return BadRequest("There is an existing role with the same name. Consider updating the role.");
                }
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(_context, SystemLogType.Insert, usr.UserName,
                    "Role", "Role has been created successfully",
                    JsonConvert.SerializeObject(model.Id));
                return Created("CreateRole", new { model.Id, Message = "Role has been created successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Insert, usr.UserName,
                    "Role", "Role Creation Failed",
                    JsonConvert.SerializeObject(model.Id));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("UpdateRole")]
        public ActionResult UpdateRole(Role model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanUpdateRoles"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "Role", "Role Update Attempt",
                    JsonConvert.SerializeObject(model));
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var claims = model.Claims;
                var role = _context.Roles.FirstOrDefault(x => x.Id == model.Id);
                if (role != null)
                {
                    role.Name = model.Name;
                    role.NormalizedName = model.Name;
                    role.Type = model.Type;
                    role.UpdatedAt = DateTime.Now.ToUniversalTime();
                    _context.SaveChanges();

                    var roleClaims = _context.RoleClaims.Where(x => x.RoleId == model.Id);
                    _context.RoleClaims.RemoveRange(roleClaims);
                    _context.SaveChanges();

                    foreach (var c in claims)
                    {
                        _context.RoleClaims.Add(new RoleClaim
                        {
                            ClaimType = GenericProperties.Privilege,
                            ClaimValue = c,
                            RoleId = model.Id
                        });
                    }
                    _context.SaveChanges();
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "Role", "Role Update Failure",
                    JsonConvert.SerializeObject(model));
                    return NotFound("Please check the role id.");
                }
                
                new LogsRepository(_context)
                   .AddLog(SystemLogType.Update, usr.UserName,
                   "Role", "Role Updated Successfully",
                   JsonConvert.SerializeObject(model));
                _context.SaveChanges();
                return Created("UpdateRole", new { model.Id, Message = "Role has been updated successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "Role", "Role Update Failure",
                    JsonConvert.SerializeObject(model));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpDelete]
        [Route("DeleteRole")]
        public ActionResult DeleteRole(string id)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                if (!StringGenerators.CheckPrivilege(usr, "CanDeleteRoles"))
                    return BadRequest("You do not have the privilege to perform this action");
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "Role", "Role Delete Attempt",
                    JsonConvert.SerializeObject(id));
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var role = _context.Roles.FirstOrDefault(x => x.Id == id);
                if (role != null)
                {
                    var roleClaims = _context.RoleClaims.Where(x => x.RoleId == id);
                    _context.RoleClaims.RemoveRange(roleClaims);

                    _context.Roles.Remove(role);
                }
                else
                {
                    new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "Role", "Role Delete Failure",
                    JsonConvert.SerializeObject(id));
                    return NotFound("Please check the role id.");
                }
                _context.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "Role", "Role Deleted Successfully",
                    JsonConvert.SerializeObject(id));
                return Ok(new { Message = "Role Deleted Successfully" });
            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Delete, usr.UserName,
                    "Role", "Role Delete Failure",
                    JsonConvert.SerializeObject(id));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Authorize]
        [Route("UpdateProfile")]
        public ActionResult UpdateProfile(User model)
        {
            var uId = User.FindFirst("Id")?.Value;
            var usr = _context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "Profile Update Attempt",
                    JsonConvert.SerializeObject(model));
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow;
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                db.SaveChanges();
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "Profile has been updated successfully",
                    JsonConvert.SerializeObject(model));
                return Created("UpdateProfile", new { model.Id, Message = "Profile has been updated successfully" });

            }
            catch (Exception ex)
            {
                new LogsRepository(_context)
                    .AddLog(SystemLogType.Update, usr.UserName,
                    "User", "Profile Update Failure",
                    JsonConvert.SerializeObject(model));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());

            var uId = User.FindFirst("Id")?.Value;
            var usr = context.Users.FirstOrDefault(x => x.Id == uId);
            try
            {
                using var userMan = _serviceProvider.GetService<UserManager<User>>();
                new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password Change Attempt",
                    JsonConvert.SerializeObject(""));
                var user = context.Users.FirstOrDefault(x => x.Id == uId);
                var result = await userMan.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

                if (!result.Succeeded)
                {
                    new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password Change Failed",
                    JsonConvert.SerializeObject(result));
                    return BadRequest(WebHelpers.ProcessException(result));
                }
                
                new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password changed sucessfully.",
                    JsonConvert.SerializeObject(""));
                return Ok(new { Message = "Password changed sucessfully." });
            }
            catch (Exception ex)
            {
                new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password Change Failed",
                    JsonConvert.SerializeObject(ex));
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("resetpassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());

            var uId = User.FindFirst("Id")?.Value;
            var usr = context.Users.FirstOrDefault(x => x.Id == uId);
            using var userMan = _serviceProvider.GetService<UserManager<User>>();

            try
            {
                if(!StringGenerators.CheckPrivilege(usr, "CanResetPassword"))
                    return BadRequest("You do not have the privilege to perform this action");
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password Reset Attempt",
                    JsonConvert.SerializeObject(""));
                
                //usr = context.Users.Where(x => x.UserName == model.UserName && !x.Hidden && !x.IsDeleted).FirstOrDefault();
                var user = await userMan.FindByNameAsync(model.UserName);
                if (user == null) return NotFound("Unknown Username");
                var result = await userMan.RemovePasswordAsync(user);
                if (result.Succeeded)
                {
                    var res = await userMan.AddPasswordAsync(user, model.Password);
                    if (!res.Succeeded) return BadRequest(WebHelpers.ProcessException(res));
                }
                else
                {
                    new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, user.UserName,
                    "User", "Password Reset Failed",
                    JsonConvert.SerializeObject(result));
                    return BadRequest(WebHelpers.ProcessException(result));
                }

                
                new LogsRepository(context)
                   .AddLog(context, SystemLogType.PasswordChange, user.UserName,
                   "User", "Password Reset Successful.",
                   JsonConvert.SerializeObject(""));
                context.SaveChanges();
                return Ok(new { Message = "Password Reset Successful" });
            }
            catch (Exception e)
            {
                new LogsRepository(context)
                    .AddLog(context, SystemLogType.PasswordChange, usr.UserName,
                    "User", "Password Reset Failed",
                    JsonConvert.SerializeObject(e));
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }

        //[HttpGet]
        //[AllowAnonymous]
        //[Route("createbulkdistrictusers")]
        //public async Task<ActionResult> CreateBulkDistrictUsers(string passcode)
        //{
        //    try
        //    {
        //        if (passcode.ToLower() != "kenkuts") throw new Exception("Wrong Passcode");
        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk District User Creation Attempt",
        //            JsonConvert.SerializeObject(""));
                
        //        var roleRepo = new RoleRepository(_context);
        //        var role = roleRepo.GetByName("District Administrator");
        //        var password = "P@ssw0rd";
        //        var date = DateTime.Now;

        //        //get all districts
        //        var districts = _context.Districts.Where(x => x.Id > 0).ToList();
        //        var extUsers = _context.Users.Where(x => !x.IsDeleted).ToList();
        //        var newUsers = new List<User>();
        //        foreach(var d in districts)
        //        {
        //            var username = $"district_{d.Code}".ToLower();
        //            var ext = extUsers.FirstOrDefault(x => x.UserName.ToLower() == username);
        //            if (ext != null) continue;
        //            newUsers.Add(new User
        //            {
        //                Id = Guid.NewGuid().ToString(),
        //                CreatedAt = date,
        //                UpdatedAt = date,
        //                Name = $"{d.Name.ToUpper()} DISTRICT USER",
        //                DistrictId = d.Id,
        //                Type = UserType.District,
        //                UserName = username,
        //                Email = $"{username}@ec.gh",
        //                Role = role.Name,
        //                Locked = false
        //            });
        //        }
        //        foreach( var user in newUsers)
        //        {
        //            var result = await userManager.CreateAsync(user, password).ConfigureAwait(true);

        //            if (result.Succeeded)
        //            {
        //                var usr = userManager.FindByNameAsync(user.UserName).Result;
        //                roleRepo.AddToRole(usr.Id, role.Name);
        //            }
        //            else
        //            {
        //                new LogsRepository(_context)
        //                .AddLog(SystemLogType.Insert, "System",
        //                "User", "District User creation failed",
        //                JsonConvert.SerializeObject(result));
        //                throw new Exception($"Could not create a distric user for {user.UserName}");
        //            }
        //        }
                
        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk District User has been created Successfully",
        //            JsonConvert.SerializeObject(""));
        //        _context.SaveChanges();
        //        return Created("CreateUser", new { Message = "Bulk User creation Successful" });
        //    }
        //    catch (Exception ex)
        //    {
        //        new LogsRepository(_context)
        //           .AddLog(SystemLogType.Insert, "System",
        //           "User", "Bulk District User creation failed",
        //           JsonConvert.SerializeObject(ex));
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //[Route("createbulkregionalusers")]
        //public async Task<ActionResult> CreateBulkRegionalUsers(string passcode)
        //{
        //    try
        //    {
        //        if (passcode.ToLower() != "kenkuts") throw new Exception("Wrong Passcode");
        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk Regional User Creation Attempt",
        //            JsonConvert.SerializeObject(""));

        //        var roleRepo = new RoleRepository(_context);
        //        var role = roleRepo.GetByName("Regional Administrator");
        //        var password = "P@ssw0rd";
        //        var date = DateTime.Now;

        //        //get all regions
        //        var regions = _context.Regions.Where(x => x.Id > 0).ToList();
        //        var extUsers = _context.Users.Where(x => !x.IsDeleted).ToList();
        //        var newUsers = new List<User>();
        //        foreach (var d in regions)
        //        {
        //            var username = $"region_{d.Code}".ToLower();
        //            var ext = extUsers.FirstOrDefault(x => x.UserName.ToLower() == username);
        //            if (ext != null) continue;
        //            newUsers.Add(new User
        //            {
        //                Id = Guid.NewGuid().ToString(),
        //                CreatedAt = date,
        //                UpdatedAt = date,
        //                Name = $"{d.Name.ToUpper()} REGIONAL USER",
        //                DistrictId = d.Id,
        //                Type = UserType.District,
        //                UserName = username,
        //                Email = $"{username}@ec.gh",
        //                Role = role.Name,
        //                Locked = false
        //            });
        //        }
        //        foreach (var user in newUsers)
        //        {
        //            var result = await userManager.CreateAsync(user, password).ConfigureAwait(true);

        //            if (result.Succeeded)
        //            {
        //                var usr = userManager.FindByNameAsync(user.UserName).Result;
        //                roleRepo.AddToRole(usr.Id, role.Name);
        //            }
        //            else
        //            {
        //                new LogsRepository(_context)
        //                .AddLog(SystemLogType.Insert, "System",
        //                "User", "Regional User creation failed",
        //                JsonConvert.SerializeObject(result));
        //                throw new Exception($"Could not create a regional user for {user.UserName}");
        //            }
        //        }

        //        new LogsRepository(_context)
        //            .AddLog(SystemLogType.Insert, "System",
        //            "User", "Bulk Regional User has been created Successfully",
        //            JsonConvert.SerializeObject(""));
        //        _context.SaveChanges();
        //        return Created("CreateUser", new { Message = "Bulk User creation Successful" });
        //    }
        //    catch (Exception ex)
        //    {
        //        new LogsRepository(_context)
        //           .AddLog(SystemLogType.Insert, "System",
        //           "User", "Bulk Regional User creation failed",
        //           JsonConvert.SerializeObject(ex));
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

    }
}
