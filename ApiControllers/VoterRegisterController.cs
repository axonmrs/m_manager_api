﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using MManager.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class VoterRegisterController : BaseController<VoterRegister>
    {
        private readonly IConfiguration configuration;
        public VoterRegisterController(ApplicationDbContext context, IConfiguration configuration) : base(context)
        {
            this.configuration = configuration;
        }

        public override async Task<ActionResult> Post(VoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(VoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VoterRegister.Where(x => x.Id > 0 && x.Reviewed && !x.HasDuplicate && !x.OnExceptionList);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }
                var total = raw.Count();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList().Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.VoterId,
                    x.RegisteredBy,
                    x.PollingStationCode,
                    x.PollingStationName,
                    x.PollingStationId,
                    PollingStation = x.PollingStation?.Name,
                    ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.Surname,
                    x.OtherNames,
                    x.PhoneNumber,
                    x.DateOfBirth,
                    x.EstimatedAge,
                    x.IsAgeEstimated,
                    Sex = x.Sex.ToString(),
                    x.IsDisabled,
                    x.IsVisuallyImpaired,
                    x.Reviewed,
                    templates.First(p => x.Id == p.VoterRegisterId)?.Photo
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var res = _context.VoterRegister.Where(x => x.Id == id).ToList();
                var ids = res.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.VoterId,
                    x.RegisteredBy,
                    x.PollingStationCode,
                    x.PollingStationName,
                    x.PollingStationId,
                    PollingStation = x.PollingStation?.Name,
                    ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.Surname,
                    x.OtherNames,
                    x.PhoneNumber,
                    x.DateOfBirth,
                    x.EstimatedAge,
                    x.IsAgeEstimated,
                    Sex = x.Sex.ToString(),
                    x.ResidentialAddress,
                    x.ResidentialTown,
                    x.ResidentialDistrictId,
                    ResidentialDistrict = x.ResidentialDistrict?.Name,
                    ResidentialRegion = x.ResidentialDistrict?.Region?.Name,
                    x.IdentificationTypeId,
                    IdentificationType = x.IdentificationType?.Name,
                    x.IdentificationNumber,
                    x.IdentificationExpiry,
                    x.FatherName,
                    x.MotherName,
                    x.HomeTown,
                    x.HomeTownAddress,
                    x.HomeTownDistrictId,
                    HomeTownDistrict = x.HomeTownDistrict?.Name,
                    HomeTownRegion = x.HomeTownDistrict?.Region?.Name,
                    x.IsDisabled,
                    x.IsVisuallyImpaired,
                    x.HearingImpaired,
                    x.Leper,
                    x.AmputatedHandsRight,
                    x.AmputatedHandsLeft,
                    x.MissingFingersLeft,
                    x.MissingFingersRight,
                    x.Reviewed,
                    x.HasDuplicate,
                    x.OnExceptionList,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.GuarantorOneName,
                    x.GuarantorTwoName,
                    x.GuarantorOneVoterId,
                    x.GuarantorTwoVoterId,
                    x.CardPrintCount
                }).FirstOrDefault();
                if (data.HasDuplicate)
                    return BadRequest("This record has duplicate records.");
                if (data.OnExceptionList)
                    return BadRequest("This record is on the exception list.");
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(VoterRegisterFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter).Where(x => x.Reviewed && !x.HasDuplicate);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }


                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });

                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterId,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.PollingStationId,
                        PollingStation = x.PollingStation?.Name,
                        ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                        Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                        District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                        Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.EstimatedAge,
                        x.IsAgeEstimated,
                        Sex = x.Sex.ToString(),
                        Photo = "data:image/jpeg;base64," + templates.FirstOrDefault(p => x.Id == p.VoterRegisterId)?.Photo,
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("searchvoter")]
        public async Task<ActionResult> SearchVoter(SearchVoterFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter).Where(x => x.Id > 0);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }

                var total = raw.Count();
                if (filter.Size > 0)
                    raw = raw.Take(filter.Size);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });

                var ids = raw.Select(x => x.Id).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterId,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.PollingStationId,
                        PollingStation = x.PollingStation?.Name,
                        ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                        Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                        District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                        Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.EstimatedAge,
                        x.IsAgeEstimated,
                        x.HasDuplicate,
                        x.OnExceptionList,
                        Sex = x.Sex.ToString(),
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryunreviewed")]
        public async Task<ActionResult> QueryUnreviewedVoters(UnreviewedVoterFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter).Where(x => !x.Reviewed && !x.HasDuplicate && !x.OnExceptionList);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }

                var total = raw.Count();
                if (filter.Size > 0)
                    raw = raw.Take(filter.Size);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });

                var ids = raw.Select(x => x.Id).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.VoterId,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.Surname,
                        x.OtherNames,
                        x.EstimatedAge,
                        Sex = x.Sex.ToString()
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("voterreview")]
        public ActionResult VoterReview(VoterReviewVM data)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var voters = _context.VoterRegister.Where(x => data.VoterIds.Contains(x.VoterId)).Distinct().ToList();
                
                foreach (var v in voters)
                {
                    v.Reviewed = true;
                    v.ReviewedBy = user.UserName;
                    v.ReviewedOn = DateTime.UtcNow;
                }
                _context.SaveChanges();
                new LogsRepository(_context).AddLog(SystemLogType.VoterRegister, user.UserName, "Voter Review", "Reviewing of Voters", JsonConvert.SerializeObject(data));
                return Ok(new { Data = new List<object>(), Message = "Reviewed Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        
        [HttpGet]
        [Route("searchbyvoterid")]
        public async Task<ActionResult> SearchByVoterId(string voterId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VoterRegister.Where(x => x.VoterId == voterId);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Voter Record Found for the given Voter Id" });

                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterId,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.PollingStationId,
                        PollingStation = x.PollingStation?.Name,
                        ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                        Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                        District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                        Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.EstimatedAge,
                        x.IsAgeEstimated,
                        x.HasDuplicate,
                        x.OnExceptionList,
                        Sex = x.Sex.ToString(),
                        Photo = "data:image/jpeg;base64," + templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    }).FirstOrDefault();
                if (data.HasDuplicate)
                    return BadRequest("This record has duplicate records.");
                if (data.OnExceptionList)
                    return BadRequest("This record is on the exception list.");
                return Ok(new
                {
                    data,
                    message = "Successful"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("printcard")]
        public async Task<ActionResult> PrintCard(int id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VoterRegister.Where(x => x.Id == id);

                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Voter Record Found for the given Voter Id" });

                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new PrintCardViewModel
                    {
                        age = x.EstimatedAge,
                        dateOfBirth = x.DateOfBirth.Value,
                        fatherName = x.FatherName,
                        motherName = x.MotherName,
                        hometown = x.HomeTown,
                        hometownAddress = x.HomeTownAddress,
                        hometownDistrict = x.HomeTownDistrict?.Name,
                        hometownRegion = x.HomeTownDistrict?.Region?.Name,
                        idNumber = x.IdentificationNumber,
                        idType = x.IdentificationType?.Name,
                        isDisabled = x.IsDisabled,
                        isVisuallyImpaired = x.IsVisuallyImpaired,
                        otherNames = x.OtherNames,
                        phoneNumber = x.PhoneNumber,
                        residentialAddress = x.ResidentialAddress,
                        residentialDistrict = x.ResidentialDistrict?.Name,
                        residentialTown = x.ResidentialTown,
                        residentialRegion = x.ResidentialDistrict?.Region?.Name,
                        sex = x.Sex.ToString(),
                        surname = x.Surname,
                        voterId = x.VoterId,
                        photo = Convert.FromBase64String(templates.First(p => x.Id == p.VoterRegisterId)?.Photo),
                        pollingStationCode = x.PollingStationCode,
                        pollingStationName = x.PollingStationName,
                        name = $"{x.OtherNames} {x.Surname}",
                        dateOfRegistration = x.CreatedAt,
                        cardPrintCount = x.CardPrintCount,
                        challenges = x.Challenges,
                        leper = x.Leper,
                        amputatedHandsL = x.AmputatedHandsLeft,
                        amputatedHandsR = x.AmputatedHandsRight,
                        missingFingersL = x.MissingFingersLeft,
                        missingFingersR = x.MissingFingersRight,
                        hideLabels = true
                    }).FirstOrDefault();
                if (data == null) throw new Exception("Print error");
                var bytes = ReportHelpers.generateReport("card.rdl", new List<PrintCardViewModel> { data });
                var filename = ReportHelpers.saveReport(data.voterId+"card without label.pdf", bytes);
                var link = Path.Combine(HttpContext.Request.Host.Value, "TempFiles", data.voterId + "card without label.pdf");
                link = link.Replace("\\", "/");
                return Ok(new { link });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("searchbyvoteridfortransfer")]
        public async Task<ActionResult> SearchByVoterIdForTransfer(string voterId, string psCode)
        {
            try
            {
                using var logsContext = new mrs_logsContext();
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VoterRegister.Where(x => x.VoterId == voterId && x.PollingStationCode.ToLower() == psCode.ToLower());

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Voter Record Found for the given Voter Id" });

                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var x = raw.OrderBy(x => x.Surname).FirstOrDefault();
                if (x == null)
                {
                    var rec = logsContext.FixedDuplicates.FirstOrDefault(x => x.OldVoterId == voterId);
                    if (rec != null)
                    {
                        return BadRequest($"The given Voter ID was replaced with a new Voter Id. The new Id is {rec.NewVoterId}.");
                    }
                    else
                    {
                        var recx = logsContext.FixedSupplementaryDuplicateVoterIds.FirstOrDefault(x => x.OldVoterId == voterId);
                        if (recx != null)
                        {
                            return BadRequest($"The given Voter ID was replaced with a new Voter Id. The new Id is {recx.NewVoterId}.");
                        }
                        else
                        {
                            return BadRequest($"There is not voter record with the given voter Id and the associated Polling Station.");
                        }
                    }
                }
                if (x.HasDuplicate)
                    return BadRequest("This record has duplicate records.");
                if (x.OnExceptionList)
                    return BadRequest("This record is on the exception list.");
                var ea = _context.Stations.Where(e => e.Id == x.PollingStationId)
                    .Include(x => x.ElectoralArea.Constituency).Select(x => x.ElectoralArea).First();
                var cnst = ea.Constituency;
                var specialPollingStation = _context.Stations
                    .FirstOrDefault(x => x.ElectoralArea.ConstituencyId == cnst.Id
                    && x.Code == $"{cnst.Code}SPS");


                var data = new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterId,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.PollingStationId,
                        PollingStation = x.PollingStation?.Name,
                        ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                        Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                        District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                        Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.EstimatedAge,
                        x.IsAgeEstimated,
                        x.HasDuplicate,
                        x.OnExceptionList,
                        Sex = x.Sex.ToString(),
                        SpecialPollingStation = specialPollingStation?.Name,
                        SpecialPollingStationId = specialPollingStation?.Id,
                        SpecialPollingStationCode = specialPollingStation?.Code,
                    SpecialElectoralArea = specialPollingStation?.ElectoralArea?.Name,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    };
                return Ok(new
                {
                    data,
                    message = "Successful"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
