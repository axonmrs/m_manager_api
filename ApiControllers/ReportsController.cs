﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using MManager.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public ReportsController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }


        [HttpPost]
        [Route("provisionalvoteregister")]
        public async Task<ActionResult> ProvisionalVoterListReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                
                var res = new ReportsRepository(_context).GenerateProvisionalVoterListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("provisionalvoteregister/word")]
        public async Task<ActionResult> DownloadProvisionalVoterListWordReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadProvisionalVoterListWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("provisionalvoteregister/excel")]
        public async Task<ActionResult> DownloadProvisionalVoterListExcelReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadProvisionalVoterListExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("finalvoteregister")]
        public async Task<ActionResult> FinalVoterListReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateFinalVoterListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("finalvoteregister/word")]
        public async Task<ActionResult> DownloadFinalVoterListWordReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadFinalVoterListWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("finalvoteregister/excel")]
        public async Task<ActionResult> DownloadFinalVoterListExcelReport(VoterListReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.PollingStationId <= 0)
                    throw new Exception("Please select a polling station");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadFinalVoterListExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        //[HttpPost]
        //[Route("voterlist")]
        //public async Task<ActionResult> VoterListReport(VoterListReportFilter filter)
        //{
        //    try
        //    {
        //        var uId = User.FindFirst("Id")?.Value;
        //        var user = _context.Users.FirstOrDefault(x => x.Id == uId);
        //        if (filter == null) throw new Exception("filter cannot be null");
        //        var res = new ReportsRepository(_context).GenerateVoterListReport(filter, user);
        //        return Ok(res);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

        //[HttpPost]
        //[Route("voterlist/word")]
        //public async Task<ActionResult> DownloadVoterListWordReport(VoterListReportFilter filter)
        //{
        //    try
        //    {
        //        var uId = User.FindFirst("Id")?.Value;
        //        var user = _context.Users.FirstOrDefault(x => x.Id == uId);
        //        if (filter == null) throw new Exception("filter cannot be null");
        //        var res = new ReportsRepository(_context).DownloadVoterListWordReport(filter, user);
        //        return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}

        ////[HttpPost]
        ////[Route("voterlist/pdf")]
        ////public async Task<ActionResult> DownloadVoterListPdfReport(VoterListReportFilter filter)
        ////{
        ////    try
        ////    {
        ////        var uId = User.FindFirst("Id")?.Value;
        ////        var user = _context.Users.FirstOrDefault(x => x.Id == uId);
        ////        if (filter == null) throw new Exception("filter cannot be null");
        ////        var res = new ReportsRepository(_context).DownloadVoterListPdfReport(filter, user);
        ////        return Ok(new { Data = "data:application/pdf;base64," + res });
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return BadRequest(WebHelpers.ProcessException(ex));
        ////    }
        ////}

        //[HttpPost]
        //[Route("voterlist/excel")]
        //public async Task<ActionResult> DownloadVoterListExcelReport(VoterListReportFilter filter)
        //{
        //    try
        //    {
        //        var uId = User.FindFirst("Id")?.Value;
        //        var user = _context.Users.FirstOrDefault(x => x.Id == uId);
        //        if (filter == null) throw new Exception("filter cannot be null");
        //        var res = new ReportsRepository(_context).DownloadVoterListExcelReport(filter, user);
        //        return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(WebHelpers.ProcessException(ex));
        //    }
        //}


        [HttpPost]
        [Route("regionalbvrpackingslip/list")]
        public async Task<ActionResult> RegionalBvrPackingSlipListReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).GenerateRegionalBVRPackingSlipReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regionalbvrpackingslip/word")]
        public async Task<ActionResult> DownloadRegionalBvrPackingSlipWordReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadRegionalBVRPackingSlipWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regionalbvrpackingslip/excel")]
        public async Task<ActionResult> DownloadRegionalBvrPackingSliptExcelReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadRegionalBVRPackingSlipExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }


        [HttpPost]
        [Route("regionalbvdpackingslip/list")]
        public async Task<ActionResult> RegionalBvdPackingSlipListReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).GenerateRegionalBVDPackingSlipReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regionalbvdpackingslip/word")]
        public async Task<ActionResult> DownloadRegionalBvdPackingSlipWordReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadRegionalBVDPackingSlipWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regionalbvdpackingslip/excel")]
        public async Task<ActionResult> DownloadRegionalBvdPackingSliptExcelReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadRegionalBVDPackingSlipExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }



        [HttpPost]
        [Route("districtbvrpackingslip/list")]
        public async Task<ActionResult> DistrictBvrPackingSlipListReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).GenerateDistrictBVRPackingSlipReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districtbvrpackingslip/word")]
        public async Task<ActionResult> DownloadDistrictBvrPackingSlipWordReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadAllDistrictsBVRPackingSlipWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districtbvrpackingslip/excel")]
        public async Task<ActionResult> DownloadDistrictBvrPackingSliptExcelReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadDistrictBVRPackingSlipExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }


        [HttpPost]
        [Route("districtbvdpackingslip/list")]
        public async Task<ActionResult> DistrictBvdPackingSlipListReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).GenerateDistrictBVDPackingSlipReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districtbvdpackingslip/word")]
        public async Task<ActionResult> DownloadDistrictBvdPackingSlipWordReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadDistrictBVDPackingSlipWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districtbvdpackingslip/excel")]
        public async Task<ActionResult> DownloadDistrictBvdPackingSliptExcelReport(PackingSlipReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).DownloadDistrictBVDPackingSlipExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("uploadedmrtfiles/list")]
        public async Task<ActionResult> UploadedMrtFilesListReport(UploadedMrtFilesReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.DistrictId <= 0)
                    throw new Exception("Please select a district");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateUploadedMrtFilesListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("uploadedmrtfiles/word")]
        public async Task<ActionResult> DownloadUploadedMrtFilesWordReport(UploadedMrtFilesReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.DistrictId <= 0)
                    throw new Exception("Please select a district");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadUploadedMrtFilesWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("uploadedmrtfiles/excel")]
        public async Task<ActionResult> DownloadUploadedMrtFilesExcelReport(UploadedMrtFilesReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.DistrictId <= 0)
                    throw new Exception("Please select a district");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadUploadedMrtFilesExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("regions/list")]
        public async Task<ActionResult> RegionsListReport(SettingsReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateRegionsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("regions/word")]
        public async Task<ActionResult> DownloadRegionsWordReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadRegionsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("regions/excel")]
        public async Task<ActionResult> DownloadRegionsExcelReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadRegionsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("districts/list")]
        public async Task<ActionResult> DistrictsListReport(SettingsReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateDistrictsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("districts/word")]
        public async Task<ActionResult> DownloadDistrictsWordReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadDistrictsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("districts/excel")]
        public async Task<ActionResult> DownloadDistrictsExcelReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadDistrictsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("constituencies/list")]
        public async Task<ActionResult> ConstituenciesListReport(SettingsReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateConstituenciesListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("constituencies/word")]
        public async Task<ActionResult> DownloadConstituenciesWordReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadConstituenciesWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("constituencies/excel")]
        public async Task<ActionResult> DownloadConstituenciesExcelReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadConstituenciesExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("electoralareas/list")]
        public async Task<ActionResult> ElectoralAreasListReport(SettingsReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GenerateElectoralAreasListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("electoralareas/word")]
        public async Task<ActionResult> DownloadElectoralAreasWordReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadElectoralAreasWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("electoralareas/excel")]
        public async Task<ActionResult> DownloadElectoralAreasExcelReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadElectoralAreasExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("pollingstations/list")]
        public async Task<ActionResult> PollingStationsListReport(SettingsReportFilter filter)
        {
            try
            {
                if (filter == null) throw new Exception("filter cannot be null");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);

                var res = new ReportsRepository(_context).GeneratePollingStationsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("pollingstations/word")]
        public async Task<ActionResult> DownloadPollingStationsWordReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadPollingStationsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("pollingstations/excel")]
        public async Task<ActionResult> DownloadPollingStationsExcelReport(SettingsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var res = new ReportsRepository(_context).DownloadPollingStationsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationsbydistricts/list")]
        public async Task<ActionResult> ApplicationsByDistrictsListReport(ApplicationsByDistrictReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicationsByDistrictsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationsbydistricts/word")]
        public async Task<ActionResult> ApplicationsByDistrictsWordReport(ApplicationsByDistrictReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicationsByDistrictsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationsbydistricts/excel")]
        public async Task<ActionResult> ApplicationsByDistrictsExcelReport(ApplicationsByDistrictReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicationsByDistrictsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationstatsbyregions/list")]
        public async Task<ActionResult> ApplicationStatsByRegionsListReport(ApplicationStatsByRegionsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.RegionId == 0) throw new Exception("Please select a region");
                var res = new ReportsRepository(_context).ApplicationStatsByRegionsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationstatsbyregions/word")]
        public async Task<ActionResult> ApplicationStatsByRegionsWordReport(ApplicationStatsByRegionsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.RegionId == 0) throw new Exception("Please select a region");
                var res = new ReportsRepository(_context).ApplicationStatsByRegionsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicationstatsbyregions/excel")]
        public async Task<ActionResult> ApplicationStatsByRegionsExcelReport(ApplicationStatsByRegionsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                if (filter.RegionId == 0) throw new Exception("Please select a region");
                var res = new ReportsRepository(_context).ApplicationStatsByRegionsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicantdetails/list")]
        public async Task<ActionResult> ApplicantDetailsListReport(ApplicantDetailsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicantDetailsListReport(filter, user);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicantdetails/word")]
        public async Task<ActionResult> ApplicantDetailsWordReport(ApplicantDetailsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicantDetailsWordReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("applicantdetails/excel")]
        public async Task<ActionResult> ApplicantDetailsExcelReport(ApplicantDetailsReportFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (filter == null) throw new Exception("filter cannot be null");
                var res = new ReportsRepository(_context).ApplicantDetailsExcelReport(filter, user);
                return Ok(new { Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + res });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }
}
