﻿using GemBox.Document;
using GemBox.Document.Tables;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using MManager.RegistrationModels;
using MManager.Repositories;
using MManager.VerificationModels;
using Newtonsoft.Json;
using QRCoder;
using System;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Color = GemBox.Document.Color;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DownloadsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public DownloadsController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }
        [HttpGet]
        [Route("downloadregistrationsetup")]
        public ActionResult DownloadRegistrationSetup(string passCode)
        {
            try
            {
                var dt = DateTime.UtcNow.ToFileTime();
                //todo: log this request and the data that is exported
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.District) throw new Exception("Not permitted");
                var district = _context.Districts.FirstOrDefault(x => x.Id == user.DistrictId);
                if (district == null) throw new Exception("District Error: Please contact the system administrator");
                var region = _context.Regions.FirstOrDefault(x => x.Id == district.RegionId);
                if (region == null) throw new Exception("Region Error: Please contact the system administrator");

                //todo:check that the user has the right privilege
                var stations = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == district.Id).Include(x => x.ElectoralArea.Constituency.District.Region).ToList();
                if (!stations.Any()) throw new Exception("There are no polling stations in your district");
                var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == district.Id && x.DistrictConfirmed);
                if (!devices.Any()) throw new Exception("There are no registration devices assigned to your district");
                var eas = _context.ElectoralAreas.Where(x => x.Constituency.DistrictId == district.Id).Include(x => x.Constituency.District.Region).ToList();
                if (!eas.Any()) throw new Exception("There are no electoral areas in your district");

                #region file location things and copy sqlite db into folder
                var regFolder = dt + "_" + district.Code + "_Reg_Setup_Data";
                var tempFolder = Path.Combine(Directory.GetCurrentDirectory(),"wwwroot", "TempFiles", regFolder);
                var zipFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder + ".zip");
                var mainDbPath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "registration_db.db");

                if (System.IO.File.Exists(zipFolder)) System.IO.File.Delete(zipFolder);
                if (Directory.Exists(tempFolder)) Directory.Delete(tempFolder, true);
                Directory.CreateDirectory(tempFolder);
                var newDbPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, "registration_db.db");
                System.IO.File.Copy(mainDbPath, newDbPath, true);
                #endregion

                //update the sqlite db with values
                var optionsBuilder = new DbContextOptionsBuilder<registration_dbContext>();
                optionsBuilder.UseSqlite($"DataSource=./wwwroot/TempFiles/{regFolder}/registration_db.db;");
                var regSqliteContext = new registration_dbContext(optionsBuilder.Options);

                //check and update stations
                var existingstations = regSqliteContext.PollingStations.Where(x => x.Id > 0).ToList();
                regSqliteContext.PollingStations.RemoveRange(existingstations);
                regSqliteContext.SaveChanges();
                foreach (var rec in stations)
                {
                    regSqliteContext.PollingStations.Add(new RegistrationModels.PollingStations
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        ElectoralArea = $"{rec.ElectoralArea?.Code}-{rec.ElectoralArea?.Name}",
                        Constituency = rec.ElectoralArea?.Constituency?.Code,
                        District = rec.ElectoralArea?.Constituency?.District?.Code,
                        Region = rec.ElectoralArea?.Constituency?.District?.Region?.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update electoral areas
                var existingeas = regSqliteContext.ElectoralAreas.Where(x => x.Id > 0).ToList();
                regSqliteContext.ElectoralAreas.RemoveRange(existingeas);
                regSqliteContext.SaveChanges();
                foreach (var rec in eas)
                {
                    regSqliteContext.ElectoralAreas.Add(new RegistrationModels.ElectoralAreas
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        Constituency = rec.Constituency?.Code,
                        District = rec.Constituency?.District?.Code,
                        Region = rec.Constituency?.District?.Region?.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update devices
                var secHelp = new SecurityHelpers();
                var existingdevices = regSqliteContext.Devices.Where(x => x.Id > 0).ToList();
                regSqliteContext.Devices.RemoveRange(existingdevices);
                regSqliteContext.SaveChanges();
                foreach (var rec in devices)
                {
                    regSqliteContext.Devices.Add(new RegistrationModels.Devices
                    {
                        Id = rec.Id,
                        SerialNumber = rec.SerialNumber,
                        IdentificationCode = rec.IdentificationCode,
                        Sequence = rec.Sequence,
                        ActivationCode = rec.ActivationCode,
                        ActivationCodeHash = secHelp.HashString(rec.ActivationCode, passCode),
                        District = "",
                        Region = "",
                        Reference = rec.Reference
                    });
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update idtypes
                var existingIds = regSqliteContext.IdTypes.Where(x => x.Id > 0).ToList();
                regSqliteContext.IdTypes.RemoveRange(existingIds);
                regSqliteContext.SaveChanges();
                var idTypes = _context.IdTypes.Where(x => x.Id > 0).ToList();
                foreach (var rec in idTypes)
                {
                    regSqliteContext.IdTypes.Add(new IdTypes
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update regions
                var existingRegs = regSqliteContext.Regions.Where(x => x.Id > 0).ToList();
                regSqliteContext.Regions.RemoveRange(existingRegs);
                regSqliteContext.SaveChanges();
                var regions = _context.Regions.Where(x => x.Id > 0).ToList();
                foreach (var rec in regions)
                {
                    regSqliteContext.Regions.Add(new Regions
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update districts
                var existingDist = regSqliteContext.Districts.Where(x => x.Id > 0).ToList();
                regSqliteContext.Districts.RemoveRange(existingDist);
                var districts = _context.Districts.Where(x => x.Id > 0).ToList();
                foreach (var rec in districts)
                {
                    regSqliteContext.Districts.Add(new Districts
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name,
                        RegionId = rec.RegionId
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update reasons
                var existingReas = regSqliteContext.Reasons.Where(x => x.Id > 0).ToList();
                regSqliteContext.Reasons.RemoveRange(existingReas);
                var reasons = _context.ChallengeReasons.Where(x => x.Id > 0).ToList();
                foreach (var rec in reasons)
                {
                    regSqliteContext.Reasons.Add(new Reasons
                    {
                        Id = rec.Id,
                        Code = rec.Code,
                        Name = rec.Name
                    });
                    rec.LastSyncedAt = DateTime.Now;
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                #region App Settings
                var appsettings = regSqliteContext.AppSettings.ToList();
                foreach (var set in appsettings)
                {
                    switch (set.Name)
                    {
                        case "district":
                            set.Value = district.Code;
                            break;
                        case "region":
                            set.Value = region.Code;
                            break;
                        case "no_of_stations":
                            set.Value = stations.Count().ToString();
                            break;
                        case "no_of_devices":
                            set.Value = devices.Count().ToString();
                            break;
                        case "date_generated":
                            set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            break;
                        case "copied":
                            set.Value = "false";
                            break;
                        case "backed_up":
                            set.Value = "false";
                            break;
                        case "last_backup_date":
                            set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            break;
                    }
                }
                regSqliteContext.SaveChanges();
                #endregion

                #region setup config qrcode
                var regCofig = new RegConfig
                {
                    district = district.Code,
                    region = region.Code,
                    no_of_devices = devices.Count().ToString(),
                    no_of_stations = stations.Count().ToString(),
                    date_generated = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                    last_backup_date = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                };
                var serializedConfig = JsonConvert.SerializeObject(regCofig);
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(serializedConfig, QRCodeGenerator.ECCLevel.H);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);
                var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, "activator.jpg");
                qrCodeImage.Save(imagePath);
                #endregion

                //todo: generate auth files for the sd card, transfer stick and backup stick

                //zip the folder and return it 
                ZipFile.CreateFromDirectory(tempFolder, zipFolder, CompressionLevel.Optimal, true);

                new LogsRepository(_context).AddLog(SystemLogType.Download, user.UserName, "RegistrationData", "Download Registration Setup Data", JsonConvert.SerializeObject(zipFolder));
                var link = Path.Combine(HttpContext.Request.Host.Value, "TempFiles", regFolder+".zip");
                //link.Replace("wwwroot\\wwwroot", "wwwroot");
                link = link.Replace("\\", "/");
                return Ok(new { link });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("downloadverificationsetup")]
        public ActionResult DownloadVerificationSetup(string passCode)
        {
            try
            {
                var dt = DateTime.UtcNow.ToFileTime();
                var fileDocx = (dt + "_Verification_Device_Activations.docx").Replace("/", "");
                //todo: log this request and the data that is exported
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type != UserType.District) throw new Exception("Not permitted");
                var district = _context.Districts.FirstOrDefault(x => x.Id == user.DistrictId);
                if (district == null) throw new Exception("District Error: Please contact the system administrator");
                var region = _context.Regions.FirstOrDefault(x => x.Id == district.RegionId);
                if (region == null) throw new Exception("Region Error: Please contact the system administrator");

                //todo:check that the user has the right privilege
                var stations = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == district.Id).Include(x => x.ElectoralArea.Constituency.District.Region).ToList();
                if (!stations.Any()) throw new Exception("There are no polling stations in your district");
                var devices = _context.VerificationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == district.Id && x.DistrictConfirmed).ToList();
                if (!devices.Any()) throw new Exception("There are no confirmed verification devices for your district");
                var voters = _context.VoterRegister.Where(x => x.PollingStation.ElectoralArea.Constituency.DistrictId == district.Id).Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
                if (!voters.Any()) throw new Exception("There are no registered voters in your district");

                var ids = voters.Select(x => x.Id).ToList();
                var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
                        .Select(x => new VoterRegisterTemplates { Photo = x.Photo, CompositeTemplate = x.CompositeTemplate, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();

                #region file location things and copy sqlite db into folder
                var regFolder = dt + "_" + district.Code + "_Ver_Setup_Data";
                var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder);
                var zipFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder + ".zip");
                var mainDbPath = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "verification_db.db");

                if (System.IO.File.Exists(zipFolder)) System.IO.File.Delete(zipFolder);
                if (Directory.Exists(tempFolder)) Directory.Delete(tempFolder, true);
                Directory.CreateDirectory(tempFolder);
                var newDbPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, "verification_db.db");
                System.IO.File.Copy(mainDbPath, newDbPath, true);
                #endregion

                //update the sqlite db with values
                var optionsBuilder = new DbContextOptionsBuilder<verification_dbContext>();
                optionsBuilder.UseSqlite($"DataSource=./wwwroot/TempFiles/{regFolder}/verification_db.db;");
                var regSqliteContext = new verification_dbContext(optionsBuilder.Options);                

                //check and update devices
                var secHelp = new SecurityHelpers();
                var existingdevices = regSqliteContext.Devices.Where(x => x.Id > 0).ToList();
                regSqliteContext.Devices.RemoveRange(existingdevices);
                regSqliteContext.SaveChanges();
                foreach (var rec in devices)
                {
                    regSqliteContext.Devices.Add(new VerificationModels.Devices
                    {
                        Id = rec.Id,
                        Imei = rec.Imei,
                        IdentificationCode = rec.IdentificationCode,
                        Sequence = rec.Sequence,
                        ActivationCode = rec.ActivationCode,
                        ActivationCodeHash = secHelp.HashString(rec.ActivationCode, passCode),
                        District = "",
                        Region = "",
                        Reference = rec.Reference
                    });
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //check and update voters
                var existingVoters = regSqliteContext.RegisteredVoters.Where(x => x.Id > 0).ToList();
                regSqliteContext.RegisteredVoters.RemoveRange(existingVoters);
                regSqliteContext.SaveChanges();
                foreach (var rec in voters)
                {
                    var bio = bios.FirstOrDefault(x => x.VoterRegisterId == rec.Id);
                    if (bio == null) continue;
                    regSqliteContext.RegisteredVoters.Add(new VerificationModels.RegisteredVoters
                    {
                        Id = rec.Id,
                        VoterId = rec.VoterId,
                        PsName = rec.PollingStationName,
                        PsCode = rec.PollingStationCode,
                        Surname = rec.Surname,
                        OtherNames = rec.OtherNames,
                        DateOfBirth = rec.DateOfBirth.Value.ToString(),
                        EstimatedAge = rec.EstimatedAge,
                        Sex = rec.Sex.ToString(),
                        PhoneNumber = rec.PhoneNumber,
                        Template = Convert.FromBase64String(bio.CompositeTemplate),
                        Photo = Convert.FromBase64String(bio.Photo)
                    });
                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();

                //document formating
                var year = DateTime.Now.Year;
                DocumentModel templateDoc = new DocumentModel();
                var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, templateDoc);
                templateDoc.Styles.Add(titleStyle);
                templateDoc.Styles.Add(new CharacterStyle("Emphasis"));
                var strongStyle = (CharacterStyle)templateDoc.Styles.GetOrAdd(StyleTemplateType.Strong);
                var subtitleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, templateDoc);
                templateDoc.Styles.Add(subtitleStyle);
                var section = new Section(templateDoc);
                // Add default (odd) header.
                section.HeadersFooters.Add(
                    new HeaderFooter(templateDoc, HeaderFooterType.HeaderDefault,
                        new Paragraph(templateDoc, "ELECTORAL COMMISION GHANA")
                        {
                            ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                        },
                        new Paragraph(templateDoc, $"ACTIVATION CODES FOR VERIFICATION DEVICES {year}")
                        {
                            ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                        },
                        new Paragraph(templateDoc, $"Location - {district.Name} ({district.Region.Name})")
                        {
                            ParagraphFormat = new ParagraphFormat() { 
                                Alignment = HorizontalAlignment.Center}
                        }));

                // Add default (odd) footer with page number.
                var footerTable = new Table(templateDoc,
                new TableRow(templateDoc,
                    new TableCell(templateDoc, new Paragraph(templateDoc, $"Generated On {DateTime.UtcNow.ToLongDateString()}")
                    { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                    new TableCell(templateDoc, new Paragraph(templateDoc,
                        new Field(templateDoc, FieldType.Page),
                        new Run(templateDoc, " of "),
                        new Field(templateDoc, FieldType.NumPages))
                    { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                    ));
                footerTable.TableFormat.DefaultCellPadding = new Padding(10);
                footerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                var footerTableBorders = footerTable.TableFormat.Borders;
                footerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                footerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                section.HeadersFooters.Add(
                    new HeaderFooter(templateDoc, HeaderFooterType.FooterDefault,
                    footerTable));
                var blocks = section.Blocks;
                templateDoc.Sections.Add(section);
                var pageSetup = section.PageSetup;
                pageSetup.PageMargins.Top = 5;
                pageSetup.PageMargins.Bottom = 5;
                pageSetup.PaperType = PaperType.A4;
                pageSetup.PageMargins.Left = 10;
                pageSetup.PageMargins.Right = 10;

                #region App Settings
                var batch = "BATCH_" + DateTime.UtcNow.ToFileTime();
                var appsettings = regSqliteContext.AppSettings.ToList();
                foreach (var set in appsettings)
                {
                    switch (set.Name)
                    {
                        case "batch":
                            set.Value = batch;
                            break;
                        case "district":
                            set.Value = district.Name;
                            break;
                        case "region":
                            set.Value = region.Name;
                            break;
                        case "no_of_stations":
                            set.Value = stations.Count().ToString();
                            break;
                        case "no_of_devices":
                            set.Value = devices.Count().ToString();
                            break;
                        case "number_of_registered_voters":
                            set.Value = voters.Count().ToString();
                            break;
                        case "date_generated":
                            set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            break;
                            //case "copied":
                            //    set.Value = "false";
                            //    break;
                            //case "backed_up":
                            //    set.Value = "false";
                            //    break;
                            //case "last_backup_date":
                            //    set.Value = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                            //    break;
                    }
                }
                regSqliteContext.SaveChanges();
                #endregion


                //check and update stations
                var existingstations = regSqliteContext.PollingStations.Where(x => x.Id > 0).ToList();
                regSqliteContext.PollingStations.RemoveRange(existingstations);
                regSqliteContext.SaveChanges();
                for (var i = 0; i < stations.Count; i++)
                {
                    Table tb = new Table(templateDoc);
                    if (stations.Count > i)
                    {
                        var rec1 = stations[i];
                        regSqliteContext.PollingStations.Add(new VerificationModels.PollingStations
                        {
                            Id = rec1.Id,
                            Code = rec1.Code,
                            Name = rec1.Name,
                            ElectoralArea = rec1.ElectoralArea?.Code,
                            Constituency = rec1.ElectoralArea?.Constituency?.Code,
                            District = rec1.ElectoralArea?.Constituency?.District?.Code,
                            Region = rec1.ElectoralArea?.Constituency?.District?.Region?.Name
                        });
                        rec1.LastSyncedAt = DateTime.Now;

                        var regCofig1 = new VDConfig
                        {
                            batch = batch,
                            district = district.Name,
                            region = region.Name,
                            ps_code = rec1.Code,
                            ps_name = rec1.Name,
                            no_of_devices = devices.Count().ToString(),
                            no_of_stations = stations.Count().ToString(),
                            date_generated = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                        };
                        var serializedConfig1 = JsonConvert.SerializeObject(regCofig1);
                        var imagePath1 = ImageHelpers.GenerateDeviceActivationQRCode(serializedConfig1, regFolder);
                        Picture qrCode1 = new Picture(templateDoc, imagePath1, 100, 100);

                        tb = new PollingStationsRepository(_context).BuildPsTable(i+1, templateDoc, qrCode1, rec1, district.Name, district.Region.Name);
                    }

                    i = i + 1;
                    Table tb2 = new Table(templateDoc);
                    if (stations.Count > i)
                    {
                        var rec2 = stations[i];
                        regSqliteContext.PollingStations.Add(new VerificationModels.PollingStations
                        {
                            Id = rec2.Id,
                            Code = rec2.Code,
                            Name = rec2.Name,
                            ElectoralArea = rec2.ElectoralArea?.Code,
                            Constituency = rec2.ElectoralArea?.Constituency?.Code,
                            District = rec2.ElectoralArea?.Constituency?.District?.Code,
                            Region = rec2.ElectoralArea?.Constituency?.District?.Region?.Name
                        });
                        rec2.LastSyncedAt = DateTime.Now;

                        var regCofig2 = new VDConfig
                        {
                            batch = batch,
                            district = district.Name,
                            region = region.Name,
                            ps_code = rec2.Code,
                            ps_name = rec2.Name,
                            no_of_devices = devices.Count().ToString(),
                            no_of_stations = stations.Count().ToString(),
                            date_generated = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                        };
                        var serializedConfig2 = JsonConvert.SerializeObject(regCofig2);
                        var imagePath2 = ImageHelpers.GenerateDeviceActivationQRCode(serializedConfig2, regFolder);
                        Picture qrCode2 = new Picture(templateDoc, imagePath2, 100, 100);
                        tb2 = new PollingStationsRepository(_context).BuildPsTable(i+1, templateDoc, qrCode2, rec2, district.Name, district.Region.Name);

                    }

                    var xTable = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, tb)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                },
                new TableCell(templateDoc, tb2)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                }));
                    //xTable.TableFormat.DefaultCellPadding = new Padding(0, 2);
                    xTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                    var xTableBorders = xTable.TableFormat.Borders;
                    xTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                    xTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);

                    foreach (ParagraphFormat paragraphFormat in xTable
                        .GetChildElements(true, ElementType.Paragraph)
                        .Cast<Paragraph>()
                        .Select(p => p.ParagraphFormat))
                    {
                        paragraphFormat.KeepLinesTogether = true;
                        paragraphFormat.KeepWithNext = true;
                    }
                    blocks.Add(xTable);
                    var size = 12;
                    if (i == 11 || (i + size) % size == 11)
                        blocks.Add(new Paragraph(templateDoc, new SpecialCharacter(templateDoc, SpecialCharacterType.PageBreak)));

                }
                _context.SaveChanges();
                regSqliteContext.SaveChanges();
                var tmp = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, fileDocx);
                templateDoc.Save(tmp);



                #region setup config qrcode
                //var regCofig = new VDConfig
                //{
                //    batch = batch,
                //    district = district.Code,
                //    region = region.Code,
                //    no_of_devices = devices.Count().ToString(),
                //    no_of_stations = stations.Count().ToString(),
                //    date_generated = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                //};
                //var serializedConfig = JsonConvert.SerializeObject(regCofig);
                //QRCodeGenerator qrGenerator = new QRCodeGenerator();
                //QRCodeData qrCodeData = qrGenerator.CreateQrCode(serializedConfig, QRCodeGenerator.ECCLevel.H);
                //QRCode qrCode = new QRCode(qrCodeData);
                //Bitmap qrCodeImage = qrCode.GetGraphic(20);
                //var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", regFolder, "activator.jpg");
                //qrCodeImage.Save(imagePath);
                #endregion

                //todo: generate auth files for the sd card, transfer stick and backup stick

                //zip the folder and return it 
                ZipFile.CreateFromDirectory(tempFolder, zipFolder, CompressionLevel.Optimal, true);

                new LogsRepository(_context).AddLog(SystemLogType.Download, user.UserName, "VerificationData", "Download Verification Setup Data", JsonConvert.SerializeObject(zipFolder));
                var link = Path.Combine(HttpContext.Request.Host.Value, "TempFiles", regFolder + ".zip");
                //link.Replace("wwwroot\\wwwroot", "wwwroot");
                link = link.Replace("\\", "/");
                return Ok(new { link });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
