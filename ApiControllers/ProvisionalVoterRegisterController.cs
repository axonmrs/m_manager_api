﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    public class ProvisionalVoterRegisterController : BaseController<VoterRegister>
    {
        private readonly IConfiguration configuration;
        public ProvisionalVoterRegisterController(ApplicationDbContext context, IConfiguration configuration) : base(context)
        {
            this.configuration = configuration;
        }

        public override async Task<ActionResult> Post(VoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(VoterRegister model)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(new { Message = "Not implemented" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _context.VoterRegister.Where(x=> x.Id > 0 && !x.Reviewed && !x.HasDuplicate && !x.OnExceptionList);

                if (user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }
                var total = raw.Count();                
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });
                var ids = raw.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates
                    {
                        Photo = x.Photo,
                        Id = x.Id,
                        VoterRegisterId = x.VoterRegisterId
                    }).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList().Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.VoterId,
                    x.RegisteredBy,
                    x.PollingStationCode,
                    x.PollingStationName,
                    x.PollingStationId,
                    PollingStation = x.PollingStation?.Name,
                    ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.Surname,
                    x.OtherNames,
                    x.PhoneNumber,
                    x.DateOfBirth,
                    x.EstimatedAge,
                    x.IsAgeEstimated,
                    Sex = x.Sex.ToString(),
                    x.IsDisabled,
                    x.IsVisuallyImpaired,
                    x.Reviewed,
                    templates.First(p=> x.Id == p.VoterRegisterId)?.Photo
                }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(int id)
        {
            try
            {
                var res = _context.VoterRegister.Where(x => x.Id == id).ToList();
                var ids = res.Select(x => x.Id).ToList();
                var templates = _context.VoterRegisterTemplates
                    .Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Reference,
                    x.VoterId,
                    x.RegisteredBy,
                    x.PollingStationCode,
                    x.PollingStationName,
                    x.PollingStationId,
                    PollingStation = x.PollingStation?.Name,
                    ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                    Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                    District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                    Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                    x.Surname,
                    x.OtherNames,
                    x.PhoneNumber,
                    x.DateOfBirth,
                    x.EstimatedAge,
                    x.IsAgeEstimated,
                    Sex = x.Sex.ToString(),
                    x.ResidentialAddress,
                    x.ResidentialTown,
                    x.ResidentialDistrictId,
                    ResidentialDistrict = x.ResidentialDistrict?.Name,
                    ResidentialRegion = x.ResidentialDistrict?.Region?.Name,
                    x.IdentificationTypeId,
                    IdentificationType = x.IdentificationType?.Name,
                    x.IdentificationNumber,
                    x.IdentificationExpiry,
                    x.FatherName,
                    x.MotherName,
                    x.HomeTown,
                    x.HomeTownAddress,
                    x.HomeTownDistrictId,
                    HomeTownDistrict = x.HomeTownDistrict?.Name,
                    HomeTownRegion = x.HomeTownDistrict?.Region?.Name,
                    x.IsDisabled,
                    x.IsVisuallyImpaired,
                    x.HearingImpaired,
                    x.Leper,
                    x.AmputatedHandsRight,
                    x.AmputatedHandsLeft,
                    x.MissingFingersLeft,
                    x.MissingFingersRight,
                    x.Reviewed,
                    x.HasDuplicate,
                    x.OnExceptionList,
                    Photo = "data:image/jpeg;base64," + templates.First(p => x.Id == p.VoterRegisterId)?.Photo,
                    x.CreatedAt,
                    x.GuarantorOneName,
                    x.GuarantorTwoName,
                    x.GuarantorOneVoterId,
                    x.GuarantorTwoVoterId,
                    x.CardPrintCount
                }).FirstOrDefault();
                if (data.HasDuplicate)
                    return BadRequest("This record has duplicate records.");
                if (data.OnExceptionList)
                    return BadRequest("This record is on the exception list.");
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(VoterRegisterFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                var raw = _repository.Query(filter).Where(x=> !x.Reviewed && !x.HasDuplicate && !x.OnExceptionList);
                
                if(user.Type != UserType.System)
                {
                    var psIds = new List<int>();
                    if (user.Type == UserType.Regional)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                    }
                    else if (user.Type == UserType.District)
                    {
                        psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                    }
                    raw = raw.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
                }
                

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Record Found" });

                var ids = raw.Select(x => x.Id).ToList();
                var data = raw.OrderBy(x => x.Surname).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Reference,
                        x.VoterId,
                        x.RegisteredBy,
                        x.PollingStationCode,
                        x.PollingStationName,
                        x.PollingStationId,
                        PollingStation = x.PollingStation?.Name,
                        ElectoralArea = x.PollingStation?.ElectoralArea?.Name,
                        Constituency = x.PollingStation?.ElectoralArea?.Constituency?.Name,
                        District = x.PollingStation?.ElectoralArea?.Constituency?.District?.Name,
                        Region = x.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name,
                        x.Surname,
                        x.OtherNames,
                        x.PhoneNumber,
                        x.DateOfBirth,
                        x.EstimatedAge,
                        Sex = x.Sex.ToString(),
                        x.Reviewed
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
