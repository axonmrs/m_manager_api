﻿using GemBox.Document.Drawing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        public readonly IServiceProvider _serviceProvider;
        public DashboardController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            //context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            //_logsContext = new mrs_logsContext();
        }
        [HttpGet]
        [Route("getstats")]
        public ActionResult GetStats()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            try
            {
                //var uId = User.FindFirst("Id")?.Value;
                //var user = context.Users.FirstOrDefault(x => x.Id == uId);
                //var logsDb = new mrs_logsContext();
                //var registrations = logsDb.TemporalVoterRegister.Where(x => x.Id > 0);
                //var voters = context.VoterRegister.Where(x => x.Id > 0);
                //var psCodes = new List<string>();
                //if (user.Type == UserType.Regional)
                //{
                //    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                //    registrations = registrations.Where(x => psCodes.Contains(x.PollingStationCode));
                //    voters = voters.Where(x => psCodes.Contains(x.PollingStationCode));
                //}

                ////var regs = registrations.Select(x => new { x.Status, x.VoterId, x.AbisRequestId, x.IsUpdate }).ToList();


                //var data = new
                //{
                //    Processed = voters.Count(),
                //    Registrations = registrations.DistinctBy(x=> x.VoterId).Count(),
                //    Duplicates = registrations.DistinctBy(x => x.VoterId).Count(x => x.Status == TemporalVoterRegisterStatus.PushedToAbisWithDuplicate),
                //    WithIssues = registrations.DistinctBy(x => x.VoterId).Count(x => x.Status == TemporalVoterRegisterStatus.WithIssues  || x.Status == TemporalVoterRegisterStatus.PushFailed || x.Status == TemporalVoterRegisterStatus.RejectedByAbis),
                //    Pending = registrations.DistinctBy(x => x.VoterId).Count(x => x.Status == TemporalVoterRegisterStatus.Pending && !x.IsUpdate),
                //    Updates = registrations.Count(x => x.IsUpdate),
                //    Adjudications = registrations.DistinctBy(x => x.VoterId).Count(x => x.Status == TemporalVoterRegisterStatus.PushedToAbisAwaitingAdjudication)
                //};
                //return Ok(data);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getduplicates")]
        public ActionResult GetDuplicates()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            using var logsContext = new mrs_logsContext();
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var data = logsContext.DuplicateVoters.Where(x => x.Id > 0 && x.Status == DuplicateVoterRecordStatus.Pending);
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations
                        .Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId)
                        .Include(x => x.ElectoralArea.Constituency.District)
                        .Select(x => x.Code).ToList();
                    data = data.Where(x => psCodes.Contains(x.TemporalVoterRegister.PollingStationCode));
                }
                var res = data.Include(x => x.Hits).DistinctBy(x=> x.VoterId).OrderByDescending(x => x.ReceivedAt).Take(7)
                    .Select(x=> new { x.VoterId, Hits = x.Hits.Count()}).ToList();
                if (!res.Any()) return Ok(new List<Duplicate>());
                var result = res.Where(x=> x.Hits > 0).Select(x => new Duplicate
                    {
                        VoterId = x.VoterId,
                        Hits = x.Hits
                    }).ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        

        [HttpGet]
        [Route("getsexstats")]
        public ActionResult GetSexStats()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            //using var logsContext = new mrs_logsContext();
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);
                var recs = context.VoterRegister.Where(x => x.Id > 0);
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    recs = recs.Where(x => psCodes.Contains(x.PollingStationCode));
                }
                var eligibleVoters = recs.GroupBy(x=> x.Sex).Select(x => new { Sex = x.Key, count = x.Count() }).ToList();
                
                var data = new
                {
                    Male = eligibleVoters.First(x => x.Sex == Models.Sex.Male).count,
                    Female = eligibleVoters.First(x => x.Sex == Models.Sex.Female).count
            };
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getdistrictstats")]
        public ActionResult GetDistrictStats()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            //using var logsContext = new mrs_logsContext();
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);
                var eligibleVoters = context.VoterRegister.Where(x => x.Id > 0);
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    eligibleVoters = eligibleVoters.Where(x => psCodes.Contains(x.PollingStationCode));
                }
                var eligibleVoterz = eligibleVoters.GroupBy(x => x.PollingStationCode)
                    .Select(x => new { PsCode = x.Key, count = x.Count() }).ToList();
                psCodes = eligibleVoterz.Select(x => x.PsCode).Distinct().ToList();
                var psCodex = context.Stations.Where(x=> psCodes.Contains(x.Code))
                    .Include(x => x.ElectoralArea.Constituency.District).Select(x => new
                {
                    District = x.ElectoralArea.Constituency.District.Name,
                    PsCode = x.Code
                }).ToList();
                var districts = psCodex.Select(x => x.District).Distinct().ToList();
                var total = eligibleVoterz.Select(x => x.count).Sum();
                var data = new List<DistrictStat>();
                foreach (var district in districts)
                {
                    var Codes = psCodex.Where(x => x.District == district).Select(x => x.PsCode).ToList(); 
                    var count = eligibleVoterz.Where(x => Codes.Contains(x.PsCode)).Select(x=> x.count).Sum();
                    var perc = ((count * 1.0) / total) * 100.0;
                    data.Add(new DistrictStat
                    {
                        District = district,
                        Count = count,
                        Percentage = perc.ToString("#.0000")
                    });

                }
                var list = data.OrderByDescending(x => x.Count).Take(5).ToList();
                var dists = list.Select(x => x.District).ToList();
                var chartData = data.Where(x => dists.Contains(x.District)).Select(x => new
                {
                    y = x.Count,
                    label = x.District,
                    percentage = x.Percentage
                }).ToList();
                return Ok(new { chartData, list });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        

        [HttpGet]
        [Route("getregionstats")]
        public ActionResult GetRegionStats()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);
                var eligibleVoters = context.VoterRegister.Where(x => x.Id > 0);
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    eligibleVoters = eligibleVoters.Where(x => psCodes.Contains(x.PollingStationCode));
                }
                var eligibleVoterz = eligibleVoters.GroupBy(x => x.PollingStationCode)
                    .Select(x => new { PsCode = x.Key, count = x.Count() }).ToList();
                psCodes = eligibleVoterz.Select(x => x.PsCode).Distinct().ToList();
                var psCodex = context.Stations.Where(x => psCodes.Contains(x.Code))
                    .Include(x => x.ElectoralArea.Constituency.District).Select(x => new
                    {
                        Region = x.ElectoralArea.Constituency.District.Region.Name,
                        RegionCode = x.ElectoralArea.Constituency.District.Region.Code,
                        PsCode = x.Code
                    }).ToList();
                var regions = psCodex.Select(x => x.Region).Distinct().ToList();
                var total = eligibleVoterz.Select(x => x.count).Sum();
                var data = new List<RegionStat>();
                foreach (var region in regions)
                {
                    var regCode = psCodex.First(x => x.Region == region).RegionCode;
                    var Codes = psCodex.Where(x => x.Region == region).Select(x => x.PsCode).ToList();
                    var count = eligibleVoterz.Where(x => Codes.Contains(x.PsCode)).Select(x => x.count).Sum();
                    var perc = ((count * 1.0) / total) * 100.0;
                    data.Add(new RegionStat
                    {
                        Region = region,
                        Code = regCode,
                        Count = count,
                        Percentage = perc.ToString("#.0000")
                    });

                }
                var chartData = data.OrderBy(x => x.Code).Select(x => new
                {
                    y = x.Count,
                    label = x.Region,
                    percentage = x.Percentage
                }).ToList();
                return Ok(chartData);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        

        [HttpGet]
        [Route("getagegroupstats")]
        public ActionResult GetAgeGroupStats()
        {
            using var context = new ApplicationDbContext(_serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = context.Users.FirstOrDefault(x => x.Id == uId);

                var recs = context.VoterRegister.Where(x => x.Id > 0);
                var psCodes = new List<string>();
                if (user.Type == UserType.Regional)
                {
                    psCodes = context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                    recs = recs.Where(x => psCodes.Contains(x.PollingStationCode));
                }
                var eligibleVoterz = recs.GroupBy(x => x.EstimatedAge)
                    .Select(x => new { Age = x.Key, count = x.Count() }).ToList();
                var grp1 = eligibleVoterz.Where(x => x.Age >= 18 && x.Age <=19).Sum(x=> x.count);
                var grp2 = eligibleVoterz.Where(x => x.Age >= 20 && x.Age <= 29).Sum(x => x.count);
                var grp3 = eligibleVoterz.Where(x => x.Age >= 30 && x.Age <= 39).Sum(x => x.count);
                var grp4 = eligibleVoterz.Where(x => x.Age >= 40 && x.Age <= 49).Sum(x => x.count);
                var grp5 = eligibleVoterz.Where(x => x.Age >= 50 && x.Age <= 59).Sum(x => x.count);
                var grp6 = eligibleVoterz.Where(x => x.Age >= 60).Sum(x => x.count);
                var data = new List<AgeGroupStat>()
                {
                    new AgeGroupStat
                    {
                        Label = "18 - 19",
                        Y = grp1 > 0? grp1 : 0.0,
                        Order = 0,
                    },
                    new AgeGroupStat
                    {
                        Label = "20 - 29",
                        Y = grp2 > 0? grp2: 0.0,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "30 - 39",
                        Y = grp3 > 0? grp3 : 0.0,
                        Order = 2,
                    },
                    new AgeGroupStat
                    {
                        Label = "40 - 49",
                        Y = grp4 > 0? grp4 : 0.0,
                        Order = 3,
                    },
                    new AgeGroupStat
                    {
                        Label = "50 - 59",
                        Y = grp5 > 0? grp5 : 0.0,
                        Order = 4,
                    },
                    new AgeGroupStat
                    {
                        Label = "60 - ",
                        Y = grp6 > 0? grp6 : 0.0,
                        Order = 5,
                    }
                };
                data = data.OrderBy(x => x.Order).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        
    }
}
