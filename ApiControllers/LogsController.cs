﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Filters;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MManager.ApiControllers
{
    [ApiController]
    [Route("api/[controller]")]
    [DisableRequestSizeLimit]
    [AllowAnonymous]
    public class LogsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LogsController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        [HttpPost]
        [Route("getsystemlogs")]
        public async Task<ActionResult> GetSystemLogs(Pager pager)
        {
            try
            {
                if (pager == null) throw new Exception("Invalid POST parameters");
                if (pager.Page == 0) throw new Exception("The page should be more than 0");

                var recs = _context.SystemLogs.Where(x => x.Id > 0)
                    .OrderByDescending(x => x.Date)
                    .Take(pager.Page * pager.Size)
                    .Select(x =>
                    new {
                        x.Date,
                        x.User,
                        x.Model,
                        x.Notes,
                        Type = x.Type.ToString()
                    }).ToList();

                return Ok(recs);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("getsystemlogswithpayload")]
        public async Task<ActionResult> GetSystemLogsWithPayload(Pager pager)
        {
            try
            {
                if (pager == null) throw new Exception("Invalid POST parameters");
                if (pager.Page == 0) throw new Exception("The page should be more than 0");

                var recs = _context.SystemLogs.Where(x => x.Id > 0)
                    .OrderByDescending(x => x.Date)
                    .Take(pager.Page * pager.Size)
                    .Select(x =>
                    new {
                        x.Date,
                        x.User,
                        x.Model,
                        x.Notes,
                        Type = x.Type.ToString(),
                        x.Payload
                    }).ToList();

                return Ok(recs);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }
}
