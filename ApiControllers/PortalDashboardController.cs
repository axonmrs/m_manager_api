﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MManager.Helpers;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MManager.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PortalDashboardController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public PortalDashboardController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }
        [HttpGet]
        [Route("getstats")]
        public ActionResult GetStats()
        {
            try
            {
                var data = new
                {
                    Voters = "12.50M",
                    Duplicates = "23.50K",
                    Stations = "10.12K",
                    OldVoters = "10.54M"
                };
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getduplicates")]
        public ActionResult GetDuplicates()
        {
            try
            {
                var data = new List<Duplicate>()
                {
                    new Duplicate
                    {
                        VoterId = "ABC10001010001",
                        Name = "John Ocran",
                        Count = 2,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010002",
                        Name = "Ama Badu",
                        Count = 2,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010003",
                        Name = "Joseph Peters",
                        Count = 2,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010004",
                        Name = "Emeka Chizedu",
                        Count = 3,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010005",
                        Name = "Fred Adu Kumi",
                        Count = 7,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010006",
                        Name = "Musa Ahmed",
                        Count = 5,
                    },
                    new Duplicate
                    {
                        VoterId = "ABC10001010007",
                        Name = "Sister Asua",
                        Count = 4,
                    }
                };
                data = data.OrderByDescending(x => x.Count).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        private class Duplicate
        {
            public string VoterId { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
        }

        [HttpGet]
        [Route("getsexstats")]
        public ActionResult GetSexStats()
        {
            try
            {
                var data = new
                {
                    Male = 6.125,
                    Female = 6.375,
                    MalePercentage = 41.00,
                    FemalePercentage = 59.00
                };
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getstationstats")]
        public ActionResult GetStationStats()
        {
            try
            {
                var data = new List<StationStat>()
                {
                    new StationStat
                    {
                        Station = "Station 1",
                        Count = 17.54,
                        Percentage = 11.23,
                    },
                    new StationStat
                    {
                        Station = "Station 2",
                        Count = 20.34,
                        Percentage = 11.23,
                    },
                    new StationStat
                    {
                        Station = "Station 3",
                        Count = 12.03,
                        Percentage = 11.23,
                    },
                    new StationStat
                    {
                        Station = "Station 4",
                        Count = 19.50,
                        Percentage = 11.23,
                    },
                    new StationStat
                    {
                        Station = "Others",
                        Count = 20.45,
                        Percentage = 11.23,
                    }
                };
                var list = data.OrderByDescending(x => x.Count).ToList();
                var chartData = data.OrderBy(x => x.Station).Select(x => new
                {
                    y = x.Count,
                    label = x.Station,
                    percentage = x.Percentage
                }).ToList();
                return Ok(new { chartData, list });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        private class StationStat
        {
            public string Station { get; set; }
            public double Count { get; set; }
            public double Percentage { get; set; }
        }

        [HttpGet]
        [Route("getregionstats")]
        public ActionResult GetRegionStats()
        {
            try
            {
                var data = new List<RegionStat>()
                {
                    new RegionStat
                    {
                        Region = "Western",
                        Code = "A",
                        Count = 17.54,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Central",
                        Code = "B",
                        Count = 20.34,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 12.03,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 19.50,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 20.45,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Western",
                        Code = "A",
                        Count = 17.54,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Central",
                        Code = "B",
                        Count = 20.34,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 12.03,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 19.50,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 20.45,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Western",
                        Code = "A",
                        Count = 17.54,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Central",
                        Code = "B",
                        Count = 20.34,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 12.03,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 19.50,
                        Percentage = 11.23,
                    },
                    new RegionStat
                    {
                        Region = "Region 1",
                        Code = "C",
                        Count = 20.45,
                        Percentage = 11.23,
                    }
                };
                var chartData = data.OrderBy(x => x.Code).Select(x => new
                {
                    y = x.Count,
                    label = x.Region,
                    percentage = x.Percentage
                }).ToList();
                return Ok(chartData);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        private class RegionStat
        {
            public string Region { get; set; }
            public string Code { get; set; }
            public double Count { get; set; }
            public double Percentage { get; set; }
        }

        [HttpGet]
        [Route("getagegroupstats")]
        public ActionResult GetAgeGroupStats()
        {
            try
            {
                var data = new List<AgeGroupStat>()
                {
                    new AgeGroupStat
                    {
                        Label = "18 - 19",
                        Y = 9.54,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "20 - 29",
                        Y = 17.54,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "30 - 39",
                        Y = 12.54,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "40 - 49",
                        Y = 78.54,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "50 - 59",
                        Y = 54.54,
                        Order = 1,
                    },
                    new AgeGroupStat
                    {
                        Label = "60 - ",
                        Y = 23.54,
                        Order = 1,
                    }
                };
                data = data.OrderBy(x => x.Order).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        private class AgeGroupStat
        {
            public int Order { get; set; }
            public string Label { get; set; }
            public double Y { get; set; }
        }
    }
}
