﻿using System;
using System.Collections.Generic;

namespace MManager.Models
{
    public class VoterListReportModel
    {
        public int Id { get; set; }
        public string VoterId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public int EstimatedAge { get; set; }
        public string Sex { get; set; }
    }

    public class VoterListReportFilter
    {
        public int Size { get; set; }
        public int PollingStationId { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }
    }

    public class PackingSlipReportModel
    {
        public int Id { get; set; }
        public string IdentificationCode { get; set; }
        public string SerialNumber { get; set; }
        public string Imei { get; set; }
        public string Region { get; set; }
        public string AssignedToRegionOn { get; set; }
        public string District { get; set; }
        public string ActivationCode { get; set; }
        public string AssignedToDistrictOn { get; set; }
        public bool Confirmed { get; set; }
    }

    public class ApplicationsByDistrictsReportModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public long ExpectedApp { get; set; } 
        public long ExtractedApp { get; set; }
        public long ReceivedMrtFiles { get; set; }
        public long ExtractedMrtFiles { get; set; }
        public long PendingMrtFiles { get; set; }
    }

    public class ApplicationStatsByRegionsReportModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public long Expected { get; set; }
        public long Extracted { get; set; }
        public long Processed { get; set; }
        public long ExtractedMale { get; set; }
        public long ExtractedFemale { get; set; }
        public long ProcessedMale { get; set; }
        public long ProcessedFemale { get; set; }
    }

    public class PackingSlipReportFilter
    {
        public int Size { get; set; }
        //public long PollingStationId;
        //public long ElectoralAreaId;
        //public long ConstituencyId;
        public long DistrictId;
        public long RegionId;
    }

    public class UploadedMrtFilesReportFilter
    {
        public int Size { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }
    }

    public class ApplicationsByDistrictReportFilter
    {
        public int Size { get; set; }
        public long DistrictId;
        public long RegionId;
    }

    public class ApplicantDetailsReportFilter
    {
        public string VoterId;
    }

    public class ApplicationStatsByRegionsReportFilter
    {
        public int Size { get; set; }
        public long RegionId;
    }

    public class GenerateVoterListReportFilter
    {
        public DateTime ElectionDate { get; set; }
        public string Secret { get; set; }
    }

    public class SettingsReportFilter
    {
        public int Size { get; set; }
        public int? PollingStationId { get; set; }
        public int? ElectoralAreaId { get; set; }
        public int? ConstituencyId { get; set; }
        public int? DistrictId { get; set; }
        public int? RegionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
    public class MrtFilesReportModel
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string FileName { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Status { get; set; }
        public string UploadedBy { get; set; }
        public string UploadedAt { get; set; }
    }

    public class SetUpFileRow
    {
        public string RegName { get; set; }
        public string RegCode { get; set; }
        public string DistName { get; set; }
        public string DistCode { get; set; }
        public string ConstName { get; set; }
        public string ConstCode { get; set; }
        public string EAName { get; set; }
        public string EACode { get; set; }
        public string PSName { get; set; }
        public string PSCode { get; set; }
    }

    public class RegionsReportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class DistrictsReportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Region { get; set; }
    }

    public class ConstituenciesReportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
    }

    public class ElectoralAreasReportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Constituency { get; set; }
    }

    public class PollingStationsReportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Constituency { get; set; }
        public string ElectoralArea { get; set; }
    }

    public class DevicesRow
    {
        public string IdentificationCode { get; set; }
        public string SerialNumber { get; set; }
    }

    public class AssignedDevicesRow
    {
        public string District { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Skip { get; set; }
        public string Add { get; set; }
        public List<string> List { get; set; } = new List<string>();
    }

    public class DistrictDevicesRow
    {
        public string SerialNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string DistrictCode { get; set; }
    }

    public class ApplicantDetailsReportModel
    {
        public int Id { get; set; }
        public string Photo { get; set; }
        public string VoterId { get; set; }
        public string CreatedAt { get; set; }
        public string RegisteredBy { get; set; }
        public string PSName { get; set; }
        public string PSCode { get; set; }
        public string District { get; set; }
        public string Region { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string PhoneNumber { get; set; }
        public string DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public string Sex { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public string IdentificationTypeCode { get; set; }
        public string IdentificationType { get; set; }
        public string IdentificationNumber { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public string HomeTownDistrict { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; }
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public string Source { get; set; } = "BVR";
    }
}
