﻿using Microsoft.AspNetCore.Identity;
using MManager.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MManager.Models
{
    public class HasId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }

    public class AuditFields : HasId
    {
        //[Required]
        public string CreatedBy { get; set; }
        //[Required]
        public string ModifiedBy { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public DateTime ModifiedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public bool Locked { get; set; } = false;
        public bool Hidden { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
    }

    public class LookUp : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Notes { get; set; }
    }

    public class Role : IdentityRole
    {
        public DateTime CreatedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public bool IsDeleted { get; set; } = false;
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<RoleClaim> RoleClaims { get; set; }
        [NotMapped]
        public List<string> Claims { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < UpdatedAt);
        public bool Locked { get; set; } = false;
        public UserType Type { get; set; } = UserType.System;
    }

    public class RoleClaim : IdentityRoleClaim<string>
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        public virtual Role Role { get; set; }
    }

    public class User : IdentityUser
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.Now.ToUniversalTime();
        public bool IsDeleted { get; set; } = false;
        public bool Locked { get; set; } = true;
        public bool Hidden { get; set; } = false;
        public virtual ICollection<UserClaim> Claims { get; set; }
        public virtual ICollection<UserLogin> Logins { get; set; }
        public virtual ICollection<UserToken> Tokens { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string Role { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < UpdatedAt);
        public UserType Type { get; set; }
        public virtual Region Region { get; set; }
        public int? RegionId { get; set; }
        public virtual District District { get; set; }
        public int? DistrictId { get; set; }
    }

    public enum UserType
    {
        System,
        Regional,
        District
    }

    public class UserClaim : IdentityUserClaim<string>
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        public virtual User User { get; set; }
    }

    public class UserLogin : IdentityUserLogin<string>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public virtual User User { get; set; }
    }

    public class UserRole : IdentityUserRole<string>
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }

    public class UserToken : IdentityUserToken<string>
    {
        public virtual User User { get; set; }
    }

    public class AppSetting : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ResetRequest : HasId
    {
        public string Ip { get; set; } = "127.0.0.1";
        public DateTime Date { get; set; } = DateTime.Now;
        public string Email { get; set; }
        public string Token { get; set; }
        public bool IsActive { get; set; } = false;
    }

    public class ResetModel
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }

    public class Message : HasId
    {
        [MaxLength(128), Required]
        public string Recipient { get; set; }
        [MaxLength(128)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Subject { get; set; }
        [Required]
        public string Text { get; set; }
        public MessageStatus Status { get; set; }
        public MessageType Type { get; set; }
        [MaxLength(5000)]
        public string Response { get; set; }
        public DateTime TimeStamp { get; set; }
        [NotMapped]
        public string Attachment { get; set; }
    }

    public enum MessageType
    {
        Sms,
        Email
    }

    public enum MessageStatus
    {
        Sent,
        Received,
        Failed
    }

    public class IdType : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class Region : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual List<District> Districts { get; set; }
        public int OrderIndex { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class District : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual Region Region { get; set; }
        public int RegionId { get; set; }
        public int OrderIndex { get; set; }
        public virtual List<Constituency> Constituencies { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class Constituency : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual District District { get; set; }
        public int DistrictId { get; set; }
        public int OrderIndex { get; set; }
        public virtual List<ElectoralArea> ElectoralAreas { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class ElectoralArea : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual Constituency Constituency { get; set; }
        public int ConstituencyId { get; set; }
        public int OrderIndex { get; set; }
        public virtual List<Station> Stations { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class Station : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual ElectoralArea ElectoralArea { get; set; }
        public int ElectoralAreaId { get; set; }
        public int OrderIndex { get; set; }
        public virtual List<VotingStation> VotingStations { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class VotingStation : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public virtual Station Station { get; set; }
        public int StationId { get; set; }
        public char? FromLetter { get; set; }
        public char? ToLetter { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class ExternalSystem : AuditFields
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public string Name { get; set; }
        public string TokenSecret { get; set; } = StringGenerators.GenerateToken(16);
        public ExternalSystemType SystemType { get; set; }
        public string Description { get; set; }
    }
    public enum ExternalSystemType
    {
        Abis,
        Verifier,
        Registrar,
        FileAnalyzer,
        FileReceiver
    }

    public class UploadFile
    {
        public string File { get; set; }
        public string Filename { get; set; }
    }

    public class UploadModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string RegionCode { get; set; }
        public string DistrictCode { get; set; }
        public string ConstituencyCode { get; set; }
        public string ElectoralAreaCode { get; set; }
        public string PollingStationCode { get; set; }
        public string Type { get; set; }
    }

    public class DeviceUploadModel
    {
        public string IdentificationCode { get; set; }
        public string SerialNumber { get; set; }
        public string DistrictCode { get; set; }
    }

    public class VerDeviceUploadModel
    {
        public string IdentificationCode { get; set; }
        public string Imei { get; set; }
        public string DistrictCode { get; set; }
    }

    public class SystemLog : HasId
    {
        public DateTime Date { get; set; } = DateTime.Now;
        public string User { get; set; }
        public string Payload { get; set; }
        public string Model { get; set; }
        public string Notes { get; set; }
        public SystemLogType Type { get; set; }
    }

    public enum SystemLogType
    {
        Login,
        Logout,
        PasswordChange,
        PasswordReset,
        Insert,
        Update,
        Delete,
        Upload,
        Download,
        Device,
        EndOfDayReport,
        VoterRegister,
        Transfers,
        Exceptions,
        DataSetup
    }

    public enum Sex
    {
        Male,
        Female
    }

    public enum DuplicateStatus
    {
        Pending,
        Resolved,
        Cancelled
    }    

    public class VoterRegister : HasId
    {
        public string Reference { get; set; }
        public string VoterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; }
        public DateTime SyncedAt { get; set; }
        public string RegisteredBy { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public int? PollingStationId { get; set; }
        public virtual Station PollingStation { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public bool IsAgeEstimated { get; set; }
        public Sex Sex { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public int? ResidentialDistrictId { get; set; }
        public virtual District ResidentialDistrict { get; set; }
        public string IdentificationTypeCode { get; set; }
        public int? IdentificationTypeId { get; set; }
        public virtual IdType IdentificationType { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public int? HomeTownDistrictId { get; set; }
        public virtual District HomeTownDistrict { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; }
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewedOn { get; set; }
        public bool Reviewed { get; set; }
        public bool HasDuplicate { get; set; }
        public string Source { get; set; } = "BVR";
        public bool OnExceptionList { get; set; } = false;
    }

    public class VoterRegisterTemplates : HasId
    {
        public int VoterRegisterId { get; set; }
        public virtual VoterRegister VoterRegister { get; set; }
        [MaxLength(10240)]
        public string Photo { get; set; }
        [MaxLength(10240)]
        public string CompositeTemplate { get; set; }
    }

    public class StationUser : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(64), Required]
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
        public string Address { get; set; }
        public string BioTemplate { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
        public StationUserType Type { get; set; }
        [NotMapped]
        public string Password { get; set; }
    }

    public class StationUserModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public StationUserType Type { get; set; }
        public string Password { get; set; }
        public string BioTemplate { get; set; }
    }

    public enum StationUserType
    {
        PresidingOfficer,
        Operator,
        Technician,
        DistrictOfficer
    }

    public class VerificationUser : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(64), Required]
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
        public string Address { get; set; }
        public string BioTemplate { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
        [NotMapped]
        public string Password { get; set; }
    }

    public class VerificationUserModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string BioTemplate { get; set; }
    }

    public class RegistrationDevice : AuditFields
    {
        public string Reference { get; set; } = StringGenerators.GenerateCode(64);
        public string ActivationCode { get; set; } = StringGenerators.GenerateRandomNumber(8);
        public string AdminCode { get; set; } = StringGenerators.GenerateRandomNumber(6);
        [MaxLength(128), Required]
        public string SerialNumber { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
        [MaxLength(128)]
        public string Sequence { get; set; } = null;
        public virtual District District { get; set; }
        public int? DistrictId { get; set; }
        public bool AssignedToDistrict { get; set; } = false;
        public DateTime? AssignedToDistrictOn { get; set; }
        public string AssignedToDistrictBy { get; set; }
        public virtual Region Region { get; set; }
        public int? RegionId { get; set; }
        public bool AssignedToRegion { get; set; } = false;
        public DateTime? AssignedToRegionOn { get; set; }
        public string AssignedToRegionBy { get; set; }
        public bool RegionConfirmed { get; set; } = false;
        public DateTime? RegionConfirmedOn { get; set; }
        public string RegionConfirmedBy { get; set; }
        public bool DistrictConfirmed { get; set; } = false;
        public DateTime? DistrictConfirmedOn { get; set; }
        public string DistrictConfirmedBy { get; set; }
        public bool IsActive { get; set; }
    }

    public class RegistrationDeviceVM
    {
        [MaxLength(128), Required]
        public string SerialNumber { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
    }

    public class RegistrationDeviceUpdateVM
    {
        public long Id { get; set; }
        [MaxLength(128), Required]
        public string SerialNumber { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
    }
    public class DistrictDevicesVM
    {
        public int DistrictId { get; set; }
        public virtual List<long> DeviceIds { get; set; }
    }

    public class RegionDevicesVM
    {
        public int RegionId { get; set; }
        public virtual List<long> DeviceIds { get; set; }
    }

    public class UnassignDevicesVM
    {
        public string Reason { get; set; }
        public virtual List<long> DeviceIds { get; set; }
    }


    public class DeviceConfirmationVM
    {
        public virtual List<int> DeviceIds { get; set; }
        public virtual List<string> IdentificationCodes { get; set; }
    }
    public class UpdateDeviceSequenceVM
    {
        public string Sequence { get; set; }
        public int Id { get; set; }
    }

    public class VerificationDevice : AuditFields
    {
        public string Reference { get; set; } = StringGenerators.GenerateCode(64);
        public string ActivationCode { get; set; } = StringGenerators.GenerateRandomNumber(8);
        [MaxLength(128), Required]
        public string Imei { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
        [MaxLength(128)]
        public string Sequence { get; set; } = null;
        public virtual District District { get; set; }
        public int? DistrictId { get; set; }
        public bool AssignedToDistrict { get; set; } = false;
        public DateTime? AssignedToDistrictOn { get; set; }
        public string AssignedToDistrictBy { get; set; }
        public virtual Region Region { get; set; }
        public int? RegionId { get; set; }
        public bool AssignedToRegion { get; set; } = false;
        public DateTime? AssignedToRegionOn { get; set; }
        public string AssignedToRegionBy { get; set; }
        public bool RegionConfirmed { get; set; } = false;
        public DateTime? RegionConfirmedOn { get; set; }
        public string RegionConfirmedBy { get; set; }
        public bool DistrictConfirmed { get; set; } = false;
        public DateTime? DistrictConfirmedOn { get; set; }
        public string DistrictConfirmedBy { get; set; }
        public bool IsActive { get; set; }
    }

    public class VerificationDeviceVM
    {
        [MaxLength(128), Required]
        public string Imei { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
    }

    public class VerificationDeviceUpdateVM
    {
        public long Id { get; set; }
        [MaxLength(128), Required]
        public string Imei { get; set; }
        [Required, MaxLength(50)]
        public string IdentificationCode { get; set; }
    }

    public class VDConfig
    {
        public string batch { get; set; }
        public string district { get; set; }
        public string region { get; set; }
        public string no_of_stations { get; set; }
        public string ps_code { get; set; }
        public string ps_name { get; set; }
        public string no_of_devices { get; set; }
        public int number_of_registered_voters { get; set; } = 0;
        public string date_generated { get; set; }
    }

    public class RegConfig
    {
        public string district { get; set; }
        public string region { get; set; }
        public string no_of_stations { get; set; }
        public string no_of_devices { get; set; }
        public string date_generated { get; set; }
        public string last_backup_date { get; set; }
    }

    public class DownloadRegistrationDataModel
    {
        public int DeviceId { get; set; }
        public int StationId { get; set; }
    }

    public class DownloadVerificationDataModel
    {
        public int DeviceId { get; set; }
        public int StationId { get; set; }
    }

    public class ChallengeReason : LookUp
    {
        [Required, MaxLength(50)]
        public string Code { get; set; }
        public DateTime? LastSyncedAt { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), NotMapped]
        public bool CanSync => !LastSyncedAt.HasValue || (LastSyncedAt.Value < ModifiedAt);
    }

    public class TransferRequest : AuditFields
    {
        public int VoterRecordId { get; set; }
        public virtual VoterRegister VoterRecord { get; set; }
        public int NewPollingStationId { get; set; }
        public virtual Station NewPollingStation { get; set; }
        public string Reason { get; set; }
        public string VettingNotes { get; set; }
        public DateTime? VettedOn { get; set; }
        public string VettedBy { get; set; }
        public TransferRequestStatus Status { get; set; } = TransferRequestStatus.Pending;
        public int? OldPollingStationId { get; set; }
        public virtual Station OldPollingStation { get; set; }
        public bool IsSpecial { get; set; } = false;
    }

    public class TransferRequestVM
    {
        public int VoterRecordId { get; set; }
        public int NewPollingStationId { get; set; }
        public string Reason { get; set; }
    }

    public class SpecialTransferRequestVM
    {
        public int VoterRecordId { get; set; }
        public int SpecialPollingStationId { get; set; }
    }

    public class TransferRequestVetVM
    {
        public int Id { get; set; }
        public string Notes { get; set; }
    }

    public enum TransferRequestStatus
    {
        Pending,
        Reviewed,
        Approved,
        Confirmed,
        Rejected,
        Cancelled,
        Reversed
    }

    public class ProxyVotingRequest : AuditFields
    {
        public int SourceVoterRecordId { get; set; }
        public virtual VoterRegister SourceVoterRecord { get; set; }
        public int ProxyVoterRecordId { get; set; }
        public virtual VoterRegister ProxyVoterRecord { get; set; }
        public string Notes { get; set; }
        public string Officials { get; set; }
        public string VettingNotes { get; set; }
        public DateTime? VettedOn { get; set; }
        public string VettedBy { get; set; }
        public ProxyVotingRequestStatus Status { get; set; } = ProxyVotingRequestStatus.Pending;
    }

    public enum ProxyVotingRequestStatus
    {
        Pending,
        Approved,
        Reversed
    }


    public class Amendment : AuditFields
    {
        public int VoterRecordId { get; set; }
        public string Reference { get; set; } = StringGenerators.GenerateCode(16);
        public virtual VoterRegister VoterRecord { get; set; }
        public string Data { get; set; }
        public string OldData { get; set; }
        public AmendmentType Type { get; set; }
        public string Reason { get; set; }
        public string VettingNotes { get; set; }
        public DateTime? VettedOn { get; set; }
        public string VettedBy { get; set; }
        public AmendmentStatus Status { get; set; } = AmendmentStatus.Pending;
    }
    public enum AmendmentStatus
    {
        Pending,
        Reviewed,
        Approved,
        Confirmed,
        Rejected,
        Cancelled,
        Reversed
    }

    public enum AmendmentType
    {
        ChangeOfName,
        ChangeOfAddress,
        ChangeOfContactInformation,
        ChangeOfIdentification
    }
    public class AmendmentVM
    {
        public int VoterRecordId { get; set; }
        public string Data { get; set; }
        public string Reason { get; set; }
        public AmendmentType Type { get; set; }
    }
    public class AmendmentVetVM
    {
        public int Id { get; set; }
        public string Notes { get; set; }
    }

    public class ChangeOfNameVM
    {
        public string Surname { get; set; }
        public string OtherNames { get; set; }
    }

    public class ChangeOfAddressVM
    {
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public int? ResidentialDistrictId { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public int? HomeTownDistrictId { get; set; }
    }

    public class ChangeOfContactInformationVM
    {
        public string PhoneNumber { get; set; }
    }
    public class ChangeOfIdentificationVM
    {
        public int? IdentificationTypeId { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
    }

    public class EndOfDayReportStatisticsVM
    {
        public string Device { get; set; }
        public string PollingStationCode { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastRegistrationDate { get; set; }
        public int NoOfFemales { get; set; }
        public int NoOfMales { get; set; }
        public string PollingStationName { get; set; }
        public string PrintedBy { get; set; }
        public string RegistrationOfficer { get; set; }
        public int TotalNumberOfRegistrations { get; set; }
    }

    public class UnassignedRegistrationDevice : AuditFields
    {
        public virtual RegistrationDevice Device { get; set; }
        public int DeviceId { get; set; }
        public string Reason { get; set; }
        public string DeviceData { get; set; }
    }

    public class UnassignedVerificationDevice : AuditFields
    {
        public virtual VerificationDevice Device { get; set; }
        public int DeviceId { get; set; }
        public string Reason { get; set; }
        public string DeviceData { get; set; }
    }

    public class VoterReviewVM
    {
        public virtual List<string> VoterIds { get; set; }
    }

    public class UploadedMrtFile : AuditFields
    {
        public string Reference { get; set; } = StringGenerators.GenerateToken(32);
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public UploadedMrtFileStatus Status { get; set; } = UploadedMrtFileStatus.Pending;
        public string Notes { get; set; }
        public int? Count { get; set; }
        public string FileName { get; set; }
        public string DeviceId { get; set; }
    }

    public enum UploadedMrtFileStatus
    {
        Pending,
        Extracted,
        Failed
    }

    public class Duplicate
    {
        public string VoterId { get; set; }
        public int Hits { get; set; }
        public string EligibleVoterId { get; set; }
    }
    public class DistrictStat
    {
        public string District { get; set; }
        public double Count { get; set; }
        public string Percentage { get; set; }
    }

    public class DistrictStatVm
    {
        public string Label { get; set; }
        public double Y { get; set; } = 0;
        public string Percentage { get; set; }
    }

    public class RegionStat
    {
        public string Region { get; set; }
        public string Code { get; set; }
        public double Count { get; set; }
        public string Percentage { get; set; }
    }

    public class AgeGroupStat
    {
        public int Order { get; set; }
        public string Label { get; set; }
        public double Y { get; set; }
    }

    public class TrainingRegister : AuditFields
    {        
        public string Reference { get; set; } = StringGenerators.GenerateCode(16);
        public int VoterRegisterId { get; set; }
        public virtual VoterRegister VoterRegister { get; set; }
        public string VoterId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public int PollingStationId { get; set; }
        public virtual Station PollingStation { get; set; }
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public bool IsActive { get; set; }
    }

    public class DuplicateVoterReference : HasId
    {
        public int DuplicateVoterRegisterId { get; set; }
        public virtual VoterRegister DuplicateVoterRegister { get; set; }
        public int HitVoterRegisterId { get; set; }
        public virtual VoterRegister HitVoterRegister { get; set; }
        public string Status { get; set; }
    }
}
