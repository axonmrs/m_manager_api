﻿namespace MManager.Models
{
    public class Privileges
    {
        public const string CanViewDashboard = "CanViewDashboard";
        public const string CanViewAdministration = "CanViewAdministration";
        public const string CanViewSettings = "CanViewSettings";
        public const string CanViewReports = "CanViewReports";
        public const string CanExportData = "CanExportData";
        public const string CanImportData = "CanImportData";        
        public const string CanDoUploads = "CanDoUploads";
        public const string CanScanEndOfDayReport = "CanScanEndOfDayReport";
        public const string CanViewEndOfDayReports = "CanViewEndOfDayReports";
        public const string CanReprintVoterCard = "CanReprintVoterCard";
        public const string CanResetPassword = "CanResetPassword";

        //Reports
        public const string CanViewSystemReports = "CanViewSystemReports";
        public const string CanViewDistrictReports = "CanViewDistrictReports";
        public const string CanViewRegionalReports = "CanViewRegionalReports";
        public const string CanViewStationReports = "CanViewStationReports";

        //Roles
        public const string CanCreateRoles = "CanCreateRoles";
        public const string CanViewRoles = "CanViewRoles";
        public const string CanUpdateRoles = "CanUpdateRoles";
        public const string CanDeleteRoles = "CanDeleteRoles";

        //Register
        public const string CanViewRegister = "CanViewRegister";
        public const string CanViewProvisionalRegister = "CanViewProvisionalRegister";
        public const string CanViewFinalRegister = "CanViewFinalRegister";
        public const string CanSearchForVoter = "CanSearchForVoter";
        public const string CanReviewVoterRecord = "CanReviewVoterRecord";

        //Settings
        public const string IsSystemUser = "IsSystemUser";
        public const string IsRegionalUser = "IsRegionalUser";
        public const string IsDistrictUser = "IsDistrictUser";
        public const string IsStationSupervisor = "IsStationSupervisor";
        public const string IsStationUser = "IsStationUser";

        //System Users
        public const string CanCreateSystemUsers = "CanCreateSystemUsers";
        public const string CanViewSystemUsers = "CanViewSystemUsers";
        public const string CanUpdateSystemUsers = "CanUpdateSystemUsers";
        public const string CanDeleteSystemUsers = "CanDeleteSystemUsers";
        public const string CanActivateSystemUsers = "CanActivateSystemUsers";
        public const string CanDeactivateSystemUsers = "CanDeactivateSystemUsers";

        //Regional Users
        public const string CanCreateRegionalUsers = "CanCreateRegionalUsers";
        public const string CanViewRegionalUsers = "CanViewRegionalUsers";
        public const string CanUpdateRegionalUsers = "CanUpdateRegionalUsers";
        public const string CanDeleteRegionalUsers = "CanDeleteRegionalUsers";
        public const string CanActivateRegionalUsers = "CanActivateRegionalUsers";
        public const string CanDeactivateRegionalUsers = "CanDeactivateRegionalUsers";

        //District Users
        public const string CanCreateDistrictUsers = "CanCreateDistrictUsers";
        public const string CanViewDistrictUsers = "CanViewDistrictUsers";
        public const string CanUpdateDistrictUsers = "CanUpdateDistrictUsers";
        public const string CanDeleteDistrictUsers = "CanDeleteDistrictUsers";
        public const string CanActivateDistrictUsers = "CanActivateDistrictUsers";
        public const string CanDeactivateDistrictUsers = "CanDeactivateDistrictUsers";

        //Station Users
        public const string CanCreateStationUsers = "CanCreateStationUsers";
        public const string CanViewStationUsers = "CanViewStationUsers";
        public const string CanUpdateStationUsers = "CanUpdateStationUsers";
        public const string CanDeleteStationUsers = "CanDeleteStationUsers";
        public const string CanActivateStationUsers = "CanActivateStationUsers";
        public const string CanDeactivateStationUsers = "CanDeactivateStationUsers";

        //Settings
        public const string CanCreateSettings = "CanCreateSettings";
        public const string CanUpdateSettings = "CanUpdateSettings";
        public const string CanDeleteSettings = "CanDeleteSettings";
        public const string CanUploadSettings = "CanUploadSettings";

        //Registration
        public const string CanDoRegistration = "CanDoRegistration";
        public const string CanUpdateRegistration = "CanUpdateRegistration";
        public const string CanDeleteRegistration = "CanDeleteRegistration";

        //Old Register
        public const string CanViewOldRegister = "CanViewOldRegister";
        public const string CanUploadOldRegister = "CanUploadOldRegister";
        public const string CanDownloadOldRegister = "CanDownloadOldRegister";

        //Duplicates
        public const string CanViewDuplicates = "CanViewDuplicates";
        public const string CanViewPendingDuplicates = "CanViewPendingDuplicates";
        public const string CanViewResolvedDuplicates = "CanViewResolvedDuplicates";
        public const string CanUpdateDuplicates = "CanUpdateDuplicates";
        public const string CanAdjudicate = "CanAdjudicate";

        //Transfers
        public const string CanViewTransfers = "CanViewTransfers";
        public const string CanCreateTransferRequest = "CanCreateTransferRequest";
        public const string CanApproveTransfer = "CanApproveTransfer";
        public const string CanReviewTransfer = "CanReviewTransfer";
        public const string CanConfirmTransfer = "CanConfirmTransfer";
        public const string CanCancelTransfer = "CanCancelTransfer";
        public const string CanRejectTransfer = "CanRejectTransfer";
        public const string CanReverseConfirmedTransfer = "CanReverseConfirmedTransfer";
        public const string CanCreateSpecialTransferRequest = "CanCreateSpecialTransferRequest";

        //Proxy
        public const string CanViewProxyVoters = "CanViewProxyVoters";
        public const string CanCreateProxyVotingRequest = "CanCreateProxyVotingRequest";
        public const string CanApproveProxyVotingRequest = "CanApproveProxyVotingRequest";
        public const string CanReverseProxyVotingRequest = "CanReverseProxyVotingRequest";

        //RegistrationDevices
        public const string CanViewAassignedRegistrationDevices = "CanViewAassignedRegistrationDevices";
        public const string CanAssignDistrictRegistrationDevices = "CanAssignDistrictRegistrationDevices";
        public const string CanAssignRegionalRegistrationDevices = "CanAssignRegionalRegistrationDevices";
        public const string CanViewRegistrationDevice = "CanViewRegistrationDevice";
        public const string CanViewDistrictRegistrationDevices = "CanViewDistrictRegistrationDevices";
        public const string CanViewRegionalRegistrationDevices = "CanViewRegionalRegistrationDevices";
        public const string CanUploadRegistrationDevices = "CanUploadRegistrationDevices";
        public const string CanCreateRegistrationDevice = "CanCreateRegistrationDevice";
        public const string CanUpdateRegistrationDevice = "CanUpdateRegistrationDevice";
        public const string CanDeleteRegistrationDevice = "CanDeleteRegistrationDevice";
        public const string CanConfirmDistrictRegistrationDevices = "CanConfirmDistrictRegistrationDevices";
        public const string CanConfirmRegionalRegistrationDevices = "CanConfirmRegionalRegistrationDevices";
        public const string CanViewUnassignedRegistrationDevices = "CanViewUnassignedRegistrationDevices";
        public const string CanUnassignRegistrationDevice = "CanUnassignRegistrationDevice";

        //VerificationDevices
        public const string CanViewAassignedVerificationDevices = "CanViewAassignedVerificationDevices";
        public const string CanAssignDistrictVerificationDevices = "CanAssignDistrictVerificationDevices";
        public const string CanAssignRegionalVerificationDevices = "CanAssignRegionalVerificationDevices";
        public const string CanViewVerificationDevice = "CanViewVerificationDevice";
        public const string CanViewDistrictVerificationDevices = "CanViewDistrictVerificationDevices";
        public const string CanViewRegionalVerificationDevices = "CanViewRegionalVerificationDevices";
        public const string CanUploadVerificationDevices = "CanUploadVerificationDevices";
        public const string CanCreateVerificationDevice = "CanCreateVerificationDevice";
        public const string CanUpdateVerificationDevice = "CanUpdateVerificationDevice";
        public const string CanDeleteVerificationDevice = "CanDeleteVerificationDevice";
        public const string CanConfirmDistrictVerificationDevices = "CanConfirmDistrictVerificationDevices";
        public const string CanConfirmRegionalVerificationDevices = "CanConfirmRegionalVerificationDevices";
        public const string CanViewUnassignedVerificationDevices = "CanViewUnassignedVerificationDevices";
        public const string CanUnassignVerificationDevice = "CanUnassignVerificationDevice";

        //DataTransmission
        public const string CanViewDataTransmission = "CanViewDataTransmission";
        public const string CanTransmitData = "CanTransmitData";
        public const string CanViewTransmittedData = "CanViewTransmittedData";
    }

    public class GenericProperties
    {
        public const string Administrator = "Administrator";
        public const string Privilege = "Privilege";
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
        public const string IsDeleted = "IsDeleted";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class ConfigKeys
    {
        public static string EmailAccountName = "EmailAccountName";
        public static string EmailApiKey = "EmailApiKey";
        public static string EmailSender = "EmailSender";
        public static string SmsApiKey = "SmsApiKey";
        public static string SmsSender = "SmsSender";
        public static string AppTitle = "AppTitle";
        public static string Logo = "Logo";
        public static string KeepAlive = "KeepAlive";
    }

    public class ResultObj
    {
        public int Total { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
