﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MManager.Models
{
    public class ApplicationDbContext : IdentityDbContext<
        User, Role, string,
        UserClaim, UserRole, UserLogin,
        RoleClaim, UserToken>
    {
        //private readonly IConfiguration _configuration;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }

        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Constituency> Constituencies { get; set; }
        public DbSet<ElectoralArea> ElectoralAreas { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<IdType> IdTypes { get; set; }
        public DbSet<ExternalSystem> ExternalSystems { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<SystemLog> SystemLogs { get; set; }        
        public DbSet<VoterRegister> VoterRegister { get; set; }
        public DbSet<VoterRegisterTemplates> VoterRegisterTemplates { get; set; }
        public DbSet<TransferRequest> TransferRequests { get; set; }
        public DbSet<Amendment> Amendments { get; set; }
        public DbSet<VerificationUser> VerificationUsers { get; set; }
        public DbSet<RegistrationDevice> RegistrationDevices { get; set; }
        public DbSet<ChallengeReason> ChallengeReasons { get; set; }
        public DbSet<VotingStation> VotingStations { get; set; }
        public DbSet<UnassignedRegistrationDevice> UnassignedRegistrationDevices { get; set; }
        public DbSet<VerificationDevice> VerificationDevices { get; set; }
        public DbSet<UnassignedVerificationDevice> UnassignedVerificationDevices { get; set; }
        public DbSet<UploadedMrtFile> UploadedMrtFiles { get; set; }
        public DbSet<TrainingRegister> TrainingRegister { get; set; }
        public DbSet<DuplicateVoterReference> DuplicateVoterReferences { get; set; }
        public DbSet<ProxyVotingRequest> ProxyVotingRequest { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>(b =>
            {
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne(e => e.User)
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne(e => e.User)
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne(e => e.User)
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<Role>(b =>
            {
                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                // Each Role can have many associated RoleClaims
                b.HasMany(e => e.RoleClaims)
                    .WithOne(e => e.Role)
                    .HasForeignKey(rc => rc.RoleId)
                    .IsRequired();
            });

            builder.Entity<RoleClaim>(b =>
            {
                b.HasKey(e => e.Id);
            });
            builder.Entity<UserClaim>(b =>
            {
                b.HasKey(e => e.Id);
            });
            builder.Entity<UserLogin>(b =>
            {
                b.HasKey(e => e.Id);
            });

            builder.Entity<AppSetting>()
            .HasIndex(b => b.Name);

            builder.Entity<Region>()
            .HasIndex(b => b.Name);
            builder.Entity<Region>()
             .HasMany(a => a.Districts);

            builder.Entity<District>()
            .HasKey(a => a.Id);
            builder.Entity<District>()
            .HasMany(a => a.Constituencies);
            builder.Entity<District>()
            .HasOne(a => a.Region);
            builder.Entity<District>()
            .HasIndex(b => b.Name);

            builder.Entity<Constituency>()
            .HasKey(a => a.Id);
            builder.Entity<Constituency>()
            .HasMany(a => a.ElectoralAreas);
            builder.Entity<Constituency>()
            .HasOne(a => a.District);
            builder.Entity<Constituency>()
            .HasIndex(b => b.Name);

            builder.Entity<ElectoralArea>()
            .HasKey(a => a.Id);
            builder.Entity<ElectoralArea>()
            .HasMany(a => a.Stations);
            builder.Entity<ElectoralArea>()
            .HasOne(a => a.Constituency);
            builder.Entity<ElectoralArea>()
            .HasIndex(b => b.Name);

            builder.Entity<Station>()
            .HasKey(a => a.Id);
            builder.Entity<Station>()
            .HasMany(a => a.VotingStations);
            builder.Entity<Station>()
            .HasOne(a => a.ElectoralArea);
            builder.Entity<Station>()
            .HasIndex(b => b.Name);

            builder.Entity<VotingStation>()
            .HasKey(a => a.Id);
            builder.Entity<VotingStation>()
            .HasOne(a => a.Station);
            builder.Entity<VotingStation>()
            .HasIndex(b => b.Name);

            builder.Entity<RegistrationDevice>()
            .HasKey(a => a.Id);
            builder.Entity<RegistrationDevice>()
            .HasOne(a => a.District);
            builder.Entity<RegistrationDevice>()
            .HasIndex(b => b.IdentificationCode);
            builder.Entity<RegistrationDevice>()
            .HasIndex(b => b.SerialNumber);
        }

        //public override int SaveChanges()
        //{
        //    var entities = from e in ChangeTracker.Entries()
        //                   where e.State == EntityState.Added
        //                       || e.State == EntityState.Modified
        //                   select e.Entity;
        //    foreach (var entity in entities)
        //    {
        //        var validationContext = new ValidationContext(entity);
        //        Validator.ValidateObject(entity, validationContext);
        //    }

        //    return base.SaveChanges();
        //}
    }
}