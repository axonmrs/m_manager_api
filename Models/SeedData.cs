﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace MManager.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetService<UserManager<User>>();
            var roleManager = serviceProvider.GetService<RoleManager<Role>>();
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                #region System Admin Role
                var adminClaims = new List<string>
                        {
                            Privileges.CanViewDashboard,
                            Privileges.CanViewSettings,
                            Privileges.CanViewReports,
                            Privileges.CanViewSystemReports,
                            Privileges.CanCreateSystemUsers,
                            Privileges.CanViewSystemUsers,
                            Privileges.CanUpdateSystemUsers,
                            Privileges.CanDeleteSystemUsers,
                            Privileges.CanDeactivateSystemUsers,
                            Privileges.CanActivateSystemUsers,
                            Privileges.CanCreateRegionalUsers,
                            Privileges.CanViewRegionalUsers,
                            Privileges.CanUpdateRegionalUsers,
                            Privileges.CanDeleteRegionalUsers,
                            Privileges.CanDeactivateRegionalUsers,
                            Privileges.CanActivateRegionalUsers,
                            Privileges.CanCreateDistrictUsers,
                            Privileges.CanViewDistrictUsers,
                            Privileges.CanUpdateDistrictUsers,
                            Privileges.CanDeleteDistrictUsers,
                            Privileges.CanDeactivateDistrictUsers,
                            Privileges.CanActivateDistrictUsers,
                            Privileges.CanCreateStationUsers,
                            Privileges.CanViewStationUsers,
                            Privileges.CanUpdateStationUsers,
                            Privileges.CanDeleteStationUsers,
                            Privileges.CanDeactivateStationUsers,
                            Privileges.CanActivateStationUsers,
                            Privileges.CanCreateSettings,
                            Privileges.CanUpdateSettings,
                            Privileges.CanDeleteSettings,
                            Privileges.IsSystemUser,
                            Privileges.CanViewRegister,
                            Privileges.CanViewProvisionalRegister,
                            Privileges.CanViewFinalRegister,
                            Privileges.CanSearchForVoter,
                            Privileges.CanResetPassword
                };
                var adminRole = new Role { Name = "System Administrator", NormalizedName = "System Administrator", Locked = true, Type = UserType.System };
                var existingRole = context.Roles.FirstOrDefault(x => x.Name == adminRole.Name);
                if (existingRole == null)
                {
                    var res = roleManager.CreateAsync(adminRole);
                    if (res.Result.Succeeded)
                    {
                        adminClaims.Distinct().ToList().ForEach(r => roleManager.AddClaimAsync(adminRole,
                        new Claim(GenericProperties.Privilege, r)).Wait());
                    }
                }
                else
                {
                    adminRole = existingRole;
                    var all = context.RoleClaims
                        .Where(x => x.RoleId == adminRole.Id)
                        .ToList();
                    var toDelete = all.Select(x => x.ClaimValue)
                        .Except(adminClaims)
                        .Select(x => all.First(y => y.ClaimValue == x))
                        .ToList();
                    context.RoleClaims.RemoveRange(toDelete);
                    context.SaveChanges();

                    var existing = context.RoleClaims
                        .Where(x => x.RoleId == adminRole.Id && adminClaims.Contains(x.ClaimValue))
                        .Select(x => x.ClaimValue)
                        .ToList();
                    adminClaims.Except(existing)
                        .ToList()
                        .ForEach(r => {
                            var newClaim = new RoleClaim { RoleId = adminRole.Id, ClaimValue = r, ClaimType = GenericProperties.Privilege };
                            context.RoleClaims.Add(newClaim);
                        });
                    context.SaveChanges();
                }
                #endregion

                #region Super Admin Role
                var superAdminClaims = new List<string>
                        {
                            Privileges.CanViewDashboard,
                            Privileges.CanViewAdministration,
                            Privileges.CanViewSettings,
                            Privileges.CanViewReports,
                            Privileges.CanExportData,
                            Privileges.CanImportData,
                            Privileges.CanViewSystemReports,
                            Privileges.CanViewRoles,
                            Privileges.CanCreateRoles,
                            Privileges.CanUpdateRoles,
                            Privileges.CanDeleteRoles,
                            Privileges.CanCreateSystemUsers,
                            Privileges.CanViewSystemUsers,
                            Privileges.CanUpdateSystemUsers,
                            Privileges.CanDeleteSystemUsers,
                            Privileges.CanDeactivateSystemUsers,
                            Privileges.CanActivateSystemUsers,
                            Privileges.CanCreateRegionalUsers,
                            Privileges.CanViewRegionalUsers,
                            Privileges.CanUpdateRegionalUsers,
                            Privileges.CanDeleteRegionalUsers,
                            Privileges.CanDeactivateRegionalUsers,
                            Privileges.CanActivateRegionalUsers,
                            Privileges.CanCreateDistrictUsers,
                            Privileges.CanViewDistrictUsers,
                            Privileges.CanUpdateDistrictUsers,
                            Privileges.CanDeleteDistrictUsers,
                            Privileges.CanDeactivateDistrictUsers,
                            Privileges.CanActivateDistrictUsers,
                            Privileges.CanCreateStationUsers,
                            Privileges.CanViewStationUsers,
                            Privileges.CanUpdateStationUsers,
                            Privileges.CanDeleteStationUsers,
                            Privileges.CanDeactivateStationUsers,
                            Privileges.CanActivateStationUsers,
                            Privileges.CanCreateSettings,
                            Privileges.CanUpdateSettings,
                            Privileges.CanDeleteSettings,
                            Privileges.IsSystemUser,
                            Privileges.CanDoUploads,
                            Privileges.CanUploadSettings,
                            Privileges.CanUploadOldRegister,
                            Privileges.CanViewOldRegister,
                            Privileges.CanDoRegistration,
                            Privileges.CanUpdateRegistration,
                            Privileges.CanDeleteRegistration,
                            Privileges.CanDownloadOldRegister,
                            Privileges.CanViewDuplicates,
                            Privileges.CanAdjudicate,
                            Privileges.CanViewTransfers,
                            Privileges.CanCreateTransferRequest,
                            Privileges.CanCreateSpecialTransferRequest,
                            Privileges.CanApproveTransfer,
                            Privileges.CanRejectTransfer,
                            Privileges.CanCancelTransfer,
                            Privileges.CanConfirmTransfer,
                            Privileges.CanReviewTransfer,
                            Privileges.CanReverseConfirmedTransfer,
                            Privileges.CanViewProxyVoters,
                            Privileges.CanCreateProxyVotingRequest,
                            Privileges.CanApproveProxyVotingRequest,
                            Privileges.CanReverseProxyVotingRequest,
                            Privileges.CanViewAassignedRegistrationDevices,
                            Privileges.CanViewRegistrationDevice,
                            Privileges.CanViewRegionalRegistrationDevices,
                            Privileges.CanViewDistrictRegistrationDevices,
                            Privileges.CanUploadRegistrationDevices,
                            Privileges.CanCreateRegistrationDevice,
                            Privileges.CanUpdateRegistrationDevice,
                            Privileges.CanDeleteRegistrationDevice,
                            Privileges.CanAssignDistrictRegistrationDevices,
                            Privileges.CanAssignRegionalRegistrationDevices,
                            Privileges.CanViewUnassignedRegistrationDevices,
                            Privileges.CanUnassignRegistrationDevice,
                            Privileges.CanViewRegister,
                            Privileges.CanViewProvisionalRegister,
                            Privileges.CanViewFinalRegister,
                            Privileges.CanReviewVoterRecord,
                            Privileges.CanSearchForVoter,
                            Privileges.CanViewAassignedVerificationDevices,
                            Privileges.CanViewVerificationDevice,
                            Privileges.CanViewRegionalVerificationDevices,
                            Privileges.CanViewDistrictVerificationDevices,
                            Privileges.CanUploadVerificationDevices,
                            Privileges.CanCreateVerificationDevice,
                            Privileges.CanUpdateVerificationDevice,
                            Privileges.CanDeleteVerificationDevice,
                            Privileges.CanAssignDistrictVerificationDevices,
                            Privileges.CanAssignRegionalVerificationDevices,
                            Privileges.CanViewUnassignedVerificationDevices,
                            Privileges.CanUnassignVerificationDevice,
                            Privileges.CanReprintVoterCard,
                            Privileges.CanViewEndOfDayReports,
                            Privileges.CanViewDataTransmission,
                            Privileges.CanViewTransmittedData,
                            Privileges.CanViewResolvedDuplicates,
                            Privileges.CanViewPendingDuplicates,
                            Privileges.CanUpdateDuplicates,
                            Privileges.CanResetPassword
                };
                var superAdminRole = new Role { Name = "Super Administrator", NormalizedName = "Super Administrator", Locked = true, Type = UserType.System };
                var existingSupRole = context.Roles.FirstOrDefault(x => x.Name == superAdminRole.Name);
                if (existingSupRole == null)
                {
                    var res = roleManager.CreateAsync(superAdminRole);
                    if (res.Result.Succeeded)
                    {
                        superAdminClaims.Distinct().ToList().ForEach(r => roleManager.AddClaimAsync(superAdminRole,
                        new Claim(GenericProperties.Privilege, r)).Wait());
                    }
                }
                else
                {
                    superAdminRole = existingSupRole;
                    var all = context.RoleClaims
                        .Where(x => x.RoleId == superAdminRole.Id)
                        .ToList();
                    var toDelete = all.Select(x => x.ClaimValue)
                        .Except(superAdminClaims)
                        .Select(x => all.First(y => y.ClaimValue == x))
                        .ToList();
                    context.RoleClaims.RemoveRange(toDelete);
                    context.SaveChanges();

                    var existing = context.RoleClaims
                        .Where(x => x.RoleId == existingSupRole.Id && superAdminClaims.Contains(x.ClaimValue))
                        .Select(x => x.ClaimValue)
                        .ToList();
                    superAdminClaims.Except(existing)
                        .ToList()
                        .ForEach(r => {
                            var newClaim = new RoleClaim { RoleId = existingSupRole.Id, ClaimValue = r, ClaimType = GenericProperties.Privilege };
                            context.RoleClaims.Add(newClaim);
                        });
                    context.SaveChanges();
                }
                #endregion

                #region Users
                var adminUser = new User
                {
                    Name = "System Administrator",
                    UserName = "Admin",

                };
                var existingUser = userManager.FindByNameAsync("Admin").Result;
                //Admin User
                if (existingUser == null)
                {
                    var res = userManager.CreateAsync(adminUser, "SysAdmin@1");
                    if (res.Result.Succeeded)
                    {
                        var user = userManager.FindByNameAsync("Admin").Result;
                        userManager.AddToRoleAsync(user, adminRole.Name).Wait();
                    }
                }
                #endregion

                #region Super Users
                var superAdminUser = new User
                {
                    Name = "Super Administrator",
                    UserName = "SAdmin",

                };
                var existingSuperUser = userManager.FindByNameAsync("SAdmin").Result;
                //Admin User
                if (existingSuperUser == null)
                {
                    var res = userManager.CreateAsync(superAdminUser, "zxcdeswweweP@zzw0rd00876wswdd");
                    if (res.Result.Succeeded)
                    {
                        var user = userManager.FindByNameAsync("SAdmin").Result;
                        userManager.AddToRoleAsync(user, superAdminRole.Name).Wait();
                    }
                }
                #endregion

                #region Regional Admin Role
                var regAdminClaims = new List<string>
                        {
                            Privileges.CanViewDashboard,
                            Privileges.CanViewReports,
                            Privileges.CanViewRegionalReports,
                            Privileges.IsRegionalUser,
                            Privileges.CanViewOldRegister,
                            Privileges.CanViewDuplicates,
                            Privileges.CanViewRegistrationDevice,
                            Privileges.CanViewRegionalRegistrationDevices,
                            Privileges.CanViewAassignedRegistrationDevices,
                            Privileges.CanViewDistrictRegistrationDevices,
                            Privileges.CanAssignDistrictRegistrationDevices,
                            Privileges.CanConfirmRegionalRegistrationDevices,
                            Privileges.CanViewRegister,
                            Privileges.CanViewProvisionalRegister,
                            Privileges.CanViewFinalRegister,
                            Privileges.CanSearchForVoter,
                            Privileges.CanViewVerificationDevice,
                            Privileges.CanViewRegionalVerificationDevices,
                            Privileges.CanViewAassignedVerificationDevices,
                            Privileges.CanViewDistrictVerificationDevices,
                            Privileges.CanAssignDistrictVerificationDevices,
                            Privileges.CanConfirmRegionalVerificationDevices,
                            Privileges.CanReprintVoterCard,
                            Privileges.CanViewEndOfDayReports,
                            Privileges.CanViewDataTransmission,
                            Privileges.CanViewTransmittedData
                };
                var regAdminRole = new Role { Name = "Regional Administrator", NormalizedName = "Regional Administrator", Locked = true, Type = UserType.Regional };
                var existingRegRole = context.Roles.FirstOrDefault(x => x.Name == regAdminRole.Name);
                if (existingRegRole == null)
                {
                    var res = roleManager.CreateAsync(regAdminRole);
                    if (res.Result.Succeeded)
                    {
                        regAdminClaims.Distinct().ToList().ForEach(r => roleManager.AddClaimAsync(regAdminRole,
                        new Claim(GenericProperties.Privilege, r)).Wait());
                    }
                }
                else
                {
                    regAdminRole = existingRegRole;
                    var all = context.RoleClaims
                        .Where(x => x.RoleId == existingRegRole.Id)
                        .ToList();
                    var toDelete = all.Select(x=> x.ClaimValue)
                        .Except(regAdminClaims)
                        .Select(x=> all.First(y=> y.ClaimValue == x))
                        .ToList();
                    context.RoleClaims.RemoveRange(toDelete);
                    context.SaveChanges();

                    var existing = context.RoleClaims
                        .Where(x => x.RoleId == existingRegRole.Id && regAdminClaims.Contains(x.ClaimValue))
                        .Select(x => x.ClaimValue)
                        .ToList();
                    regAdminClaims.Except(existing)
                        .ToList()
                        .ForEach(r => {
                            var newClaim = new RoleClaim { RoleId = existingRegRole.Id, ClaimValue = r, ClaimType = GenericProperties.Privilege };
                            context.RoleClaims.Add(newClaim);
                        });
                    context.SaveChanges();
                }
                #endregion

                #region District Admin Role
                var distAdminClaims = new List<string>
                        {
                            Privileges.CanViewDashboard,
                            Privileges.CanViewReports,
                            Privileges.CanViewDistrictReports,
                            Privileges.IsDistrictUser,
                            Privileges.CanViewOldRegister,
                            Privileges.CanViewDuplicates,
                            Privileges.CanExportData,
                            Privileges.CanViewRegistrationDevice,
                            Privileges.CanViewAassignedRegistrationDevices,
                            Privileges.CanConfirmDistrictRegistrationDevices,
                            Privileges.CanViewDistrictRegistrationDevices,
                            Privileges.CanViewTransfers,
                            Privileges.CanCreateTransferRequest,
                            Privileges.CanCreateSpecialTransferRequest,
                            Privileges.CanApproveTransfer,
                            Privileges.CanRejectTransfer,
                            Privileges.CanCancelTransfer,
                            Privileges.CanReviewTransfer,
                            Privileges.CanViewProxyVoters,
                            Privileges.CanCreateProxyVotingRequest,
                            Privileges.CanApproveProxyVotingRequest,
                            Privileges.CanReverseProxyVotingRequest,
                            Privileges.CanScanEndOfDayReport,
                            Privileges.CanViewRegister,
                            Privileges.CanViewProvisionalRegister,
                            Privileges.CanViewFinalRegister,
                            Privileges.CanReviewVoterRecord,
                            Privileges.CanSearchForVoter,
                            Privileges.CanViewVerificationDevice,
                            Privileges.CanViewAassignedVerificationDevices,
                            Privileges.CanConfirmDistrictVerificationDevices,
                            Privileges.CanViewDistrictVerificationDevices,
                            Privileges.CanReprintVoterCard,
                            Privileges.CanViewEndOfDayReports,
                            Privileges.CanViewDataTransmission,
                            Privileges.CanTransmitData,
                            Privileges.CanViewTransmittedData
                };
                var distAdminRole = new Role { Name = "District Administrator", NormalizedName = "District Administrator", Locked = true, Type = UserType.District };
                var existingDistRole = context.Roles.FirstOrDefault(x => x.Name == distAdminRole.Name);
                if (existingDistRole == null)
                {
                    var res = roleManager.CreateAsync(distAdminRole);
                    if (res.Result.Succeeded)
                    {
                        distAdminClaims.Distinct().ToList().ForEach(r => roleManager.AddClaimAsync(distAdminRole,
                        new Claim(GenericProperties.Privilege, r)).Wait());
                    }
                }
                else
                {
                    distAdminRole = existingDistRole;
                    var all = context.RoleClaims
                        .Where(x => x.RoleId == existingDistRole.Id)
                        .ToList();
                    var toDelete = all.Select(x => x.ClaimValue)
                        .Except(distAdminClaims)
                        .Select(x => all.First(y => y.ClaimValue == x))
                        .ToList();
                    context.RoleClaims.RemoveRange(toDelete);
                    context.SaveChanges();

                    var existing = context.RoleClaims
                        .Where(x => x.RoleId == existingDistRole.Id && distAdminClaims.Contains(x.ClaimValue))
                        .Select(x => x.ClaimValue)
                        .ToList();
                    distAdminClaims.Except(existing)
                        .ToList()
                        .ForEach(r => {
                            var newClaim = new RoleClaim { RoleId = existingDistRole.Id, ClaimValue = r, ClaimType = GenericProperties.Privilege };
                            context.RoleClaims.Add(newClaim);
                        });
                    context.SaveChanges();
                }
                #endregion

                context.SaveChanges();
            }
        }

    }
}