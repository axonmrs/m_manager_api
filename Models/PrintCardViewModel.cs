﻿using System;

namespace MManager.Models
{
    public class PrintCardViewModel
    {
        public string otherNames { get; set; }
        public string surname { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int age { get; set; }
        public string sex { get; set; }
        public string phoneNumber { get; set; }
        public string residentialAddress { get; set; }
        public string residentialTown { get; set; }
        public string residentialDistrict { get; set; }
        public string residentialRegion { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
        public DateTime idExpiry { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public string hometownAddress { get; set; }
        public string hometown { get; set; }
        public string hometownDistrict { get; set; }
        public string hometownRegion { get; set; }
        public bool isVisuallyImpaired { get; set; }
        public bool isDisabled { get; set; }
        public byte[] photo { get; set; }
        public byte[] qrCode { get; set; }
        public string voterId { get; set; }
        public string pollingStationCode { get; set; }
        public string pollingStationName { get; set; }
        public string name { get; set; }
        public DateTime dateOfRegistration { get; set; }
        public string errorMessage { get; set; }
        public bool canPrint { get; set; }
        public int cardPrintCount { get; set; }
        public bool busy { get; set; }
        public string challenges { get; set; }
        public bool leper { get; set; }
        public bool missingFingersL { get; set; }
        public bool missingFingersR { get; set; }
        public bool amputatedHandsL { get; set; }
        public bool amputatedHandsR { get; set; }
        public bool hideLabels { get; set; }
    }
}
