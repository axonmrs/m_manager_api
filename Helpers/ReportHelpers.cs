﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;

namespace MManager.Helpers
{
    public class ReportHelpers
    {
        public static byte[] generateReport(Func<(List<ReportDataSource> data, string reportFile)> getData)
        {
            var ret = getData();
            return generateReport(ret.reportFile, ret.data);
        }

        public static byte[] generateReport(string rdlcFile, object data, string srcName = "Data")
        {
            var ds = new ReportDataSource { Name = srcName, Value = data };
            return generateReport(rdlcFile, new List<ReportDataSource> { ds });
        }

        public static byte[] generateReport(string rdlcFile, List<ReportDataSource> data)
        {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", $"{rdlcFile}");
            Warning[] warnings;
            string[] streams;
            var report = new LocalReport
            {
                ReportPath = Path.GetFullPath(file),
                EnableExternalImages = true
            };
            if (data != null)
                foreach (var entry in data)
                    report.DataSources.Add(entry);
            string mimeType;
            string encoding;
            string extension;
            var deviceInfo = string.Format("<DeviceInfo><PageHeight>{0}</PageHeight><PageWidth>{1}</PageWidth></DeviceInfo>", "11.7in", "8.3in");
            var bytes = report.Render("PDF", deviceInfo,
                out mimeType, out encoding, out extension, out streams, out warnings);
            return bytes;
        }

        public static string saveReport(string name, byte[] data)
        {
            var filename = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", name);
            File.WriteAllBytes(filename, data);
            return filename;
        }
    }
}
