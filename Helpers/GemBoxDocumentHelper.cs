﻿using GemBox.Document;

namespace MManager.Helpers
{
    public static class GemBoxDocumentHelper
    {
        public static DocumentModel JoinWith(this DocumentModel destinationDocument, string filePath)
        {
            DocumentModel sourceDocument = DocumentModel.Load(filePath);
            foreach (Section sourceSection in sourceDocument.Sections)
            {
                Section destinationSection = destinationDocument.Import(sourceSection, true, false);
                destinationDocument.Sections.Add(destinationSection);
            }
            return destinationDocument;
        }
    }
}
