﻿using Newtonsoft.Json;
using MManager.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MManager.Helpers
{
    public class NasaraSmsApi
    {
        private readonly ApplicationDbContext _context;
        public NasaraSmsApi(ApplicationDbContext context)
        {
            _context = context;
        }
        public bool Success { get; private set; }
        public string Response { get; private set; }

        public async Task SendMessage(string phoneNumbers, string message)
        {
            var client = new HttpClient();
            var values = new Dictionary<string, string>
            {
                { "api_key", "xxxxx" },
                { "phone_numbers", phoneNumbers },
                { "message", message },
                { "sender_id", "xxxxx" }
            };

            var content = new FormUrlEncodedContent(values);
            var response = client.PostAsync("http://sms.nasaramobile.com/api/v2/sendsms", content).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeAnonymousType(responseString, new { code = 0, status = 0, message = "" });
            ResultHandler(result.message, (result.code == 1816) || (result.code == 1801));
        }

        public string CreditBalance()
        {
            try
            {
                //Building SMS Request
                var client = new HttpClient();
                var resData = client.GetStringAsync($"http://sms.nasaramobile.com/api/v2/accounts/credit?api_key=xxxx").Result;
                var data = JsonConvert.DeserializeAnonymousType(resData,
                    new { status = "", data = "", code = "" });

                return string.IsNullOrWhiteSpace(data.data) ? "0" : data.data;
            }
            catch (Exception)
            {
                // _logger.ErrorException("Error getting credit balance", ex);
            }
            return "-";
        }


        private void ResultHandler(string message, bool success = true)
        {
            Success = success;
            Response = message;
        }

    }
}
