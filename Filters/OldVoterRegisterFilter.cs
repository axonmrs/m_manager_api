﻿using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class OldVoterRegisterFilter : Filter<OldVoterRegister>
    {

        public long Id;
        public string Reference;
        public string VoterNumber;
        public string Name;
        public string PhoneNumber; 
        public Sex? Sex;
        public string Comments;
        public string PollingStationCode;
        public string ElectoralAreaCode;
        public string ConstituencyCode;
        public string DistrictCode;
        public string RegionCode;
        public string Town;

        public override IQueryable<OldVoterRegister> BuildQuery(IQueryable<OldVoterRegister> query)
        {
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterNumber)) query = query.Where(q => q.VoterNumber.ToLower().Contains(VoterNumber.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Surname.ToLower().Contains(Name.ToLower()) || q.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Sex.HasValue) query = query.Where(q => q.Sex == Sex);
            if (!string.IsNullOrEmpty(Comments)) query = query.Where(q => q.Comments.ToLower().Contains(Comments.ToLower()));
            if (!string.IsNullOrEmpty(PollingStationCode)) query = query.Where(q => q.PollingStationCode.ToLower().Contains(PollingStationCode.ToLower()));
            if (!string.IsNullOrEmpty(ElectoralAreaCode)) query = query.Where(q => q.ElectoralAreaCode.ToLower().Contains(ElectoralAreaCode.ToLower()));
            if (!string.IsNullOrEmpty(ConstituencyCode)) query = query.Where(q => q.ConstituencyCode.ToLower().Contains(ConstituencyCode.ToLower()));
            if (!string.IsNullOrEmpty(DistrictCode)) query = query.Where(q => q.DistrictCode.ToLower().Contains(DistrictCode.ToLower()));
            if (!string.IsNullOrEmpty(RegionCode)) query = query.Where(q => q.RegionCode.ToLower().Contains(RegionCode.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.ResidentialTown.ToLower().Contains(Name.ToLower()) || q.HomeTown.ToLower().Contains(Name.ToLower()));

            return query;
        }
    }
}
