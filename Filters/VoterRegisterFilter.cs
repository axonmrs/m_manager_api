﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using MManager.MrsLogsModels;
using System.Linq;

namespace MManager.Filters
{
    public class VoterRegisterFilter : Filter<VoterRegister>
    {

        public int Id;
        public string Reference;
        public string VoterId;
        public string Name;
        public string PhoneNumber;
        public Models.Sex? Sex;
        public string Comments;
        public int PollingStationId;
        public int ElectoralAreaId;
        public int ConstituencyId;
        public int DistrictId;
        public int RegionId;
        public string Town;
        public string Tag;
        public bool? Reviewed;

        public override IQueryable<VoterRegister> BuildQuery(IQueryable<VoterRegister> query)
        {
            query = query.Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region)
                .Include(x=> x.IdentificationType)
                .Include(x => x.HomeTownDistrict.Region)
                .Include(x => x.ResidentialDistrict.Region);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterId)) query = query.Where(q => q.VoterId.ToLower().Contains(VoterId.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Surname.ToLower().Contains(Name.ToLower()) || q.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Sex.HasValue) query = query.Where(q => q.Sex == Sex);
            if (PollingStationId > 0) query = query.Where(x => x.PollingStationId == PollingStationId);
            if (ElectoralAreaId > 0) query = query.Where(x => x.PollingStation.ElectoralAreaId == ElectoralAreaId);
            if (ConstituencyId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.ConstituencyId == ConstituencyId);
            if (DistrictId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.District.RegionId == RegionId);
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.ResidentialTown.ToLower().Contains(Name.ToLower()) || q.HomeTown.ToLower().Contains(Name.ToLower()));
            if (Reviewed.HasValue) query = query.Where(q => q.Reviewed == Reviewed.Value);
            query = query.Where(x => !x.HasDuplicate && !x.OnExceptionList);
            return query;
        }
    }

    public class SearchVoterFilter : Filter<VoterRegister>
    {

        public int Id;
        public string Reference;
        public string VoterId;
        public string Name;
        public string PhoneNumber;
        public int PollingStationId;
        public int ElectoralAreaId;
        public int ConstituencyId;
        public int DistrictId;
        public int RegionId;
        public string Town;
        public int Size = 50;

        public override IQueryable<VoterRegister> BuildQuery(IQueryable<VoterRegister> query)
        {
            query = query.Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region)
                .Include(x => x.IdentificationType)
                .Include(x => x.HomeTownDistrict.Region)
                .Include(x => x.ResidentialDistrict.Region);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterId)) query = query.Where(q => q.VoterId.ToLower().Contains(VoterId.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Surname.ToLower().Contains(Name.ToLower()) || q.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (PollingStationId > 0) query = query.Where(x => x.PollingStationId == PollingStationId);
            if (ElectoralAreaId > 0) query = query.Where(x => x.PollingStation.ElectoralAreaId == ElectoralAreaId);
            if (ConstituencyId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.ConstituencyId == ConstituencyId);
            if (DistrictId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.District.RegionId == RegionId);
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.ResidentialTown.ToLower().Contains(Name.ToLower()) || q.HomeTown.ToLower().Contains(Name.ToLower()));
            query = query.Where(x => !x.HasDuplicate && !x.OnExceptionList);
            return query;
        }
    }

    public class UnreviewedVoterFilter : Filter<VoterRegister>
    {

        public string Query;
        public int Size = 200;

        public override IQueryable<VoterRegister> BuildQuery(IQueryable<VoterRegister> query)
        {
            if (!string.IsNullOrEmpty(Query)) query = query.Where(q => q.VoterId.ToLower().Contains(Query.ToLower()) || q.Surname.ToLower().Contains(Query.ToLower()) || q.OtherNames.ToLower().Contains(Query.ToLower()));
            query = query.Where(x => !x.HasDuplicate && !x.OnExceptionList);
            return query;
        }
    }

    public class DuplicatesFilter : Filter<DuplicateVoter>
    {

        public int Id;
        public string Reference;
        public string VoterId;
        public string Name;
        public string PhoneNumber;
        public MrsLogsModels.Sex? Sex;
        public string PollingStationCode;
        public string Town;

        public override IQueryable<DuplicateVoter> BuildQuery(IQueryable<DuplicateVoter> query)
        {
            query = query.Include(x => x.TemporalVoterRegister).Include(x=> x.Hits);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.TemporalVoterRegister.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterId)) query = query.Where(q => q.VoterId.ToLower().Contains(VoterId.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query
                    .Where(q => q.TemporalVoterRegister.Surname.ToLower().Contains(Name.ToLower()) || q.TemporalVoterRegister.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.TemporalVoterRegister.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Sex.HasValue) query = query.Where(q => q.TemporalVoterRegister.Sex == Sex);
            if (!string.IsNullOrEmpty(PollingStationCode)) query = query.Where(q => q.TemporalVoterRegister.PollingStationCode.ToLower().Contains(PollingStationCode.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.TemporalVoterRegister.ResidentialTown.ToLower().Contains(Town.ToLower()) || q.TemporalVoterRegister.HomeTown.ToLower().Contains(Town.ToLower()));
            return query;
        }
    }
}
