﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class UserFilter : Filter<User>
    {
        public string Email;
        public string Username;
        public UserType? Type;
        public int? RegionId;
        public int? DistrictId;

        public override IQueryable<User> BuildQuery(IQueryable<User> query)
        {
            query = query.Include(x => x.Region).Include(x => x.District);
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (!string.IsNullOrEmpty(Username)) query = query.Where(q => q.UserName.ToLower().Contains(Username.ToLower()));
            if (Type.HasValue) query = query.Where(q => q.Type == Type);
            if (RegionId != null && RegionId > 0) query = query.Where(x => x.RegionId == RegionId);
            if (DistrictId != null && DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class StationUserFilter : Filter<StationUser>
    {
        public string Email;
        public string Username;
        public StationUserType? Type;
        public string Name;

        public override IQueryable<StationUser> BuildQuery(IQueryable<StationUser> query)
        {
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (!string.IsNullOrEmpty(Username)) query = query.Where(q => q.UserName.ToLower().Contains(Username.ToLower()));
            if (Type.HasValue) query = query.Where(q => q.Type == Type);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class VerificationUserFilter : Filter<VerificationUser>
    {
        public string Email;
        public string Username;
        public string Name;

        public override IQueryable<VerificationUser> BuildQuery(IQueryable<VerificationUser> query)
        {
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (!string.IsNullOrEmpty(Username)) query = query.Where(q => q.UserName.ToLower().Contains(Username.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
