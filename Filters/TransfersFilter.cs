﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class TransfersFilter : Filter<TransferRequest>
    {

        public int Id;
        public string Reference;
        public string VoterId;
        public string Name;
        public string PhoneNumber;
        public Sex? Sex;
        public string Reason;
        public int PollingStationId;
        public int ElectoralAreaId;
        public int ConstituencyId;
        public int DistrictId;
        public int RegionId;
        public string Town;
        public int NewPollingStationId;

        public override IQueryable<TransferRequest> BuildQuery(IQueryable<TransferRequest> query)
        {
            query = query.Include(x => x.NewPollingStation.ElectoralArea.Constituency.District.Region)
                .Include(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.Region)
                .Include(x => x.VoterRecord.HomeTownDistrict.Region).Include(x => x.VoterRecord.ResidentialDistrict.Region); ;
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.VoterRecord.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterId)) query = query.Where(q => q.VoterRecord.VoterId.ToLower().Contains(VoterId.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.VoterRecord.Surname.ToLower().Contains(Name.ToLower()) || q.VoterRecord.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.VoterRecord.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Sex.HasValue) query = query.Where(q => q.VoterRecord.Sex == Sex);
            if (!string.IsNullOrEmpty(Reason)) query = query.Where(q => q.Reason.ToLower().Contains(Reason.ToLower()));
            if (PollingStationId > 0) query = query.Where(x => x.VoterRecord.PollingStationId == PollingStationId);
            if (ElectoralAreaId > 0) query = query.Where(x => x.VoterRecord.PollingStation.ElectoralAreaId == ElectoralAreaId);
            if (ConstituencyId > 0) query = query.Where(x => x.VoterRecord.PollingStation.ElectoralArea.ConstituencyId == ConstituencyId);
            if (DistrictId > 0) query = query.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.VoterRecord.PollingStation.ElectoralArea.Constituency.District.RegionId == RegionId);
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.VoterRecord.ResidentialTown.ToLower().Contains(Name.ToLower()) || q.VoterRecord.HomeTown.ToLower().Contains(Name.ToLower()));
            if (NewPollingStationId > 0) query = query.Where(x => x.NewPollingStationId == NewPollingStationId);

            return query;
        }
    }
}
