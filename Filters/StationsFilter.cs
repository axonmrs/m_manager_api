﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class StationsFilter : Filter<Station>
    {
        public string Name;
        public string Code;
        public int? RegionId;
        public int? DistrictId;
        public int? ConstituencyId;
        public int? ElectoralAreaId;
        public int? Id;
        public int? PollingStationId;

        public override IQueryable<Station> BuildQuery(IQueryable<Station> query)
        {
            query = query.Include(x => x.ElectoralArea.Constituency.District.Region);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (RegionId.HasValue && RegionId.Value > 0) query = query.Where(x => x.ElectoralArea.Constituency.District.RegionId == RegionId.Value);
            if (DistrictId.HasValue && DistrictId.Value > 0) query = query.Where(x => x.ElectoralArea.Constituency.DistrictId == DistrictId.Value);
            if (ConstituencyId.HasValue && ConstituencyId.Value > 0) query = query.Where(x => x.ElectoralArea.ConstituencyId == ConstituencyId.Value);
            if (ElectoralAreaId.HasValue && ElectoralAreaId.Value > 0) query = query.Where(x => x.ElectoralAreaId == ElectoralAreaId.Value);
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (PollingStationId.HasValue && PollingStationId.Value > 0) query = query.Where(x => x.Id == PollingStationId.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class VotingStationsFilter : Filter<VotingStation>
    {
        public string Name;
        public string Code;
        public int RegionId;
        public int DistrictId;
        public int ConstituencyId;
        public int ElectoralAreaId;
        public int StationId;
        public int Id;

        public override IQueryable<VotingStation> BuildQuery(IQueryable<VotingStation> query)
        {
            query = query.Include(x => x.Station.ElectoralArea.Constituency.District.Region);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (RegionId > 0) query = query.Where(x => x.Station.ElectoralArea.Constituency.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.Station.ElectoralArea.Constituency.DistrictId == DistrictId);
            if (ConstituencyId > 0) query = query.Where(x => x.Station.ElectoralArea.ConstituencyId == ConstituencyId);
            if (ElectoralAreaId > 0) query = query.Where(x => x.Station.ElectoralAreaId == ElectoralAreaId);
            if (StationId > 0) query = query.Where(x => x.StationId == StationId);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
