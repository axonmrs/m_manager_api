﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class DistrictsFilter : Filter<District>
    {
        public string Name;
        public string Code;
        public int? RegionId;
        public int? Id;
        public int? DistrictId;

        public override IQueryable<District> BuildQuery(IQueryable<District> query)
        {
            query = query.Include(x => x.Region);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (RegionId.HasValue && RegionId.Value > 0) query = query.Where(x => x.RegionId == RegionId.Value);
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (DistrictId.HasValue && DistrictId.Value > 0) query = query.Where(x => x.Id == DistrictId.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class IdTypesFilter : Filter<IdType>
    {
        public string Name;
        public string Code;
        public int Id;

        public override IQueryable<IdType> BuildQuery(IQueryable<IdType> query)
        {
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (Id > 0) query = query.Where(x => x.Id == Id);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
