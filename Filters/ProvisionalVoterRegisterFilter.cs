﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class ProvisionalVoterRegisterFilter : Filter<ProvisionalVoterRegister>
    {

        public long Id;
        public string Reference;
        public string VoterNumber;
        public string Name;
        public string PhoneNumber;
        public Sex? Sex;
        public string Comments;
        public long PollingStationId;
        public long ElectoralAreaId;
        public long ConstituencyId;
        public long DistrictId;
        public long RegionId;
        public string Town;
        public string Tag;

        public override IQueryable<ProvisionalVoterRegister> BuildQuery(IQueryable<ProvisionalVoterRegister> query)
        {
            query = query.Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region).Include(x => x.IdentificationType).Include(x => x.HomeTownDistrict.Region).Include(x => x.ResidentialDistrict.Region); ;
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.Reference.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(VoterNumber)) query = query.Where(q => q.VoterNumber.ToLower().Contains(VoterNumber.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Surname.ToLower().Contains(Name.ToLower()) || q.OtherNames.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Sex.HasValue) query = query.Where(q => q.Sex == Sex);
            if (!string.IsNullOrEmpty(Comments)) query = query.Where(q => q.Comments.ToLower().Contains(Comments.ToLower()));
            if (PollingStationId > 0) query = query.Where(x => x.PollingStationId == PollingStationId);
            if (ElectoralAreaId > 0) query = query.Where(x => x.PollingStation.ElectoralAreaId == ElectoralAreaId);
            if (ConstituencyId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.ConstituencyId == ConstituencyId);
            if (DistrictId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.PollingStation.ElectoralArea.Constituency.District.RegionId == RegionId);
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.ResidentialTown.ToLower().Contains(Name.ToLower()) || q.HomeTown.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Tag)) query = query.Where(q => q.Tags.ToLower().Contains(Tag.ToLower()));

            return query;
        }
    }
}
