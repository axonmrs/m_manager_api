﻿using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class AppSettingsFilter : Filter<AppSetting>
    {
        public int Id;
        public string Name;

        public override IQueryable<AppSetting> BuildQuery(IQueryable<AppSetting> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));

            query = query.Where(q => !q.IsDeleted);
            return query;
        }
    }
}
