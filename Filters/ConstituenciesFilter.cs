﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class ConstituenciesFilter : Filter<Constituency>
    {
        public string Name;
        public string Code;
        public int? RegionId;
        public int? DistrictId;
        public int? ConstituencyId;
        public int? Id;

        public override IQueryable<Constituency> BuildQuery(IQueryable<Constituency> query)
        {
            query = query.Include(x => x.District.Region).Include(x => x.ElectoralAreas);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (RegionId.HasValue && RegionId.Value > 0) query = query.Where(x => x.District.RegionId == RegionId.Value);
            if (DistrictId.HasValue && DistrictId.Value > 0) query = query.Where(x => x.DistrictId == DistrictId.Value);
            if (ConstituencyId.HasValue && ConstituencyId.Value > 0) query = query.Where(x => x.Id == ConstituencyId.Value);
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
