﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class ElectoralAreasFilter : Filter<ElectoralArea>
    {
        public string Name;
        public string Code;
        public int? RegionId;
        public int? DistrictId;
        public int? ConstituencyId;
        public int? Id;
        public int? ElectoralAreaId;

        public override IQueryable<ElectoralArea> BuildQuery(IQueryable<ElectoralArea> query)
        {
            query = query.Include(x => x.Constituency.District.Region).Include(x => x.Stations);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (RegionId.HasValue && RegionId.Value > 0) query = query.Where(x => x.Constituency.District.RegionId == RegionId.Value);
            if (DistrictId.HasValue && DistrictId.Value > 0) query = query.Where(x => x.Constituency.DistrictId == DistrictId.Value);
            if (ConstituencyId.HasValue && ConstituencyId.Value > 0) query = query.Where(x => x.ConstituencyId == ConstituencyId.Value);
            if (ElectoralAreaId.HasValue && ElectoralAreaId.Value > 0) query = query.Where(x => x.Id == ElectoralAreaId.Value);
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
