﻿using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class ChallengeReasonsFilter : Filter<ChallengeReason>
    {
        public string Name;
        public string Code;
        public int Id;

        public override IQueryable<ChallengeReason> BuildQuery(IQueryable<ChallengeReason> query)
        {
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (Id > 0) query = query.Where(x => x.Id == Id);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
