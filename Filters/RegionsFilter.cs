﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class RegionsFilter : Filter<Region>
    {
        
        public int? Id;
        public string Name;
        public string Code;
        public int? RegionId;

        public override IQueryable<Region> BuildQuery(IQueryable<Region> query)
        {
            query = query.Include(x => x.Districts);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (RegionId.HasValue && RegionId.Value > 0) query = query.Where(x => x.Id == RegionId.Value);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
