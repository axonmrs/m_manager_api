﻿using MManager.MrsLogsModels;
using System;
using System.Linq;

namespace MManager.Filters
{
    public class QueryEndOfDayReportsFilter : Filter<EndOfDayReportStatistics>
    {

        public int Id;
        public string PollingStationCode;
        public string PollingStationName;
        public DateTime? DateFrom;
        public DateTime? DateTo;
        public string Device;
        public EndOfDayReportStatisticsStatus? Status;

        public override IQueryable<EndOfDayReportStatistics> BuildQuery(IQueryable<EndOfDayReportStatistics> query)
        {
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (!string.IsNullOrEmpty(Device)) query = query.Where(q => q.Device.ToLower().Contains(Device.ToLower()));
            if (Status.HasValue) query = query.Where(q => q.Status == Status.Value);
            if (!string.IsNullOrEmpty(PollingStationCode)) query = query.Where(q => q.PollingStationCode.ToLower().Contains(PollingStationCode.ToLower()));
            if (!string.IsNullOrEmpty(PollingStationName)) query = query.Where(q => q.PollingStationName.ToLower().Contains(PollingStationName.ToLower()));
            if (DateFrom.HasValue)
            {
                DateFrom = DateFrom.Value.Date;
                query = query.Where(q => q.Date >= DateFrom.Value.Date);
            }
            if (DateTo.HasValue)
            {
                DateTo = DateTo.Value.Date;
                query = query.Where(q => q.Date >= DateTo.Value.Date);
            }

            return query;
        }
    }
}
