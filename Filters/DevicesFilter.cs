﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using System.Linq;

namespace MManager.Filters
{
    public class RegistrationDevicesFilter : Filter<RegistrationDevice>
    {
        public string SerialNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }

        public override IQueryable<RegistrationDevice> BuildQuery(IQueryable<RegistrationDevice> query)
        {
            if (!string.IsNullOrEmpty(SerialNumber)) query = query.Where(q => q.SerialNumber.ToLower().Contains(SerialNumber.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value || x.AssignedToRegion == Assigned.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class UnassignedRegistrationDevicesFilter : Filter<UnassignedRegistrationDevice>
    {
        public string SerialNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int Id { get; set; }

        public override IQueryable<UnassignedRegistrationDevice> BuildQuery(IQueryable<UnassignedRegistrationDevice> query)
        {
            if (!string.IsNullOrEmpty(SerialNumber)) query = query.Where(q => q.Device.SerialNumber.ToLower().Contains(SerialNumber.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Device.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.Device.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id > 0) query = query.Where(x => x.Id == Id);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class RegionRegistrationDevicesFilter : Filter<RegistrationDevice>
    {
        public string SerialNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }
        public int RegionId { get; set; }

        public override IQueryable<RegistrationDevice> BuildQuery(IQueryable<RegistrationDevice> query)
        {
            query = query.Include(x => x.District.Region);
            if (!string.IsNullOrEmpty(SerialNumber)) query = query.Where(q => q.SerialNumber.ToLower().Contains(SerialNumber.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class DistrictRegistrationDevicesFilter : Filter<RegistrationDevice>
    {
        public string SerialNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }

        public override IQueryable<RegistrationDevice> BuildQuery(IQueryable<RegistrationDevice> query)
        {
            query = query.Include(x => x.District.Region);
            if (!string.IsNullOrEmpty(SerialNumber)) query = query.Where(q => q.SerialNumber.ToLower().Contains(SerialNumber.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class VerificationDevicesFilter : Filter<VerificationDevice>
    {
        public string Imei { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }

        public override IQueryable<VerificationDevice> BuildQuery(IQueryable<VerificationDevice> query)
        {
            if (!string.IsNullOrEmpty(Imei)) query = query.Where(q => q.Imei.ToLower().Contains(Imei.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value || x.AssignedToRegion == Assigned.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class UnassignedVerificationDevicesFilter : Filter<UnassignedVerificationDevice>
    {
        public string Imei { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }

        public override IQueryable<UnassignedVerificationDevice> BuildQuery(IQueryable<UnassignedVerificationDevice> query)
        {
            if (!string.IsNullOrEmpty(Imei)) query = query.Where(q => q.Device.Imei.ToLower().Contains(Imei.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Device.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.Device.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class RegionVerificationDevicesFilter : Filter<VerificationDevice>
    {
        public string Imei { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }
        public int RegionId { get; set; }

        public override IQueryable<VerificationDevice> BuildQuery(IQueryable<VerificationDevice> query)
        {
            query = query.Include(x => x.District.Region);
            if (!string.IsNullOrEmpty(Imei)) query = query.Where(q => q.Imei.ToLower().Contains(Imei.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class DistrictVerificationDevicesFilter : Filter<VerificationDevice>
    {
        public string Imei { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public int? Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? Assigned { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }

        public override IQueryable<VerificationDevice> BuildQuery(IQueryable<VerificationDevice> query)
        {
            query = query.Include(x => x.District.Region);
            if (!string.IsNullOrEmpty(Imei)) query = query.Where(q => q.Imei.ToLower().Contains(Imei.ToLower()));
            if (!string.IsNullOrEmpty(Sequence)) query = query.Where(q => q.Sequence.ToLower().Contains(Sequence.ToLower()));
            if (!string.IsNullOrEmpty(IdentificationCode)) query = query.Where(q => q.IdentificationCode.ToLower().Contains(IdentificationCode.ToLower()));
            if (Id.HasValue && Id.Value > 0) query = query.Where(x => x.Id == Id.Value);
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.Value);
            if (Assigned.HasValue) query = query.Where(x => x.AssignedToDistrict == Assigned.Value);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
