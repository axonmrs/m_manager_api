﻿using MManager.Models;
using MManager.MrsLogsModels;
using System;
using System.Linq;

namespace MManager.Repositories
{
    public class LogsRepository : BaseRepository<SystemLog>
    {
        public LogsRepository(ApplicationDbContext context) : base(context)
        {
        }
        public void AddLog(SystemLogType type, string user, string model, string notes = null, string payload = null)
        {
            var log = new SystemLog
            {
                Date = DateTime.Now,
                User = user,
                Model = model,
                Notes = notes,
                Payload = payload,
                Type = type
            };
            _context.SystemLogs.Add(log);
            _context.SaveChanges();
        }

        public void AddLog(ApplicationDbContext db, SystemLogType type, string user, string model, string notes = null, string payload = null)
        {
            var log = new SystemLog
            {
                Date = DateTime.Now,
                User = user,
                Model = model,
                Notes = notes,
                Payload = payload,
                Type = type
            };
            db.SystemLogs.Add(log);
            db.SaveChanges();
        }
    }

    public class TempBioRepository
    {
        public TempBioRepository()
        {
        }
        public string GetPhoto(mrs_logsContext db, long TemporalVoterRegisterId)
        {
            var photo = db.TemporalVoterRegisterBioNew.Where(x => x.TemporalVoterRegisterId == TemporalVoterRegisterId)
                .Select(x=> x.Photo)
                    .FirstOrDefault();
            if (photo != null) 
                return photo;
            var photox = db.TemporalVoterRegisterBio.Where(x => x.TemporalVoterRegisterId == TemporalVoterRegisterId)
                .Select(x => x.Photo)
                    .FirstOrDefault();
            return photox;

        }
    }
}
