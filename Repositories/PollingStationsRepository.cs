﻿using GemBox.Document;
using GemBox.Document.Tables;
using MManager.Models;
using System.Linq;

namespace MManager.Repositories
{
    public class PollingStationsRepository : BaseRepository<Station>
    {
        public PollingStationsRepository(ApplicationDbContext context) : base(context)
        {
        }
        public Table BuildPsTable(int index, DocumentModel templateDoc, Picture qrCode, Station s, string district, string region)
        {
            var table = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"{index}. Ps Code: {s.Code}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, qrCode)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 2,
                    RowSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(33.32, TableWidthUnit.Percentage)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Ps Name: {s.Name}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"District: {district}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Region: {region}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }));

            //table.TableFormat.DefaultCellPadding = new Padding(5);
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var tableBorders = table.TableFormat.Borders;
            tableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.Single, Color.LightGray, 0.1);
            foreach (ParagraphFormat paragraphFormat in table
.GetChildElements(true, ElementType.Paragraph)
.Cast<Paragraph>()
.Select(p => p.ParagraphFormat))
            {
                paragraphFormat.KeepLinesTogether = true;
                paragraphFormat.KeepWithNext = true;
            }
            return table;
        }
    }
}
