﻿using MManager.Models;
using System.Linq;

namespace MManager.Repositories
{
    public class AppSettingRepository : BaseRepository<AppSetting>
    {
        public AppSettingRepository(ApplicationDbContext context) : base(context)
        {
        }
        public AppSetting Get(string name)
        {
            return DbSet.FirstOrDefault(q => q.Name == name);
        }
    }
}
