﻿using GemBox.Document;
using GemBox.Document.Tables;
using GemBox.Spreadsheet;
using Microsoft.EntityFrameworkCore;
using MManager.Helpers;
using MManager.Models;
using MManager.MrsLogsModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Color = GemBox.Document.Color;
using Style = GemBox.Document.Style;

namespace MManager.Repositories
{
    public class ReportsRepository
    {
        public readonly ApplicationDbContext _context;
        public ReportsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Table BuildVoterTable(DocumentModel templateDoc, Picture picture, Picture qrCode, VoterListReportModel s)
        {
            var table = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Voter ID: {s.VoterId}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, picture)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 2,
                    RowSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(33.32, TableWidthUnit.Percentage)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Age: {s.EstimatedAge}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Sex: {s.Sex}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    },
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Name: {s.Surname} {s.OtherNames}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, qrCode)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 3,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(49.98, TableWidthUnit.Percentage),
                        Padding = new Padding(2),
                        VerticalAlignment = VerticalAlignment.Center,
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, "Tick")
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(16.66, TableWidthUnit.Percentage)
                    }
                }));

            //table.TableFormat.DefaultCellPadding = new Padding(5);
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var tableBorders = table.TableFormat.Borders;
            tableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.Single, Color.LightGray, 0.1);
            foreach (ParagraphFormat paragraphFormat in table
.GetChildElements(true, ElementType.Paragraph)
.Cast<Paragraph>()
.Select(p => p.ParagraphFormat))
            {
                paragraphFormat.KeepLinesTogether = true;
                paragraphFormat.KeepWithNext = true;
            }
            return table;
        }

        public Table BuildFinalVoterTable(DocumentModel templateDoc, Picture picture, Picture qrCode, VoterListReportModel s)
        {
            var table = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Voter ID: {s.VoterId}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, picture)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 2,
                    RowSpan = 5,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(33.32, TableWidthUnit.Percentage)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Age: {s.EstimatedAge}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Sex: {s.Sex}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    },
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Name: {s.Surname} {s.OtherNames}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Code: {s.PollingStationCode}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Name: {s.PollingStationName}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }));

            //table.TableFormat.DefaultCellPadding = new Padding(5);
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var tableBorders = table.TableFormat.Borders;
            tableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.Single, Color.LightGray, 0.1);
            foreach (ParagraphFormat paragraphFormat in table
.GetChildElements(true, ElementType.Paragraph)
.Cast<Paragraph>()
.Select(p => p.ParagraphFormat))
            {
                paragraphFormat.KeepLinesTogether = true;
                paragraphFormat.KeepWithNext = true;
            }
            return table;
        }

        public Table BuildVoterDetailsTable(DocumentModel templateDoc, Picture picture, Picture qrCode, VoterRegister s)
        {
            var table = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Voter ID: {s.VoterId}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, picture)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 2,
                    RowSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(33.32, TableWidthUnit.Percentage)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Age: {s.EstimatedAge}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Sex: {s.Sex}"))
                {
                    ColumnSpan = 2,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(33.22, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    },
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Name: {s.Surname} {s.OtherNames}"))
                {
                    ColumnSpan = 4,
                    CellFormat = new TableCellFormat
                    {
                        WrapText = true,
                        PreferredWidth = new TableWidth(66.64, TableWidthUnit.Percentage),
                        Padding = new Padding(2)
                    }
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, qrCode)
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    ColumnSpan = 3,
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(49.98, TableWidthUnit.Percentage),
                        Padding = new Padding(2),
                        VerticalAlignment = VerticalAlignment.Center,
                    }
                },
                new TableCell(templateDoc, new Paragraph(templateDoc, "Tick")
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center } })
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(16.66, TableWidthUnit.Percentage)
                    }
                }));

            //table.TableFormat.DefaultCellPadding = new Padding(5);
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var tableBorders = table.TableFormat.Borders;
            tableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.Single, Color.LightGray, 0.1);
            foreach (ParagraphFormat paragraphFormat in table
.GetChildElements(true, ElementType.Paragraph)
.Cast<Paragraph>()
.Select(p => p.ParagraphFormat))
            {
                paragraphFormat.KeepLinesTogether = true;
                paragraphFormat.KeepWithNext = true;
            }
            return table;
        }

        #region Provisional Voter List Reports
        public List<VoterListReportModel> GenerateProvisionalVoterListReport(VoterListReportFilter filter, User user)
        {
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && !x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();
            return res;
        }
        public string DownloadProvisionalVoterListWordReport(VoterListReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && !x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var ids = res.Select(x => x.Id).ToList();
            var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();

            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var year = DateTime.Now.Year;
            var fileDocx = (dt + "_Prov_Voter_List_Report.docx").Replace("/", "");

            DocumentModel templateDoc = new DocumentModel();

            var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, templateDoc);
            templateDoc.Styles.Add(titleStyle);
            templateDoc.Styles.Add(new CharacterStyle("Emphasis"));
            var strongStyle = (CharacterStyle)templateDoc.Styles.GetOrAdd(StyleTemplateType.Strong);
            var subtitleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, templateDoc);
            templateDoc.Styles.Add(subtitleStyle);
            var section = new Section(templateDoc);
            // Add default (odd) header.
            var headerTable = new Table(templateDoc,
                new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, "ELECTORAL COMMISION GHANA")
                {
                    ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                })
                {
                    ColumnSpan = 2
                }),
                new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PROVISIONAL VOTER REGISTER OF {year}")
                {
                    ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                })
                {
                    ColumnSpan = 2
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Region: {consti.Region}")),
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Constituency: {consti.Name}"))),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Code: {ps.Code}")),
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Name: {ps.Name}"))));
            //headerTable.TableFormat.DefaultCellPadding = new Padding(10);
            headerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var headerTableBorders = headerTable.TableFormat.Borders;
            headerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
            headerTableBorders.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
            headerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
            section.HeadersFooters.Add(
                new GemBox.Document.HeaderFooter(templateDoc, HeaderFooterType.HeaderDefault,
                    headerTable));

            // Add default (odd) footer with page number.
            var footerTable = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Generated On {date}")
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                new TableCell(templateDoc, new Paragraph(templateDoc,
                    new Field(templateDoc, FieldType.Page),
                    new Run(templateDoc, " of "),
                    new Field(templateDoc, FieldType.NumPages))
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                ));
            footerTable.TableFormat.DefaultCellPadding = new Padding(10);
            footerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var footerTableBorders = footerTable.TableFormat.Borders;
            footerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
            footerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
            section.HeadersFooters.Add(
                new GemBox.Document.HeaderFooter(templateDoc, HeaderFooterType.FooterDefault,
                footerTable));
            var blocks = section.Blocks;
            templateDoc.Sections.Add(section);
            var pageSetup = section.PageSetup;
            pageSetup.PageMargins.Top = 5;
            pageSetup.PageMargins.Bottom = 5;
            pageSetup.PaperType = GemBox.Document.PaperType.A4;
            pageSetup.PageMargins.Left = 10;
            pageSetup.PageMargins.Right = 10;

            for (var i = 0; i < res.Count; i++)
            {

                Table tb = new Table(templateDoc);
                if (res.Count > i)
                {
                    var s = res[i];
                    var bio = bios.First(x => x.VoterRegisterId == s.Id);
                    var imagePath = ImageHelpers.SaveImage(bio.Photo);
                    var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);
                    Picture qrCode = new Picture(templateDoc, qrCodePath, 50, 50);
                    Picture picture = new Picture(templateDoc, imagePath, 80, 100);
                    tb = BuildVoterTable(templateDoc, picture, qrCode, s);
                }

                i = i + 1;
                Table tb2 = new Table(templateDoc);
                if (res.Count > i)
                {
                    var ss = res[i];
                    var bio2 = bios.First(x => x.VoterRegisterId == ss.Id);
                    var imagePath2 = ImageHelpers.SaveImage(bio2.Photo);
                    var qrCodePath2 = ImageHelpers.GenerateVoterQRCode(ss.VoterId);
                    Picture qrCode2 = new Picture(templateDoc, qrCodePath2, 50, 50);
                    Picture picture2 = new Picture(templateDoc, imagePath2, 80, 100);
                    tb2 = BuildVoterTable(templateDoc, picture2, qrCode2, ss);
                }
                var xTable = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, tb)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                },
                new TableCell(templateDoc, tb2)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                }));
                //xTable.TableFormat.DefaultCellPadding = new Padding(0,2);
                xTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                var xTableBorders = xTable.TableFormat.Borders;
                xTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                xTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);

                foreach (ParagraphFormat paragraphFormat in xTable
                    .GetChildElements(true, ElementType.Paragraph)
                    .Cast<Paragraph>()
                    .Select(p => p.ParagraphFormat))
                {
                    paragraphFormat.KeepLinesTogether = true;
                    paragraphFormat.KeepWithNext = true;
                }
                blocks.Add(xTable);
                var size = 12;
                if (i == 11 || (i + size) % size == 11)
                    blocks.Add(new Paragraph(templateDoc, new SpecialCharacter(templateDoc, SpecialCharacterType.PageBreak)));
            }
            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadProvisionalVoterListExcelReport(VoterListReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && !x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var data = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Prov_Voter_List_Report";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 10).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 10).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "PROVISIONAL VOTER REGISTER";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Status", "Voter Id", "Surname", "Other Names", "Sex", "Estimated Age", "Polling Station Code", "Polling Station Name", "Electoral Area", "Constituency", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add("PROVISIONAL", sub.VoterId, sub.Surname, sub.OtherNames, sub.Sex.ToString(), sub.EstimatedAge, sub.PollingStationCode, sub.PollingStationName, ea.Name, consti.Name, consti.District, consti.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }

        #endregion

        #region Final Voter List Reports
        public List<VoterListReportModel> GenerateFinalVoterListReport(VoterListReportFilter filter, User user)
        {
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();
            return res;
        }
        public string DownloadFinalVoterListWordReport(VoterListReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var ids = res.Select(x => x.Id).ToList();
            var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
                    .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();

            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var year = DateTime.Now.Year;
            var fileDocx = (dt + "_Final_Voter_List_Report.docx").Replace("/", "");

            DocumentModel templateDoc = new DocumentModel();

            var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, templateDoc);
            templateDoc.Styles.Add(titleStyle);
            templateDoc.Styles.Add(new CharacterStyle("Emphasis"));
            var strongStyle = (CharacterStyle)templateDoc.Styles.GetOrAdd(StyleTemplateType.Strong);
            var subtitleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, templateDoc);
            templateDoc.Styles.Add(subtitleStyle);
            var section = new Section(templateDoc);
            // Add default (odd) header.
            var headerTable = new Table(templateDoc,
                new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, "ELECTORAL COMMISION GHANA")
                {
                    ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                })
                {
                    ColumnSpan = 2
                }),
                new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"FINAL VOTER REGISTER OF {year}")
                {
                    ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                })
                {
                    ColumnSpan = 2
                }),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Region: {consti.Region}")),
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Constituency: {consti.Name}"))),
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Code: {ps.Code}")),
                new TableCell(templateDoc, new Paragraph(templateDoc, $"PS Name: {ps.Name}"))));
            //headerTable.TableFormat.DefaultCellPadding = new Padding(10);
            headerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var headerTableBorders = headerTable.TableFormat.Borders;
            headerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
            headerTableBorders.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
            headerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
            section.HeadersFooters.Add(
                new GemBox.Document.HeaderFooter(templateDoc, HeaderFooterType.HeaderDefault,
                    headerTable));

            // Add default (odd) footer with page number.
            var footerTable = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, new Paragraph(templateDoc, $"Generated On {date}")
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                new TableCell(templateDoc, new Paragraph(templateDoc,
                    new Field(templateDoc, FieldType.Page),
                    new Run(templateDoc, " of "),
                    new Field(templateDoc, FieldType.NumPages))
                { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                ));
            footerTable.TableFormat.DefaultCellPadding = new Padding(10);
            footerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            var footerTableBorders = footerTable.TableFormat.Borders;
            footerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
            footerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
            section.HeadersFooters.Add(
                new GemBox.Document.HeaderFooter(templateDoc, HeaderFooterType.FooterDefault,
                footerTable));
            var blocks = section.Blocks;
            templateDoc.Sections.Add(section);
            var pageSetup = section.PageSetup;
            pageSetup.PageMargins.Top = 5;
            pageSetup.PageMargins.Bottom = 5;
            pageSetup.PaperType = GemBox.Document.PaperType.A4;
            pageSetup.PageMargins.Left = 10;
            pageSetup.PageMargins.Right = 10;

            for (var i = 0; i < res.Count; i++)
            {
                Table tb = new Table(templateDoc);
                if (res.Count > i)
                {
                    var s = res[i];
                    var bio = bios.First(x => x.VoterRegisterId == s.Id);
                    var imagePath = ImageHelpers.SaveImage(bio.Photo);
                    var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);
                    Picture qrCode = new Picture(templateDoc, qrCodePath, 50, 50);
                    Picture picture = new Picture(templateDoc, imagePath, 80, 100);
                    tb = BuildFinalVoterTable(templateDoc, picture, qrCode, s);
                }

                i = i + 1;
                Table tb2 = new Table(templateDoc);
                if (res.Count > i)
                {
                    var ss = res[i];
                    var bio2 = bios.First(x => x.VoterRegisterId == ss.Id);
                    var imagePath2 = ImageHelpers.SaveImage(bio2.Photo);
                    var qrCodePath2 = ImageHelpers.GenerateVoterQRCode(ss.VoterId);
                    Picture qrCode2 = new Picture(templateDoc, qrCodePath2, 50, 50);
                    Picture picture2 = new Picture(templateDoc, imagePath2, 80, 100);
                    tb2 = BuildFinalVoterTable(templateDoc, picture2, qrCode2, ss);
                }
                var xTable = new Table(templateDoc,
            new TableRow(templateDoc,
                new TableCell(templateDoc, tb)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                },
                new TableCell(templateDoc, tb2)
                {
                    CellFormat = new TableCellFormat
                    {
                        PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                    }
                }));
                //xTable.TableFormat.DefaultCellPadding = new Padding(0, 2);
                xTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                var xTableBorders = xTable.TableFormat.Borders;
                xTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                xTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);

                foreach (ParagraphFormat paragraphFormat in xTable
                    .GetChildElements(true, ElementType.Paragraph)
                    .Cast<Paragraph>()
                    .Select(p => p.ParagraphFormat))
                {
                    paragraphFormat.KeepLinesTogether = true;
                    paragraphFormat.KeepWithNext = true;
                }
                blocks.Add(xTable);
                var size = 12;
                if (i == 11 || (i + size) % size == 11)
                    blocks.Add(new Paragraph(templateDoc, new SpecialCharacter(templateDoc, SpecialCharacterType.PageBreak)));
            }
            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadFinalVoterListExcelReport(VoterListReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var ps = _context.Stations.Where(x => x.Id == filter.PollingStationId)
                .Select(x => new Station { Id = x.Id, ElectoralAreaId = x.ElectoralAreaId, Name = x.Name, Code = x.Code })
                .FirstOrDefault();
            if (ps == null) throw new Exception("Please provide a valid polling station code");
            var ea = _context.ElectoralAreas.Where(x => x.Id == ps.ElectoralAreaId)
                .Select(x => new ElectoralArea { Id = x.Id, ConstituencyId = x.ConstituencyId, Name = x.Name, Code = x.Code })
                .First();
            var consti = _context.Constituencies.Where(x => x.Id == ea.ConstituencyId)
                .Select(x => new
                {
                    Id = x.Id,
                    DistrictId = x.DistrictId,
                    Name = x.Name,
                    Code = x.Code,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                })
                .First();
            var voters = _context.VoterRegister.Where(x => !x.HasDuplicate && x.Reviewed && x.PollingStationId == filter.PollingStationId);
            if (user.Type != UserType.System)
            {
                var psIds = new List<int>();
                if (user.Type == UserType.Regional)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
                }
                else if (user.Type == UserType.District)
                {
                    psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
                }
                voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
            }
            var data = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
            {
                Id = x.Id,
                VoterId = x.VoterId,
                Surname = x.Surname,
                OtherNames = x.OtherNames,
                PollingStationName = x.PollingStationName,
                PollingStationCode = x.PollingStationCode,
                EstimatedAge = x.EstimatedAge,
                Sex = x.Sex.ToString()
            }).ToList();

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Final_Voter_List_Report";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 10).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 10).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "FINAL VOTER REGISTER";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Voter Id", "Surname", "Other Names", "Sex", "Estimated Age", "Polling Station Code", "Polling Station Name", "Electoral Area", "Constituency", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.VoterId, sub.Surname, sub.OtherNames, sub.Sex.ToString(), sub.EstimatedAge, sub.PollingStationCode, sub.PollingStationName, ea.Name, consti.Name, consti.District, consti.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }

        #endregion

        //#region Voter List Reports
        //public List<VoterListReportModel> GenerateVoterListReport(VoterListReportFilter filter, User user)
        //{
        //    var voters = _context.VoterRegister.Where(x => !x.HasDuplicate);
        //    if (user.Type != UserType.System)
        //    {
        //        var psIds = new List<int>();
        //        if (user.Type == UserType.Regional)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
        //        }
        //        else if (user.Type == UserType.District)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
        //        }
        //        voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
        //    }

        //    if (!string.IsNullOrEmpty(filter.PollingStationCode)) voters = voters.Where(x =>
        //    x.PollingStationCode.ToLower().Contains(filter.PollingStationCode.ToLower()));

        //    var res = voters.OrderBy(x => x.VoterId).Take(filter.Size).ToList().Select(x => new VoterListReportModel
        //    {
        //        VoterId = x.VoterId,
        //        Surname = x.Surname,
        //        OtherNames = x.OtherNames,
        //        PollingStationName = x.PollingStationName,
        //        PollingStationCode = x.PollingStationCode,
        //        EstimatedAge = x.EstimatedAge,
        //        Sex = x.Sex.ToString()
        //    }).ToList();
        //    return res;
        //}
        //public string DownloadVoterListWordReport(VoterListReportFilter filter, User user)
        //{
        //    var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
        //    var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
        //    var voters = _context.VoterRegister.Where(x => !x.HasDuplicate);
        //    if (user.Type != UserType.System)
        //    {
        //        var psIds = new List<int>();
        //        if (user.Type == UserType.Regional)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
        //        }
        //        else if (user.Type == UserType.District)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
        //        }
        //        voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
        //    }

        //    if (!string.IsNullOrEmpty(filter.PollingStationCode)) voters = voters.Where(x =>
        //    x.PollingStationCode.ToLower().Contains(filter.PollingStationCode.ToLower()));

        //    var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
        //    {
        //        Id = x.Id,
        //        VoterId = x.VoterId,
        //        Surname = x.Surname,
        //        OtherNames = x.OtherNames,
        //        PollingStationName = x.PollingStationName,
        //        PollingStationCode = x.PollingStationCode,
        //        EstimatedAge = x.EstimatedAge,
        //        Sex = x.Sex.ToString()
        //    }).ToList();
        //    if (!res.Any()) throw new Exception("This report has no data");
        //    var ids = res.Select(x => x.Id).ToList();
        //    var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
        //            .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList();

        //    var dt = DateTime.Now.ToFileTime();
        //    var date = DateTime.Now.ToShortDateString();
        //    var fileDocx = (dt + "_Voter_List_Report.docx").Replace("/", "");

        //    DocumentModel templateDoc = new DocumentModel();
        //    var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, templateDoc);
        //    var emphasisStyle = new CharacterStyle("Emphasis");
        //    emphasisStyle.CharacterFormat.Italic = true;
        //    templateDoc.Styles.Add(titleStyle);
        //    templateDoc.Styles.Add(emphasisStyle);
        //    var strongStyle = (CharacterStyle)templateDoc.Styles.GetOrAdd(StyleTemplateType.Strong);
        //    var section = new Section(templateDoc);
        //    var blocks = section.Blocks;
        //    templateDoc.Sections.Add(section);
        //    blocks.Add(new Paragraph(templateDoc, "VOTER LIST FOR VERIFICATION") { ParagraphFormat = { Style = titleStyle } });

        //    var index = 1;
        //    foreach (var s in res)
        //    {
        //        var paragraph = new Paragraph(templateDoc);
        //        blocks.Add(paragraph);
        //        var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);
        //        Picture qrCode = new Picture(templateDoc, qrCodePath, 60, 60);
        //        paragraph.Inlines.Add(qrCode);
        //        blocks.Add(new Paragraph(templateDoc, $"{index}. {s.VoterId} - {s.Surname} {s.OtherNames}"));
        //        index++;

        //        blocks.Add(new Paragraph(templateDoc));
        //    }

        //    if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
        //    templateDoc.Save(tempFolder + fileDocx);

        //    byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
        //    File.Delete(tempFolder + fileDocx);
        //    return Convert.ToBase64String(bytes);
        //}

        //public string DownloadVoterListExcelReport(VoterListReportFilter filter, User user)
        //{
        //    var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
        //    var voters = _context.VoterRegister.Where(x => !x.HasDuplicate);
        //    if (user.Type != UserType.System)
        //    {
        //        var psIds = new List<int>();
        //        if (user.Type == UserType.Regional)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
        //        }
        //        else if (user.Type == UserType.District)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
        //        }
        //        voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
        //    }

        //    if (!string.IsNullOrEmpty(filter.PollingStationCode)) voters = voters.Where(x =>
        //    x.PollingStationCode.ToLower().Contains(filter.PollingStationCode.ToLower()));

        //    var data = voters.OrderBy(x => x.VoterId).ToList();

        //    var date = DateTime.Now.ToShortDateString();
        //    var sheetName = "Voter_List_Report";
        //    ExcelFile ef = new ExcelFile();
        //    ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
        //    ws.Cells.GetSubrangeAbsolute(0, 0, 0, 10).Merged = true;
        //    ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
        //    ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
        //    ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
        //    ws.Cells.GetSubrangeAbsolute(1, 0, 1, 10).Merged = true;
        //    ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
        //    ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
        //    ws.Cells[1, 0].Value = "VOTER LIST REPORT";

        //    DataTable dt = new DataTable();
        //    var headers = new List<string> { "Voter Id", "Surname", "Other Names", "Sex", "Estimated Age", "Polling Station Code", "Polling Station Name", "Electoral Area", "Constituency", "District", "Region" };
        //    for (var i = 0; i < headers.Count(); i++)
        //    {
        //        var str = headers[i];
        //        dt.Columns.Add(str.ToUpper(), typeof(string));
        //    }

        //    foreach (var sub in data)
        //    {
        //        var region = sub.PollingStation?.ElectoralArea?.Constituency?.District?.Region?.Name;
        //        var district = sub.PollingStation?.ElectoralArea?.Constituency?.District?.Name;
        //        var consti = sub.PollingStation?.ElectoralArea?.Constituency?.Name;
        //        var ea = sub.PollingStation?.ElectoralArea?.Name;
        //        dt.Rows.Add(sub.VoterId, sub.Surname, sub.OtherNames, sub.Sex.ToString(), sub.EstimatedAge, sub.PollingStationCode, sub.PollingStationName, ea, consti, district, region);
        //    }

        //    ws.InsertDataTable(dt,
        //            new InsertDataTableOptions
        //            {
        //                ColumnHeaders = true,
        //                StartRow = 3
        //            });
        //    var now = DateTime.Now;
        //    var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
        //    var filename = $"{sheetName}-{nowDate}.xlsx";
        //    ef.SaveXlsx(tempFolder + filename);
        //    var bytes = File.ReadAllBytes(tempFolder + filename);
        //    File.Delete(tempFolder + filename);
        //    return Convert.ToBase64String(bytes);
        //}

        //public string DownloadVoterListPdfReport(VoterListReportFilter filter, User user)
        //{
        //    var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
        //    var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
        //    var voters = _context.VoterRegister.Where(x => !x.HasDuplicate);
        //    if (user.Type != UserType.System)
        //    {
        //        var psIds = new List<int>();
        //        if (user.Type == UserType.Regional)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Id).ToList();
        //        }
        //        else if (user.Type == UserType.District)
        //        {
        //            psIds = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == user.DistrictId).Include(x => x.ElectoralArea.Constituency).Select(x => x.Id).ToList();
        //        }
        //        voters = voters.Where(x => x.PollingStationId.HasValue && psIds.Contains(x.PollingStationId.Value));
        //    }

        //    if (!string.IsNullOrEmpty(filter.PollingStationCode)) voters = voters.Where(x =>
        //    x.PollingStationCode.ToLower().Contains(filter.PollingStationCode.ToLower()));

        //    var res = voters.OrderBy(x => x.VoterId).ToList().Select(x => new VoterListReportModel
        //    {
        //        Id = x.Id,
        //        VoterId = x.VoterId,
        //        Surname = x.Surname,
        //        OtherNames = x.OtherNames,
        //        PollingStationName = x.PollingStationName,
        //        PollingStationCode = x.PollingStationCode,
        //        EstimatedAge = x.EstimatedAge,
        //        Sex = x.Sex.ToString()
        //    }).ToList();
        //    if (!res.Any()) throw new Exception("This report has no data");
        //    var ids = res.Select(x => x.Id).ToList();
        //    var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
        //            .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId }).ToList(); ;

        //    var dt = DateTime.Now.ToFileTime();
        //    var date = DateTime.Now.ToShortDateString();
        //    var filePdf = (dt + "_Voter_List_Report.pdf").Replace("/", "");

        //    DocumentModel templateDoc = DocumentModel.Load(template);
        //    templateDoc.Content.Replace("[TITLE]", "VOTER LIST FOR VERIFICATION REPORT");

        //    var headers = new List<string> { "#", "QrCode", "Photo", "Voter Id", "Surname", "Other Names", "Sex", "Estimated Age", "Polling Station" };
        //    int dtRowCount = res.Count() + 1;
        //    int dtColumnCount = headers.Count();
        //    DataTable dataTable = new DataTable();
        //    DataRow headerRow = dataTable.NewRow();
        //    for (int i = 0; i < dtColumnCount; i++)
        //    {
        //        dataTable.Columns.Add(headers[i]);
        //        headerRow[i] = $"{headers[i].ToUpper()}";
        //    }
        //    dataTable.Rows.Add(headerRow);

        //    TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
        //    templateDoc.Styles.Add(lightShadingStyle);

        //    var tabIndex = 1;
        //    foreach (var s in res)
        //    {
        //        var bio = bios.FirstOrDefault(x => x.VoterRegisterId == s.Id);
        //        if (bio == null) continue;

        //        var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);

        //        var photoPath = Path.Combine(tempFolder, s.Id + ".jpeg");
        //        byte[] photoBytes = Convert.FromBase64String(bio.Photo);
        //        File.WriteAllBytes(photoPath, photoBytes);
        //        Picture photo = new Picture(templateDoc, photoPath, 50, 50);

        //        DataRow row = dataTable.NewRow();
        //        row[0] = $"{tabIndex.ToString()}";
        //        row[1] = $"qrcode";
        //        row[2] = $"{bio.Photo}";
        //        row[3] = $"{s.VoterId}";
        //        row[4] = $"{s.Surname}";
        //        row[5] = $"{s.OtherNames}";
        //        row[6] = $"{s.Sex}";
        //        row[7] = $"{s.EstimatedAge}";
        //        row[8] = $"{s.PollingStationCode}";
        //        tabIndex = tabIndex + 1;
        //        dataTable.Rows.Add(row);
        //    }

        //    Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
        //    (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
        //    table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
        //    table.TableFormat.Style = lightShadingStyle;

        //    ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
        //    tablePlaceholder.Start.InsertRange(table.Content);
        //    templateDoc.Content.Replace("[TABLE]", "");

        //    if (File.Exists(tempFolder + filePdf)) File.Delete(tempFolder + filePdf);
        //    templateDoc.Save(tempFolder + filePdf);

        //    byte[] bytes = File.ReadAllBytes(tempFolder + filePdf);
        //    File.Delete(tempFolder + filePdf);
        //    return Convert.ToBase64String(bytes);
        //}
        //#endregion

        #region Regional BVR Packing Slip Reports
        public List<PackingSlipReportModel> GenerateRegionalBVRPackingSlipReport(PackingSlipReportFilter filter, User user)
        {
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var res = devices.OrderBy(x => x.IdentificationCode).Take(filter.Size).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            return res;
        }
        public string DownloadRegionalBVRPackingSlipWordReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var res = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_Regional_BVR_Packing_Slip_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "REGIONAL BVR PACKING SLIP REPORT");

            var headers = new List<string> { "#", "ID Code", "Serial Number", "Region", "Assigned On", "Confirmed" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.IdentificationCode}";
                row[2] = $"{s.SerialNumber}";
                row[3] = $"{s.Region}";
                row[4] = $"{s.AssignedToRegionOn}";
                row[5] = $"{s.Confirmed.ToString()}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadRegionalBVRPackingSlipExcelReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var data = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Regional_BVR_Packing_Slip";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 4).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 4).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "REGIONAL BVR PACKING SLIP REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "ID Code", "Serial Number", "Region", "Assigned On", "Confirmed" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.IdentificationCode, sub.SerialNumber, sub.Region, sub.AssignedToRegionOn, sub.Confirmed.ToString());
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Regional BVD Packing Slip Reports
        public List<PackingSlipReportModel> GenerateRegionalBVDPackingSlipReport(PackingSlipReportFilter filter, User user)
        {
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var res = devices.OrderBy(x => x.IdentificationCode).Take(filter.Size).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            return res;
        }
        public string DownloadRegionalBVDPackingSlipWordReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var res = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_Regional_BVD_Packing_Slip_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "REGIONAL BVD PACKING SLIP REPORT");

            var headers = new List<string> { "#", "ID Code", "IMEI", "Region", "Assigned On", "Confirmed" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.IdentificationCode}";
                row[2] = $"{s.Imei}";
                row[3] = $"{s.Region}";
                row[4] = $"{s.AssignedToRegionOn}";
                row[5] = $"{s.Confirmed.ToString()}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadRegionalBVDPackingSlipExcelReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            if (user.Type == UserType.District) throw new Exception("You are not permited to generate this report");
            if (filter.RegionId == 0) throw new Exception("Please select a region");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToRegion && x.RegionId == filter.RegionId);

            var data = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToRegionOn = x.AssignedToRegionOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.Region.Name,
                Confirmed = x.RegionConfirmed
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Regional_BVD_Packing_Slip";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 4).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 4).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "REGIONAL BVD PACKING SLIP REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "ID Code", "IMEI", "Region", "Assigned On", "Confirmed" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.IdentificationCode, sub.Imei, sub.Region, sub.AssignedToRegionOn, sub.Confirmed.ToString());
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region District BVR Packing Slip Reports
        public List<PackingSlipReportModel> GenerateDistrictBVRPackingSlipReport(PackingSlipReportFilter filter, User user)
        {
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var res = devices.OrderBy(x => x.IdentificationCode).Take(filter.Size).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            return res;
        }
        public string DownloadDistrictBVRPackingSlipWordReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var res = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_District_BVR_Packing_Slip_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "DISTRICT BVR PACKING SLIP REPORT");

            var headers = new List<string> { "#", "ID Code", "Serial Number", "Activation Code", "Region", "District", "Assigned On", "Confirmed" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.IdentificationCode}";
                row[2] = $"{s.SerialNumber}";
                row[3] = $"{s.ActivationCode}";
                row[4] = $"{s.Region}";
                row[5] = $"{s.District}";
                row[6] = $"{s.AssignedToDistrictOn}";
                row[7] = $"{s.Confirmed.ToString()}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadDistrictBVRPackingSlipExcelReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var data = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "District_BVR_Packing_Slip";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "DISTRICT BVR PACKING SLIP REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "ID Code", "Serial Number", "Activation Code", "Region", "District", "Assigned On", "Confirmed" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.IdentificationCode, sub.SerialNumber, sub.ActivationCode, sub.Region, sub.District, sub.AssignedToDistrictOn, sub.Confirmed.ToString());
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadAllDistrictsBVRPackingSlipWordReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            var devices = _context.RegistrationDevices.Where(x => x.AssignedToDistrict);

            var res = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                SerialNumber = x.SerialNumber,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();

            var dists = res.Select(x => x.District).Distinct().ToList();
            foreach (var dst in dists)
            {
                var data = res.Where(x => x.District == dst).ToList();
                var fileDocx = (dst + "_BVR_Packing_Slip_Report.docx").Replace("/", "");

                DocumentModel templateDoc = DocumentModel.Load(template);
                templateDoc.Content.Replace("[TITLE]", "DISTRICT BVR PACKING SLIP REPORT");

                var headers = new List<string> { "#", "ID Code", "Serial Number", "Activation Code", "Region", "District", "Assigned On", "Confirmed" };
                int dtRowCount = data.Count() + 1;
                int dtColumnCount = headers.Count();
                DataTable dataTable = new DataTable();
                DataRow headerRow = dataTable.NewRow();
                for (int i = 0; i < dtColumnCount; i++)
                {
                    dataTable.Columns.Add(headers[i]);
                    headerRow[i] = $"{headers[i].ToUpper()}";
                }
                dataTable.Rows.Add(headerRow);

                TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
                templateDoc.Styles.Add(lightShadingStyle);

                var tabIndex = 1;
                foreach (var s in data)
                {
                    DataRow row = dataTable.NewRow();
                    row[0] = $"{tabIndex.ToString()}";
                    row[1] = $"{s.IdentificationCode}";
                    row[2] = $"{s.SerialNumber}";
                    row[3] = $"{s.ActivationCode}";
                    row[4] = $"{s.Region}";
                    row[5] = $"{s.District}";
                    row[6] = $"{s.AssignedToDistrictOn}";
                    row[7] = $"{s.Confirmed.ToString()}";
                    tabIndex = tabIndex + 1;
                    dataTable.Rows.Add(row);
                }

                Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
                (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
                table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                table.TableFormat.Style = lightShadingStyle;

                ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
                tablePlaceholder.Start.InsertRange(table.Content);
                templateDoc.Content.Replace("[TABLE]", "");
                var pth = Path.Combine(tempFolder, fileDocx);
                if (File.Exists(pth)) File.Delete(pth);
                templateDoc.Save(pth);
            }
            return "";
        }
        #endregion

        #region District BVD Packing Slip Reports
        public List<PackingSlipReportModel> GenerateDistrictBVDPackingSlipReport(PackingSlipReportFilter filter, User user)
        {
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var res = devices.OrderBy(x => x.IdentificationCode).Take(filter.Size).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            return res;
        }
        public string DownloadDistrictBVDPackingSlipWordReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");

            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var res = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_District_BVD_Packing_Slip_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "DISTRICT BVD PACKING SLIP REPORT");

            var headers = new List<string> { "#", "ID Code", "IMEI", "Activation Code", "Region", "District", "Assigned On", "Confirmed" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.IdentificationCode}";
                row[2] = $"{s.Imei}";
                row[3] = $"{s.ActivationCode}";
                row[4] = $"{s.Region}";
                row[5] = $"{s.District}";
                row[6] = $"{s.AssignedToDistrictOn}";
                row[7] = $"{s.Confirmed.ToString()}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadDistrictBVDPackingSlipExcelReport(PackingSlipReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.VerificationDevices.Where(x => x.AssignedToDistrict && x.DistrictId == filter.DistrictId);

            var data = devices.OrderBy(x => x.IdentificationCode).ToList().Select(x => new PackingSlipReportModel
            {
                Id = x.Id,
                IdentificationCode = x.IdentificationCode,
                Imei = x.Imei,
                AssignedToDistrictOn = x.AssignedToDistrictOn?.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                Confirmed = x.DistrictConfirmed,
                ActivationCode = x.ActivationCode
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "District_BVD_Packing_Slip";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "DISTRICT BVD PACKING SLIP REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "ID Code", "IMEI", "Activation Code", "Region", "District", "Assigned On", "Confirmed" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.IdentificationCode, sub.Imei, sub.ActivationCode, sub.Region, sub.District, sub.AssignedToDistrictOn, sub.Confirmed.ToString());
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Uploaded MRT Files Reports
        public List<MrtFilesReportModel> GenerateUploadedMrtFilesListReport(UploadedMrtFilesReportFilter filter, User user)
        {
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.UploadedMrtFiles.Where(x => x.DistrictId == filter.DistrictId);

            var res = devices.OrderByDescending(x => x.CreatedAt).Take(filter.Size).ToList().Select(x => new MrtFilesReportModel
            {
                Id = x.Id,
                FileName = x.FileName,
                Reference = x.Reference,
                UploadedAt = x.CreatedAt.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                UploadedBy = x.CreatedBy,
                Status = x.Status.ToString()
            }).ToList();
            return res;
        }
        public string DownloadUploadedMrtFilesWordReport(UploadedMrtFilesReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.UploadedMrtFiles.Where(x => x.DistrictId == filter.DistrictId);

            var res = devices.OrderByDescending(x => x.CreatedAt).ToList().Select(x => new MrtFilesReportModel
            {
                Id = x.Id,
                FileName = x.FileName,
                Reference = x.Reference,
                UploadedAt = x.CreatedAt.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                UploadedBy = x.CreatedBy,
                Status = x.Status.ToString()
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_UploadedMrtFiles.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "UPLOADED MRT FILES REPORT");

            var headers = new List<string> { "#", "File Name", "Reference", "Uploaded By", "Uploaded On", "Status", "District", "Region" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)GemBox.Document.Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.FileName}";
                row[2] = $"{s.Reference}";
                row[3] = $"{s.UploadedBy}";
                row[4] = $"{s.UploadedAt}";
                row[5] = $"{s.Status}";
                row[6] = $"{s.District}";
                row[7] = $"{s.Region}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadUploadedMrtFilesExcelReport(UploadedMrtFilesReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            if (filter.DistrictId == 0) throw new Exception("Please select a district");
            var devices = _context.UploadedMrtFiles.Where(x => x.DistrictId == filter.DistrictId);

            var data = devices.OrderByDescending(x => x.CreatedAt).ToList().Select(x => new MrtFilesReportModel
            {
                Id = x.Id,
                FileName = x.FileName,
                Reference = x.Reference,
                UploadedAt = x.CreatedAt.ToString("dd/MM/yyyy HH:mm:ss"),
                Region = x.District.Region.Name,
                District = x.District.Name,
                UploadedBy = x.CreatedBy,
                Status = x.Status.ToString()
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Mrt Files";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "UPLOADED MRT FILES REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "File Name", "Reference", "Uploaded By", "Uploaded On", "Status", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.FileName, sub.Reference, sub.UploadedBy, sub.UploadedAt, sub.Status, sub.District, sub.Region.ToString());
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Regions Reports
        public List<RegionsReportModel> GenerateRegionsListReport(SettingsReportFilter filter, User user)
        {
            var records = _context.Regions.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));

            var res = records.OrderBy(x => x.Code).Take(filter.Size).ToList().Select(x => new RegionsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code
            }).ToList();
            return res;
        }
        public string DownloadRegionsWordReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            var records = _context.Regions.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));

            var res = records.OrderBy(x => x.Code).ToList().Select(x => new RegionsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "RegionsReport.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "REGIONS REPORT");

            var headers = new List<string> { "#", "Name", "Code" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Name}";
                row[2] = $"{s.Code}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string DownloadRegionsExcelReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var records = _context.Regions.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));

            var data = records.OrderBy(x => x.Code).ToList().Select(x => new RegionsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Regions";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "REGIONS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Name", "Code" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Name, sub.Code);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.Save(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Districts Reports
        public List<DistrictsReportModel> GenerateDistrictsListReport(SettingsReportFilter filter, User user)
        {
            var records = _context.Districts.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.RegionId == filter.RegionId.Value);

            var res = records.Include(x => x.Region).OrderBy(x => x.Code).Take(filter.Size).ToList().Select(x => new DistrictsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Region = $"{x.Region.Code}-{x.Region.Name}"
            }).ToList();
            return res;
        }
        public string DownloadDistrictsWordReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            var records = _context.Districts.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.RegionId == filter.RegionId.Value);

            var res = records.Include(x => x.Region).OrderBy(x => x.Code).ToList().Select(x => new DistrictsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Region = $"{x.Region.Code}-{x.Region.Name}"
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "DistrictsReport.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "DISTRICTS REPORT");

            var headers = new List<string> { "#", "Name", "Code", "Region" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Name}";
                row[2] = $"{s.Code}";
                row[3] = $"{s.Region}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }
        public string DownloadDistrictsExcelReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var records = _context.Districts.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.RegionId == filter.RegionId.Value);

            var data = records.Include(x => x.Region).OrderBy(x => x.Code).ToList().Select(x => new DistrictsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Region = $"{x.Region.Code}-{x.Region.Name}"
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Districts";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "DISTRICTS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Name", "Code", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Name, sub.Code, sub.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.Save(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Constituencies Reports
        public List<ConstituenciesReportModel> GenerateConstituenciesListReport(SettingsReportFilter filter, User user)
        {
            var records = _context.Constituencies.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.DistrictId == filter.DistrictId.Value);

            var res = records.Include(x => x.District.Region).OrderBy(x => x.Code).Take(filter.Size).ToList().Select(x => new ConstituenciesReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                District = $"{x.District.Code}-{x.District.Name}",
                Region = $"{x.District.Region.Code}-{x.District.Region.Name}"
            }).ToList();
            return res;
        }
        public string DownloadConstituenciesWordReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp1.docx");
            var records = _context.Constituencies.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.DistrictId == filter.DistrictId.Value);

            var res = records.Include(x => x.District.Region).OrderBy(x => x.Code).ToList().Select(x => new ConstituenciesReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                District = $"{x.District.Code}-{x.District.Name}",
                Region = $"{x.District.Region.Code}-{x.District.Region.Name}"
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "ConstReport.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "CONSTITUENCIES REPORT");

            var headers = new List<string> { "#", "Name", "Code", "District", "Region" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Name}";
                row[2] = $"{s.Code}";
                row[3] = $"{s.District}";
                row[4] = $"{s.Region}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }
        public string DownloadConstituenciesExcelReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var records = _context.Constituencies.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.DistrictId == filter.DistrictId.Value);

            var data = records.Include(x => x.District.Region).OrderBy(x => x.Code).ToList().Select(x => new ConstituenciesReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                District = $"{x.District.Code}-{x.District.Name}",
                Region = $"{x.District.Region.Code}-{x.District.Region.Name}"
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Consts";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 3).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 3).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "CONSTITUENCIES REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Name", "Code", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Name, sub.Code, sub.District, sub.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.Save(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Electoral Areas Reports
        public List<ElectoralAreasReportModel> GenerateElectoralAreasListReport(SettingsReportFilter filter, User user)
        {
            var records = _context.ElectoralAreas.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ConstituencyId == filter.ConstituencyId.Value);

            var res = records.Include(x => x.Constituency.District.Region).OrderBy(x => x.Code).Take(filter.Size).ToList().Select(x => new ElectoralAreasReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Constituency = $"{x.Constituency.Code}-{x.Constituency.Name}",
                District = $"{x.Constituency.District.Code}-{x.Constituency.District.Name}",
                Region = $"{x.Constituency.District.Region.Code}-{x.Constituency.District.Region.Name}"
            }).ToList();
            return res;
        }
        public string DownloadElectoralAreasWordReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            var records = _context.ElectoralAreas.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ConstituencyId == filter.ConstituencyId.Value);

            var res = records.Include(x => x.Constituency.District.Region).OrderBy(x => x.Code).ToList().Select(x => new ElectoralAreasReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Constituency = $"{x.Constituency.Code}-{x.Constituency.Name}",
                District = $"{x.Constituency.District.Code}-{x.Constituency.District.Name}",
                Region = $"{x.Constituency.District.Region.Code}-{x.Constituency.District.Region.Name}"
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "ElectAreaReport.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "ELECTORAL AREAS REPORT");

            var headers = new List<string> { "#", "Name", "Code", "Constituency", "District", "Region" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Name}";
                row[2] = $"{s.Code}";
                row[3] = $"{s.Constituency}";
                row[4] = $"{s.District}";
                row[5] = $"{s.Region}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }
        public string DownloadElectoralAreasExcelReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var records = _context.ElectoralAreas.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ConstituencyId == filter.ConstituencyId.Value);

            var data = records.Include(x => x.Constituency.District.Region).OrderBy(x => x.Code).ToList().Select(x => new ElectoralAreasReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Constituency = $"{x.Constituency.Code}-{x.Constituency.Name}",
                District = $"{x.Constituency.District.Code}-{x.Constituency.District.Name}",
                Region = $"{x.Constituency.District.Region.Code}-{x.Constituency.District.Region.Name}"
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "ElectAreas";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 4).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 4).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "ELECTORAL AREAS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Name", "Code", "Constituency", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Name, sub.Code, sub.Constituency, sub.District, sub.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.Save(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Polling Stations Reports
        public List<PollingStationsReportModel> GeneratePollingStationsListReport(SettingsReportFilter filter, User user)
        {
            var records = _context.Stations.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ElectoralArea.ConstituencyId == filter.ConstituencyId.Value);
            if (filter.ElectoralAreaId.HasValue) records = records.Where(x => x.ElectoralAreaId == filter.ElectoralAreaId.Value);

            var res = records.Include(x => x.ElectoralArea.Constituency.District.Region).OrderBy(x => x.Code).Take(filter.Size).ToList().Select(x => new PollingStationsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                ElectoralArea = $"{x.ElectoralArea.Code}-{x.ElectoralArea.Name}",
                Constituency = $"{x.ElectoralArea.Constituency.Code}-{x.ElectoralArea.Constituency.Name}",
                District = $"{x.ElectoralArea.Constituency.District.Code}-{x.ElectoralArea.Constituency.District.Name}",
                Region = $"{x.ElectoralArea.Constituency.District.Region.Code}-{x.ElectoralArea.Constituency.District.Region.Name}"
            }).ToList();
            return res;
        }
        public string DownloadPollingStationsWordReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            var records = _context.Stations.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ElectoralArea.ConstituencyId == filter.ConstituencyId.Value);
            if (filter.ElectoralAreaId.HasValue) records = records.Where(x => x.ElectoralAreaId == filter.ElectoralAreaId.Value);

            var res = records.Include(x => x.ElectoralArea.Constituency.District.Region).OrderBy(x => x.Code).ToList().Select(x => new PollingStationsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                ElectoralArea = $"{x.ElectoralArea.Code}-{x.ElectoralArea.Name}",
                Constituency = $"{x.ElectoralArea.Constituency.Code}-{x.ElectoralArea.Constituency.Name}",
                District = $"{x.ElectoralArea.Constituency.District.Code}-{x.ElectoralArea.Constituency.District.Name}",
                Region = $"{x.ElectoralArea.Constituency.District.Region.Code}-{x.ElectoralArea.Constituency.District.Region.Name}"
            }).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "PStationsReport.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "POLLING STATIONS REPORT");

            var headers = new List<string> { "#", "Name", "Code", "Electoral Area", "Constituency", "District", "Region" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Name}";
                row[2] = $"{s.Code}";
                row[3] = $"{s.ElectoralArea}";
                row[4] = $"{s.Constituency}";
                row[5] = $"{s.District}";
                row[6] = $"{s.Region}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }
        public string DownloadPollingStationsExcelReport(SettingsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var records = _context.Stations.Where(x => x.Id > 0);
            if (!string.IsNullOrEmpty(filter.Name))
                records = records.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
            if (!string.IsNullOrEmpty(filter.Code))
                records = records.Where(x => x.Code.ToLower().Contains(filter.Code.ToLower()));
            if (filter.RegionId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId.Value);
            if (filter.DistrictId.HasValue) records = records.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId.Value);
            if (filter.ConstituencyId.HasValue) records = records.Where(x => x.ElectoralArea.ConstituencyId == filter.ConstituencyId.Value);
            if (filter.ElectoralAreaId.HasValue) records = records.Where(x => x.ElectoralAreaId == filter.ElectoralAreaId.Value);

            var data = records.Include(x => x.ElectoralArea.Constituency.District.Region).OrderBy(x => x.Code).ToList().Select(x => new PollingStationsReportModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                ElectoralArea = $"{x.ElectoralArea.Code}-{x.ElectoralArea.Name}",
                Constituency = $"{x.ElectoralArea.Constituency.Code}-{x.ElectoralArea.Constituency.Name}",
                District = $"{x.ElectoralArea.Constituency.District.Code}-{x.ElectoralArea.Constituency.District.Name}",
                Region = $"{x.ElectoralArea.Constituency.District.Region.Code}-{x.ElectoralArea.Constituency.District.Region.Name}"
            }).ToList();
            if (!data.Any()) throw new Exception("This report has no data");

            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Polling Stations";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 5).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 5).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "POLLING STATIONS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Name", "Code", "Electoral Area", "Constituency", "District", "Region" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Name, sub.Code, sub.ElectoralArea, sub.Constituency, sub.District, sub.Region);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.Save(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Applications By District Reports
        public List<ApplicationsByDistrictsReportModel> ApplicationsByDistrictsListReport(ApplicationsByDistrictReportFilter filter, User user)
        {

            var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var districtIds = new List<int>();
            if (filter.RegionId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                districtIds = _context.Districts.Where(x => x.RegionId == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => districtIds.Contains(x.DistrictId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            if (filter.DistrictId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => filter.DistrictId == x.DistrictId);
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            psCodes = allRegistrations.Select(x => x.PollingStationCode).Distinct().ToList();
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.ToList();
            var stations = _context.Stations.Where(x => psCodes.Contains(x.Code)).Include(x => x.ElectoralArea.Constituency.District.Region).Distinct().ToList();
            var districts = stations.Select(x => x.ElectoralArea.Constituency.District).DistinctBy(x => x.Code).ToList();
            var res = new List<ApplicationsByDistrictsReportModel>();
            foreach (var d in districts)
            {
                var dPsCodes = stations.Where(x => x.ElectoralArea.Constituency.DistrictId == d.Id).Select(x => x.Code).ToList();
                var dEodUploads = eodUploadsList.Where(x => dPsCodes.Contains(x.PollingStationCode));
                var dMrtFiles = mrtFilesList.Where(x => x.DistrictId == d.Id);
                res.Add(new ApplicationsByDistrictsReportModel
                {
                    Code = d.Code,
                    Name = d.Name,
                    Region = d.Region.Name,
                    ExtractedApp = allRegistrationsList.Count(x => dPsCodes.Contains(x.PollingStationCode)),
                    ExpectedApp = dEodUploads.Sum(x => x.TotalNumberOfRegistrations),
                    ExtractedMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Extracted),
                    PendingMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Pending),
                    ReceivedMrtFiles = dMrtFiles.Count()
                });
            }
            res = res.OrderByDescending(x => x.ExpectedApp).ToList();
            if (filter.Size > 0)
            {
                res = res.Take(filter.Size).ToList();
            }
            return res;
        }
        public string ApplicationsByDistrictsWordReport(ApplicationsByDistrictReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var districtIds = new List<int>();
            if (filter.RegionId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                districtIds = _context.Districts.Where(x => x.RegionId == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => districtIds.Contains(x.DistrictId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            if (filter.DistrictId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => filter.DistrictId == x.DistrictId);
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            psCodes = allRegistrations.Select(x => x.PollingStationCode).Distinct().ToList();
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.ToList();
            var stations = _context.Stations.Where(x => psCodes.Contains(x.Code)).Include(x => x.ElectoralArea.Constituency.District.Region).Distinct().ToList();
            var districts = stations.Select(x => x.ElectoralArea.Constituency.District).DistinctBy(x => x.Code).ToList();
            var res = new List<ApplicationsByDistrictsReportModel>();
            foreach (var d in districts)
            {
                var dPsCodes = stations.Where(x => x.ElectoralArea.Constituency.DistrictId == d.Id).Select(x => x.Code).ToList();
                var dEodUploads = eodUploadsList.Where(x => dPsCodes.Contains(x.PollingStationCode));
                var dMrtFiles = mrtFilesList.Where(x => x.DistrictId == d.Id);
                res.Add(new ApplicationsByDistrictsReportModel
                {
                    Code = d.Code,
                    Name = d.Name,
                    Region = d.Region.Name,
                    ExtractedApp = allRegistrationsList.Count(x => dPsCodes.Contains(x.PollingStationCode)),
                    ExpectedApp = dEodUploads.Sum(x => x.TotalNumberOfRegistrations),
                    ExtractedMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Extracted),
                    PendingMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Pending),
                    ReceivedMrtFiles = dMrtFiles.Count()
                });
            }
            res = res.OrderByDescending(x => x.ExpectedApp).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            res = res.OrderByDescending(x => x.ExpectedApp).ToList();
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_Apps_By_District_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "APPLICATIONS BY DISTRICTS REPORT");

            var headers = new List<string> { "#", "Code", "Name", "Region", "Expected Applications", "Extracted Applications", "Received MRT Files", "Extracted MRT Files", "Pending MRT Files" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Code}";
                row[2] = $"{s.Name}";
                row[3] = $"{s.Region}";
                row[4] = $"{s.ExpectedApp}";
                row[5] = $"{s.ExtractedApp}";
                row[6] = $"{s.ReceivedMrtFiles}";
                row[7] = $"{s.ExtractedMrtFiles}";
                row[8] = $"{s.PendingMrtFiles}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string ApplicationsByDistrictsExcelReport(ApplicationsByDistrictReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var districtIds = new List<int>();
            if (filter.RegionId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                districtIds = _context.Districts.Where(x => x.RegionId == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => districtIds.Contains(x.DistrictId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            if (filter.DistrictId > 0)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.DistrictId == filter.DistrictId).Include(x => x.ElectoralArea.Constituency.District).Select(x => x.Code).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => filter.DistrictId == x.DistrictId);
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            psCodes = allRegistrations.Select(x => x.PollingStationCode).Distinct().ToList();
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.ToList();
            var stations = _context.Stations.Where(x => psCodes.Contains(x.Code)).Include(x => x.ElectoralArea.Constituency.District.Region).Distinct().ToList();
            var districts = stations.Select(x => x.ElectoralArea.Constituency.District).DistinctBy(x => x.Code).ToList();
            var data = new List<ApplicationsByDistrictsReportModel>();
            foreach (var d in districts)
            {
                var dPsCodes = stations.Where(x => x.ElectoralArea.Constituency.DistrictId == d.Id).Select(x => x.Code).ToList();
                var dEodUploads = eodUploadsList.Where(x => dPsCodes.Contains(x.PollingStationCode));
                var dMrtFiles = mrtFilesList.Where(x => x.DistrictId == d.Id);
                data.Add(new ApplicationsByDistrictsReportModel
                {
                    Code = d.Code,
                    Name = d.Name,
                    Region = d.Region.Name,
                    ExtractedApp = allRegistrationsList.Count(x => dPsCodes.Contains(x.PollingStationCode)),
                    ExpectedApp = dEodUploads.Sum(x => x.TotalNumberOfRegistrations),
                    ExtractedMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Extracted),
                    PendingMrtFiles = dMrtFiles.Count(x => x.Status == UploadedMrtFileStatus.Pending),
                    ReceivedMrtFiles = dMrtFiles.Count()
                });
            }
            data = data.OrderByDescending(x => x.ExpectedApp).ToList();

            if (!data.Any()) throw new Exception("This report has no data");
            data = data.OrderByDescending(x => x.ExpectedApp).ToList();
            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Applications_By_Districts";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 7).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 7).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "APPLICATIONS BY DISTRICTS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Code", "Name", "Region", "Expected Applications", "Extracted Applications", "Received MRT Files", "Extracted MRT Files", "Pending MRT Files" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Code, sub.Name, sub.Region, sub.Region, sub.ExpectedApp, sub.ExtractedApp, sub.ReceivedMrtFiles, sub.PendingMrtFiles);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Application Stats By Regions Reports
        public List<ApplicationStatsByRegionsReportModel> ApplicationStatsByRegionsListReport(ApplicationStatsByRegionsReportFilter filter, User user)
        {
            using var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var regions = _context.Regions.Where(x => x.Id > 0);
            var stations = _context.Stations.Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var regionIds = new List<int>();
            var regns = new List<Region>();
            if (filter.RegionId > 0)
            {
                regns = regions.Where(x => x.Id == filter.RegionId).Select(x => new Region { Id = x.Id, Code = x.Code, Name = x.Name }).ToList();
                stations = stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId);
                psCodes = stations.Select(x => x.Code).ToList();
                regionIds = regns.Where(x => x.Id == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => regionIds.Contains(x.District.RegionId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.DistinctBy(x => x.VoterId)
                .Select(x => new { x.Id, x.Status, x.VoterId, x.PollingStationCode, x.Sex }).ToList();
            var stationx = stations
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => new { x.Code, x.Name, RegionId = x.ElectoralArea.Constituency.District.RegionId }).ToList();
            var res = new List<ApplicationStatsByRegionsReportModel>();
            foreach (var r in regns)
            {
                var rPsCodes = stationx.Where(x => x.RegionId == r.Id).Select(x => x.Code).ToList();
                var rEodUploads = eodUploadsList.Where(x => rPsCodes.Contains(x.PollingStationCode));
                var rMrtFiles = mrtFilesList.Where(x => x.District.RegionId == r.Id);
                res.Add(new ApplicationStatsByRegionsReportModel
                {
                    Code = r.Code,
                    Name = r.Name,
                    Extracted = allRegistrationsList.DistinctBy(x => x.VoterId).Count(x => rPsCodes.Contains(x.PollingStationCode)),
                    Expected = rEodUploads.DistinctBy(x => x.Date.Date).Sum(x => x.TotalNumberOfRegistrations),
                    Processed = allRegistrationsList.Where(x => rPsCodes.Contains(x.PollingStationCode) && x.Status == TemporalVoterRegisterStatus.PushedToAbis).Count(),
                    ExtractedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male),
                    ExtractedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female),
                    ProcessedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male && x.Status == TemporalVoterRegisterStatus.PushedToAbis),
                    ProcessedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female && x.Status == TemporalVoterRegisterStatus.PushedToAbis)
                });
            }
            res = res.OrderBy(x => x.Extracted).ToList();
            if (filter.Size > 0)
            {
                res = res.Take(filter.Size).ToList();
            }
            return res;
        }
        public string ApplicationStatsByRegionsWordReport(ApplicationStatsByRegionsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "report_temp2.docx");
            using var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var regions = _context.Regions.Where(x => x.Id > 0);
            var stations = _context.Stations.Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var regionIds = new List<int>();
            var regns = new List<Region>();
            if (filter.RegionId > 0)
            {
                regns = regions.Where(x => x.Id == filter.RegionId).Select(x => new Region { Id = x.Id, Code = x.Code, Name = x.Name }).ToList();
                stations = stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId);
                psCodes = stations.Select(x => x.Code).ToList();
                regionIds = regns.Where(x => x.Id == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => regionIds.Contains(x.District.RegionId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.DistinctBy(x => x.VoterId)
                .Select(x => new { x.Id, x.Status, x.VoterId, x.PollingStationCode, x.Sex }).ToList();
            var stationx = stations
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => new { x.Code, x.Name, RegionId = x.ElectoralArea.Constituency.District.RegionId }).ToList();
            var res = new List<ApplicationStatsByRegionsReportModel>();
            foreach (var r in regns)
            {
                var rPsCodes = stationx.Where(x => x.RegionId == r.Id).Select(x => x.Code).ToList();
                var rEodUploads = eodUploadsList.Where(x => rPsCodes.Contains(x.PollingStationCode));
                var rMrtFiles = mrtFilesList.Where(x => x.District.RegionId == r.Id);
                res.Add(new ApplicationStatsByRegionsReportModel
                {
                    Code = r.Code,
                    Name = r.Name,
                    Extracted = allRegistrationsList.DistinctBy(x => x.VoterId).Count(x => rPsCodes.Contains(x.PollingStationCode)),
                    Expected = rEodUploads.DistinctBy(x => x.Date.Date).Sum(x => x.TotalNumberOfRegistrations),
                    Processed = allRegistrationsList.Where(x => rPsCodes.Contains(x.PollingStationCode) && x.Status == TemporalVoterRegisterStatus.PushedToAbis).Count(),
                    ExtractedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male),
                    ExtractedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female),
                    ProcessedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male && x.Status == TemporalVoterRegisterStatus.PushedToAbis),
                    ProcessedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female && x.Status == TemporalVoterRegisterStatus.PushedToAbis)
                });
            }
            res = res.OrderBy(x => x.Extracted).ToList();
            if (!res.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_App_Stats_By_Reg_Report.docx").Replace("/", "");

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[TITLE]", "APPLICATION STATISTICS BY REGIONS REPORT");

            var headers = new List<string> { "#", "Code", "Name", "Expected", "Extracted", "Processed", "Extracted Male", "Extracted Female", "Processed Male", "Processed Female" };
            int dtRowCount = res.Count() + 1;
            int dtColumnCount = headers.Count();
            DataTable dataTable = new DataTable();
            DataRow headerRow = dataTable.NewRow();
            for (int i = 0; i < dtColumnCount; i++)
            {
                dataTable.Columns.Add(headers[i]);
                headerRow[i] = $"{headers[i].ToUpper()}";
            }
            dataTable.Rows.Add(headerRow);

            TableStyle lightShadingStyle = (TableStyle)Style.CreateStyle(StyleTemplateType.ListTable2Accent5, templateDoc);
            templateDoc.Styles.Add(lightShadingStyle);

            var tabIndex = 1;
            foreach (var s in res)
            {
                DataRow row = dataTable.NewRow();
                row[0] = $"{tabIndex.ToString()}";
                row[1] = $"{s.Code}";
                row[2] = $"{s.Name}";
                row[3] = $"{s.Expected}";
                row[4] = $"{s.Extracted}";
                row[5] = $"{s.Processed}";
                row[6] = $"{s.ExtractedMale}";
                row[7] = $"{s.ExtractedFemale}";
                row[8] = $"{s.ProcessedMale}";
                row[8] = $"{s.ProcessedFemale}";
                tabIndex = tabIndex + 1;
                dataTable.Rows.Add(row);
            }

            Table table = new Table(templateDoc, dtRowCount, dtColumnCount,
            (int rowIndex, int columnIndex) => new TableCell(templateDoc, new Paragraph(templateDoc, dataTable.Rows[rowIndex][columnIndex].ToString())));
            table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
            table.TableFormat.Style = lightShadingStyle;

            ContentRange tablePlaceholder = templateDoc.Content.Find("[TABLE]").First();
            tablePlaceholder.Start.InsertRange(table.Content);
            templateDoc.Content.Replace("[TABLE]", "");

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string ApplicationStatsByRegionsExcelReport(ApplicationStatsByRegionsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            using var logsBd = new mrs_logsContext();
            var allRegistrations = logsBd.TemporalVoterRegister
                .Where(x => x.Id > 0);
            var mrtFiles = _context.UploadedMrtFiles
                .Where(x => x.Id > 0);
            var eodUploads = logsBd.EndOfDayReportStatistics
                .Where(x => x.Id > 0);
            var regions = _context.Regions.Where(x => x.Id > 0);
            var stations = _context.Stations.Where(x => x.Id > 0);
            var psCodes = new List<string>();
            var regionIds = new List<int>();
            var regns = new List<Region>();
            if (filter.RegionId > 0)
            {
                regns = regions.Where(x => x.Id == filter.RegionId).Select(x => new Region { Id = x.Id, Code = x.Code, Name = x.Name }).ToList();
                stations = stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == filter.RegionId);
                psCodes = stations.Select(x => x.Code).ToList();
                regionIds = regns.Where(x => x.Id == filter.RegionId).Select(x => x.Id).ToList();
                allRegistrations = allRegistrations.Where(x => psCodes.Contains(x.PollingStationCode));
                mrtFiles = mrtFiles.Where(x => regionIds.Contains(x.District.RegionId));
                eodUploads = eodUploads.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            var mrtFilesList = mrtFiles.DistinctBy(x => x.FileName).ToList();
            var eodUploadsList = eodUploads.ToList();
            var allRegistrationsList = allRegistrations.DistinctBy(x => x.VoterId)
                .Select(x => new { x.Id, x.Status, x.VoterId, x.PollingStationCode, x.Sex }).ToList();
            var stationx = stations
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => new { x.Code, x.Name, RegionId = x.ElectoralArea.Constituency.District.RegionId }).ToList();
            var data = new List<ApplicationStatsByRegionsReportModel>();
            foreach (var r in regns)
            {
                var rPsCodes = stationx.Where(x => x.RegionId == r.Id).Select(x => x.Code).ToList();
                var rEodUploads = eodUploadsList.Where(x => rPsCodes.Contains(x.PollingStationCode));
                var rMrtFiles = mrtFilesList.Where(x => x.District.RegionId == r.Id);
                data.Add(new ApplicationStatsByRegionsReportModel
                {
                    Code = r.Code,
                    Name = r.Name,
                    Extracted = allRegistrationsList.DistinctBy(x => x.VoterId).Count(x => rPsCodes.Contains(x.PollingStationCode)),
                    Expected = rEodUploads.DistinctBy(x => x.Date.Date).Sum(x => x.TotalNumberOfRegistrations),
                    Processed = allRegistrationsList.Where(x => rPsCodes.Contains(x.PollingStationCode) && x.Status == TemporalVoterRegisterStatus.PushedToAbis).Count(),
                    ExtractedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male),
                    ExtractedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female),
                    ProcessedMale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Male && x.Status == TemporalVoterRegisterStatus.PushedToAbis),
                    ProcessedFemale = allRegistrationsList.DistinctBy(x => x.VoterId)
                    .Count(x => rPsCodes.Contains(x.PollingStationCode) && x.Sex == MrsLogsModels.Sex.Female && x.Status == TemporalVoterRegisterStatus.PushedToAbis)
                });
            }
            data = data.OrderBy(x => x.Extracted).ToList();

            if (!data.Any()) throw new Exception("This report has no data");
            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Application_Stats_By_Regions";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 8).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 8).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "APPLICATIONS STATISTICS BY REGIONS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Code", "Name", "Expected", "Extracted", "Processed", "Extracted Male", "Extracted Female", "Processed Male", "Processed Female" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.Code, sub.Name, sub.Expected, sub.Extracted, sub.Processed, sub.ExtractedMale, sub.ExtractedFemale, sub.ProcessedMale, sub.ProcessedFemale);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Applicant Details Reports
        public List<ApplicantDetailsReportModel> ApplicantDetailsListReport(ApplicantDetailsReportFilter filter, User user)
        {

            var registrations = _context.VoterRegister
                .Where(x => x.VoterId == filter.VoterId);
            var psCodes = new List<string>();
            if (user.Type == UserType.Regional)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId)
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => x.Code).ToList();
                registrations = registrations.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            var registrationx = registrations
                .Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
            var res = new List<ApplicantDetailsReportModel>();
            foreach (var d in registrationx)
            {
                res.Add(new ApplicantDetailsReportModel
                {
                    Id = d.Id,
                    VoterId = d.VoterId,
                    PSCode = d.PollingStationCode,
                    PSName = d.PollingStationName,
                    Region = d.PollingStation.ElectoralArea.Constituency.District.Region.Name,
                    District = d.PollingStation.ElectoralArea.Constituency.District.Name,
                    DateOfBirth = d.DateOfBirth.Value.ToString("MM/dd/yyyy HH:mm:ss"),
                    EstimatedAge = d.EstimatedAge,
                    FatherName = d.FatherName,
                    MotherName = d.MotherName,
                    CreatedAt = d.CreatedAt.ToString("MM/dd/yyyy HH:mm:ss"),
                    HomeTown = d.HomeTown,
                    Sex = d.Sex.ToString(),
                    Surname = d.Surname,
                    OtherNames = d.OtherNames,
                    PhoneNumber = d.PhoneNumber,
                    RegisteredBy = d.RegisteredBy,
                    ResidentialAddress = d.ResidentialAddress
                });
            }
            return res.ToList();
        }
        public string ApplicantDetailsWordReport(ApplicantDetailsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var template = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "prov_voter_details.docx");
            var registrations = _context.VoterRegister
                .Where(x => x.VoterId == filter.VoterId);
            var psCodes = new List<string>();
            if (user.Type == UserType.Regional)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId)
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => x.Code).ToList();
                registrations = registrations.Where(x => psCodes.Contains(x.PollingStationCode));
            }
            var registrationx = registrations
                .Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
            var data = new List<ApplicantDetailsReportModel>();
            foreach (var d in registrationx)
            {
                data.Add(new ApplicantDetailsReportModel
                {
                    Id = d.Id,
                    VoterId = d.VoterId,
                    PSCode = d.PollingStationCode,
                    PSName = d.PollingStationName,
                    Region = d.PollingStation.ElectoralArea.Constituency.District.Region.Name,
                    District = d.PollingStation.ElectoralArea.Constituency.District.Name,
                    DateOfBirth = d.DateOfBirth.Value.ToString("MM/dd/yyyy HH:mm:ss"),
                    EstimatedAge = d.EstimatedAge,
                    FatherName = d.FatherName,
                    MotherName = d.MotherName,
                    CreatedAt = d.CreatedAt.ToString("MM/dd/yyyy HH:mm:ss"),
                    HomeTown = d.HomeTown,
                    Sex = d.Sex.ToString(),
                    Surname = d.Surname,
                    OtherNames = d.OtherNames,
                    PhoneNumber = d.PhoneNumber,
                    RegisteredBy = d.RegisteredBy,
                    ResidentialAddress = d.ResidentialAddress
                });
            }
            if (!data.Any()) throw new Exception("This report has no data");
            var dt = DateTime.Now.ToFileTime();
            var date = DateTime.Now.ToShortDateString();
            var fileDocx = (dt + "_Applicant_Details.docx").Replace("/", "");
            var app = data.First();
            var photo = _context.VoterRegisterTemplates.Where(x => x.VoterRegisterId == app.Id)
                    .Select(x => x.Photo).FirstOrDefault();
            var imagePath = ImageHelpers.SaveImage(photo);

            DocumentModel templateDoc = DocumentModel.Load(template);
            templateDoc.Content.Replace("[VOTERID]", app.VoterId);
            templateDoc.Content.Replace("[SURNAME]", app.Surname);
            templateDoc.Content.Replace("[OTHERNAMES]", app.OtherNames);
            templateDoc.Content.Replace("[SEX]", app.Sex);
            templateDoc.Content.Replace("[AGE]", app.EstimatedAge.ToString());
            templateDoc.Content.Replace("[PHONENUMBER]", app.PhoneNumber);
            templateDoc.Content.Replace("[PSCODE]", app.PSCode);
            templateDoc.Content.Replace("[PSNAME]", app.PSName);
            templateDoc.Content.Replace("[DISTRICT]", app.District);
            templateDoc.Content.Replace("[REGION]", app.Region);
            templateDoc.Content.Replace("[FATHER]", app.FatherName);
            templateDoc.Content.Replace("[MOTHER]", app.MotherName);
            templateDoc.Content.Replace("[REGISTEREDBY]", app.RegisteredBy);
            //replace picture
            Picture picture = new Picture(templateDoc, imagePath, 160, 200);
            ContentRange imagePlaceholder = templateDoc.Content.Find("[PICTURE]").First();
            imagePlaceholder.Set(picture.Content);

            if (File.Exists(tempFolder + fileDocx)) File.Delete(tempFolder + fileDocx);
            templateDoc.Save(tempFolder + fileDocx);

            byte[] bytes = File.ReadAllBytes(tempFolder + fileDocx);
            File.Delete(tempFolder + fileDocx);
            return Convert.ToBase64String(bytes);
        }

        public string ApplicantDetailsExcelReport(ApplicantDetailsReportFilter filter, User user)
        {
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles");
            var registrations = _context.VoterRegister
                .Where(x => x.VoterId == filter.VoterId);
            var psCodes = new List<string>();
            if (user.Type == UserType.Regional)
            {
                psCodes = _context.Stations.Where(x => x.ElectoralArea.Constituency.District.RegionId == user.RegionId)
                    .Include(x => x.ElectoralArea.Constituency.District)
                    .Select(x => x.Code).ToList();
            }
            var registrationx = registrations.Where(x => psCodes.Contains(x.PollingStationCode))
                .Include(x => x.PollingStation.ElectoralArea.Constituency.District.Region).ToList();
            var data = new List<ApplicantDetailsReportModel>();
            foreach (var d in registrationx)
            {
                data.Add(new ApplicantDetailsReportModel
                {
                    Id = d.Id,
                    VoterId = d.VoterId,
                    PSCode = d.PollingStationCode,
                    PSName = d.PollingStationName,
                    Region = d.PollingStation.ElectoralArea.Constituency.District.Region.Name,
                    District = d.PollingStation.ElectoralArea.Constituency.District.Name,
                    DateOfBirth = d.DateOfBirth.Value.ToString("MM/dd/yyyy HH:mm:ss"),
                    EstimatedAge = d.EstimatedAge,
                    FatherName = d.FatherName,
                    MotherName = d.MotherName,
                    CreatedAt = d.CreatedAt.ToString("MM/dd/yyyy HH:mm:ss"),
                    HomeTown = d.HomeTown,
                    Sex = d.Sex.ToString(),
                    Surname = d.Surname,
                    OtherNames = d.OtherNames,
                    PhoneNumber = d.PhoneNumber,
                    RegisteredBy = d.RegisteredBy,
                    ResidentialAddress = d.ResidentialAddress
                });
            }

            if (!data.Any()) throw new Exception("This report has no data");
            data = data.OrderByDescending(x => x.VoterId).ToList();
            var date = DateTime.Now.ToShortDateString();
            var sheetName = "Applicant_Details";
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(sheetName);
            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 6).Merged = true;
            ws.Cells[0, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[0, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[0, 0].Value = "ELECTORAL COMMISSION GHANA";
            ws.Cells.GetSubrangeAbsolute(1, 0, 1, 6).Merged = true;
            ws.Cells[1, 0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            ws.Cells[1, 0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Cells[1, 0].Value = "APPLICANT DETAILS REPORT";

            DataTable dt = new DataTable();
            var headers = new List<string> { "Voter Id", "Surname", "Other Names", "Sex", "Age", "Date Of birth", "PS Code" };
            for (var i = 0; i < headers.Count(); i++)
            {
                var str = headers[i];
                dt.Columns.Add(str.ToUpper(), typeof(string));
            }

            foreach (var sub in data)
            {
                dt.Rows.Add(sub.VoterId, sub.Surname, sub.OtherNames, sub.Sex, sub.EstimatedAge, sub.DateOfBirth, sub.PSCode);
            }

            ws.InsertDataTable(dt,
                    new InsertDataTableOptions
                    {
                        ColumnHeaders = true,
                        StartRow = 3
                    });
            var now = DateTime.Now;
            var nowDate = $"{now.Year}{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}";
            var filename = $"{sheetName}-{nowDate}.xlsx";
            ef.SaveXlsx(tempFolder + filename);
            var bytes = File.ReadAllBytes(tempFolder + filename);
            File.Delete(tempFolder + filename);
            return Convert.ToBase64String(bytes);
        }
        #endregion

        #region Provisional Voter List
        public async Task GenerateProvisionalVoterListReport(GenerateVoterListReportFilter filter)
        {
            var month = filter.ElectionDate.ToString("MMMM");
            var year = filter.ElectionDate.Year;
            var date = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");
            var dt = DateTime.UtcNow.ToString("ddMMyyyyHHmmss");
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles","Provisional List",dt);
            Directory.CreateDirectory(tempFolder);
            var stations = _context.Stations.Where(x => x.Id > 0)
                .Include(x=> x.ElectoralArea.Constituency.District.Region)
                .OrderBy(x=> x.Code)
                .Select(x => new { 
                    x.Id, 
                    ElectoralArea = x.ElectoralArea.Name, 
                    PsName = x.Name, 
                    PsCode = x.Code,
                    ConstituencyName = x.ElectoralArea.Constituency.Name,
                    ConstituencyCode = x.ElectoralArea.Constituency.Code,
                    DistrictName = x.ElectoralArea.Constituency.District.Name,
                    DistrictCode = x.ElectoralArea.Constituency.District.Code,
                    RegionName = x.ElectoralArea.Constituency.District.Region.Name,
                    RegionCode = x.ElectoralArea.Constituency.District.Region.Code
                })
                .ToList();
            var regions = stations.Select(x => x.RegionCode).Distinct().ToList();

            //change to parallel
            foreach(var region in regions)
            {
                var reg = stations.FirstOrDefault(x => x.RegionCode == region);
                var regionalFolder = Path.Combine(tempFolder, reg.RegionName);
                Directory.CreateDirectory(regionalFolder);
                var districts = stations.Where(x=> x.RegionCode == region)
                    .Select(x => x.DistrictCode).Distinct().ToList();
                //change to parallel
                foreach (var district in districts)
                {
                    var districtFolder = Path.Combine(regionalFolder, district);
                    Directory.CreateDirectory(districtFolder);
                    var pStations = stations.Where(x => x.DistrictCode == district).ToList();
                    //change to parallel
                    foreach (var pStation in pStations)
                    {
                        var pStationCode = pStation.PsCode.Replace("�", "");

                        var coverFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_Cover_Prov_Voter_List_Report.docx"));
                        var mergedFileDocxToc = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_TOC.docx"));
                        var mergedFileDocxMain = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_Main.docx"));
                        var mergedFilePdfToc = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_TOC.pdf"));
                        var mergedFilePdfMain = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_Main.pdf"));
                        var mainFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_Main_Prov_Voter_List_Report.docx"));
                        var tocFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_TOC_Prov_Voter_List_Report.docx"));
                        var coverDoc = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "voter_list_report_temp_cover.docx");

                        var list = _context.VoterRegister
                            .Where(x => !x.HasDuplicate && !x.Reviewed && x.PollingStationCode.Contains(pStationCode) && string.IsNullOrEmpty(x.Challenges))
                            .OrderBy(x=> x.Surname)
                            .ToList();
                        if (!list.Any()) continue;
                        var ids = list.Select(x => x.Id).ToList();
                        var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
                                .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId })
                                .ToList();
                        if (!bios.Any()) continue;
                        #region Cover Doc
                        DocumentModel coverTemplateDoc = DocumentModel.Load(coverDoc);
                        coverTemplateDoc.Content.Replace("[TYPE]", "PROVISIONAL");
                        coverTemplateDoc.Content.Replace("[MONTH]", month.ToUpper());
                        coverTemplateDoc.Content.Replace("[YEAR]", year.ToString());
                        coverTemplateDoc.Content.Replace("[ELECTIONDATE]", filter.ElectionDate.ToString("dddd, dd MMMM yyyy"));
                        coverTemplateDoc.Content.Replace("[PSCODE]", pStation.PsCode);
                        coverTemplateDoc.Content.Replace("[PSNAME]", pStation.PsName);
                        coverTemplateDoc.Content.Replace("[CONSTITUENCY]", pStation.ConstituencyName);
                        coverTemplateDoc.Content.Replace("[DISTRICT]", pStation.DistrictName);
                        coverTemplateDoc.Content.Replace("[REGION]", pStation.RegionName);
                        #endregion
                        #region TOC Doc settings
                        DocumentModel tocDoc = new DocumentModel();
                        var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, tocDoc);
                        tocDoc.Styles.Add(titleStyle);
                        tocDoc.Styles.Add(new CharacterStyle("Emphasis"));
                        var strongStyle = (CharacterStyle)tocDoc.Styles.GetOrAdd(StyleTemplateType.Strong);
                        var subtitleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, tocDoc);
                        tocDoc.Styles.Add(subtitleStyle);
                        var section = new Section(tocDoc);
                        // Add default (odd) header.
                        var headerTable = new Table(tocDoc,
                            new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, "ELECTORAL COMMISION GHANA")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                            new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PROVISIONAL VOTER REGISTER OF {month} {year}")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Region: {pStation.RegionName}")),
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Constituency: {pStation.ConstituencyName}"))),
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PS Code: {pStation.PsCode}")),
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PS Name: {pStation.PsName}"))));
                        headerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var headerTableBorders = headerTable.TableFormat.Borders;
                        headerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        headerTableBorders.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
                        headerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        section.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(tocDoc, HeaderFooterType.HeaderDefault,
                                headerTable));

                        // Add default (odd) footer with page number.
                        var footerTable = new Table(tocDoc,
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Printed: {date}")
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                            new TableCell(tocDoc, new Paragraph(tocDoc,
                                new Field(tocDoc, FieldType.Page),
                                new Run(tocDoc, " of "),
                                new Field(tocDoc, FieldType.NumPages))
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                            ));
                        footerTable.TableFormat.DefaultCellPadding = new Padding(10);
                        footerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var footerTableBorders = footerTable.TableFormat.Borders;
                        footerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        footerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        section.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(tocDoc, HeaderFooterType.FooterDefault,
                            footerTable));
                        var blocks = section.Blocks;
                        tocDoc.Sections.Add(section);
                        var pageSetup = section.PageSetup;
                        pageSetup.PageMargins.Top = 5;
                        pageSetup.PageMargins.Bottom = 5;
                        pageSetup.PaperType = GemBox.Document.PaperType.A4;
                        pageSetup.PageMargins.Left = 15;
                        pageSetup.PageMargins.Right = 10;


                        var tocHeaders = new List<string> { "Full Name", "Page#", "Voter ID", "Sex", "Age" };
                        int dtRowCount = list.Count() + 1;
                        int dtColumnCount = tocHeaders.Count();
                        DataTable tocDataTable = new DataTable();
                        DataRow headerRow = tocDataTable.NewRow();
                        for (int i = 0; i < dtColumnCount; i++)
                        {
                            tocDataTable.Columns.Add(tocHeaders[i]);
                            headerRow[i] = $"{tocHeaders[i].ToUpper()}";
                        }
                        tocDataTable.Rows.Add(headerRow);

                        #endregion

                        #region Main Doc settings
                        DocumentModel mainDoc = new DocumentModel();

                        var titleStylex = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, mainDoc);
                        mainDoc.Styles.Add(titleStylex);
                        mainDoc.Styles.Add(new CharacterStyle("Emphasis"));
                        var subtitleStylex = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, mainDoc);
                        mainDoc.Styles.Add(subtitleStylex);
                        var sectionx = new Section(mainDoc);
                        // Add default (odd) header.
                        var headerTablex = new Table(mainDoc,
                            new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, "ELECTORAL COMMISION GHANA")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                            new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PROVISIONAL VOTER REGISTER OF {month} {year}")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Region: {pStation.RegionName}")),
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Constituency: {pStation.ConstituencyName}"))),
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PS Code: {pStation.PsCode}")),
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PS Name: {pStation.PsName}"))));
                        headerTablex.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var headerTableBordersx = headerTablex.TableFormat.Borders;
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        sectionx.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(mainDoc, HeaderFooterType.HeaderDefault,
                                headerTablex));

                        // Add default (odd) footer with page number.
                        var footerTablex = new Table(mainDoc,
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Printed: {date}")
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                            new TableCell(mainDoc, new Paragraph(mainDoc,
                                new Field(mainDoc, FieldType.Page),
                                new Run(mainDoc, " of "),
                                new Field(mainDoc, FieldType.NumPages))
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                            ));
                        footerTablex.TableFormat.DefaultCellPadding = new Padding(10);
                        footerTablex.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var footerTableBordersx = footerTablex.TableFormat.Borders;
                        footerTableBordersx.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        footerTableBordersx.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        sectionx.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(mainDoc, HeaderFooterType.FooterDefault,
                            footerTablex));
                        var blocksx = sectionx.Blocks;
                        mainDoc.Sections.Add(sectionx);
                        var pageSetupx = sectionx.PageSetup;
                        pageSetupx.PageMargins.Top = 5;
                        pageSetupx.PageMargins.Bottom = 5;
                        pageSetupx.PaperType = GemBox.Document.PaperType.A4;
                        pageSetupx.PageMargins.Left = 15;
                        pageSetupx.PageMargins.Right = 10;
                        #endregion

                        for (var i = 0; i < list.Count; i++)
                        {
                            var page = (i / 12) + 1;
                            Table tb = new Table(mainDoc);
                            if (list.Count > i)
                            {
                                var s = list[i];
                                var bio = bios.First(x => x.VoterRegisterId == s.Id);
                                var imagePath = ImageHelpers.SaveImage(bio.Photo);
                                var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);
                                Picture qrCode = new Picture(mainDoc, qrCodePath, 50, 50);
                                Picture picture = new Picture(mainDoc, imagePath, 80, 100);
                                tb = BuildVoterDetailsTable(mainDoc, picture, qrCode, s);

                                //toc row
                                DataRow row = tocDataTable.NewRow();
                                row[0] = $"{s.Surname} {s.OtherNames}";
                                row[1] = $"{page} (L)";
                                row[2] = $"{s.VoterId}";
                                row[3] = $"{s.Sex.ToString().Substring(0,1)}";
                                row[4] = $"{s.EstimatedAge}";
                                tocDataTable.Rows.Add(row);
                            }

                            i = i + 1;
                            Table tb2 = new Table(mainDoc);
                            if (list.Count > i)
                            {
                                var ss = list[i];
                                var bio2 = bios.First(x => x.VoterRegisterId == ss.Id);
                                var imagePath2 = ImageHelpers.SaveImage(bio2.Photo);
                                var qrCodePath2 = ImageHelpers.GenerateVoterQRCode(ss.VoterId);
                                Picture qrCode2 = new Picture(mainDoc, qrCodePath2, 50, 50);
                                Picture picture2 = new Picture(mainDoc, imagePath2, 80, 100);
                                tb2 = BuildVoterDetailsTable(mainDoc, picture2, qrCode2, ss);

                                //toc row
                                DataRow row = tocDataTable.NewRow();
                                row[0] = $"{ss.Surname} {ss.OtherNames}";
                                row[1] = $"{page} (R)";
                                row[2] = $"{ss.VoterId}";
                                row[3] = $"{ss.Sex.ToString().Substring(0, 1)}";
                                row[4] = $"{ss.EstimatedAge}";
                                tocDataTable.Rows.Add(row);
                            }
                            var xTable = new Table(mainDoc,
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, tb)
                            {
                                CellFormat = new TableCellFormat
                                {
                                    PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                                }
                            },
                            new TableCell(mainDoc, tb2)
                            {
                                CellFormat = new TableCellFormat
                                {
                                    PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                                }
                            }));

                            xTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                            var xTableBorders = xTable.TableFormat.Borders;
                            xTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                            xTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);

                            foreach (ParagraphFormat paragraphFormat in xTable
                                .GetChildElements(true, ElementType.Paragraph)
                                .Cast<Paragraph>()
                                .Select(p => p.ParagraphFormat))
                            {
                                paragraphFormat.KeepLinesTogether = true;
                                paragraphFormat.KeepWithNext = true;
                            }
                            blocksx.Add(xTable);
                            var size = 12;
                            if (i == 11 || (i + size) % size == 11)
                                blocksx.Add(new Paragraph(mainDoc, new SpecialCharacter(mainDoc, SpecialCharacterType.PageBreak)));

                        }

                        //toc things
                        Table table = new Table(tocDoc, dtRowCount, dtColumnCount, (int rowIndex, int columnIndex)
                            => new TableCell(tocDoc, new Paragraph(tocDoc, tocDataTable.Rows[rowIndex][columnIndex].ToString())));
                        table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        blocks.Add(table);

                        if (File.Exists(mainFileDocx)) File.Delete(mainFileDocx);
                        mainDoc.Save(mainFileDocx);
                        if (File.Exists(tocFileDocx)) File.Delete(tocFileDocx);
                        tocDoc.Save(tocFileDocx);
                        if (File.Exists(coverFileDocx)) File.Delete(coverFileDocx);
                        coverTemplateDoc.Save(coverFileDocx);
                        if (File.Exists(mergedFileDocxToc)) File.Delete(mergedFileDocxToc);
                        if (File.Exists(mergedFileDocxMain)) File.Delete(mergedFileDocxMain);
                        DocumentModel.Load(coverFileDocx)
                            .JoinWith(tocFileDocx)
                            .Save(mergedFileDocxToc);
                        DocumentModel.Load(coverFileDocx)
                            .JoinWith(mainFileDocx)
                            .Save(mergedFileDocxMain);
                        File.Delete(mainFileDocx);
                        File.Delete(tocFileDocx);
                        File.Delete(coverFileDocx);
                        DocumentModel document = DocumentModel.Load(mergedFileDocxToc);                        
                        DocumentModel documentx = DocumentModel.Load(mergedFileDocxMain);
                        document.Save(mergedFilePdfToc);
                        documentx.Save(mergedFilePdfMain);
                    }
                }
            }
        }

        public async Task GenerateProvisionalVoterList()
        {
            var electionDate = new DateTime(2020, 12, 7);
            var month = electionDate.ToString("MMMM");
            var year = electionDate.Year;
            var date = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");
            var dt = DateTime.UtcNow.ToString("ddMMyyyyHHmmss");
            var tempFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TempFiles", "Provisional List", dt);
            Directory.CreateDirectory(tempFolder);
            var stations = _context.Stations.Where(x => x.Id > 0)
                .Include(x => x.ElectoralArea.Constituency.District.Region)
                .OrderBy(x => x.Code)
                .Select(x => new {
                    x.Id,
                    ElectoralArea = x.ElectoralArea.Name,
                    PsName = x.Name,
                    PsCode = x.Code,
                    ConstituencyName = x.ElectoralArea.Constituency.Name,
                    ConstituencyCode = x.ElectoralArea.Constituency.Code,
                    DistrictName = x.ElectoralArea.Constituency.District.Name,
                    DistrictCode = x.ElectoralArea.Constituency.District.Code,
                    RegionName = x.ElectoralArea.Constituency.District.Region.Name,
                    RegionCode = x.ElectoralArea.Constituency.District.Region.Code
                })
                .ToList();
            var regions = stations.Select(x => x.RegionCode).Distinct().ToList();

            //change to parallel
            foreach (var region in regions)
            {
                var reg = stations.FirstOrDefault(x => x.RegionCode == region);
                var regionalFolder = Path.Combine(tempFolder, reg.RegionName);
                Directory.CreateDirectory(regionalFolder);
                var districts = stations.Where(x => x.RegionCode == region)
                    .Select(x => x.DistrictCode).Distinct().ToList();
                //change to parallel
                foreach (var district in districts)
                {
                    var districtFolder = Path.Combine(regionalFolder, district);
                    Directory.CreateDirectory(districtFolder);
                    var pStations = stations.Where(x => x.DistrictCode == district).ToList();
                    //change to parallel
                    foreach (var pStation in pStations)
                    {
                        var pStationCode = pStation.PsCode.Replace("�", "");

                        var coverFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_Cover_Prov_Voter_List_Report.docx"));
                        var mergedFileDocxToc = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_TOC.docx"));
                        var mergedFileDocxMain = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_Main.docx"));
                        var mergedFilePdfToc = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_TOC.pdf"));
                        var mergedFilePdfMain = Path.Combine(districtFolder, ($"{pStationCode}_Prov_Voter_List_Report_Main.pdf"));
                        var mainFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_Main_Prov_Voter_List_Report.docx"));
                        var tocFileDocx = Path.Combine(districtFolder, ($"{pStationCode}_TOC_Prov_Voter_List_Report.docx"));
                        var coverDoc = Path.Combine(Directory.GetCurrentDirectory(), "Documents", "Templates", "voter_list_report_temp_cover.docx");

                        var list = _context.VoterRegister
                            .Where(x => !x.HasDuplicate && !x.Reviewed && x.PollingStationCode.Contains(pStationCode) && string.IsNullOrEmpty(x.Challenges))
                            .OrderBy(x => x.Surname)
                            .ToList();
                        if (!list.Any()) continue;
                        var ids = list.Select(x => x.Id).ToList();
                        var bios = _context.VoterRegisterTemplates.Where(x => ids.Contains(x.VoterRegisterId))
                                .Select(x => new VoterRegisterTemplates { Photo = x.Photo, Id = x.Id, VoterRegisterId = x.VoterRegisterId })
                                .ToList();
                        if (!bios.Any()) continue;
                        #region Cover Doc
                        DocumentModel coverTemplateDoc = DocumentModel.Load(coverDoc);
                        coverTemplateDoc.Content.Replace("[TYPE]", "PROVISIONAL");
                        coverTemplateDoc.Content.Replace("[MONTH]", month.ToUpper());
                        coverTemplateDoc.Content.Replace("[YEAR]", year.ToString());
                        coverTemplateDoc.Content.Replace("[ELECTIONDATE]", electionDate.ToString("dddd, dd MMMM yyyy"));
                        coverTemplateDoc.Content.Replace("[PSCODE]", pStation.PsCode);
                        coverTemplateDoc.Content.Replace("[PSNAME]", pStation.PsName);
                        coverTemplateDoc.Content.Replace("[CONSTITUENCY]", pStation.ConstituencyName);
                        coverTemplateDoc.Content.Replace("[DISTRICT]", pStation.DistrictName);
                        coverTemplateDoc.Content.Replace("[REGION]", pStation.RegionName);
                        #endregion
                        #region TOC Doc settings
                        DocumentModel tocDoc = new DocumentModel();
                        var titleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, tocDoc);
                        tocDoc.Styles.Add(titleStyle);
                        tocDoc.Styles.Add(new CharacterStyle("Emphasis"));
                        var subtitleStyle = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, tocDoc);
                        tocDoc.Styles.Add(subtitleStyle);
                        var section = new Section(tocDoc);
                        // Add default (odd) header.
                        var headerTable = new Table(tocDoc,
                            new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, "ELECTORAL COMMISION GHANA")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                            new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PROVISIONAL VOTER REGISTER OF {month} {year}")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Region: {pStation.RegionName}")),
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Constituency: {pStation.ConstituencyName}"))),
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PS Code: {pStation.PsCode}")),
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"PS Name: {pStation.PsName}"))));
                        headerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var headerTableBorders = headerTable.TableFormat.Borders;
                        headerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        headerTableBorders.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
                        headerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        section.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(tocDoc, HeaderFooterType.HeaderDefault,
                                headerTable));

                        // Add default (odd) footer with page number.
                        var footerTable = new Table(tocDoc,
                        new TableRow(tocDoc,
                            new TableCell(tocDoc, new Paragraph(tocDoc, $"Printed: {date}")
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                            new TableCell(tocDoc, new Paragraph(tocDoc,
                                new Field(tocDoc, FieldType.Page),
                                new Run(tocDoc, " of "),
                                new Field(tocDoc, FieldType.NumPages))
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                            ));
                        footerTable.TableFormat.DefaultCellPadding = new Padding(10);
                        footerTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var footerTableBorders = footerTable.TableFormat.Borders;
                        footerTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        footerTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        section.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(tocDoc, HeaderFooterType.FooterDefault,
                            footerTable));
                        var blocks = section.Blocks;
                        tocDoc.Sections.Add(section);
                        var pageSetup = section.PageSetup;
                        pageSetup.PageMargins.Top = 5;
                        pageSetup.PageMargins.Bottom = 5;
                        pageSetup.PaperType = GemBox.Document.PaperType.A4;
                        pageSetup.PageMargins.Left = 15;
                        pageSetup.PageMargins.Right = 10;


                        var tocHeaders = new List<string> { "Full Name", "Page#", "Voter ID", "Sex", "Age" };
                        int dtRowCount = list.Count() + 1;
                        int dtColumnCount = tocHeaders.Count();
                        DataTable tocDataTable = new DataTable();
                        DataRow headerRow = tocDataTable.NewRow();
                        for (int i = 0; i < dtColumnCount; i++)
                        {
                            tocDataTable.Columns.Add(tocHeaders[i]);
                            headerRow[i] = $"{tocHeaders[i].ToUpper()}";
                        }
                        tocDataTable.Rows.Add(headerRow);

                        #endregion

                        #region Main Doc settings
                        DocumentModel mainDoc = new DocumentModel();

                        var titleStylex = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Title, mainDoc);
                        mainDoc.Styles.Add(titleStylex);
                        mainDoc.Styles.Add(new CharacterStyle("Emphasis"));
                        var subtitleStylex = (ParagraphStyle)Style.CreateStyle(StyleTemplateType.Subtitle, mainDoc);
                        mainDoc.Styles.Add(subtitleStylex);
                        var sectionx = new Section(mainDoc);
                        // Add default (odd) header.
                        var headerTablex = new Table(mainDoc,
                            new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, "ELECTORAL COMMISION GHANA")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                            new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PROVISIONAL VOTER REGISTER OF {month} {year}")
                            {
                                ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Center }
                            })
                            {
                                ColumnSpan = 2
                            }),
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Region: {pStation.RegionName}")),
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Constituency: {pStation.ConstituencyName}"))),
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PS Code: {pStation.PsCode}")),
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"PS Name: {pStation.PsName}"))));
                        headerTablex.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var headerTableBordersx = headerTablex.TableFormat.Borders;
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Bottom, BorderStyle.Single, Color.Black, 1);
                        headerTableBordersx.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        sectionx.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(mainDoc, HeaderFooterType.HeaderDefault,
                                headerTablex));

                        // Add default (odd) footer with page number.
                        var footerTablex = new Table(mainDoc,
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, new Paragraph(mainDoc, $"Printed: {date}")
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Left } }),
                            new TableCell(mainDoc, new Paragraph(mainDoc,
                                new Field(mainDoc, FieldType.Page),
                                new Run(mainDoc, " of "),
                                new Field(mainDoc, FieldType.NumPages))
                            { ParagraphFormat = new ParagraphFormat() { Alignment = HorizontalAlignment.Right } })
                            ));
                        footerTablex.TableFormat.DefaultCellPadding = new Padding(10);
                        footerTablex.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        var footerTableBordersx = footerTablex.TableFormat.Borders;
                        footerTableBordersx.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                        footerTableBordersx.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);
                        sectionx.HeadersFooters.Add(
                            new GemBox.Document.HeaderFooter(mainDoc, HeaderFooterType.FooterDefault,
                            footerTablex));
                        var blocksx = sectionx.Blocks;
                        mainDoc.Sections.Add(sectionx);
                        var pageSetupx = sectionx.PageSetup;
                        pageSetupx.PageMargins.Top = 5;
                        pageSetupx.PageMargins.Bottom = 5;
                        pageSetupx.PaperType = GemBox.Document.PaperType.A4;
                        pageSetupx.PageMargins.Left = 15;
                        pageSetupx.PageMargins.Right = 10;
                        #endregion

                        for (var i = 0; i < list.Count; i++)
                        {
                            var page = (i / 12) + 1;
                            Table tb = new Table(mainDoc);
                            if (list.Count > i)
                            {
                                var s = list[i];
                                var bio = bios.First(x => x.VoterRegisterId == s.Id);
                                var imagePath = ImageHelpers.SaveImage(bio.Photo);
                                var qrCodePath = ImageHelpers.GenerateVoterQRCode(s.VoterId);
                                Picture qrCode = new Picture(mainDoc, qrCodePath, 50, 50);
                                Picture picture = new Picture(mainDoc, imagePath, 80, 100);
                                tb = BuildVoterDetailsTable(mainDoc, picture, qrCode, s);

                                //toc row
                                DataRow row = tocDataTable.NewRow();
                                row[0] = $"{s.Surname} {s.OtherNames}";
                                row[1] = $"{page} (L)";
                                row[2] = $"{s.VoterId}";
                                row[3] = $"{s.Sex.ToString().Substring(0, 1)}";
                                row[4] = $"{s.EstimatedAge}";
                                tocDataTable.Rows.Add(row);
                            }

                            i = i + 1;
                            Table tb2 = new Table(mainDoc);
                            if (list.Count > i)
                            {
                                var ss = list[i];
                                var bio2 = bios.First(x => x.VoterRegisterId == ss.Id);
                                var imagePath2 = ImageHelpers.SaveImage(bio2.Photo);
                                var qrCodePath2 = ImageHelpers.GenerateVoterQRCode(ss.VoterId);
                                Picture qrCode2 = new Picture(mainDoc, qrCodePath2, 50, 50);
                                Picture picture2 = new Picture(mainDoc, imagePath2, 80, 100);
                                tb2 = BuildVoterDetailsTable(mainDoc, picture2, qrCode2, ss);

                                //toc row
                                DataRow row = tocDataTable.NewRow();
                                row[0] = $"{ss.Surname} {ss.OtherNames}";
                                row[1] = $"{page} (R)";
                                row[2] = $"{ss.VoterId}";
                                row[3] = $"{ss.Sex.ToString().Substring(0, 1)}";
                                row[4] = $"{ss.EstimatedAge}";
                                tocDataTable.Rows.Add(row);
                            }
                            var xTable = new Table(mainDoc,
                        new TableRow(mainDoc,
                            new TableCell(mainDoc, tb)
                            {
                                CellFormat = new TableCellFormat
                                {
                                    PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                                }
                            },
                            new TableCell(mainDoc, tb2)
                            {
                                CellFormat = new TableCellFormat
                                {
                                    PreferredWidth = new TableWidth(50, TableWidthUnit.Percentage)
                                }
                            }));

                            xTable.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                            var xTableBorders = xTable.TableFormat.Borders;
                            xTableBorders.SetBorders(MultipleBorderTypes.Outside, BorderStyle.None, Color.Black, 0.0);
                            xTableBorders.SetBorders(MultipleBorderTypes.Inside, BorderStyle.None, Color.Black, 0.0);

                            foreach (ParagraphFormat paragraphFormat in xTable
                                .GetChildElements(true, ElementType.Paragraph)
                                .Cast<Paragraph>()
                                .Select(p => p.ParagraphFormat))
                            {
                                paragraphFormat.KeepLinesTogether = true;
                                paragraphFormat.KeepWithNext = true;
                            }
                            blocksx.Add(xTable);
                            var size = 12;
                            if (i == 11 || (i + size) % size == 11)
                                blocksx.Add(new Paragraph(mainDoc, new SpecialCharacter(mainDoc, SpecialCharacterType.PageBreak)));

                        }

                        //toc things
                        Table table = new Table(tocDoc, dtRowCount, dtColumnCount, (int rowIndex, int columnIndex)
                            => new TableCell(tocDoc, new Paragraph(tocDoc, tocDataTable.Rows[rowIndex][columnIndex].ToString())));
                        table.TableFormat.PreferredWidth = new TableWidth(100, TableWidthUnit.Percentage);
                        blocks.Add(table);

                        if (File.Exists(mainFileDocx)) File.Delete(mainFileDocx);
                        mainDoc.Save(mainFileDocx);
                        if (File.Exists(tocFileDocx)) File.Delete(tocFileDocx);
                        tocDoc.Save(tocFileDocx);
                        if (File.Exists(coverFileDocx)) File.Delete(coverFileDocx);
                        coverTemplateDoc.Save(coverFileDocx);
                        if (File.Exists(mergedFileDocxToc)) File.Delete(mergedFileDocxToc);
                        if (File.Exists(mergedFileDocxMain)) File.Delete(mergedFileDocxMain);
                        DocumentModel.Load(coverFileDocx)
                            .JoinWith(tocFileDocx)
                            .Save(mergedFileDocxToc);
                        DocumentModel.Load(coverFileDocx)
                            .JoinWith(mainFileDocx)
                            .Save(mergedFileDocxMain);
                        File.Delete(mainFileDocx);
                        File.Delete(tocFileDocx);
                        File.Delete(coverFileDocx);
                        DocumentModel document = DocumentModel.Load(mergedFileDocxToc);
                        DocumentModel documentx = DocumentModel.Load(mergedFileDocxMain);
                        document.Save(mergedFilePdfToc);
                        documentx.Save(mergedFilePdfMain);
                    }
                }
            }
        }
        #endregion
    }
}

