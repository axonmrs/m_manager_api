﻿using Microsoft.EntityFrameworkCore;
using MManager.Models;
using Neurotec.Abis.Client.Management.Rest.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MManager.MrsLogsModels
{
    public partial class mrs_logsContext : DbContext
    {
        public mrs_logsContext()
        {
        }

        public mrs_logsContext(DbContextOptions<mrs_logsContext> options)
            : base(options)
        {
        }

        public DbSet<TemporalVoterRegister> TemporalVoterRegister { get; set; }
        public DbSet<TemporalVoterRegisterBio> TemporalVoterRegisterBio { get; set; }
        public DbSet<TemporalVoterRegisterBioNew> TemporalVoterRegisterBioNew { get; set; }
        public DbSet<DuplicateVoter> DuplicateVoters { get; set; }
        public DbSet<DuplicateVoterHit> DuplicateVoterHits { get; set; }
        public DbSet<EndOfDayReportStatistics> EndOfDayReportStatistics { get; set; }
        public DbSet<FixedDuplicate> FixedDuplicates { get; set; }
        public DbSet<MultipleRegistration> MultipleRegistrations { get; set; }
        public DbSet<MultipleRegistrationBio> MultipleRegistrationBios { get; set; }
        public DbSet<EligibleVoter> EligibleVoters { get; set; }
        public DbSet<ExceptionList> ExceptionList { get; set; }
        public DbSet<FixedSupplementaryDuplicateVoterId> FixedSupplementaryDuplicateVoterIds { get; set; }
        public DbSet<ExceptionListApplicant> ExceptionListApplicants { get; set; }
        public DbSet<ExceptionListApplicantBio> ExceptionListApplicantsBios { get; set; }
        public DbSet<ChallengedVoter> ChallengedVoters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(SystemConfig.Setting.MrsLogsConnectionString, x => x.ServerVersion("8.0.19-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<TemporalVoterRegister>()
        .HasIndex(b => b.VoterId);
            builder.Entity<TemporalVoterRegister>()
        .HasIndex(p => new { p.Surname, p.OtherNames });
            builder.Entity<TemporalVoterRegisterBioNew>().ToTable("temporalvoterregisterbionew");
        }
    }

    public class ChallengedVoter : HasId
    {
        public string CaseNumber { get; set; }
        public string VoterId { get; set; }
        public string VoterReference { get; set; }
        public string PollingStationCode { get; set; }
        public string Challenges { get; set; }
        public DateTime? ResolvedAt { get; set; }
        public string Notes { get; set; }
        public string Decision { get; set; }
        public string Officials { get; set; }
        public bool Confirmed { get; set; }
        public string ResolvedBy { get; set; }
        public ChallengedVoterStatus Status { get; set; } = ChallengedVoterStatus.Pending;
    }

    public enum ChallengedVoterStatus
    {
        Pending,
        Resolved
    }

    public class ResolveDuplicateVoterVM : HasId
    {
        public bool Confirmed { get; set; }
        public string Notes { get; set; }
        public string Officials { get; set; }
        public string Decision { get; set; }
    }

    public class UpdateDuplicateDecisionVM : HasId
    {
        public bool Confirmed { get; set; }
        public string Notes { get; set; }
        public string Officials { get; set; }
        public string Decision { get; set; }
        public string VoterId { get; set; }
    }

    public class TemporalVoterRegister : HasId
    {
        public string Reference { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public string VoterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; } = DateTime.UtcNow;
        public string RegisteredBy { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public bool IsAgeEstimated { get; set; }
        public Sex Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictId { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public string IdentificationTypeId { get; set; }
        public string IdentificationTypeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public string HomeTownDistrictId { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; } = false;
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public bool Pushed { get; set; } = false;
        public DateTime? PushedAt { get; set; }
        public DateTime? PushCompletedAt { get; set; }
        public string AbisResponse { get; set; }
        public Transaction.StatusEnum? TransStatus { get; set; }
        public string AbisRequestId { get; set; }
        public string CheckSum { get; set; }
        public DateTime? BackedUpOn { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public DateTime TransmittedOn { get; set; } = DateTime.UtcNow;
        public TemporalVoterRegisterStatus Status { get; set; }
        public string StatusNotes { get; set; }
        public bool IsUpdate { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string DeviceId { get; set; }
        public string Source { get; set; } = "BVR";
        public string SyncedStatus { get; set; } = "Pending";
    }
    public class TemporalVoterRegisterBio : Models.HasId
    {
        public int TemporalVoterRegisterId { get; set; }
        //public virtual TemporalVoterRegister TemporalVoterRegister { get; set; }
        [MaxLength(10240)]
        public string Photo { get; set; }
        [MaxLength(10240)]
        public string PhotoJ2K { get; set; }
        [MaxLength(10240)]
        public string FaceTemplate { get; set; }
        [MaxLength(10240)]
        public string CompositeTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftThumbTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftIndexTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftMiddleTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftRingTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftPinkyTemplate { get; set; }
        [MaxLength(10240)]
        public string RightThumbTemplate { get; set; }
        [MaxLength(10240)]
        public string RightIndexTemplate { get; set; }
        [MaxLength(10240)]
        public string RightMiddleTemplate { get; set; }
        [MaxLength(10240)]
        public string RightRingTemplate { get; set; }
        [MaxLength(10240)]
        public string RightPinkyTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftThumbWsq { get; set; }
        [MaxLength(10240)]
        public string LeftIndexWsq { get; set; }
        [MaxLength(10240)]
        public string LeftMiddleWsq { get; set; }
        [MaxLength(10240)]
        public string LeftRingWsq { get; set; }
        [MaxLength(10240)]
        public string LeftPinkyWsq { get; set; }
        [MaxLength(10240)]
        public string RightThumbWsq { get; set; }
        [MaxLength(10240)]
        public string RightIndexWsq { get; set; }
        [MaxLength(10240)]
        public string RightMiddleWsq { get; set; }
        [MaxLength(10240)]
        public string RightRingWsq { get; set; }
        [MaxLength(10240)]
        public string RightPinkyWsq { get; set; }
    }

    public class TemporalVoterRegisterBioNew : HasId
    {
        public int TemporalVoterRegisterId { get; set; }
        //public virtual TemporalVoterRegister TemporalVoterRegister { get; set; }
        [MaxLength(10240)]
        public string Photo { get; set; }
        [MaxLength(10240)]
        public string PhotoJ2K { get; set; }
        [MaxLength(10240)]
        public string FaceTemplate { get; set; }
        [MaxLength(10240)]
        public string CompositeTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftThumbTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftIndexTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftMiddleTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftRingTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftPinkyTemplate { get; set; }
        [MaxLength(10240)]
        public string RightThumbTemplate { get; set; }
        [MaxLength(10240)]
        public string RightIndexTemplate { get; set; }
        [MaxLength(10240)]
        public string RightMiddleTemplate { get; set; }
        [MaxLength(10240)]
        public string RightRingTemplate { get; set; }
        [MaxLength(10240)]
        public string RightPinkyTemplate { get; set; }
        [MaxLength(10240)]
        public string LeftThumbWsq { get; set; }
        [MaxLength(10240)]
        public string LeftIndexWsq { get; set; }
        [MaxLength(10240)]
        public string LeftMiddleWsq { get; set; }
        [MaxLength(10240)]
        public string LeftRingWsq { get; set; }
        [MaxLength(10240)]
        public string LeftPinkyWsq { get; set; }
        [MaxLength(10240)]
        public string RightThumbWsq { get; set; }
        [MaxLength(10240)]
        public string RightIndexWsq { get; set; }
        [MaxLength(10240)]
        public string RightMiddleWsq { get; set; }
        [MaxLength(10240)]
        public string RightRingWsq { get; set; }
        [MaxLength(10240)]
        public string RightPinkyWsq { get; set; }
    }

    public enum TemporalVoterRegisterStatus
    {
        Pending,
        PassedCheckSumCheck,
        FailedCheckSumCheck,
        PushedToAbis,
        PushedToAbisWithDuplicate,
        RejectedByAbis,
        RejectedByAbisRegistered,
        PushFailed,
        ReceivedEncounter,
        InitialPush,
        PushedToAbisAwaitingAdjudication,
        RegisteredByAbis,
        WithIssues,
        DuplicateResolved,
        ExistingTransaction,
        HasDuplicate,
        DataVerified = 25,
        DataMovedToTemp = 26,
        HasExistingVoterId = 27,
        HasNoSupBioData = 28,
        HasNoOnlineBioData = 29
    }

    public enum Sex
    {
        Male,
        Female
    }

    public class DuplicateVoter : HasId
    {
        public DateTime ReceivedAt { get; set; } = DateTime.UtcNow;
        public string VoterId { get; set; }
        public string VoterReference { get; set; }
        public int TemporalVoterRegisterId { get; set; }
        public virtual TemporalVoterRegister TemporalVoterRegister { get; set; }
        public double FusedMatchingScore { get; set; }
        public double FingerPrintsScore { get; set; }
        public double FaceScore { get; set; }
        public DuplicateVoterRecordStatus Status { get; set; } = DuplicateVoterRecordStatus.Pending;
        public DateTime? ResolvedAt { get; set; }
        public string Notes { get; set; }
        public string Decision { get; set; }
        public string Officials { get; set; }
        public bool Confirmed { get; set; }
        public string ResolvedBy { get; set; }
        public virtual List<DuplicateVoterHit> Hits { get; set; }
    }

    public class DuplicateVoterHit : HasId
    {
        public int DuplicateVoterId { get; set; }
        public virtual DuplicateVoter DuplicateVoter { get; set; }
        public int HitTemporalVoterRegisterId { get; set; }
        public virtual TemporalVoterRegister HitTemporalVoterRegister { get; set; }
        public string HitVoterId { get; set; }
        public string HitVoterReference { get; set; }
        public double FusedMatchingScore { get; set; }
        public double FingerPrintsScore { get; set; }
        public double FaceScore { get; set; }
    }
    public enum DuplicateVoterRecordStatus
    {
        Pending,
        Verified,
        UnknownDuplicateVoterId,
        UnknownEligibleVoterId,
        Resolved,
        Completed
    }

    public class EndOfDayReportStatistics : HasId
    {
        [MaxLength(512), Required]
        public string Device { get; set; }
        [MaxLength(128), Required]
        public string PollingStationCode { get; set; }
        [MaxLength(128), Required]
        public string PollingStationName { get; set; }
        public string Reference { get; set; }
        public string RegistrationOfficer { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastRegistrationDate { get; set; }
        public int TotalNumberOfRegistrations { get; set; }
        public int NoOfMales { get; set; }
        public int NoOfFemales { get; set; }
        public string PrintedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public EndOfDayReportStatisticsStatus Status { get; set; } = EndOfDayReportStatisticsStatus.Pending;
        public string StatusNotes { get; set; }
        public DateTime? LastRunDate { get; set; } = DateTime.Now.AddDays(-1);
    }
    public enum EndOfDayReportStatisticsStatus
    {
        Pending,
        Failed,
        Passed,
        Verified,
        DuplicateDate
    }

    public class FixedDuplicate : AuditFields
    {
        [MaxLength(32), Required]
        public string OldVoterId { get; set; }
        [MaxLength(32), Required]
        public string NewVoterId { get; set; }
        public int TemporalVoterRegisterId { get; set; }
        public virtual TemporalVoterRegister TemporalVoterRegister { get; set; }
        public string Status { get; set; }
    }

    public class MultipleRegistration : HasId
    {
        public string Reference { get; set; }
        public string VoterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; }
        public DateTime SyncedAt { get; set; }
        public string RegisteredBy { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public int? PollingStationId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public bool IsAgeEstimated { get; set; }
        public Models.Sex Sex { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public int? ResidentialDistrictId { get; set; }
        public string IdentificationTypeCode { get; set; }
        public int? IdentificationTypeId { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public int? HomeTownDistrictId { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; }
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewedOn { get; set; }
        public bool Reviewed { get; set; }
        public bool HasDuplicate { get; set; }
        public string Source { get; set; } = "BVR";
    }

    public class MultipleRegistrationBio : HasId
    {
        public int MultipleRegistrationId { get; set; }
        public virtual MultipleRegistration MultipleRegistration { get; set; }
        [MaxLength(10240)]
        public string Photo { get; set; }
        [MaxLength(10240)]
        public string CompositeTemplate { get; set; }
    }

    public class EligibleVoter : HasId
    {
        public DateTime ReceivedAt { get; set; } = DateTime.UtcNow;
        public string VoterId { get; set; }
        public string VoterReference { get; set; }
        public EligibleVoterRecordStatus Status { get; set; } = EligibleVoterRecordStatus.Pending;
    }
    public enum EligibleVoterRecordStatus
    {
        Pending,
        Verified,
        Synced,
        DuplicateFound,
        DuplicateRemoved,
        UnknownVoterId
    }

    public class ExceptionList : HasId
    {
        public string CaseNumber { get; set; }
        public string VoterId { get; set; }
        public string VoterReference { get; set; }
        public string PollingStationCode { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; } = true;
    }

    public class ExceptionListApplicant : HasId
    {
        public string Reference { get; set; }
        public string VoterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; }
        public DateTime SyncedAt { get; set; }
        public string RegisteredBy { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public int? PollingStationId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public bool IsAgeEstimated { get; set; }
        public Models.Sex Sex { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public int? ResidentialDistrictId { get; set; }
        public string IdentificationTypeCode { get; set; }
        public int? IdentificationTypeId { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public int? HomeTownDistrictId { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; }
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewedOn { get; set; }
        public bool Reviewed { get; set; }
        public bool HasDuplicate { get; set; }
        public string Source { get; set; } = "BVR";
    }

    public class ExceptionListApplicantBio : HasId
    {
        public int ExceptionListApplicantId { get; set; }
        public virtual ExceptionListApplicant ExceptionListApplicant { get; set; }
        [MaxLength(10240)]
        public string Photo { get; set; }
        [MaxLength(10240)]
        public string CompositeTemplate { get; set; }
    }

    public class FixedSupplementaryDuplicateVoterId : AuditFields
    {
        [MaxLength(32), Required]
        public string OldVoterId { get; set; }
        [MaxLength(32), Required]
        public string NewVoterId { get; set; }
        public int SupplementaryVoterRegisterId { get; set; }
        public virtual SupplementaryVoterRegister SupplementaryVoterRegister { get; set; }
        public string Status { get; set; }
    }

    public class SupplementaryVoterRegister : HasId
    {
        public string Reference { get; set; }
        public string PollingStationName { get; set; }
        public string PollingStationCode { get; set; }
        public string VoterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; } = DateTime.UtcNow;
        public string RegisteredBy { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EstimatedAge { get; set; }
        public bool IsAgeEstimated { get; set; }
        public Sex Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string ResidentialAddress { get; set; }
        public string ResidentialTown { get; set; }
        public string ResidentialDistrictId { get; set; }
        public string ResidentialDistrictCode { get; set; }
        public string IdentificationTypeId { get; set; }
        public string IdentificationTypeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? IdentificationExpiry { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HomeTownAddress { get; set; }
        public string HomeTown { get; set; }
        public string HomeTownDistrictId { get; set; }
        public string HomeTownDistrictCode { get; set; }
        public bool IsVisuallyImpaired { get; set; }
        public bool IsDisabled { get; set; }
        public bool HearingImpaired { get; set; } = false;
        public bool Leper { get; set; } = false;
        public bool MissingFingersLeft { get; set; }
        public bool MissingFingersRight { get; set; }
        public bool AmputatedHandsLeft { get; set; }
        public bool AmputatedHandsRight { get; set; }
        public string Challenges { get; set; }
        public bool Pushed { get; set; } = false;
        public DateTime? PushedAt { get; set; }
        public DateTime? PushCompletedAt { get; set; }
        public string AbisResponse { get; set; }
        public Transaction.StatusEnum? TransStatus { get; set; }
        public string AbisRequestId { get; set; }
        public string CheckSum { get; set; }
        public DateTime? BackedUpOn { get; set; }
        public string GuarantorOneVoterId { get; set; }
        public string GuarantorTwoVoterId { get; set; }
        public string GuarantorOneName { get; set; }
        public string GuarantorTwoName { get; set; }
        public int CardPrintCount { get; set; }
        public DateTime TransmittedOn { get; set; } = DateTime.UtcNow;
        public TemporalVoterRegisterStatus Status { get; set; }
        public string StatusNotes { get; set; }
        public bool IsUpdate { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string DeviceId { get; set; }
        public string Source { get; set; } = "BVR";
        public string SyncedStatus { get; set; } = "Pending";
    }
}
