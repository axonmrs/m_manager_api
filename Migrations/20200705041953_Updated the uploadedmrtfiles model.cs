﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class Updatedtheuploadedmrtfilesmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "UploadedMrtFiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                table: "UploadedMrtFiles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "UploadedMrtFiles");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "UploadedMrtFiles");
        }
    }
}
