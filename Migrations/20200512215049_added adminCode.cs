﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class addedadminCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdminCode",
                table: "RegistrationDevices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminCode",
                table: "RegistrationDevices");
        }
    }
}
