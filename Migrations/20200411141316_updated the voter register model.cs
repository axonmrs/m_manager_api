﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class updatedthevoterregistermodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LeftIndexPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftMiddlePhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftPinkyPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftRingPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftThumbPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightIndexPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightMiddlePhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightPinkyPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightRingPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightThumbPhoto",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "AbisReference",
                table: "VoterRegister");

            migrationBuilder.DropColumn(
                name: "AbisRequestId",
                table: "VoterRegister");

            migrationBuilder.DropColumn(
                name: "BackedUpOn",
                table: "VoterRegister");

            migrationBuilder.DropColumn(
                name: "DuplicateId",
                table: "VoterRegister");

            migrationBuilder.DropColumn(
                name: "PushedAt",
                table: "VoterRegister");

            migrationBuilder.DropColumn(
                name: "TransStatus",
                table: "VoterRegister");

            migrationBuilder.RenameColumn(
                name: "PhotoJ2k",
                table: "VoterRegisterTemplates",
                newName: "PhotoJ2K");

            migrationBuilder.RenameColumn(
                name: "VoterNumber",
                table: "VoterRegister",
                newName: "VoterId");

            migrationBuilder.RenameColumn(
                name: "TransmittedOn",
                table: "VoterRegister",
                newName: "ReviewedOn");

            migrationBuilder.RenameColumn(
                name: "Transmitted",
                table: "VoterRegister",
                newName: "MissingFingersRight");

            migrationBuilder.RenameColumn(
                name: "Tags",
                table: "VoterRegister",
                newName: "ReviewedBy");

            migrationBuilder.RenameColumn(
                name: "Pushed",
                table: "VoterRegister",
                newName: "MissingFingersLeft");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "VoterRegister",
                newName: "ResidentialDistrictCode");

            migrationBuilder.RenameColumn(
                name: "ModifiedAt",
                table: "VoterRegister",
                newName: "SyncedAt");

            migrationBuilder.RenameColumn(
                name: "Locked",
                table: "VoterRegister",
                newName: "Leper");

            migrationBuilder.RenameColumn(
                name: "IsEstimatedAge",
                table: "VoterRegister",
                newName: "IsAgeEstimated");

            migrationBuilder.RenameColumn(
                name: "IsDeleted",
                table: "VoterRegister",
                newName: "HearingImpaired");

            migrationBuilder.RenameColumn(
                name: "Hidden",
                table: "VoterRegister",
                newName: "AmputatedHandsRight");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "VoterRegister",
                newName: "IdentificationTypeCode");

            migrationBuilder.RenameColumn(
                name: "Comments",
                table: "VoterRegister",
                newName: "HomeTownDistrictCode");

            migrationBuilder.RenameColumn(
                name: "BackedUp",
                table: "VoterRegister",
                newName: "AmputatedHandsLeft");

            migrationBuilder.RenameColumn(
                name: "AbisResponse",
                table: "VoterRegister",
                newName: "Challenges");

            migrationBuilder.AddColumn<string>(
                name: "CompositeTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftIndexWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftMiddleWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftPinkyWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftRingWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftThumbWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightIndexWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightMiddleWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightPinkyWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightRingWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightThumbWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReceivedAt",
                table: "VoterRegister",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompositeTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftIndexWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftMiddleWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftPinkyWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftRingWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftThumbWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightIndexWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightMiddleWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightPinkyWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightRingWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightThumbWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "ReceivedAt",
                table: "VoterRegister");

            migrationBuilder.RenameColumn(
                name: "PhotoJ2K",
                table: "VoterRegisterTemplates",
                newName: "PhotoJ2k");

            migrationBuilder.RenameColumn(
                name: "VoterId",
                table: "VoterRegister",
                newName: "VoterNumber");

            migrationBuilder.RenameColumn(
                name: "SyncedAt",
                table: "VoterRegister",
                newName: "ModifiedAt");

            migrationBuilder.RenameColumn(
                name: "ReviewedOn",
                table: "VoterRegister",
                newName: "TransmittedOn");

            migrationBuilder.RenameColumn(
                name: "ReviewedBy",
                table: "VoterRegister",
                newName: "Tags");

            migrationBuilder.RenameColumn(
                name: "ResidentialDistrictCode",
                table: "VoterRegister",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "MissingFingersRight",
                table: "VoterRegister",
                newName: "Transmitted");

            migrationBuilder.RenameColumn(
                name: "MissingFingersLeft",
                table: "VoterRegister",
                newName: "Pushed");

            migrationBuilder.RenameColumn(
                name: "Leper",
                table: "VoterRegister",
                newName: "Locked");

            migrationBuilder.RenameColumn(
                name: "IsAgeEstimated",
                table: "VoterRegister",
                newName: "IsEstimatedAge");

            migrationBuilder.RenameColumn(
                name: "IdentificationTypeCode",
                table: "VoterRegister",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "HomeTownDistrictCode",
                table: "VoterRegister",
                newName: "Comments");

            migrationBuilder.RenameColumn(
                name: "HearingImpaired",
                table: "VoterRegister",
                newName: "IsDeleted");

            migrationBuilder.RenameColumn(
                name: "Challenges",
                table: "VoterRegister",
                newName: "AbisResponse");

            migrationBuilder.RenameColumn(
                name: "AmputatedHandsRight",
                table: "VoterRegister",
                newName: "Hidden");

            migrationBuilder.RenameColumn(
                name: "AmputatedHandsLeft",
                table: "VoterRegister",
                newName: "BackedUp");

            migrationBuilder.AddColumn<string>(
                name: "LeftIndexPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftMiddlePhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftPinkyPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftRingPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftThumbPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightIndexPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightMiddlePhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightPinkyPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightRingPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightThumbPhoto",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AbisReference",
                table: "VoterRegister",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AbisRequestId",
                table: "VoterRegister",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BackedUpOn",
                table: "VoterRegister",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DuplicateId",
                table: "VoterRegister",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "PushedAt",
                table: "VoterRegister",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransStatus",
                table: "VoterRegister",
                nullable: true);
        }
    }
}
