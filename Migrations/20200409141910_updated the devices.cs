﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class updatedthedevices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhotoJ2k",
                table: "VoterRegisterTemplates",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Sequence",
                table: "RegistrationDevices",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 128);

            migrationBuilder.AddColumn<bool>(
                name: "AssignedToRegion",
                table: "RegistrationDevices",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AssignedToRegionBy",
                table: "RegistrationDevices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "AssignedToRegionOn",
                table: "RegistrationDevices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RegionId",
                table: "RegistrationDevices",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RegistrationDevices_RegionId",
                table: "RegistrationDevices",
                column: "RegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDevices_Regions_RegionId",
                table: "RegistrationDevices",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDevices_Regions_RegionId",
                table: "RegistrationDevices");

            migrationBuilder.DropIndex(
                name: "IX_RegistrationDevices_RegionId",
                table: "RegistrationDevices");

            migrationBuilder.DropColumn(
                name: "PhotoJ2k",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "AssignedToRegion",
                table: "RegistrationDevices");

            migrationBuilder.DropColumn(
                name: "AssignedToRegionBy",
                table: "RegistrationDevices");

            migrationBuilder.DropColumn(
                name: "AssignedToRegionOn",
                table: "RegistrationDevices");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "RegistrationDevices");

            migrationBuilder.AlterColumn<string>(
                name: "Sequence",
                table: "RegistrationDevices",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);
        }
    }
}
