﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class updatedthetransferrequestmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSpecial",
                table: "TransferRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OldPollingStationId",
                table: "TransferRequests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransferRequests_OldPollingStationId",
                table: "TransferRequests",
                column: "OldPollingStationId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequests_Stations_OldPollingStationId",
                table: "TransferRequests",
                column: "OldPollingStationId",
                principalTable: "Stations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequests_Stations_OldPollingStationId",
                table: "TransferRequests");

            migrationBuilder.DropIndex(
                name: "IX_TransferRequests_OldPollingStationId",
                table: "TransferRequests");

            migrationBuilder.DropColumn(
                name: "IsSpecial",
                table: "TransferRequests");

            migrationBuilder.DropColumn(
                name: "OldPollingStationId",
                table: "TransferRequests");
        }
    }
}
