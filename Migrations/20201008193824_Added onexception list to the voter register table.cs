﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class Addedonexceptionlisttothevoterregistertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "OnExceptionList",
                table: "VoterRegister",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OnExceptionList",
                table: "VoterRegister");
        }
    }
}
