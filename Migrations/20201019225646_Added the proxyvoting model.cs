﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class Addedtheproxyvotingmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProxyVotingRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    SourceVoterRecordId = table.Column<int>(nullable: false),
                    ProxyVoterRecordId = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    Officials = table.Column<string>(nullable: true),
                    VettingNotes = table.Column<string>(nullable: true),
                    VettedOn = table.Column<DateTime>(nullable: true),
                    VettedBy = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProxyVotingRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProxyVotingRequest_VoterRegister_ProxyVoterRecordId",
                        column: x => x.ProxyVoterRecordId,
                        principalTable: "VoterRegister",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProxyVotingRequest_VoterRegister_SourceVoterRecordId",
                        column: x => x.SourceVoterRecordId,
                        principalTable: "VoterRegister",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProxyVotingRequest_ProxyVoterRecordId",
                table: "ProxyVotingRequest",
                column: "ProxyVoterRecordId");

            migrationBuilder.CreateIndex(
                name: "IX_ProxyVotingRequest_SourceVoterRecordId",
                table: "ProxyVotingRequest",
                column: "SourceVoterRecordId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProxyVotingRequest");
        }
    }
}
