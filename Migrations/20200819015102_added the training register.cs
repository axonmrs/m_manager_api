﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class addedthetrainingregister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TrainingRegister",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    VoterRegisterId = table.Column<int>(nullable: false),
                    VoterId = table.Column<int>(nullable: false),
                    Surname = table.Column<string>(nullable: true),
                    OtherNames = table.Column<string>(nullable: true),
                    PollingStationId = table.Column<int>(nullable: false),
                    DistrictId = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingRegister", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrainingRegister_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrainingRegister_Stations_PollingStationId",
                        column: x => x.PollingStationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrainingRegister_VoterRegister_VoterRegisterId",
                        column: x => x.VoterRegisterId,
                        principalTable: "VoterRegister",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TrainingRegister_DistrictId",
                table: "TrainingRegister",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingRegister_PollingStationId",
                table: "TrainingRegister",
                column: "PollingStationId");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingRegister_VoterRegisterId",
                table: "TrainingRegister",
                column: "VoterRegisterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrainingRegister");
        }
    }
}
