﻿// <auto-generated />
using System;
using MManager.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MManager.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200411141316_updated the voter register model")]
    partial class updatedthevoterregistermodel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("MManager.Models.Amendment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("Data");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("OldData");

                    b.Property<string>("Reason");

                    b.Property<string>("Reference");

                    b.Property<int>("Status");

                    b.Property<int>("Type");

                    b.Property<string>("VettedBy");

                    b.Property<DateTime?>("VettedOn");

                    b.Property<string>("VettingNotes");

                    b.Property<int>("VoterRecordId");

                    b.HasKey("Id");

                    b.HasIndex("VoterRecordId");

                    b.ToTable("Amendments");
                });

            modelBuilder.Entity("MManager.Models.AppSetting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.ToTable("AppSettings");
                });

            modelBuilder.Entity("MManager.Models.ChallengeReason", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.HasKey("Id");

                    b.ToTable("ChallengeReasons");
                });

            modelBuilder.Entity("MManager.Models.Constituency", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<int>("DistrictId");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("OrderIndex");

                    b.HasKey("Id");

                    b.HasIndex("DistrictId");

                    b.HasIndex("Name");

                    b.ToTable("Constituencies");
                });

            modelBuilder.Entity("MManager.Models.District", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("OrderIndex");

                    b.Property<int>("RegionId");

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.HasIndex("RegionId");

                    b.ToTable("Districts");
                });

            modelBuilder.Entity("MManager.Models.ElectoralArea", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("ConstituencyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("OrderIndex");

                    b.HasKey("Id");

                    b.HasIndex("ConstituencyId");

                    b.HasIndex("Name");

                    b.ToTable("ElectoralAreas");
                });

            modelBuilder.Entity("MManager.Models.ExternalSystem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("Description");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name");

                    b.Property<int>("SystemType");

                    b.Property<string>("TokenSecret");

                    b.HasKey("Id");

                    b.ToTable("ExternalSystems");
                });

            modelBuilder.Entity("MManager.Models.IdType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.HasKey("Id");

                    b.ToTable("IdTypes");
                });

            modelBuilder.Entity("MManager.Models.Message", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .HasMaxLength(128);

                    b.Property<string>("Recipient")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Response")
                        .HasMaxLength(5000);

                    b.Property<int>("Status");

                    b.Property<string>("Subject")
                        .HasMaxLength(128);

                    b.Property<string>("Text")
                        .IsRequired();

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Messages");
                });

            modelBuilder.Entity("MManager.Models.Region", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("OrderIndex");

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.ToTable("Regions");
                });

            modelBuilder.Entity("MManager.Models.RegistrationDevice", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ActivationCode");

                    b.Property<bool>("AssignedToDistrict");

                    b.Property<string>("AssignedToDistrictBy");

                    b.Property<DateTime?>("AssignedToDistrictOn");

                    b.Property<bool>("AssignedToRegion");

                    b.Property<string>("AssignedToRegionBy");

                    b.Property<DateTime?>("AssignedToRegionOn");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("DistrictConfirmed");

                    b.Property<string>("DistrictConfirmedBy");

                    b.Property<DateTime?>("DistrictConfirmedOn");

                    b.Property<int?>("DistrictId");

                    b.Property<bool>("Hidden");

                    b.Property<string>("IdentificationCode")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Reference");

                    b.Property<bool>("RegionConfirmed");

                    b.Property<string>("RegionConfirmedBy");

                    b.Property<DateTime?>("RegionConfirmedOn");

                    b.Property<int?>("RegionId");

                    b.Property<string>("Sequence")
                        .HasMaxLength(128);

                    b.Property<string>("SerialNumber")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.HasKey("Id");

                    b.HasIndex("DistrictId");

                    b.HasIndex("IdentificationCode");

                    b.HasIndex("RegionId");

                    b.HasIndex("SerialNumber");

                    b.ToTable("RegistrationDevices");
                });

            modelBuilder.Entity("MManager.Models.Role", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.Property<int>("Type");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("MManager.Models.RoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("MManager.Models.Station", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<int>("ElectoralAreaId");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("OrderIndex");

                    b.HasKey("Id");

                    b.HasIndex("ElectoralAreaId");

                    b.HasIndex("Name");

                    b.ToTable("Stations");
                });

            modelBuilder.Entity("MManager.Models.SystemLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Model");

                    b.Property<string>("Notes");

                    b.Property<string>("Payload");

                    b.Property<int>("Type");

                    b.Property<string>("User");

                    b.HasKey("Id");

                    b.ToTable("SystemLogs");
                });

            modelBuilder.Entity("MManager.Models.TransferRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<int>("NewPollingStationId");

                    b.Property<string>("Reason");

                    b.Property<int>("Status");

                    b.Property<string>("VettedBy");

                    b.Property<DateTime?>("VettedOn");

                    b.Property<string>("VettingNotes");

                    b.Property<int>("VoterRecordId");

                    b.HasKey("Id");

                    b.HasIndex("NewPollingStationId");

                    b.HasIndex("VoterRecordId");

                    b.ToTable("TransferRequests");
                });

            modelBuilder.Entity("MManager.Models.UnassignedRegistrationDevice", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("DeviceData");

                    b.Property<int>("DeviceId");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Reason");

                    b.HasKey("Id");

                    b.HasIndex("DeviceId");

                    b.ToTable("UnassignedRegistrationDevices");
                });

            modelBuilder.Entity("MManager.Models.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int?>("DistrictId");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<int?>("RegionId");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("DistrictId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("RegionId");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("MManager.Models.UserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("MManager.Models.UserLogin", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider")
                        .IsRequired();

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("ProviderKey")
                        .IsRequired();

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAlternateKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("MManager.Models.UserRole", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("MManager.Models.UserToken", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("MManager.Models.VerificationUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("BioTemplate");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("Email");

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("Photo");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(64);

                    b.HasKey("Id");

                    b.ToTable("VerificationUsers");
                });

            modelBuilder.Entity("MManager.Models.VoterRegister", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AmputatedHandsLeft");

                    b.Property<bool>("AmputatedHandsRight");

                    b.Property<int>("CardPrintCount");

                    b.Property<string>("Challenges");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DateOfBirth");

                    b.Property<int>("EstimatedAge");

                    b.Property<string>("FatherName");

                    b.Property<string>("GuarantorOneName");

                    b.Property<string>("GuarantorOneVoterId");

                    b.Property<string>("GuarantorTwoName");

                    b.Property<string>("GuarantorTwoVoterId");

                    b.Property<bool>("HasDuplicate");

                    b.Property<bool>("HearingImpaired");

                    b.Property<string>("HomeTown");

                    b.Property<string>("HomeTownAddress");

                    b.Property<string>("HomeTownDistrictCode");

                    b.Property<int?>("HomeTownDistrictId");

                    b.Property<DateTime?>("IdentificationExpiry");

                    b.Property<string>("IdentificationNumber");

                    b.Property<string>("IdentificationTypeCode");

                    b.Property<int?>("IdentificationTypeId");

                    b.Property<bool>("IsAgeEstimated");

                    b.Property<bool>("IsDisabled");

                    b.Property<bool>("IsVisuallyImpaired");

                    b.Property<bool>("Leper");

                    b.Property<bool>("MissingFingersLeft");

                    b.Property<bool>("MissingFingersRight");

                    b.Property<string>("MotherName");

                    b.Property<string>("OtherNames");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("PollingStationCode");

                    b.Property<int?>("PollingStationId");

                    b.Property<string>("PollingStationName");

                    b.Property<DateTime>("ReceivedAt");

                    b.Property<string>("Reference");

                    b.Property<string>("RegisteredBy");

                    b.Property<string>("ResidentialAddress");

                    b.Property<string>("ResidentialDistrictCode");

                    b.Property<int?>("ResidentialDistrictId");

                    b.Property<string>("ResidentialTown");

                    b.Property<bool>("Reviewed");

                    b.Property<string>("ReviewedBy");

                    b.Property<DateTime?>("ReviewedOn");

                    b.Property<int>("Sex");

                    b.Property<string>("Surname");

                    b.Property<DateTime>("SyncedAt");

                    b.Property<string>("VoterId");

                    b.HasKey("Id");

                    b.HasIndex("HomeTownDistrictId");

                    b.HasIndex("IdentificationTypeId");

                    b.HasIndex("PollingStationId");

                    b.HasIndex("ResidentialDistrictId");

                    b.ToTable("VoterRegister");
                });

            modelBuilder.Entity("MManager.Models.VoterRegisterTemplates", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompositeTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("FaceTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftIndexTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftIndexWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftMiddleTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftMiddleWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftPinkyTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftPinkyWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftRingTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftRingWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftThumbTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("LeftThumbWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("Photo")
                        .HasMaxLength(10240);

                    b.Property<string>("PhotoJ2K")
                        .HasMaxLength(10240);

                    b.Property<string>("RightIndexTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("RightIndexWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("RightMiddleTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("RightMiddleWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("RightPinkyTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("RightPinkyWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("RightRingTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("RightRingWsq")
                        .HasMaxLength(10240);

                    b.Property<string>("RightThumbTemplate")
                        .HasMaxLength(10240);

                    b.Property<string>("RightThumbWsq")
                        .HasMaxLength(10240);

                    b.Property<int>("VoterRegisterId");

                    b.HasKey("Id");

                    b.HasIndex("VoterRegisterId");

                    b.ToTable("VoterRegisterTemplates");
                });

            modelBuilder.Entity("MManager.Models.VotingStation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("FromLetter")
                        .HasConversion(new ValueConverter<string, string>(v => default(string), v => default(string), new ConverterMappingHints(size: 1)));

                    b.Property<bool>("Hidden");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastSyncedAt");

                    b.Property<bool>("Locked");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("Notes")
                        .HasMaxLength(128);

                    b.Property<int>("StationId");

                    b.Property<string>("ToLetter")
                        .HasConversion(new ValueConverter<string, string>(v => default(string), v => default(string), new ConverterMappingHints(size: 1)));

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.HasIndex("StationId");

                    b.ToTable("VotingStations");
                });

            modelBuilder.Entity("MManager.Models.Amendment", b =>
                {
                    b.HasOne("MManager.Models.VoterRegister", "VoterRecord")
                        .WithMany()
                        .HasForeignKey("VoterRecordId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.Constituency", b =>
                {
                    b.HasOne("MManager.Models.District", "District")
                        .WithMany("Constituencies")
                        .HasForeignKey("DistrictId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.District", b =>
                {
                    b.HasOne("MManager.Models.Region", "Region")
                        .WithMany("Districts")
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.ElectoralArea", b =>
                {
                    b.HasOne("MManager.Models.Constituency", "Constituency")
                        .WithMany("ElectoralAreas")
                        .HasForeignKey("ConstituencyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.RegistrationDevice", b =>
                {
                    b.HasOne("MManager.Models.District", "District")
                        .WithMany()
                        .HasForeignKey("DistrictId");

                    b.HasOne("MManager.Models.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId");
                });

            modelBuilder.Entity("MManager.Models.RoleClaim", b =>
                {
                    b.HasOne("MManager.Models.Role", "Role")
                        .WithMany("RoleClaims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.Station", b =>
                {
                    b.HasOne("MManager.Models.ElectoralArea", "ElectoralArea")
                        .WithMany("Stations")
                        .HasForeignKey("ElectoralAreaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.TransferRequest", b =>
                {
                    b.HasOne("MManager.Models.Station", "NewPollingStation")
                        .WithMany()
                        .HasForeignKey("NewPollingStationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MManager.Models.VoterRegister", "VoterRecord")
                        .WithMany()
                        .HasForeignKey("VoterRecordId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.UnassignedRegistrationDevice", b =>
                {
                    b.HasOne("MManager.Models.RegistrationDevice", "Device")
                        .WithMany()
                        .HasForeignKey("DeviceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.User", b =>
                {
                    b.HasOne("MManager.Models.District", "District")
                        .WithMany()
                        .HasForeignKey("DistrictId");

                    b.HasOne("MManager.Models.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId");
                });

            modelBuilder.Entity("MManager.Models.UserClaim", b =>
                {
                    b.HasOne("MManager.Models.User", "User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.UserLogin", b =>
                {
                    b.HasOne("MManager.Models.User", "User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.UserRole", b =>
                {
                    b.HasOne("MManager.Models.Role", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MManager.Models.User", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.UserToken", b =>
                {
                    b.HasOne("MManager.Models.User", "User")
                        .WithMany("Tokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.VoterRegister", b =>
                {
                    b.HasOne("MManager.Models.District", "HomeTownDistrict")
                        .WithMany()
                        .HasForeignKey("HomeTownDistrictId");

                    b.HasOne("MManager.Models.IdType", "IdentificationType")
                        .WithMany()
                        .HasForeignKey("IdentificationTypeId");

                    b.HasOne("MManager.Models.Station", "PollingStation")
                        .WithMany()
                        .HasForeignKey("PollingStationId");

                    b.HasOne("MManager.Models.District", "ResidentialDistrict")
                        .WithMany()
                        .HasForeignKey("ResidentialDistrictId");
                });

            modelBuilder.Entity("MManager.Models.VoterRegisterTemplates", b =>
                {
                    b.HasOne("MManager.Models.VoterRegister", "VoterRegister")
                        .WithMany()
                        .HasForeignKey("VoterRegisterId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MManager.Models.VotingStation", b =>
                {
                    b.HasOne("MManager.Models.Station", "Station")
                        .WithMany("VotingStations")
                        .HasForeignKey("StationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
