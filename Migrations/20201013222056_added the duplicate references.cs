﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class addedtheduplicatereferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DuplicateVoterReferences",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DuplicateVoterRegisterId = table.Column<int>(nullable: false),
                    HitVoterRegisterId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DuplicateVoterReferences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DuplicateVoterReferences_VoterRegister_DuplicateVoterRegiste~",
                        column: x => x.DuplicateVoterRegisterId,
                        principalTable: "VoterRegister",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DuplicateVoterReferences_VoterRegister_HitVoterRegisterId",
                        column: x => x.HitVoterRegisterId,
                        principalTable: "VoterRegister",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DuplicateVoterReferences_DuplicateVoterRegisterId",
                table: "DuplicateVoterReferences",
                column: "DuplicateVoterRegisterId");

            migrationBuilder.CreateIndex(
                name: "IX_DuplicateVoterReferences_HitVoterRegisterId",
                table: "DuplicateVoterReferences",
                column: "HitVoterRegisterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DuplicateVoterReferences");
        }
    }
}
