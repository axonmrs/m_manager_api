﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class addedtheverificationdevicemodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VerificationDevices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    ActivationCode = table.Column<string>(nullable: true),
                    Imei = table.Column<string>(maxLength: 128, nullable: false),
                    IdentificationCode = table.Column<string>(maxLength: 50, nullable: false),
                    Sequence = table.Column<string>(maxLength: 128, nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    AssignedToDistrict = table.Column<bool>(nullable: false),
                    AssignedToDistrictOn = table.Column<DateTime>(nullable: true),
                    AssignedToDistrictBy = table.Column<string>(nullable: true),
                    RegionId = table.Column<int>(nullable: true),
                    AssignedToRegion = table.Column<bool>(nullable: false),
                    AssignedToRegionOn = table.Column<DateTime>(nullable: true),
                    AssignedToRegionBy = table.Column<string>(nullable: true),
                    RegionConfirmed = table.Column<bool>(nullable: false),
                    RegionConfirmedOn = table.Column<DateTime>(nullable: true),
                    RegionConfirmedBy = table.Column<string>(nullable: true),
                    DistrictConfirmed = table.Column<bool>(nullable: false),
                    DistrictConfirmedOn = table.Column<DateTime>(nullable: true),
                    DistrictConfirmedBy = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerificationDevices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VerificationDevices_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VerificationDevices_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnassignedVerificationDevices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeviceId = table.Column<int>(nullable: false),
                    Reason = table.Column<string>(nullable: true),
                    DeviceData = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnassignedVerificationDevices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnassignedVerificationDevices_VerificationDevices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "VerificationDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UnassignedVerificationDevices_DeviceId",
                table: "UnassignedVerificationDevices",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_VerificationDevices_DistrictId",
                table: "VerificationDevices",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_VerificationDevices_RegionId",
                table: "VerificationDevices",
                column: "RegionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UnassignedVerificationDevices");

            migrationBuilder.DropTable(
                name: "VerificationDevices");
        }
    }
}
