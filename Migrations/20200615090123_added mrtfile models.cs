﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MManager.Migrations
{
    public partial class addedmrtfilemodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FaceTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftIndexTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftIndexWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftMiddleTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftMiddleWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftPinkyTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftPinkyWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftRingTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftRingWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftThumbTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "LeftThumbWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "PhotoJ2K",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightIndexTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightIndexWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightMiddleTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightMiddleWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightPinkyTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightPinkyWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightRingTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightRingWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightThumbTemplate",
                table: "VoterRegisterTemplates");

            migrationBuilder.DropColumn(
                name: "RightThumbWsq",
                table: "VoterRegisterTemplates");

            migrationBuilder.CreateTable(
                name: "UploadedMrtFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    DistrictId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadedMrtFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UploadedMrtFiles_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UploadedMrtFiles_DistrictId",
                table: "UploadedMrtFiles",
                column: "DistrictId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UploadedMrtFiles");

            migrationBuilder.AddColumn<string>(
                name: "FaceTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftIndexTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftIndexWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftMiddleTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftMiddleWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftPinkyTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftPinkyWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftRingTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftRingWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftThumbTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeftThumbWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhotoJ2K",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightIndexTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightIndexWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightMiddleTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightMiddleWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightPinkyTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightPinkyWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightRingTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightRingWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightThumbTemplate",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RightThumbWsq",
                table: "VoterRegisterTemplates",
                maxLength: 10240,
                nullable: true);
        }
    }
}
