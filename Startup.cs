﻿using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MManager.Models;
using MManager.Services;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.IO;
using GemBox.Spreadsheet;
using GemBox.Document;
using Microsoft.OpenApi.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace MManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }
        private IHostingEnvironment _env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(
                x => x
                    .UseLazyLoadingProxies()
                    .UseMySql(connection,
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion("8.0.19-mysql");
                    }));
            SystemConfig.Setting = new Setting
            {
                MrsLogsConnectionString = Configuration.GetConnectionString("MrsLogsConnection")
            };

            // configure token generation
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password = new PasswordOptions()
                {
                    RequireDigit = bool.Parse(Configuration.GetSection("PasswordPolicies:RequireDigit").Value),
                    RequiredLength = int.Parse(Configuration.GetSection("PasswordPolicies:MinimumLength").Value),
                    RequireLowercase = bool.Parse(Configuration.GetSection("PasswordPolicies:RequireLowercase").Value),
                    RequireUppercase = bool.Parse(Configuration.GetSection("PasswordPolicies:RequireUppercase").Value),
                    RequireNonAlphanumeric = bool.Parse(Configuration.GetSection("PasswordPolicies:RequireNonLetterOrDigit").Value),
                    RequiredUniqueChars = int.Parse(Configuration.GetSection("PasswordPolicies:RequiredUniqueChars").Value)
                };
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(600);
                options.Lockout.MaxFailedAccessAttempts = 50;
                options.Lockout.AllowedForNewUsers = true;
                // User settings.
                options.User.AllowedUserNameCharacters =
                            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._+?!@#$%^&*";
                options.User.RequireUniqueEmail = false;
            });
            services.AddCors();
            services.AddAuthorization();

            // Register the Swagger generator, defining 1 or more Swagger documents
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Management Portal", Version = "v1" });
            //});

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(options =>
            {
                  options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                  options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                  options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = Configuration["Tokens:Issuer"],
                    ValidAudience = Configuration["Tokens:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"])),
                    ClockSkew = TimeSpan.Zero // remove delay of token when expire
                };
            });

            services.AddMvc()
    .AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    });

            //Start Background Services
            services.AddScoped<StatisticsProcessService>();
            services.AddScoped<CleanUpProcessService>();
            services.AddScoped<VoterListService>();
            var serviceProvider = services.BuildServiceProvider();
            ServicesScheduler.StartAsync(serviceProvider).GetAwaiter();

            //Gembox Initializer
            SpreadsheetInfo.SetLicense("SN-2019Sep19-pRn52M6+aQbhC24aoY9xl92M2m4FJqB7zxRTHoi9SZwVCHhc1p62E34/8M0cJgZd38AHodkAU3Z2It07BqAOC0RuaVw==A");
            ComponentInfo.SetLicense("DN-2019Sep19-KK1LgeuyO0RuSBfBy4ovO3KtjqQ7oBX1n+CdY3egYZ8z+JD1NaIk1lsVIeImr7gFYVKxs5iCXe31zGaCWnpVSRGyU6w==A");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext dataContext)
        {
            dataContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            
            app.UseHttpsRedirection();
            app.Use(async (context, next) => {
                await next();
                if (context.Response.StatusCode == 404 &&
                   !Path.HasExtension(context.Request.Path.Value) &&
                   !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseMvcWithDefaultRoute();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger();

            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            //// specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Management Portal");
            //});
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapHub<ChatHub>("/chat");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
