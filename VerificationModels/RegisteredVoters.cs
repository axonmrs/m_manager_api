﻿namespace MManager.VerificationModels
{
    public partial class RegisteredVoters
    {
        public long Id { get; set; }
        public string VoterId { get; set; }
        public string PsName { get; set; }
        public string PsCode { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string DateOfBirth { get; set; }
        public long? EstimatedAge { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public byte[] Template { get; set; }
        public byte[] Photo { get; set; }
    }
}
