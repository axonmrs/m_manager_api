﻿using Microsoft.EntityFrameworkCore;

namespace MManager.VerificationModels
{
    public partial class verification_dbContext : DbContext
    {
        public verification_dbContext()
        {
        }

        public verification_dbContext(DbContextOptions<verification_dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppSettings> AppSettings { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<FailedVerifications> FailedVerifications { get; set; }
        public virtual DbSet<PollingStations> PollingStations { get; set; }
        public virtual DbSet<RegisteredVoters> RegisteredVoters { get; set; }
        public virtual DbSet<SuccessfulVerifications> SuccessfulVerifications { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite("DataSource=./Documents/Templates/verification_db.db;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AppSettings>(entity =>
            {
                entity.ToTable("app_settings");

                entity.HasIndex(e => e.Name)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasColumnType("NVARCHAR");
            });

            modelBuilder.Entity<Devices>(entity =>
            {
                entity.ToTable("devices");

                entity.HasIndex(e => e.IdentificationCode)
                    .IsUnique();

                entity.HasIndex(e => e.Imei)
                    .IsUnique();

                entity.HasIndex(e => e.Reference)
                    .IsUnique();

                entity.HasIndex(e => e.Sequence)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivationCode)
                    .IsRequired()
                    .HasColumnName("activation_code")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.ActivationCodeHash)
                    .IsRequired()
                    .HasColumnName("activation_code_hash")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.IdentificationCode)
                    .IsRequired()
                    .HasColumnName("identification_code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Imei)
                    .IsRequired()
                    .HasColumnName("imei")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("region")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Sequence)
                    .IsRequired()
                    .HasColumnName("sequence")
                    .HasColumnType("NVARCHAR2(128)");
            });

            modelBuilder.Entity<FailedVerifications>(entity =>
            {
                entity.ToTable("failed_verifications");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasColumnName("date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("NVARCHAR(512)");
            });

            modelBuilder.Entity<PollingStations>(entity =>
            {
                entity.ToTable("polling_stations");

                entity.HasIndex(e => e.Code)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("NVARCHAR2(128)");

                entity.Property(e => e.Constituency)
                    .IsRequired()
                    .HasColumnName("constituency")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.ElectoralArea)
                    .IsRequired()
                    .HasColumnName("electoral_area")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR2(256)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("region")
                    .HasColumnType("NVARCHAR(128)");
            });

            modelBuilder.Entity<RegisteredVoters>(entity =>
            {
                entity.ToTable("registered_voters");

                entity.HasIndex(e => e.VoterId)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("DATE");

                entity.Property(e => e.EstimatedAge)
                    .HasColumnName("estimated_age")
                    .HasColumnType("INT")
                    .HasDefaultValueSql("18");

                entity.Property(e => e.OtherNames)
                    .HasColumnName("other_names")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Photo)
                    .IsRequired()
                    .HasColumnName("photo");

                entity.Property(e => e.PsCode)
                    .IsRequired()
                    .HasColumnName("ps_code")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.PsName)
                    .IsRequired()
                    .HasColumnName("ps_name")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasColumnName("surname")
                    .HasColumnType("NVARCHAR");

                entity.Property(e => e.Template)
                    .IsRequired()
                    .HasColumnName("template");

                entity.Property(e => e.VoterId)
                    .IsRequired()
                    .HasColumnName("voter_id")
                    .HasColumnType("NVARCHAR(64)");
            });

            modelBuilder.Entity<SuccessfulVerifications>(entity =>
            {
                entity.ToTable("successful_verifications");

                entity.HasIndex(e => e.VoterId)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("LARGEINT")
                    .ValueGeneratedNever();

                entity.Property(e => e.VerificationDate)
                    .IsRequired()
                    .HasColumnName("verification_date")
                    .HasColumnType("DATETIME");

                entity.Property(e => e.VoterId)
                    .IsRequired()
                    .HasColumnName("voter_id")
                    .HasColumnType("NVARCHAR");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CompositeTemplate).HasColumnName("compositeTemplate");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("is_active")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("NVARCHAR(256)");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnName("password_hash")
                    .HasColumnType("NVARCHAR(128)");

                entity.Property(e => e.Picture).HasColumnName("picture");

                entity.Property(e => e.Selected)
                    .IsRequired()
                    .HasColumnName("selected")
                    .HasColumnType("BOOL")
                    .HasDefaultValueSql("False");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("NVARCHAR(64)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("NVARCHAR(64)");
            });
        }
    }
}
