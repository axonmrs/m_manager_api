﻿namespace MManager.VerificationModels
{
    public partial class SuccessfulVerifications
    {
        public long Id { get; set; }
        public string VoterId { get; set; }
        public string VerificationDate { get; set; }
    }
}
