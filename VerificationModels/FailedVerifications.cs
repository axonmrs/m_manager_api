﻿namespace MManager.VerificationModels
{
    public partial class FailedVerifications
    {
        public long Id { get; set; }
        public string Date { get; set; }
        public string Reason { get; set; }
    }
}
