﻿namespace MManager.VerificationModels
{
    public partial class Devices
    {
        public long Id { get; set; }
        public string Imei { get; set; }
        public string IdentificationCode { get; set; }
        public string Sequence { get; set; }
        public string Reference { get; set; }
        public string ActivationCode { get; set; }
        public string ActivationCodeHash { get; set; }
        public string District { get; set; }
        public string Region { get; set; }
    }
}
