﻿namespace MManager.VerificationModels
{
    public partial class PollingStations
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ElectoralArea { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Region { get; set; }
    }
}
